<?php

/**
 * Testing form helper generic methods (i.e. not using instances)
 *
 * @author Paul Willoughby <paul@fivetide.com>
 */

require dirname(__FILE__) . '/../../../../src/lib/Mitaji/Helper/Form.php';

class FormTest extends \PHPUnit\Framework\TestCase {

    /**
     * Test text field
     */
    public function testTextfield(){
        $expected = '<input id="Product_price" name="Product[price]" size="28" type="text" value="50.00">';
        $form = new Mitaji_Helper_Form();
        $this->assertEquals($expected, $form->textfield('Product', 'price', array('value' => '50.00')));
    }

    /**
     * Testing simple context
     */
    public function testContext(){
        $expected = '<input id="Product_Brand_name" name="Product[Brand][name]" size="28" type="text" value="Pike Air">';
        $form = new Mitaji_Helper_Form();
        $form->setContext('Product');
        $this->assertEquals($expected, $form->textfield('Brand', 'name', array('value' => 'Pike Air')));
    }

    /**
     * Testing has many context
     * Should automatically increment array keys in such a way that each item's fields are
     * kept together under the same array key
     */
    public function testContextHasMany(){
        $expected = ''
            . "\n" . '<label for="Product_Skus_0_id">Id</label>'
            . "\n" . '<input id="Product_Skus_0_id" name="Product[Skus][0][id]" size="28" type="text" value="22">'
            . "\n" . '<label for="Product_Skus_0_colour">Colour</label>'
            . "\n" . '<input id="Product_Skus_0_colour" name="Product[Skus][0][colour]" size="28" type="text" value="Red">'
            . "\n" . '<label for="Product_Skus_0_size">Size</label>'
            . "\n" . '<input id="Product_Skus_0_size" name="Product[Skus][0][size]" size="28" type="text" value="30">'

            . "\n" . '<label for="Product_Skus_1_id">Id</label>'
            . "\n" . '<input id="Product_Skus_1_id" name="Product[Skus][1][id]" size="28" type="text" value="">'
            . "\n" . '<label for="Product_Skus_1_colour">Colour</label>'
            . "\n" . '<input id="Product_Skus_1_colour" name="Product[Skus][1][colour]" size="28" type="text" value="Blue">'
            . "\n" . '<label for="Product_Skus_1_size">Size</label>'
            . "\n" . '<input id="Product_Skus_1_size" name="Product[Skus][1][size]" size="28" type="text" value="31">'

            . "\n" . '<label for="Product_Skus_2_id">Id</label>'
            . "\n" . '<input id="Product_Skus_2_id" name="Product[Skus][2][id]" size="28" type="text" value="">'
            . "\n" . '<label for="Product_Skus_2_colour">Colour</label>'
            . "\n" . '<input id="Product_Skus_2_colour" name="Product[Skus][2][colour]" size="28" type="text" value="Green">'
            . "\n" . '<label for="Product_Skus_2_size">Size</label>'
            . "\n" . '<input id="Product_Skus_2_size" name="Product[Skus][2][size]" size="28" type="text" value="">'

            . "\n" . '<label for="Product_Skus_3_id">Id</label>'
            . "\n" . '<input id="Product_Skus_3_id" name="Product[Skus][3][id]" size="28" type="text" value="99">'
            . "\n" . '<label for="Product_Skus_3_colour">Colour</label>'
            . "\n" . '<input id="Product_Skus_3_colour" name="Product[Skus][3][colour]" size="28" type="text" value="White">'
            . "\n" . '<label for="Product_Skus_3_size">Size</label>'
            . "\n" . '<input id="Product_Skus_3_size" name="Product[Skus][3][size]" size="28" type="text" value="33">';
        $form = new Mitaji_Helper_Form();
        $form->setContext('Product', 'Skus');
        $result = '';
        $data = array(
            array(
                'id'        => 22,
                'colour'    => 'Red',
                'size'      => 30,
            ),
            array(
                'colour'    => 'Blue',
                'size'      => 31,
            ),
            array(
                'colour'    => 'Green',
            ),
            array(
                'id'        => 99,
                'colour'    => 'White',
                'size'      => 33,
            ),
        );
        foreach ($data as $sku) {
            $result .= trim($form->label('Sku', 'id')) . "\n";
            $result .= $form->textfield('Sku', 'id', array('value' => @$sku['id'])). "\n";
            $result .= trim($form->label('Sku', 'colour')) . "\n";
            $result .= $form->textfield('Sku', 'colour', array('value' => @$sku['colour'])). "\n";
            $result .= trim($form->label('Sku', 'size')) . "\n";
            $result .= $form->textfield('Sku', 'size', array('value' => @$sku['size'])) . "\n";
        }
        $this->assertEquals(trim($expected), trim($result));
    }
}
