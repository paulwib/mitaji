<?php

/**
 * Testing link helper link and linkTo methods
 *
 * @author Paul Willoughby <paul@fivetide.com>
 */

require dirname(__FILE__) . '/../../../../src/lib/Mitaji/Helper/Link.php';

class LinkTest extends \PHPUnit\Framework\TestCase {

    public function testLink(){
        $urls = (object)array(
            'Product' => (object)array(
                'edit' => '/products/{id}/-/',
                'view' => '/products/{id}/',
                'list' => '/products/',
            ),
            'Order' => (object)array (
                'edit' => '/orders/{id}/-/',
                'view' => '/orders/{id}/',
                'list' => '/orders/',
            ),
        );
        $lh = new Mitaji_Helper_Link($urls);
        $this->assertEquals('/products/', $lh->link('Product.list'));
        $this->assertEquals('/products/55/', $lh->link('Product.view', array('id' => 55)));
        $this->assertEquals('/products/999/-/', $lh->link('Product.edit', array('id' => 999)));
        $this->assertEquals('/orders/', $lh->link('Order.list'));
        $this->assertEquals('/orders/55/', $lh->link('Order.view', array('id' => 55)));
        $this->assertEquals('/orders/999/-/', $lh->link('Order.edit', array('id' => 999)));
    }
}
