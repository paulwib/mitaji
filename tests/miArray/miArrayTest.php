<?php

class miArrayTest extends \PHPUnit\Framework\TestCase {

    function testKeyBy(){
        // Labelled keys
        $test = array(
            array('id'=>'a'),
            array('id'=>'b'),
            array('id'=>'c'),
            );
        $expected = array(
            'a'=>array('id'=>'a'),
            'b'=>array('id'=>'b'),
            'c'=>array('id'=>'c'),
            );
        $this->assertEquals($expected, miArray::keyBy($test, 'id'));

        // Numeric keys
        $test = array(
            array('a'),
            array('b'),
            array('c'),
            );
        $expected = array(
            'a'=>array('a'),
            'b'=>array('b'),
            'c'=>array('c'),
            );
        $this->assertEquals($expected, miArray::keyBy($test, 0));
        $test = array(
            array('a','b','c'),
            array('b','c','a'),
            array('c','a','b'),
            );
        $expected = array(
            'c'=>array('a','b','c'),
            'a'=>array('b','c','a'),
            'b'=>array('c','a','b'),
            );
        $this->assertEquals($expected, miArray::keyBy($test, 2));

        // Duplicate keys, last one will override others
        $test = array(
            array('id'=>'a', 'fred'),
            array('id'=>'a', 'dave'),
            array('id'=>'a', 'mike'),
            );
        $expected = array(
            'a'=>array('id'=>'a', 'mike'),
            );
        $this->assertEquals($expected, miArray::keyBy($test, 'id'));

        // Multiple keys, will join key values with hyphen
        $test = array(
            array('id'=>'a', 'fred'),
            array('id'=>'a', 'dave'),
            array('id'=>'a', 'mike'),
            );
        $expected = array(
            'a-fred'=>array('id'=>'a', 'fred'),
            'a-dave'=>array('id'=>'a', 'dave'),
            'a-mike'=>array('id'=>'a', 'mike'),
            );
        $this->assertEquals($expected, miArray::keyBy($test, array('id', 0)));
    }
}
