<?php

class TemplateUriTest extends \PHPUnit\Framework\TestCase {

    function testExpand(){

        $tpl_vars = array(
                    'a'=>'fred',
                    'b'=>'barney',
                    'c'=>'cheeseburger',
                    '20'=>'this-is-spinal-tap',
                    'a~b'=>'none%20of%20the%20above',
                    'scheme'=>'https',
                    'p'=>'quote=to+bo+or+not+to+be',
                    'e'=>'',
                    'q'=>'hullo#world'
                    );

        $tpl = new TemplateUri('http://example.org/{a}/{b}/');
        $this->assertEquals($tpl->expand($tpl_vars),'http://example.org/fred/barney/');

        $tpl = new TemplateUri('http://example.org/{a}{b}/');
        $this->assertEquals($tpl->expand($tpl_vars),'http://example.org/fredbarney/');

        $tpl = new TemplateUri('http://example.org/page1#{a}');
        $this->assertEquals($tpl->expand($tpl_vars),'http://example.org/page1#fred');

        $tpl = new TemplateUri('{scheme}://{20}.example.org?date={wilma}&option={a}');
        $this->assertEquals($tpl->expand($tpl_vars),'https://this-is-spinal-tap.example.org?date=&option=fred');

        $tpl = new TemplateUri('http://example.org/{a~b}');
        $this->assertEquals($tpl->expand($tpl_vars),'http://example.org/none%20of%20the%20above');

        $tpl = new TemplateUri('http://example.org?{p}');
        $this->assertEquals($tpl->expand($tpl_vars),'http://example.org?quote=to+bo+or+not+to+be');

        $tpl = new TemplateUri('http://example.com/order/{c}/{c}/{c}/');
        $this->assertEquals($tpl->expand($tpl_vars),'http://example.com/order/cheeseburger/cheeseburger/cheeseburger/');

        $tpl = new TemplateUri('http://example.com/{q}');
        $this->assertEquals($tpl->expand($tpl_vars),'http://example.com/hullo#world');

        $tpl = new TemplateUri('http://example.com/{e}/');
        $this->assertEquals($tpl->expand($tpl_vars),'http://example.com//');

        # A template with no variables
        $tpl = new TemplateUri('http://example.com/test/');
        $this->assertEquals($tpl->expand($tpl_vars),'http://example.com/test/');

    }

    function testMatch(){

        $tpl_vars = array(
                    'a'=>'fred',
                    'b'=>'barney',
                    'c'=>'cheeseburger',
                    '20'=>'this-is-spinal-tap',
                    'a~b'=>'none%20of%20the%20above',
                    'scheme'=>'https',
                    'p'=>'quote=to+bo+or+not+to+be',
                    'e'=>'',
                    'q'=>'hullo#world'
                    );

        $tpl = new TemplateUri('/');
        $match = $tpl->matchUri('/');
        $this->assertTrue($match);

        $tpl = new TemplateUri('http://example.org/{a}/{b}/');
        $match = $tpl->matchUri('http://example.org/fred/barney/');
        $this->assertEquals($match,array('a'=>'fred','b'=>'barney'));

        $match = $tpl->matchUri('http://example.org/fred/');
        $this->assertFalse($match);

        $tpl = new TemplateUri('http://example.org/{a}{b}/');
        $tpl->setVarRegexp('a','[a-z]{4}');
        $tpl->setVarRegexp('b','[a-z]{6}');
        $match = $tpl->matchUri('http://example.org/fredbarney/');
        $this->assertEquals($match,array('a'=>'fred','b'=>'barney'));

        $tpl = new TemplateUri('http://example.org/page1#{a}');
        $match = $tpl->matchUri('http://example.org/page1#fred');
        $this->assertEquals($match,array('a'=>'fred'));

        $tpl = new TemplateUri('{scheme}://{20}.example.org?date={wilma}&option={a}');
        $match = $tpl->matchUri('https://this-is-spinal-tap.example.org?date=&option=fred');
        //$this->assertEquals($match,array('scheme'=>'https','20'=>'this-is-spinal-tap','wilma'=>'','a'=>'fred'));

        $tpl = new TemplateUri('http://example.org/{a~b}');
        $match = $tpl->matchUri('http://example.org/none%20of%20the%20above');
        $this->assertEquals($match,array('a~b'=>'none%20of%20the%20above'));

        $tpl = new TemplateUri('http://example.org?{p}');
        $match = $tpl->matchUri('http://example.org?quote=to+bo+or+not+to+be');
        $this->assertEquals($match,array('p'=>'quote=to+bo+or+not+to+be'));

        $tpl = new TemplateUri('http://example.com/order/{c}/{c}/{c}/');
        $match = $tpl->matchUri('http://example.com/order/cheeseburger/cheeseburger/cheeseburger/');
        $this->assertEquals($match,array('c'=>'cheeseburger'));

        $tpl = new TemplateUri('http://example.com/{q}');
        $match = $tpl->matchUri('http://example.com/hullo#world');
        $this->assertEquals($match,array('q'=>'hullo#world'));

        $tpl = new TemplateUri('http://example.com/{e}/');
        $match = $tpl->matchUri('http://example.com//');
        //$this->assertEquals($match,array('e'=>''));

        $tpl = new TemplateUri('http://example.com/archives/{year}/{month}/{day}/');
        $tpl->setVarRegexp('year','\d{4}');
        $tpl->setVarRegexp('month','\d{2}');
        $tpl->setVarRegexp('day','\d{2}');
        $match = $tpl->matchUri('http://example.com/archives/2006/12/29/');
        $this->assertEquals($match,array('year'=>'2006','month'=>'12','day'=>'29'));

        $match = $tpl->matchUri('http://example.com/archives/2006/dec/29/');
        $this->assertFalse($match);

        $tpl->setVarRegexp('month','(\d{2}|[a-z]{3})');
        $match = $tpl->matchUri('http://example.com/archives/2006/dec/29/');
        $this->assertEquals($match,array('year'=>'2006','month'=>'dec','day'=>'29'));

        $tpl = new TemplateUri('http://example.com/archives/{year}{month}{day}');
        $tpl->setVarRegexp('year','\d{4}\/');
        $tpl->setVarRegexp('month','(\d{2}\/)?');
        $tpl->setVarRegexp('day','(\d{2}\/)?');
        $match = $tpl->matchUri('http://example.com/archives/2006/12/29/');
        $this->assertEquals($match,array('year'=>'2006','month'=>'12','day'=>'29'));
        if($match){
            foreach($match as $k=>$str){
                $match[$k] = trim($match[$k],'/');
            }
        }
        $this->assertEquals($match,array('year'=>'2006','month'=>'12','day'=>'29'));

        # A template with no variables
        $tpl = new TemplateUri('http://example.com/test/');
        $match = $tpl->matchUri('http://example.com/test/');
        $this->assertEquals($match,true);

    }

    function testConstraints() {
        $tpl = new TemplateUri('http://example.com/test/{id:NUMERIC}/{slug:ALPHA}');
        $names = $tpl->getNames();
        $this->assertEquals($names, array('id','slug'));
        $this->assertFalse($tpl->matchUri('http://example.com/test/foo/55'));
        $this->assertFalse($tpl->matchUri('http://example.com/test/55bar/foo'));
        $this->assertFalse($tpl->matchUri('http://example.com/test/55/foo666'));
        $this->assertFalse($tpl->matchUri('http://example.com/test/foo/foo'));
        $this->assertFalse($tpl->matchUri('http://example.com/test/55/66'));
        //$this->assertTrue($tpl->matchUri('http://example.com/test/695/hello+yeah'));
    }
}
