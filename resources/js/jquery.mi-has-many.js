/**
 * hasMany
 */
(function($){
    $.fn.hasMany = function(options){
        var settings = jQuery.extend({
							   list_el: false,
							   replace_el: false,
                               prepend: false,
                               through: false
                              }, options || {});
		var $this = $(this);
		settings.$list_el = $(settings.list_el);

        // Load function i.e. loads up the resources you want to attach to the 
        // current resource. This will be opened in a modal pop-up.
        var load_fun = function(e){
            //var $has_many = $(this).closest('.has-many');
            var $mibox = $('.mibox');
            if(!$mibox.length){
				var id = $this.attr('id')+'_popup';
                $mibox = $('<div class="mibox" id="'+id+'">'+
                                 '<div class="mibox-inner"></div>'+
                                 '</div>').appendTo('body');
            }
            $mibox.css('display','block');
            $mibox.centerOf('window');
			$list_el = settings.$list_el;
            $('.mibox-inner',$mibox).loadRecursive($(this).attr('href'), null, 
                function(){ 
					if($('.dragger',$mibox).length){
						$('.dragger',$mibox).css('cursor','pointer');
						$('.dragger',$mibox).attr('title','Click and drag to move');
						$mibox.draggable({ handle : '.dragger', cursor : 'move'});
					}
                    // Update add links so they call the add function and pass a reference
                    // to settings.$list_el in event.data
                    $('a.add,.close,.cancel',$mibox).unbind('click');
                    $('a.add',$mibox).click(add_fun);
					$('.close,.cancel', $mibox).click(
                    function(){
                        $(this).closest('.mibox').css('display','none');
                    });
                    // Trigger custom event
                    $this.trigger('hasManyLoad', [ $mibox ]);
                });
            return false;
        }
        // Remove function
        var remove_fun = function(){
            $(this).closest('tr').fadeOut('normal',function(){ 
                $(this).remove(); 
                $this.trigger('hasManyRemove');
            });     
            return false;
        }
        
        // Add function 
        var add_fun = function(e){
            var assoc_count = $(settings.list_el).children().length;
            $.get($(this).attr('href'),{ "assoc_count" : assoc_count}, 
                function(data, textStatus){
                    var new_el;
                    // Append (or prepend) to existing wrapper if already associates
                    if(assoc_count){
                        new_el = $(data)[(settings.prepend ? 'prepend' : 'append')+'To']($(settings.list_el));
                    // Otherwise append whole thing to container
                    } else {
                        new_el = $(data).insertAfter($(settings.replace_el));
						$(settings.replace_el).remove();
                    }
                    
                    // Add events to any new remove buttons
                    $('.remove',new_el).click(remove_fun);
                    $this.trigger('hasManyAdd', [new_el, assoc_count==0])
                });
            return false;
        }
        // Assign events
        if(settings.through){
            $(this).click(load_fun);
        } else {
            $(this).click(add_fun);
        }
        $('.remove',settings.$list_el).click(remove_fun);
    };
})(jQuery);
