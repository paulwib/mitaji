/*
    A width resize handle for the worksheet.
    Allows you to resize to full width of screen, which means you can drag over 
    the sidebar. For the right effect don't use floats, position the worksheet
    statically and make sidebar fixed to right hand side of window.
*/
$(
    function(){

        // Add resize handles
        $('.resizable').each(
            function(){
                var r = $(this).append('<code class="resize-handle" title="Drag to resize, double click to reset">::</code>');
            }
        );
        
        // Double click on handles to make worksheet optimal size, giving room for sidebar
        $('.resizable .resize-handle').dblclick( function(){ $(window).resize(); } );
        
        // On mouse down start resize
        $('.resizable .resize-handle').mousedown(
            function(e){
                var ws = $(this).parents('.worksheet:first');
                if(!ws.length){
                    return false;
                }
                var min_w = 450;
                var max_w = $('.app-content').width();
                // Resize when mouse moves
                $().mousemove(
                    function(e){
                        var w = Math.min(Math.max(min_w,(e.pageX)),max_w);
                        $(ws).width(w);    
                    }
                );
                // Stop resize when mouse up anywhere
                $().mouseup(
                    function () { $().unbind('mousemove'); }
                );
            }
        );
        // On resize make resizable optimal width, which is the content width
        // minus the sidebar width 
        $(window).resize(
            function(){
                $('.resizable').each(
                   function(){
                        var cw = $('.app-content').width(), sb = $('.sidebar'), adj = 0;
                        if(sb.length){
                            var adj = sb.width()+18;
                        }
                        $(this).width(cw-adj);
                        if(sb){
                            sb.css({'left': ($(this).width()+36)+"px"});
                        }
                   }
                );
           }
        );
        // Fire resize event on load
        $(window).resize();
    }
);

