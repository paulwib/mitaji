/*
 * This plugin helps with performing actions on several resources at once
 * in a Mitaji based system.
 * 
 * It's invoked by calling the plug-in on an element wrapping a <select> of 
 * options and a submit <button>:
 * 
 * $('select.batch_actions').batchActions(collection)
 * 
 * The `collection` is a table that contains the resources the action will be 
 * applied to. If not provided it's assumed to be a `.collection` in the 
 * closest `.worksheet`.
 * 
 * Mark up constraints (aim to reduce these):
 * 
 * 1. The resources must be marked up as a table
 * 2. Each row must contain a single checkbox
 * 3. The select box must be wrapped in an element with an id
 * 4. There must be a corresponding <form> for each select option in the same 
 *     order as the options
 * 5. The <form>s must be wrapped in an element that has an id like 
 *    id="{select wrapper id}_forms" (where {select wrapper id} is the id of 
 *    the wrapper mentioned in (3) 
 * 
 * 
 * The forms are what define the action through normal form attributes i.e. 
 * `action` and `method`.
 * 
 * When the action is selected and the submit button pressed all the currently 
 * selected checkbox values (which should be resource ids) will be appended 
 * to the form's action URL as a query string paramater like:
 * ?id=in 1,2,6
 * 
 * This fits nicely with Mitaji's built in filters so will return a collection
 * of those items. Using miResourceResponder the only valid methods for this 
 * are PATCH or DELETE.
 * 
 * (Maybe you should be able to define a callback that will allow you to do 
 * what you like with the ids? And this could be called in the scope of the form?) 
 * 
 * If the form contains nothing but hidden elements then it will be submitted
 * immediately with no further interaction.
 * 
 * However if it does contain some other elements then the form will be shown 
 * in a modal dialog (i.e. pop-up box) where these additional fields can be
 * filled or confirmation button pressed.
 */
(function($){
    // Plug-in definition
    $.fn.batchActions = function(collection,actions){
		if(!$(this).length) return false;
        var $this = $(this);		
		var $collection = $(collection);
		if(actions){
			$actions = $(actions);
		} else {
			$actions = $this;
		}
		
        // Enable/disable controls depending on whether anything checked
        var toggle_fun = function(e){
			var checked_count = $('tbody input:checked',$collection).length;
			if(checked_count){
				$('> :input',$this).removeAttr('disabled');
				$this.addClass('enabled');
			} else {
				$('> :input',$this).attr('disabled','disabled');
				$this.removeClass('enabled');
			}
			$('> .checked-count',$this).text(checked_count);
			if(e){
				var $cb = $(e.target);
				var $tr = $cb.closest('tr'); 			
				if($tr.length && $cb.length){
					$cb.attr('checked') ? $tr.addClass('selected') : $tr.removeClass('selected');
				}
			}
        }
        // Using a checkbox to check everything
        var check_all_fun = function(){
            if($(this).attr('checked')){
                $('tbody tr',$collection).addClass('selected');
                $(':checkbox',$collection).attr('checked','checked');
            } else {
                $('tbody tr',$collection).removeClass('selected');
                $(':checkbox',$collection).removeAttr('checked');
            }
            toggle_fun();
        }

        // You can click anywhere on <tbody> to check the box
        var click_table_fun = function(e){                
            var $tr = $(e.target).closest('tr'); 
            // Clicking links should work as expected
            if(e.target.nodeName.toLowerCase() == 'a') return true; 
            // Toggle tr.selected class              
            $tr.toggleClass('selected');                
            // Clicking checkbox handled by normal events
            if(e.target.nodeName.toLowerCase() == 'input') return true;
            // Toggle checkbox                
            var $checkbox = $('input:checkbox',$tr);
            $checkbox.attr('checked') ? 
                $checkbox.removeAttr('checked') :  $checkbox.attr('checked','checked');
            // Toggle action form
            toggle_fun();
            return false;   
        };
        // What to do when you submit an action
        var click_submit_fun = function(){ 
			if($this.hasClass('enabled') == false){
				$(this).blur();
				return false;
			}
			var i;
			// If a select work out form based on currently selected value
			if($('>select',$this).length){
				var select_val = $('select',$this).val();
				// Check something is selected
				if(select_val == '-'){
					alert('Please choose an action');
					return false;
				}
				// Find <form> from select option's value which should match nth-child index
				i = select_val;
				
			// If a button or a link then need to work out it's position relative to others
			} else {
				var clicked = this;
				$('>'+this.nodeName,$this).each(function(j,el){
					if(el == clicked){
						i = j;
						return false;
					}
				});
			}
			var frms = $('form',$actions);
			var frm = frms.get(i);
			if(!frm){
				console.error('Could not find form');
				return false;
			}
			
			// Disable the controls while sorting this out
            $('> :input',$this).attr('disabled','disabled');
            
			// Collect checked primary keys
            var primary_keys = [];
            $('input:checked',$collection).each(function(i,el) {
				if($(el).hasClass('check-all')){
					return true;
				} 
				primary_keys.push($(this).val()); 
			});
            primary_keys.sort();            

            // Add primary keys to form - if there is an input with the class `primary-keys`
			// will be set as a value of that, otherwise appended to action URL `in` query var
			if($('.primary-keys',frm).length){
				$('.primary-keys',frm).val(primary_keys.join(','));
			} else {
				var url = $(frm).attr('action');
				if(url.indexOf('?') != -1){
					url += '&';
				} else {
					url += '?';
				}
				url += 'id=in+'+primary_keys.join(escape(','));
				$(frm).attr('action',url);
			}
            // If non-hidden inputs or an error pop-up form to complete
            if($(':input[type!=hidden], .error',frm).length){
                // Clone from (so can be removed later) and wrap in a mibox
				// and center on screen
				var id = $this.attr('id')+'_popup';
				//var frm = $(frm).clone(true).appendTo('body');
				if(!$(frm).parent().hasClass('mibox-inner')){
					$(frm).wrap('<div class="mibox" id="'+id+'"><div class="mibox-inner"></div></div>');
				}
				$('#'+id).css({'display':'block','z-index':'9999'});
				$('#'+id+' form').css('display','block');
                $('#'+id).centerOf('window');
				// Add action to cancel button
				$('#'+id+' .cancel, #'+id+' .close').click(function(){
					$(this).closest('.mibox').hide();
					toggle_fun();
					return false;
				});
            // Otherwise all inputs hidden so just submit
            } else {
                frm.submit();
            }
            return false;
        }
		// Submit the action with a button or link
        $('> button, > a',$this).click(click_submit_fun);
        // Init table click
        //$('tbody',$collection).click(click_table_fun);
        // Init current selection  
        $('input:checked',$collection).closest('tr').addClass('selected');
        // Assign change event to row tick boxes    
        $(':checkbox',$collection).click(toggle_fun);
		// Init change of checkbox that selects all
		$('input:checkbox.check-all',$collection).unbind('click');
        $('input:checkbox.check-all',$collection).click(check_all_fun);
        // Init enabled state of action form
        toggle_fun();
    }
})(jQuery);
