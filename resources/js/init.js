$(function(){
    
    // Focus first
    $($('.focus-first').get(0)).focus();
// --------------------------------------------------------------------------    
    // Add these classes with javascript as pain to do in template when moving target
    $('.collection tr').find('td:odd').addClass('odd');
    $('.collection thead tr.sorters th:odd').addClass('odd');
    $('.collection thead tr.filters th:odd').addClass('odd');
// --------------------------------------------------------------------------    
// Double click on a collection table row to open it for editing
    $('.collection tbody tr').dblclick(function(){
        window.location.href = $('a:first',this).attr('href');
    });
// --------------------------------------------------------------------------    
/*
 * When filters are submitted, disable sending of empty ones so URLs are 
 * a bit more readable (more useful for debugging than anything else)
 */
    $('.collection tr.filters button').click(
        function(){
            // This weird selector is because of bug #3990 in jquery 1.3.1
            $('input:not([value!=""])',$(this).parents('tr').get(0)).attr('disabled','disabled');
            // This is what was used before and is more readable
            //$('input[value=""]',$(this).parents('tr').get(0)).attr('disabled','disabled');
            return true;
        });
// --------------------------------------------------------------------------    
// Delete button changes hacked method
    $('button.delete').click(
        function(){
            if(confirm($(this).attr('title'))){
                $('#MI_METHOD').val('DELETE');
                return true;
            } else {
                return false;
            }
        });
// --------------------------------------------------------------------------    
/*
 * Button with class 'append-value' will have it's name and value appended
 * to the parent form when it is clicked and submitted. The button itself
 * will be disabled so it's own value does not conflict.
 */
    $('button.append-value').click(
        function(){
            $(this).parent().append('<input type="hidden" name="'+
                                    $(this).attr('name')+
                                    '" value="'+$(this).val()+'">');
            $(this).attr('disabled','disabled');
            return true;
        }
    );
// --------------------------------------------------------------------------
/*
 * When a form is successfully saved the header background color changes, this
 * will change it back after a slight delay.
 */
    $('form.saved .header').animate({'backgroundColor':'#DCE2D2'},5000);


// --------------------------------------------------------------------------
});
