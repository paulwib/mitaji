(function($){
    // Plug-in  definition
    $.fn.isBelongsTo = function(base_url, options){
        if(!$(this).length){ return this; }

        var settings = $.extend({
            empty_msg: 'Use the links to search or add',
            links: false,
            change_callback: false
          }, options);
        var $this = $(this);
        
        // Context
        var nm = $(this).attr('name');
        var context = nm.substr(0,nm.indexOf('['));
// ------------------------------------------------------------------------        
// Refresh function
        var refresh_fun = function(){  
            if(!$this.val()){
                return;
            }
            // Reset edit link
            var el = $('a.edit',settings.links);          
            // Add the loading class to the container
            $this.parent().addClass('loading');
            // Get content
            $.get(base_url+$this.val()+'/?context='+context,
                null, 
                function(data,textStatus){
                    // Write content to page
                    $this.before('<div class="data-belongs-to">'+data+'</div>');
                    // Update edit URL
                    el.attr('href',base_url+$this.val()+'/');
                    // Remove loading class
                    $this.parent().removeClass('loading');
                });
        };
// ------------------------------------------------------------------------        
// Refresh function
        var change_fun = function(new_id,old_id){
            if(new_id != old_id && settings.change_callback){
                settings.change_callback();
            }
            // Reset edit link
            var el = $('a.edit',settings.links);
            el.unbind('click');
            // Remove current content
            $this.parent().find('.data-belongs-to').remove();
            // If no value then disable edit link
            if(!$this.val()){
                el.addClass('disabled');
                el.click(function(){return false;});
                $this.before('<div class="data-belongs-to">'+settings.empty_msg+'</div>');
                if(settings.change_callback){
                    settings.change_callback();
                }
                return false;
            } else {
                el.removeClass('disabled');
                el.click(function() { return edit_fun(this.href); });
                refresh_fun();
            }
        };
// ------------------------------------------------------------------------ 
// Search function
        var search_fun = function(url){
            load_fun(url, function(){
                // Modify collection controls
                $('.mibox tbody tr th').each(function(){
                    var id = $('input:first',this).val();
                    var old_id = $this.val();
                    $(this).html('<button>Choose</button>');
                    // On click sets the id on the input
                    $('button',this).click(function(){
                        // Set id
                        $this.val(id);
                        // Refresh display
                        change_fun(id,old_id);
                        return false;
                    });
                });
            });
            return false;
        };
// ------------------------------------------------------------------------ 
// Edit function (also used for add)
        var edit_fun = function(url){
            var get = true;
            load_fun(url, function(){
                if(get){
                    get = false;
                    return;
                }
                // Callback updates the value of the input and refreshes
                var old_id = $this.val();
                var url = $('.mibox form').attr('action');
                var new_id = url.replace(/[^0-9]/g,'');
                $this.val(new_id);
                // Always force refresh
                change_fun(new_id,old_id);
            });
            return false;
        };
 // ------------------------------------------------------------------------        
// Load function
        var load_fun = function(url, cb){
            // Create pop-up box
            if(!$('.mibox').length){
                $('body').append('<div class="mibox">'+
                                 '<span class="close" title="Click to close">[Close]</span>'+
                                 '<div class="mibox-content"></div>'+
                                 '</div>');
                $('.mibox').draggable({ handle : '.header', cursor : 'move'});
                $('.mibox .close').click(
                    function(){
                        $(this).parents('div').css('display','none');
                    });
            }
            // Display and center in window
            $('.mibox').css('display','block');
            $('.mibox').centerOf('window');
            
            // Load recursively
            $('.mibox .mibox-content').loadRecursive(url, null, function(){
                $('.mibox .header').attr('title','Click and drag to move');
                $('.mibox a:contains(Cancel)').unbind('click');
                $('.mibox a:contains(Cancel)').click(
                    function(){
                        $('.mibox .close').click();
                        return false;
                    });
                cb();
            });
            return false;
        }
// ------------------------------------------------------------------------ 
// Add links
        if(!settings.links){
            var links = $('<ul></ul>').insertAfter($this);
            $.each([['Search',base_url],
                    ['Add', base_url+'-/'],
                    ['Edit',base_url+$this.val()+'/'],
                    ['Reset','#']], 
                    function(i,link){
                        var a = $('<a class="'+link[0].toLowerCase()+
                                    '" href="'+link[1]+'">'+link[0]+'</a>')
                        .appendTo($('<li></li>')
                        .appendTo(links));
                    });
            settings.links = links;
        }
        $('a.search',settings.links).click(function() { return search_fun(this.href); });
        $('a.edit, a.add',settings.links).click(function(){return edit_fun(this.href);});
        $('a.reset',settings.links).click(function() { 
                    var old_id = $this.val(); 
                    $this.val(''); 
                    return change_fun('',old_id); 
                });
        // Start off with a refresh
        refresh_fun(0,1);
        return this;
    };
})(jQuery);
