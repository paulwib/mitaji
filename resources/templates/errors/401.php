<h1><?=$this->page_title;?></h1>

<p>Authorization is required to view this page. If you reload this page you should get a box asking for your user name and password. Enter these and click OK to login.</p> 

<p>If you continue to see this page there is a problem with your user name or password. Contact the sytem administrator for help.</p>
