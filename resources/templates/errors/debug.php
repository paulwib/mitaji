<div class="_e">

<h1>Debug for Environment <?=@$_ENV['MITAJI'] ? $_ENV['MITAJI'] : '<i>Not set</i>';?></h1>


<?php if($recent_errors):?>
<h2>Recent Errors</h2>
<ol>
<?php foreach($recent_errors as $e):?>
<li>
    <span class="_e_msg"><?=$e['str'];?></span> on line 
    <span class="_e_line"><?=$e['line'];?></span> of 
    <span class="_e_file"><?=$e['file'];?></span>, severity 
    <span class="_e_level"><?=$this->request->getErrorLevelString($e['no']);?></span>
</li>
<?php endforeach;?>
</ol>
<?php endif;?>

<?php if (@$mybacktrace):?>
<h2>Backtrace</h2>

<ol class="_e_backtrace">
<?php foreach($mybacktrace as $t):?>
    <li>
	<span class="_e_msg"><?=$t['str'];?></span> on line 
    <span class="_e_line"><?=$t['line'];?></span> of 
    <span class="_e_file"><?=$t['file'];?></span>
    </li>
<?php endforeach;?>
</ol>

<?php endif;?>

<h2>Request</h2>
<table class="_e_request">
<?php foreach($this->request as $prop=>$value):?>
<?php if($prop == 'config') continue;?>
<tr>
<th><?=$prop;?></th>
<td><?php
    if (is_string($value)) {
        echo $value === '' ? '<em>Empty string</em>' : htmlentities($value);
    } else if (is_bool($value)) {
        echo $value === true ? 'true' : 'false';
    } else if(is_object($value) && method_exists($value, 'getData')){
        $data = $value->getData();
        $tmp = array();
        $longest = miStr::longest(array_keys($data));
        foreach($data as $k=>$v){
            $tmp[] = str_pad($k, $longest+8).print_r($v, true); 
        }
        echo implode('<br>',$tmp);
    } else {
        echo print_r($value, true);
    }
?>
</td>
</tr>
<?php endforeach;?>
</table>


<?php if($errors):?>
<h2>All Errors</h2>
<ol>
<?php foreach($errors as $e):?>
<li>
    <span class="_e_msg"><?=$e['str'];?></span> on line 
    <span class="_e_line"><?=$e['line'];?></span> of 
    <span class="_e_file"><?=$e['file'];?></span>, severity 
    <span class="_e_level"><?=$this->request->getErrorLevelString($e['no']);?></span>
</li>
<?php endforeach;?>
</ol>
<?php endif;?>


<h2>Include Path</h2>

<?php
$inc_path = explode(PATH_SEPARATOR, get_include_path());
?>
<ol>
<?php foreach($inc_path as $path):?>
<li><?=$path;?></li>
<?php endforeach;?>
</ol>


</div>
