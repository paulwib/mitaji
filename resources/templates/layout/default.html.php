<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title><?=@$page_title;?>  
	<?=$this->config->mitaji->app_name;?></title>
    <?=miHtml::cssLink('/css/default.css');?>  
    <?=miHtml::cssLink('/css/print.css','print');?> 
    <?=miHtml::cssLink('/css/ie.css','screen','IE');?>
    </head>
    <body>
    <div class="app-header">
        <p><a href="/"><?=$this->config->mitaji->app_name;?></a></p>
    </div>
	<div class="app-content clearfix">
        <?=$this->body;?>
    </div>
    
    <div class="app-footer">
    <p class="small quiet">
	<?=$this->config->mitaji->app_name;?> version 
	<?=$this->config->mitaji->app_version;?></p>
    </div>
    </body>
</html>
