RewriteEngine on
RewriteCond %%{REQUEST_FILENAME} !index\.php$
RewriteCond %{REQUEST_FILENAME} !(robots\.txt|favicon\.ico)$
RewriteCond %{REQUEST_URI} !^/(css|js|images)/?.*$
RewriteRule (.*) index.php
