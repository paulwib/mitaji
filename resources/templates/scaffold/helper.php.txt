<?php

class %1$sHelper {
	
	protected static $__response = false;

//---------------------------------------------------------------------------
	static function setResponse(&$response){
		self::$__response = $response;
	}

//---------------------------------------------------------------------------
	static function listStatus($%2$s){
		$builder = miHtml::builder();
		if($%2$s->resourcesTotal() > $%2$s->getPageSize()){
		 	return $builder->p(sprintf(%4$s_COUNT_MANY,
			 	$%2$s->resourcesStart(),
			 	$%2$s->resourcesEnd(),
			 	$%2$s->resourcesTotal()));
		} else if($%2$s->count() > 1){
			return $builder->p(sprintf(%4$s_COUNT_SOME,
				$%2$s->count()));
		} else if($%2$s->count()) {
			return $builder->p(%4$s_COUNT_ONE);
		}
		return false;
	}
//---------------------------------------------------------------------------
	static function formTitle($%1$s){
		return $%1$s->primaryKey() ? 
			sprintf(%4$s_TITLE_EDIT, $%1$s->primaryKey()) : 
			%4$s_TITLE_CREATE;
	}
//---------------------------------------------------------------------------
	static function formAction($%1$s){
		return $%1$s->primaryKey() ? 
			self::$__response->link('%1$s.edit',$%1$s) : 
			self::$__response->link('%1$s.list');
	}
//---------------------------------------------------------------------------
	static function formStatus($%1$s){		
		$builder = miHtml::builder();
		if(self::$__response->status == 422){
			$list = '';
			foreach($%1$s->errors() as $e){
				$list .= $builder->li(
					$builder->label(array('for'=>$e[1]),$e[0])
				);
			}
			return $builder->p(array('class'=>'error'),%4$s_STATUS_ERROR).
				   $builder->ul(array('class'=>'error-list'),$list);

		// Success, get message from redirect state
		} else if(($state = self::$__response->redirectState())){
			return $builder->p(
					array('class'=>($state == 'deleted' ? 'deleted' : 'saved')),
					constant('%4$s_STATUS_'.strtoupper($state)));
		}
		return false;
	}
	
//---------------------------------------------------------------------------
}
