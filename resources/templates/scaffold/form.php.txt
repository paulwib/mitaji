			<h1><?=$this->formTitle($%1$s);?></h1>			
			
			<?php if($status = $this->formStatus($%1$s)):?>
			<div class="status">
			<?=$status;?>
			</div>
			<?php endif;?>
			
			<form class="%1$s" action="<?=$this->formAction($%1$s);?>" method="post">

%3$s

				<p class="controls">
				<button type="submit">Save</button>
				<?php if($%1$s->primaryKey()):?>
				<button type="submit" name="REQUEST_METHOD_HACK" value="DELETE">Delete</button>
				<?php endif;?>
				</p>
				
			</form>
			
			<p class="back"><?=$this->linkTo(%2$s_LINK_BACK,'%1$s.list');?></p>
