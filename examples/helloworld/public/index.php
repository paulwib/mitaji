<?php

require_once __DIR__ . '/../../../vendor/autoload.php';

// Set start time for debugging
define('MITAJI_REQUEST_START_MICROTIME', microtime(true));

// Set temp dir
$_ENV['TMPDIR'] = sys_get_temp_dir();

// Set environment from server var (e.g. set in Apache config)
if(array_key_exists('MITAJI_ENV', $_SERVER)){
    $_ENV['MITAJI'] = $_SERVER['MITAJI_ENV'];
}
// Or default to development
else {
    $_ENV['MITAJI'] = 'development';
}

// Start Mitaji, passing application path for config, template and other non-PHP files
$resource_paths = realpath(__DIR__ . '/../') . PATH_SEPARATOR .
    realpath(__DIR__ . '/../../../resources/');

$m = new Mitaji($resource_paths);
$m->httpRun();
