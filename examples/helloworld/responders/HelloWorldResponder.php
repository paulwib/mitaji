<?php

class HelloWorldResponder {

	function get($request){
		$c = @$request->url_vars['c'];
		if($c > $request->config->helloworld->max_worlds){
			$request->notfound();
		}
		if(!$c){
			$c = 1;
		}
		$this->body = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
							"http://www.w3.org/TR/html4/strict.dtd">'.
						'<html><title>Hello World</title>'.
						'<p>'.str_repeat('Hello World<br>',$c).'</p>'.
						'</html>';
		return $this;
	}
}
