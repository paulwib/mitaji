<?php

/**
 * Example of a command line script using bootstrap/cli2.php
 */
require_once __DIR__ . '/../../vendor/autoload.php';

// Add resources directory to path for accessing config and templates
// You should add any other resources paths first
set_include_path(__DIR__ . '/../resources' . PATH_SEPARATOR . get_include_path());

// Define options before requiring bootstrap/cli2.php as that will parse the options
$short_opts = 'w:dh';
$long_opts = array('who=', 'display-env', 'help');
$normalize_opts = array('w'=>'who', 'd' => 'display-env', 'h'=>'help');

// Define help
$help = <<<EOT

Say hello to someone

-w, --who           Who to say hello to
-e, --env           The environment
-d, --display-env   Display environment values
-h, --help          Display this help

EOT;

// Include the bootstrap which will parse options into $_ENV['OPTS']
require_once __DIR__ . '/../../bootstrap/cli2.php';

// Help
if (isset($_ENV['OPTS']['help'])) {
    fputs(STDOUT, $help);
    exit(0);
}

// Who to say hello to is required
if (!isset($_ENV['OPTS']['who'])) {
    fputs(STDERR, "ERROR: Please specify who to say hello to with --who\n");
    fputs(STDOUT, $help);
    exit(1);
}

fputs(STDOUT, 'Hello ' . $_ENV['OPTS']['who'] . "\n");

if (isset($_ENV['OPTS']['display-env'])) {
    fputs(STDOUT, "\n_ENV\n");
    fputs(STDOUT, print_r($_ENV, true));
}
