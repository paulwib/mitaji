<?php

include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

class miHtmlTests extends UnitTestCase {
    
    function __construct(){
        $this->UnitTestCase('Test miHtml');
    }
    
    function testInputOpts(){
        $default_opts = miHtml::getDefaultInputOpts();
        $this->assertTrue(is_array($default_opts));
        $new_opts = miHtml::setDefaultInputOpts(array('control_wrapper'=>'td'));
        $this->assertEqual($new_opts['control_wrapper'],'td');
        miHtml::revertDefaultInputOpts();
        $default_opts_2 = miHtml::getDefaultInputOpts();
        $this->assertEqual($default_opts['control_wrapper'],
                           $default_opts_2['control_wrapper']);
        
    }
}

$test = new miHtmlTests();
if (TextReporter::inCli()) {
	exit ($test->run(new TextReporter()) ? 0 : 1);
}
$test->run(new HTMLReporter2());
