
<?php

include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

/*
 * Fake user class for testing as creator_id and updater_id are required
 * to save a product
 */
class User extends miResource {
    function primaryKey(){
        return $this->id;
    }
}

class testSaveUserIds extends UnitTestCase {
    
    function __construct(){
        parent::__construct('Auto-saving of user ids for creator and updater (miResource Test)');
        miDb::connect(json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test", 
                                    "username" : "root","password" : "skippy",
                                    "no_cache" : 0, "warn_slow_query" : 100 }'));
    }
    
    function testSave(){
        $user = new User();
        $user->id = 20;
        $product_data = array('supplier_id'=>1,
                              'description'=>'Wool Boots',
                              'retail_price'=>'25.55');
        $Product = new miResource(false,'products');
        if(!$this->assertTrue($Product->save($product_data,$user))){
            return false;
        }
        // Check created on and updated on saved
        $creator_id = miDb::getVar("SELECT creator_id FROM products 
                                        WHERE id=$Product->id;");
        $updater_id = miDb::getVar("SELECT updater_id FROM products 
                                        WHERE id=$Product->id;");
        $this->assertTrue($creator_id);
        $this->assertTrue($updater_id);
        $this->assertEqual($creator_id,$updater_id);
        // Change the user id
        $user->id = 5463;
        if(!$this->assertTrue($Product->save($product_data,$user))){
            return false;
        }
        $creator_id_2 = miDb::getVar("SELECT creator_id FROM products 
                                        WHERE id=$Product->id;");
        $updater_id_2 = miDb::getVar("SELECT updater_id FROM products 
                                        WHERE id=$Product->id;");
        $this->assertTrue($creator_id_2);
        $this->assertTrue($updater_id_2);
        $this->assertEqual($creator_id,$creator_id_2);
        
        $this->assertNotEqual($updater_id,$updater_id_2);
        $this->assertNotEqual($creator_id,$updater_id_2);
        
        
        // Clean up
        $this->assertTrue($Product->delete());
    }
}

$test = new testSaveUserIds();
$test->run(new HTMLReporter2());
