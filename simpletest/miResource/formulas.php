<?php

# simpletest
include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

class testMiResourceFormulas extends UnitTestCase {
    
    function __construct(){
        $this->UnitTestCase('Test miResource Formulas');
        miDb::connect(
            json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test",
                          "username" : "root", 
                          "password" : "skippy", 
                          "no_cache" : 0, 
                          "warn_slow_query" : 50 }')
                    );
    }
// -------------------------------------------------------------------------    
    function testFormulas(){
        /*
         * Formulas allow you to add little snippets of SQL and alias them
         * to a field name. These will almost always take the form of 
         * subselects on has many associates or transforming some logic
         * into a flag.
         * 
         * You need to know what you're doing SQL wise to use these. I've 
         * been down the road of trying to make helpers to make it a bit
         * easier to define formulas without SQL and it never really worked
         * out, although might come back to this later.
         * 
         * This first one gets the total quantity of products in an order
         * without actually having to access products.
         */
        $order_model = new miResource(false,'Orders');
        $order_model->formulate('product_quantity',
                'IFNULL(
                    (SELECT SUM(quantity) FROM orders_products 
                    WHERE orders_products.order_id=orders.id),
                 0)');
        $Order = $order_model->findByPrimaryKey(12);
        $this->assertTrue($Order->product_quantity);
        $this->assertEqual(miDb::getVar("SELECT SUM(quantity) 
                            FROM orders_products WHERE order_id=$Order->id"),
                        $Order->product_quantity);
        
        // You can add multiple formulas
        $order_model->formulate('net_total',
                'IFNULL(
                    (SELECT SUM(quantity*price) FROM orders_products 
                    WHERE orders_products.order_id=orders.id),
                 0)');
        $Order = $order_model->findByPrimaryKey(12);
        $this->assertEqual(miDb::getVar("SELECT SUM(quantity) 
                            FROM orders_products WHERE order_id=$Order->id"),
                        $Order->product_quantity);
        $this->assertEqual(miDb::getVar("SELECT SUM(quantity*price) 
                            FROM orders_products WHERE order_id=$Order->id"),
                        $Order->net_total);
        
        /*
         * This may all seem a little pointless as a subselect isn't much 
         * quicker than adding an additional query to look this up. Well
         * I guess the answer is convenience, but more so because miCollection
         * can handle filtering on this formulas.
         * 
         * Of course if you want real performance you should denormalize
         * such aggregates. A plan for the future is to somehow automate
         * this from your defined formulas, so if the alias really exists 
         * in your table it will use the query to update it on save.
         */
        $Orders = $order_model->find();
        $Orders->filter('net_total','>1000');
        foreach($Orders as $Order){
            $this->assertTrue($Order->net_total > 1000);
            $this->assertEqual(miDb::getVar("SELECT SUM(quantity*price) 
                            FROM orders_products WHERE order_id=$Order->id"),
                        $Order->net_total);
        }
        
        /*
         * Here's a more logical filter that tells you if an order is late.
         * This gives you a `late` property on the resource, but it's
         * also letting you sneak in a complex filter that you could not
         * create with the filter method. This is no accident, it was always 
         * the goal to keep `filter()` relatively simple.
         */
        $order_model->formulate('late','(paid_on IS NULL AND 
                                        cancelled_on IS NULL AND 
                                        NOW() > due_on)');
        $Orders = $order_model->find();
        // Note if you don't give a value for a filter it defaults to 1 (true)
        $Orders->filter('late',1);
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertTrue($Order->late);
        }
        $Orders->filter('late',0);
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertFalse($Order->late);
        }
    }
// -------------------------------------------------------------------------    
    function testDenormalizeFormula(){
        /* 
         * It's pretty straightforward to denormalize a formula, just add a
         * column to your table named after the alias and everything will
         * automatically work. Once denormalized any find() won't execute the
         * formula, but every save will update the formula col.
         * Shifts subselects from read to write operations.
         */
        
        // So add the field to the table
        miDb::getResults("ALTER TABLE `orders` ADD `product_quantity` 
                        SMALLINT UNSIGNED NOT NULL DEFAULT '0' 
                        AFTER `cancelled_on`") ;
        miDb::deleteCachedTableInfo('orders');
        // Make sure all existing have been updated, you would have to do this
        // manually if denormlaizing on a live database
        $formula = 'IFNULL(
                    (SELECT SUM(quantity) FROM orders_products 
                    WHERE orders_products.order_id=orders.id),
                 0)';
        miDb::getResults("UPDATE orders SET product_quantity=$formula;");
        
        // Now do the same as above
        $order_model = new miResource(false,'Orders');
        $order_model->formulate('product_quantity',$formula);
        $Order = $order_model->findByPrimaryKey(12);
        $this->assertTrue($Order->product_quantity);
        $this->assertEqual(miDb::getVar("SELECT product_quantity 
                            FROM orders WHERE id=$Order->id"),
                        $Order->product_quantity);
        // Clean up
        miDb::getResults("ALTER TABLE `orders` DROP `product_quantity`") ;
        miDb::deleteCachedTableInfo('orders');
    }
}


$test = new testMiResourceFormulas();
$test->run(new HTMLReporter2());

?>
