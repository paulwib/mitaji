<?php

include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

/*
 * It's possible to define a different primary table than that which would be 
 * inferred from the class name. This is useful for having like a table called
 * contacts but then storing various different types of contact in there.
 * 
 * You could even make a generic contact class to extend that would handle
 * making sure the type is automatically saved to the database based on the 
 * class name and correct are pulled out with the find method by setting the 
 * type as a filter.
 */

class Supplier extends miResource {
    public $primary_table = 'contacts';
}

class testNonModelNamedTable extends UnitTestCase {
    function __construct(){
        $this->UnitTestCase('Test Saving in a Non-Standard Table (Mitaji Tests) ');
        miDb::connect(
            json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test",
                          "username" : "root", 
                          "password" : "skippy", 
                          "no_cache" : 0, 
                          "warn_slow_query" : 0 }')
                    );
        // Empty the test tables
        miDb::getResults('TRUNCATE contacts;');
    }
    
    function testSave(){
        $Supplier = new Supplier();
        if(!$this->assertTrue(
                $Supplier->save(array('name'=>'Bob'))
            )){
            return false;
        }
        $this->assertEqual(miDb::getVar('SELECT COUNT(*) FROM contacts'),1);
        $Supplier->delete();
        $this->assertEqual(miDb::getVar('SELECT COUNT(*) FROM contacts'),0);
    }
}

$test = new testNonModelNamedTable();
$test->run(new HTMLReporter2());
