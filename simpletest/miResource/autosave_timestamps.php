<?php

include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

/*
 * Fake user class for testing as creator_id and updater_id are required
 * to save a product
 */
class User extends miResource {
    function primaryKey(){
        return $this->id;
    }
}
class testSaveTimestamps extends UnitTestCase {
    
    function __construct(){
        parent::__construct('Auto-saving of timestamps (miResource Test)');
        miDb::connect(json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test", 
                                    "username" : "root","password" : "skippy",
                                    "no_cache" : 0, "warn_slow_query" : 100 }'));
    }
    
    function testSave(){
        $user = new User();
        $user->id = 20;
        $product_data = array('supplier_id'=>1,
                              'description'=>'Wool Boots',
                              'retail_price'=>'25.55');
        $Product = new miResource(false,'products');
        if(!$this->assertTrue($Product->save($product_data,$user))){
            return false;
        }
        // Check created on and updated on saved
        $created_on = miDb::getVar("SELECT created_on FROM products 
                                        WHERE id=$Product->id;");
        $updated_on = miDb::getVar("SELECT updated_on FROM products 
                                        WHERE id=$Product->id;");
        $this->assertTrue($created_on);
        $this->assertTrue($updated_on);
        $this->assertEqual($created_on,$updated_on);
        // When updating, updated_on will change but not created_on
        // lets wait a second....
        sleep(2);
        if(!$this->assertTrue($Product->save($product_data,$user))){
            return false;
        }
        $created_on_2 = miDb::getVar("SELECT created_on FROM products 
                                        WHERE id=$Product->id;");
        $updated_on_2 = miDb::getVar("SELECT updated_on FROM products 
                                        WHERE id=$Product->id;");
        $this->assertTrue($created_on_2);
        $this->assertTrue($updated_on_2);
        $this->assertEqual($created_on,$created_on_2);
        
        $this->assertNotEqual($updated_on,$updated_on_2);
        $this->assertNotEqual($created_on,$updated_on_2);

        // Clean up
        $this->assertTrue($Product->delete());
    }
}

$test = new testSaveTimestamps();
$test->run(new HTMLReporter2());
