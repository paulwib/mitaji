<?php

# simpletest
include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

define('DB_SETUP_SQL', 'DROP TABLE IF EXISTS clients;
     CREATE TABLE clients (
    `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
    `name` VARCHAR( 255 ) NOT NULL ,
    `created_on` DATETIME NOT NULL ,
    `updated_on` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    PRIMARY KEY ( `id` )
    ) ENGINE = InnoDB;
    
    DROP TABLE IF EXISTS profiles;
    
    CREATE TABLE profiles (
    `client_id` MEDIUMINT UNSIGNED NOT NULL ,
    `location` VARCHAR( 255 ) NOT NULL ,
    `phone` VARCHAR( 32 ) NULL,
    PRIMARY KEY ( `client_id` )
    ) ENGINE = InnoDB;
    
    DROP TABLE IF EXISTS projects;
    
     CREATE TABLE projects (
    `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
    `client_id` MEDIUMINT UNSIGNED NOT NULL ,
    `description` VARCHAR( 255 ) NOT NULL ,
    `created_on` DATETIME NOT NULL ,
    `updated_on` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    PRIMARY KEY ( `id` )
    ) ENGINE = InnoDB;
    
    DROP TABLE IF EXISTS tasks; 

    CREATE TABLE tasks (
    `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
    `project_id` MEDIUMINT UNSIGNED NOT NULL ,
    `description` VARCHAR( 255 ) NOT NULL ,
    `estimated_hours` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
    `actual_hours` MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
    `created_on` DATETIME NOT NULL ,
    `updated_on` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    `completed_on` DATETIME NULL ,
    PRIMARY KEY ( `id` )
    ) ENGINE = InnoDB;
    
    DROP TABLE IF EXISTS tags; 

    CREATE TABLE tags (
    `id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT ,
    `tag` VARCHAR( 255 ) NOT NULL ,
    PRIMARY KEY ( `id` )
    ) ENGINE = InnoDB;
    
    DROP TABLE IF EXISTS tags_tasks; 
    
    CREATE TABLE tags_tasks (
    `tag_id` MEDIUMINT UNSIGNED NOT NULL ,
    `task_id` MEDIUMINT UNSIGNED NOT NULL ,
    PRIMARY KEY ( `tag_id`,`task_id` )
    ) ENGINE = InnoDB;'
);

# Test models for project management
class Client extends miResource { 
    public $delete_cascade = array('Profile');
    public function __construct($data=false,$primary_table=false){
        parent::__construct($data,$primary_table);
        $this->associate('Profile');
    } 
}
class Profile extends miResource {
    public function __construct($data=false,$primary_table=false){
        parent::__construct($data,$primary_table);
        $this->associate('Client');
    } 
}
class Project extends miResource { 
    public $delete_cascade = array('Tasks');
    public function __construct($data=false,$primary_table=false){
        parent::__construct($data,$primary_table);
        $this->associate('Client');
    } 
}    
class Task extends miResource { 
    public function __construct($data=false,$primary_table=false){
        parent::__construct($data,$primary_table);
        $this->associate('Project');
    } 
}
class Tag extends miResource {  }
class TagsTask extends miResource { 
        
    public function save($data=false, $user=false){
        // Skip if already has primary key as this is the only data in this resource
        if($this->primaryKey()){
            return $this->primaryKey();
        }
        // Tried to link to a tag without an id, so let's look for it
        if(trim($data['tag']) && !$data['tag_id']){
            $tag_model = new Tag();
            $Tag = $tag_model->findOne($data['tag'],'tag');
            if($Tag){
                $data['tag_id'] = $Tag->id;
                // Double check don't exist by creating a mock from data
                // and then looking for existing with that primary key
                $mock = new TagsTask($data);
                // See if association already exists
                if($this->findByPrimaryKey($mock->primaryKey())){
                    return $mock->primaryKey();
                }
                
            // Doesn't exist so create it (in this case association cannot exist)
            } else {
                $Tag = new Tag();
                // Second argument of false means do not bother reloading
                if($Tag->save($data, false)){
                    $data['tag_id'] = $Tag->id;
                }
            }
        }
        return parent::save($data, $user);
    }
}
# Test models for order management
class Order extends miResource {
    public $delete_cascade = array('OrdersProducts'); 
    public function save($data, $user=false){
        // If paid make sure products are unset, but also give error
        // message if you tried to change quantities. This message is
        // not 100% reliable as depends what order submission arrives
        if($this->paid_on && @$data['Products']){
            $products = $data['Products'];
            unset($data['Products']);
            $cur_quantities = miArray::extractByKey($this->Products,'quantity');
            $new_quantities = miArray::extractByKey($products,'quantity');
            if($new_quantities != $cur_quantities){
                $this->warnings('Products','Cannot change quantities once order has been saved');
            }
        }
        return parent::save($data,$user);
    }    
}
class Product extends miResource { 
    public function __construct($data=false,$primary_table=false){
        parent::__construct($data,$primary_table);
        $this->associate('Supplier');
    } 
}
class OrdersProduct extends miResource {

    public function save($data, $user=false){
        
        // Delete if exists and quantity is zero
        if($this->primaryKey() && $data['quantity'] == 0){
            return $this->delete();
        }
        // Don't do anything if quantity hasn't changed
        if($this->primaryKey() && $data['quantity'] == $this->quantity){
            return $this->primaryKey();
        }
        // Check product exists
        $model = new Product();
        $product = $model->findByPrimaryKey($data['product_id']);
        if(!$product){
            $this->errors('product_id','Cannot add product to order as does not exist');
            return false;
        }
        // Add price
        if(!$data['price']){
            $data['price'] = $product->retail_price;
        }
        // Save
        return parent::save($data, $user);
    }    
}

# ----------------------------------------------------------------------------

class testSaveHasMany extends UnitTestCase {
    
    # Test data (JSON string)
    public $project_create_data = '{"description" : "My new projct", 
                                    "Tasks" : [
                                    { "description" : "Fuffle the wuffle", 
                                      "estimated_hours" : 10 },
                                    { "description" : "Splonk the conk" },
                                    { "description" : "Spook the cook" }
                                        ]
                                    }';
    public $task_create_data = '';
# ----------------------------------------------------------------------------
    function __construct(){
        $this->UnitTestCase('Test Save and Save Partial');
        miDb::connect(json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test", 
                                    "username" : "root","password" : "skippy",
                                    "no_cache" : 0, "warn_slow_query" : 100 }'));
        if(miDB::getResults(DB_SETUP_SQL)){
            return false;
        }
    }
# ----------------------------------------------------------------------------    
    function testCreate(){

        $qc = miDb::queryCount();
        
        // Create a client
        $Client = new Client();
        $client_data = array('name'=>'Bunglers',
                                    'Profile'=>array('location'=>'London')
                                    );
        if(!$this->assertTrue($Client->save($client_data),'Client saved')){
            return false;
        }

        // Project data
        $data = json_decode($this->project_create_data,true);

        // Assign client id to data
        $data['client_id'] = $Client->primaryKey();
        
        // Create the project and save
        $Project = new Project();
        if(!$this->assertTrue($Project->save($data),'Project saved')){
            return false;
        }
        // Project automatically reloaded as save successful      
        $this->assertEqual($Project->Client->name,'Bunglers');
        $pk = $Project->primaryKey();
        unset($Project);

        // Force a hard reload of the project and check all tasks saved
        $project_model = new Project();
        $Project = $project_model->findByPrimaryKey($pk);
        if(!$this->assertTrue($Project->Tasks)){
            return false;
        }
        $this->assertTrue($Project->Tasks->count());
        $assert_tasks = miArray::extractByKey($data['Tasks'],'description');
        $project_tasks = miArray::extractByKey($Project->Tasks,'description');
        $db_tasks = miDb::getCol('SELECT description FROM tasks;');
        sort($assert_tasks); sort($project_tasks); sort($db_tasks);
        $this->assertEqual($assert_tasks, $project_tasks);
        $this->assertEqual($assert_tasks, $db_tasks);
        
        /* 
         * When re-saving the has many associates must have their primary keys 
         * or they will be recreated. To demonstrate this I save the same data
         * again and the old tasks should be deleted and new ones created.
         */
        $current_ids = miArray::extractByKey($Project->Tasks,'id');
        if(!$this->assertTrue($Project->save($data))){
            return false;
        }
        $new_ids = miArray::extractByKey($Project->Tasks,'id');
        $this->assertEqual($current_ids, array(1,2,3));
        $this->assertEqual($new_ids, array(4,5,6));
        /*
         * Now do another proper save with the newly saved data as that will
         * contain primary keys. This demonstartes that if you send back 
         * primary keys of the has many they will not be created again.
         */
        $current_ids = $new_ids;
        $task_data = $Project->Tasks->getData();
        $data['Tasks'] = $task_data;
        if(!$this->assertTrue($Project->save($data))){
            return false;
        }
        $new_ids = miArray::extractByKey($Project->Tasks,'id');
        $this->assertEqual($new_ids,$current_ids);
        
        // Double check task descriptions
        $assert_tasks = miArray::extractByKey($data['Tasks'],'description');
        $project_tasks = miArray::extractByKey($Project->Tasks,'description');
        $db_tasks = miDb::getCol('SELECT description FROM tasks;');
        sort($assert_tasks); sort($project_tasks); sort($db_tasks);
        $this->assertEqual($assert_tasks, $project_tasks);
        $this->assertEqual($assert_tasks, $db_tasks);

        // Delete Project and make sure associated Tasks deleted
        $this->assertTrue($Project->delete(),'Project deleted');
        $this->assertFalse(miDb::getResults("SELECT * FROM tasks WHERE project_id=$pk;"));
        
        // The client will not be deleted as no delete cascade and wouldn't work
        // anyway as can't delete something you belong to
        $this->assertTrue(miDb::getResults("SELECT * FROM clients WHERE id=".$Client->primaryKey()));
        $this->assertTrue($Client->delete());
        $this->assertFalse(miDb::getResults('SELECT * FROM clients'));
        $this->assertFalse(miDb::getResults('SELECT * FROM profiles'));
    }
# ----------------------------------------------------------------------------    
    function testCreateBadData(){ 

        // Project data - no client id added
        $data = json_decode($this->project_create_data,true);
                
        $Project = new Project();
        $this->assertFalse($Project->save($data),'Project saved');
        
        // Project Tasks should still be intact even if not saved  
        $data_tasks = miArray::extractByKey($data['Tasks'],'description');
        $project_tasks = miArray::extractByKey($Project->Tasks,'description');
        sort($data_tasks); sort($project_tasks);
        $this->assertEqual($data_tasks, $project_tasks);
        
        // No tasks should have been created
        $this->assertFalse(miDb::getResults('SELECT * FROM tasks'));
        
        /* 
         * Save will also fail if anything wrong with associates
         * For example, I'm going to add the required client id
         * but put some letters in one of the tasks when it is
         * supposed to be a number
         */
        $Client = new Client();
        $this->assertTrue($Client->save(array('name'=>'Spokles')),'Client saved');
        $data['client_id'] = $Client->primaryKey();
        $data['Tasks'][0]['estimated_hours'] = 'aksjgsakgfk';
        $this->assertFalse($Project->save($data));

        // You can check which task had the error
        $error_count = 0;
        foreach($Project->Tasks as $Task){
            if($Task->errors()){
                $error_count++;
            }
        }
        $this->assertEqual($error_count,1);
        
        // No project or tasks as any executed queries would be rolled back
        $this->assertFalse(miDb::getResults('SELECT * FROM projects'));
        $this->assertFalse(miDb::getResults('SELECT * FROM tasks'));
        
        // Just to confirm, let's correct the data error and save
        $data['Tasks'][0]['estimated_hours'] = '30';
        if(!$this->assertTrue($Project->save($data))){
            return;
        }
        $this->assertEqual(miDb::getVar('SELECT COUNT(*) FROM projects'),1);
        $this->assertEqual(miDb::getVar('SELECT COUNT(*) FROM tasks'),3);
        
    }
# ----------------------------------------------------------------------------   
    function testSaveHasManyThrough(){

        // Create client, project and tags
        $Client = new Client();
        $this->assertTrue($Client->save(array('name'=>'Bunglers')),'Client saved');
        $data = json_decode($this->project_create_data,true);
        $data['client_id'] = $Client->primaryKey();
        $Project = new Project();
        $this->assertTrue($Project->save($data),'Project saved');
        $Tag1 = new Tag();
        $this->assertTrue($Tag1->save(array('tag'=>'design')),'Tag saved');        
        $Tag2 = new Tag();
        $this->assertTrue($Tag2->save(array('tag'=>'blocker')),'Tag saved');

        // Now get a task
        $task_model = new Task();
        $tasks = $task_model->find();
        $Task = $tasks->getFirst();
        // Extract data so we can simulate what data from a POST might look like
        $task_data = $Task->getData();
        unset($task_data['Project']);
        
        // Add the tags to the task data and save, should pick up on the through model
        $task_data['Tags'] = array(
                array('tag_id'=>$Tag1->id),
                array('tag_id'=>$Tag2->id)
                                    );
        if(!$this->assertTrue($Task->save($task_data))){
            return false;
        }
        $assert_tags = array('design','blocker');
        $actual_tags = miArray::extractByKey($Task->Tags,'tag');
        sort($assert_tags); sort($actual_tags);
        $this->assertEqual($assert_tags, $actual_tags);
        
        /*
         * TagsTask->save() will automatically create tasks if you send
         * some that do not already exist
         */
        $task_data['Tags'][] = array('tag'=>'late');
        $Task->save($task_data);        
        $assert_tags = array('design','blocker','late');
        $actual_tags = miArray::extractByKey($Task->Tags,'tag');
        $db_tags = miDb::getCol('SELECT tag FROM tags');
        sort($assert_tags); sort($actual_tags); sort($db_tags);
        $this->assertEqual($assert_tags, $actual_tags);
        $this->assertEqual($assert_tags, $db_tags);

        /*
         * TagsTask->save also checks if the tag is already associated
         * so if you save with the same tags again no new associations
         * will be created_on, even if you don't pass in the tag ids
         */
        $assert_assoc = miDb::getResults('SELECT * FROM tags_tasks');
        $task_data['Tags'] = array(
                                 array('tag'=>'design'),
                                 array('tag'=>'blocker'),
                                 array('tag'=>'late')
                             );
        $this->assertTrue($Task->save($task_data));
        $this->assertEqual($assert_assoc,miDb::getResults('SELECT * FROM tags_tasks'));
        
        // Check no new tags
        $assert_tags = array('design','blocker','late');
        $actual_tags = miDb::getCol('SELECT tag FROM tags');
        sort($assert_tags); sort($actual_tags);
        $this->assertEqual($assert_tags, $actual_tags);
        
        /* 
         * Don't send a tag and the association is deleted but not the tag itself
         * You'd probably need some custom code to sort this kind of behaviour
         * Or some kind of clean up script that would clear out unused tags after 
         * each POST/PUT/DELETE
         */
        $task_data['Tags'] = array(
                                array('tag'=>'design'),
                                array('tag'=>'blocker')
                            );
        $this->assertTrue($Task->save($task_data));
        $assert_tags = array('design','blocker');
        $actual_tags = miArray::extractByKey($Task->Tags,'tag');
        $db_tags = miDb::getCol('SELECT tag FROM tags');
        sort($assert_tags); sort($actual_tags);
        $this->assertEqual($assert_tags, $actual_tags);
        $this->assertNotEqual($assert_tags, $db_tags);
    }

# ----------------------------------------------------------------------------
    public function testOrderSave(){

        $order_data = array('customer_id'=>400,
                            'payment_method'=>'Cash',
                            'creator_id'=>1, 
                            'updater_id'=>1,
                            'Products'=>array(
                                array('product_id'=>1,'quantity'=>10),
                                array('product_id'=>2,'quantity'=>6),
                                array('product_id'=>3,'quantity'=>3)
                                )
                            );
        $Order = new Order();
        if(!$this->assertTrue($Order->save($order_data))){
            return false;
        }
        
        // Check resource and database product ids match
        $assert_product_ids = array(1,2,3);
        $resource_product_ids = miArray::extractByKey($Order->Products,'id');
        $db_product_ids = miDB::getCol('SELECT product_id FROM orders_products WHERE order_id='.$Order->id.';');
        $this->assertEqual($assert_product_ids,$resource_product_ids);
        $this->assertEqual($assert_product_ids,$db_product_ids);
        
        // Re-save to check associations only created once       
        $Order->save($order_data);
        $assert_product_ids = array(1,2,3);
        $resource_product_ids = miArray::extractByKey($Order->Products,'id');
        $db_product_ids = miDB::getCol('SELECT product_id FROM orders_products
                     WHERE order_id='.$Order->id.' ORDER BY product_id;');
        $this->assertEqual($assert_product_ids,$resource_product_ids);
        $this->assertEqual($assert_product_ids,$db_product_ids);
        
        // Now add a product to the order and check association added for it
        $order_data['Products'][] = array('product_id'=>15,'quantity'=>6);
        $Order->save($order_data);
        $assert_product_ids = array(1,2,3,15);
        $resource_product_ids = miArray::extractByKey($Order->Products,'id');
        $db_product_ids = miDB::getCol('SELECT product_id FROM orders_products
                     WHERE order_id='.$Order->id.' ORDER BY product_id;');
        $this->assertEqual($assert_product_ids,$resource_product_ids);
        $this->assertEqual($assert_product_ids,$db_product_ids);
        
        // Remove a product and check it's gone
        $order_data['Products'] = array_slice($order_data['Products'],1);
        $Order->save($order_data);
        $assert_product_ids = array(2,3,15);
        $resource_product_ids = miArray::extractByKey($Order->Products,'id');
        $db_product_ids = miDB::getCol('SELECT product_id FROM orders_products
                     WHERE order_id='.$Order->id.' ORDER BY product_id;');
        $this->assertEqual($assert_product_ids,$resource_product_ids);
        $this->assertEqual($assert_product_ids,$db_product_ids);
        
        // Add, delete and update all in one go
        $order_data['Products'] = array_slice($order_data['Products'],1);
        $order_data['Products'][0]['quantity'] = 10;
        $order_data['Products'][] = array('product_id'=>11,'quantity'=>10);
        $Order->save($order_data);
        $assert_product_ids = array(3,11,15);
        $resource_product_ids = miArray::extractByKey($Order->Products,'id');
        $db_product_ids = miDB::getCol('SELECT product_id FROM orders_products
                     WHERE order_id='.$Order->id.' ORDER BY product_id;');
        $this->assertEqual($assert_product_ids,$resource_product_ids);
        $this->assertEqual($assert_product_ids,$db_product_ids);
        
        /*
         * Application specific logic incorporated above in OrdersProduct
         * save() method
         */
        // Cannot save an order with a product that doesn't exist
        $order_data['Products'][] = array('product_id'=>999999,'quantity'=>10);
        $this->assertFalse($Order->save($order_data));
        
        // Cannot change quantites after order been paid
        if(!$this->assertTrue($Order->update(array('paid_on'=>'now')))){
            return false;
        }
        $this->assertEqual(substr($Order->paid_on,0,10),date('Y-m-d'));
        $order_data['Products'][0]['quantity'] = 20;
        // Will save OK
        $this->assertTrue($Order->save($order_data));
        // But should be a warning that quantities cannot be saved
        // The reason for a warning rather than an error is because you
        // don't want to show them the data back they sent, otherwise they'll
        // have to correct it to save. Or maybe you do want to force that, but
        // that's down to the application.
        $this->assertTrue($Order->warnings());
        
        // Delete the order and all associates should be deleted
        // but not products themselves
        $this->assertTrue($Order->delete());
        $this->assertFalse(miDb::getVar('SELECT SUM(quantity) AS q 
                                        FROM orders_products 
                                        WHERE order_id ='.$Order->id));
        $this->assertTrue(miDb::getCol('SELECT id FROM products WHERE id IN(1,2,3,11,15)'));
        
    } 
}

$test = new testSaveHasMany();
$test->run(new HTMLReporter2());

