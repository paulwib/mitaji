<?php

include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

class miDateTests extends UnitTestCase {

// ---------------------------------------------------------------------------
/*
 * miDate::strToTime wil treat dates differently depending what timezone 
 * is set
 */
	function testStrToTime(){
		
		// Store current timezone
		$timezone = date_default_timezone_get();
		
		// Switch to London
		date_default_timezone_set('Europe/London');
		$this->assertEqual(miDate::strToTime('06/10/09'),
							strtotime('6 Oct 2009'));
		
		// Switch to New York
		if(!date_default_timezone_set('America/New_York')){
			$this->assertTrue(false, 'Could not set timezone to America/New_York');
			return false;
		}
		// Test this should work
		$this->assertEqual(date_default_timezone_get(),'America/New_York');
		$this->assertEqual(miDate::strToTime('06/10/09'),
								strtotime('10 June 2009'));
		
		// Restore original timezone
		date_default_timezone_set($timezone);
	}
// ---------------------------------------------------------------------------
	function testDays(){
		// Defaults format October 2009
		$days = miDate::days('Y-m-d',strtotime('2009-10-03'));
		$expected = array ( 0 => '2009-10-01',
							1 => '2009-10-02',
							2 => '2009-10-03',
							3 => '2009-10-04',
							4 => '2009-10-05',
							5 => '2009-10-06',
							6 => '2009-10-07',
							7 => '2009-10-08',
							8 => '2009-10-09',
							9 => '2009-10-10',
							10 => '2009-10-11',
							11 => '2009-10-12',
							12 => '2009-10-13',
							13 => '2009-10-14',
							14 => '2009-10-15',
							15 => '2009-10-16',
							16 => '2009-10-17',
							17 => '2009-10-18',
							18 => '2009-10-19',
							19 => '2009-10-20',
							20 => '2009-10-21',
							21 => '2009-10-22',
							22 => '2009-10-23',
							23 => '2009-10-24',
							24 => '2009-10-25',
							25 => '2009-10-26',
							26 => '2009-10-27',
							27 => '2009-10-28',
							28 => '2009-10-29',
							29 => '2009-10-30',
							30 => '2009-10-31');
		$this->assertEqual($days,$expected);
		
		// Formatting for URLs
		$days = miDate::days('/Y/m/d/',strtotime('2009-10-03'));
		$expected = array ( 0 => '/2009/10/01/',
							1 => '/2009/10/02/',
							2 => '/2009/10/03/',
							3 => '/2009/10/04/',
							4 => '/2009/10/05/',
							5 => '/2009/10/06/',
							6 => '/2009/10/07/',
							7 => '/2009/10/08/',
							8 => '/2009/10/09/',
							9 => '/2009/10/10/',
							10 => '/2009/10/11/',
							11 => '/2009/10/12/',
							12 => '/2009/10/13/',
							13 => '/2009/10/14/',
							14 => '/2009/10/15/',
							15 => '/2009/10/16/',
							16 => '/2009/10/17/',
							17 => '/2009/10/18/',
							18 => '/2009/10/19/',
							19 => '/2009/10/20/',
							20 => '/2009/10/21/',
							21 => '/2009/10/22/',
							22 => '/2009/10/23/',
							23 => '/2009/10/24/',
							24 => '/2009/10/25/',
							25 => '/2009/10/26/',
							26 => '/2009/10/27/',
							27 => '/2009/10/28/',
							28 => '/2009/10/29/',
							29 => '/2009/10/30/',
							30 => '/2009/10/31/');
		$this->assertEqual($days,$expected);
		
		// Check a month with strange number of days
		$days = miDate::days('Y-m-d',strtotime('2009-02-15'));
		$expected = array ( 0 => '2009-02-01',
							1 => '2009-02-02',
							2 => '2009-02-03',
							3 => '2009-02-04',
							4 => '2009-02-05',
							5 => '2009-02-06',
							6 => '2009-02-07',
							7 => '2009-02-08',
							8 => '2009-02-09',
							9 => '2009-02-10',
							10 => '2009-02-11',
							11 => '2009-02-12',
							12 => '2009-02-13',
							13 => '2009-02-14',
							14 => '2009-02-15',
							15 => '2009-02-16',
							16 => '2009-02-17',
							17 => '2009-02-18',
							18 => '2009-02-19',
							19 => '2009-02-20',
							20 => '2009-02-21',
							21 => '2009-02-22',
							22 => '2009-02-23',
							23 => '2009-02-24',
							24 => '2009-02-25',
							25 => '2009-02-26',
							26 => '2009-02-27',
							27 => '2009-02-28');
		$this->assertEqual($days,$expected);
		
		
		// Leap year
		$days = miDate::days('Y-m-d',strtotime('2008-02-24'));
		$expected = array ( 0 => '2008-02-01',
							1 => '2008-02-02',
							2 => '2008-02-03',
							3 => '2008-02-04',
							4 => '2008-02-05',
							5 => '2008-02-06',
							6 => '2008-02-07',
							7 => '2008-02-08',
							8 => '2008-02-09',
							9 => '2008-02-10',
							10 => '2008-02-11',
							11 => '2008-02-12',
							12 => '2008-02-13',
							13 => '2008-02-14',
							14 => '2008-02-15',
							15 => '2008-02-16',
							16 => '2008-02-17',
							17 => '2008-02-18',
							18 => '2008-02-19',
							19 => '2008-02-20',
							20 => '2008-02-21',
							21 => '2008-02-22',
							22 => '2008-02-23',
							23 => '2008-02-24',
							24 => '2008-02-25',
							25 => '2008-02-26',
							26 => '2008-02-27',
							27 => '2008-02-28',
							28 => '2008-02-29');
		$this->assertEqual($days,$expected);
		
		// Using strftime
		$days = miDate::days('%Y-%m-%d',strtotime('2008-02-24'));
		$expected = array ( 0 => '2008-02-01',
							1 => '2008-02-02',
							2 => '2008-02-03',
							3 => '2008-02-04',
							4 => '2008-02-05',
							5 => '2008-02-06',
							6 => '2008-02-07',
							7 => '2008-02-08',
							8 => '2008-02-09',
							9 => '2008-02-10',
							10 => '2008-02-11',
							11 => '2008-02-12',
							12 => '2008-02-13',
							13 => '2008-02-14',
							14 => '2008-02-15',
							15 => '2008-02-16',
							16 => '2008-02-17',
							17 => '2008-02-18',
							18 => '2008-02-19',
							19 => '2008-02-20',
							20 => '2008-02-21',
							21 => '2008-02-22',
							22 => '2008-02-23',
							23 => '2008-02-24',
							24 => '2008-02-25',
							25 => '2008-02-26',
							26 => '2008-02-27',
							27 => '2008-02-28',
							28 => '2008-02-29');
		$this->assertEqual($days,$expected);
	}
// ------------------------------------------------------------------------
	function testMonths(){
		// Short month names
		$expected = array('Jan',
						  'Feb',
						  'Mar',
						  'Apr',
						  'May',
						  'Jun',
						  'Jul',
						  'Aug',
						  'Sep',
						  'Oct',
						  'Nov',
						  'Dec');
		$months = miDate::months();
		$this->assertEqual($months,$expected);  
		
		// Long month names
		$expected = array('January', 
						  'February', 
						  'March', 
						  'April', 
						  'May', 
						  'June', 
						  'July', 
						  'August', 
						  'September', 
						  'October', 
						  'November', 
						  'December');
		$months = miDate::months('F');
		$this->assertEqual($months,$expected);  
		
		// Including year part, use timestamp of year you want (or blank for current)
		$expected = array('2006-01', 
						  '2006-02', 
						  '2006-03', 
						  '2006-04', 
						  '2006-05', 
						  '2006-06', 
						  '2006-07', 
						  '2006-08', 
						  '2006-09', 
						  '2006-10', 
						  '2006-11', 
						  '2006-12');
		$months = miDate::months('Y-m',strtotime('2006-03-10'));
		$this->assertEqual($months,$expected);
		
		// A common kind of URL formatting
		$expected = array('/1978/01/', 
						  '/1978/02/', 
						  '/1978/03/', 
						  '/1978/04/', 
						  '/1978/05/', 
						  '/1978/06/', 
						  '/1978/07/', 
						  '/1978/08/', 
						  '/1978/09/', 
						  '/1978/10/', 
						  '/1978/11/', 
						  '/1978/12/');
		$months = miDate::months('/Y/m/',strtotime('1978-09-12'));
		$this->assertEqual($months,$expected);  
		
		// Will detect use of strftime which is useful for localization 
		if(setlocale(LC_ALL, 'de_DE@euro', 'de_DE', 'de_DE.utf8','deu_deu')){
			$expected = array ( 'Januar', 
								'Februar', 
								'März', 
								'April',
								'Mai',
								'Juni',
								'Juli',
								'August', 
								'September', 
								'Oktober', 
								'November', 
								'Dezember' 
								);
			$this->assertEqual(miDate::months('%B'),$expected); 
		} else {
			$this->assertFalse(true, 'Could not test German months as locale not available');
		}
	}
}

$test = new miDateTests();
if (TextReporter::inCli()) {
	exit ($test->run(new TextReporter()) ? 0 : 1);
}
$test->run(new HTMLReporter2());
