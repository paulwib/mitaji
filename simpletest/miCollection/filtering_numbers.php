<?php

# simpletest
include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

class testMiCollectionFilterNumber extends UnitTestCase {
    
    function __construct(){
        $this->UnitTestCase('Test miCollection Filtering Numbers');
        miDb::connect(
            json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test",
                          "username" : "root", 
                          "password" : "skippy", 
                          "no_cache" : 0, 
                          "warn_slow_query" : 50 }')
                    );
    }
    
    
    function testNumberFilter(){

        $order_model = new miResource(false,'Order');
        $Orders = $order_model->find();
        
        // Match a number exactly
        $Orders->filter('Order.id',150);
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertEqual($Order->id,150);
        }
        
        // Does not match number
        $Orders->filter('Order.id','!1');
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertNotEqual($Order->id,1);
        }
        
        // Greater than
        $Orders->filter('Order.id','>150');
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertTrue($Order->id>150);
        }
        
        // Less than
        $Orders->filter('Order.id','<150');
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertTrue($Order->id<150);
        }
        
        // In a group 
        $values = array(150, 567, 765);
        $Orders->filter('Order.id','in '.implode(',',$values));
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertTrue(in_array($Order->id,$values));
        }
        
        // Not in a group
        $Orders->filter('Order.id','not in '.implode(',',$values));
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertFalse(in_array($Order->id,$values));
        }
        
        // In range
        // A bad range filter - should go from low to high
        $this->expectError();
        $Orders->filter('Order.id','150-75');
        // A good range filter
        $Orders->filter('Order.id','150-175');
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertTrue($Order->id>=150 && $Order->id<=175);
        }
    }
}

$test = new testMiCollectionFilterNumber();
$test->run(new HTMLReporter2());

?>
