<?php

# simpletest
include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

class testMiCollectionPaging extends UnitTestCase {
    
    function __construct(){
        $this->UnitTestCase('Test miCollection Paging');
        miDb::connect(
            json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test",
                          "username" : "root", 
                          "password" : "skippy", 
                          "no_cache" : 0, 
                          "warn_slow_query" : 50 }')
                    );
    }
    
    function testPaging(){

        $order_model = new miResource(false,'Orders');
        $Orders = $order_model->find();
        $Orders->pageSize(25);
        $Orders->page(10);
        $i = 0;
        foreach($Orders as $Order){
            $i++;
        }
        // Check correct number of resources retrieved in collection
        $this->assertEqual($i,25);
        $this->assertEqual($Orders->count(),25);
        
        // Check overall total of resources
        $r_total = $Orders->resourcesTotal();
        $r_verified = miDb::getVar('SELECT COUNT(*) FROM orders;');
        $this->assertEqual($r_total,$r_verified);
        
        // Check start and end
        $this->assertEqual($Orders->resourcesStart(),226);
        $this->assertEqual($Orders->resourcesEnd(),250);
        
        // Changing page size and page forces query to repeat
        $Orders->pageSize(50);
        $Orders->page(20);
        $this->assertEqual($Orders->resourcesStart(),951);
        $this->assertEqual($Orders->resourcesEnd(),1000);
        
        // So confirm that's a total of 2 finds
        $this->assertEqual($Orders->find_count,2);
        
        /*
         *  And if no results all the counts will be false
         * Note that the best way to test for an empty collection is use the 
         * `count()` method
         */
        $Orders->page(634);
        $this->assertFalse($Orders->count());
        /*
         * The resourcesTotal will always tell you the total amount in the 
         * table matching the current filter, so this will still be true
         * even if there are no resources on the current page, so watch
         * out for this
         */
        $this->assertTrue($Orders->resourcesTotal());
        $this->assertFalse($Orders->resourcesStart());
        $this->assertFalse($Orders->resourcesEnd());
    }
}
$test = new testMiCollectionPaging();
$test->run(new HTMLReporter2());
