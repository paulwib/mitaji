<?php

# simpletest
include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

class testMiCollectionFiltering extends UnitTestCase {
    
    function __construct(){
        $this->UnitTestCase('Test miCollection Filtering');
        miDb::connect(
            json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test",
                          "username" : "root", 
                          "password" : "skippy", 
                          "no_cache" : 0, 
                          "warn_slow_query" : 50 }')
                    );
    }
    
    function testFilter(){
        
        $order_model = new miResource(false,'Orders');
        $Orders = $order_model->find();
        
        /*
         * Add filters by calling the `filter()` on the collection
         * (More specific date examples in `filtering_dates.php`)
         */
        $Orders->filter('Order.created_on', 'Feb 2006');
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertPattern('/^2006-02.+/',$Order->created_on);
        }
        
        // You can have a look at the filters added so far with getFilters
        $filters = $Orders->getFilters();
        $this->assertEqual($filters['Order.created_on'], 'Feb 2006');
        
        // You remove filters by calling unfilter
        $Orders->unfilter();
        $this->assertEqual(count($Orders->getFilters()),0);
        
        /* If you're filtering on a field directly attached to the resource 
         * you can just use the field name
         * (see `filtering_associates.php` for how to filter on associates)
         */
        $Orders->filter('created_on', 'Feb 2006');
        /*
         * Check the filter has the correct resource name added
         * Also note here I'm using getFilter with the name of the filter 
         * to just return the value for that filter
         */
        $this->assertEqual($Orders->getFilters('Order.created_on'), 'Feb 2006');
        
        /* 
         * You can unfilter with/without the primary resource name
         * Also note again that by specifying the name of the filter in
         * unfilter I'm only removing that specific filter
         */
        $Orders->unfilter('created_on');
        $this->assertNull($Orders->getFilters('Order.created_on'));
        
        /*
         * These techniques can be mixed up and miCollection will do
         * right thing
         */
        $Orders->filter('created_on', 'Feb 2006');
        $this->assertEqual($Orders->getFilters('created_on'), 'Feb 2006');
        $Orders->unfilter('Order.created_on');
        $this->assertNull($Orders->getFilters('created_on'));
        
        /*
         * You can filter on multiple fields so this gets all orders created
         * in February 2006 with creator_id 5
         */
        $Orders->unfilter();
        $Orders->filter('created_on', 'Feb 2006');
        $Orders->filter('creator_id',5);
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertEqual($Order->creator_id,5);
            $this->assertPattern('/^2006-02.+/',$Order->created_on);
        }
        
        /*
         * You can also set multiple filters at once by passing a single 
         * array of filters where key is field name and filter is value. 
         * This is the same structure returned from getFilters
         */
        $Orders->unfilter();
        $filters = array('Order.created_on' => 'Feb 2006',
                         'Order.creator_id' => 5);        
        $Orders->filter($filters);
        $this->assertIdentical($Orders->getFilters(),$filters);
        
        /*
         * You can also use a format where each value of the array is another 
         * array of arguments where the first is the field to filter on and
         * the second is the value to find.
         * Note getFilters will always return in the format used above.
         */
        $expected_filters = $filters;
        $Orders->unfilter();
        $filters = array(array('Order.created_on','Feb 2006'), 
                        array('Order.creator_id',5));
        $Orders->filter($filters);
        $this->assertIdentical($Orders->getFilters(),$expected_filters);
        
        /*
         * You can also use named keys is your filter come from some 
         * programmatically generated thing and you're not sure if arguments
         * will be in correct order
         */
        $Orders->unfilter();
        $filters = array(array('field'=>'Order.created_on','value'=>'Feb 2006'), 
                        array('field'=>'Order.creator_id','value'=>5));
        $Orders->filter($filters);
        $this->assertIdentical($Orders->getFilters(),$expected_filters);
        
        // This allows you to do things like get filters from a JSON string
        $Orders->unfilter();
        $filters = json_decode('[
                                 ["Order.created_on","Feb 2006"],
                                 ["Order.creator_id",5]
                                ]');
        $Orders->filter($filters);
        $this->assertIdentical($Orders->getFilters(),$expected_filters);
        
        /*
         * Filtering on a field that doesn't exist will return false and
         * generate an E_USER_WARNING
         */
        $this->expectError();
        $this->assertFalse($Orders->filter('foobar',5));
    }
}

$test = new testMiCollectionFiltering();
$test->run(new HTMLReporter2());

?>
