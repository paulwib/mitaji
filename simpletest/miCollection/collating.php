<?php

# simpletest
include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

class testMiCollectionCollating extends UnitTestCase {
    
    function __construct(){
        $this->UnitTestCase('Test miCollection Filters');
        miDb::connect(
            json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test",
                          "username" : "root", 
                          "password" : "skippy", 
                          "no_cache" : 0, 
                          "warn_slow_query" : 50 }')
                    );
    }    
    
    function testCollate(){
        /*
         * Collations allow you to group a collection of resources based
         * on common resource attributes.
         * The collate method returns an array of collections.
         */
        $customer_model = new miResource(false,'Customers');
        $Customers = $customer_model->find();
        $Customers->filter('town','cray*');
        $Customers->orderBy('town');
        $this->assertTrue($Customers->count());
        $collated = $Customers->collate('town');
        $expected_towns = array('CRAY','CRAYFORD','CRAYS HILL');
        $i=0;
        foreach($collated as $collection){
            foreach($collection as $Customer){
                $this->assertEqual($Customer->town,$expected_towns[$i]);
            }
            $i++;
        }
    }
    function testCollateAssociate(){
        /*
         * You can collate on the value of an associate as well, just precede
         * the field name with the model name, same as filtering
         * (See associates tests for more on that)
         */
        $order_model = new miResource(false,'Orders');
        $order_model->associate('Customer.Country');
        $Orders = $order_model->find();
        $Orders->filter('Customer.last_name','in Jones, Smith');
        $collated = $Orders->collate('Customer.last_name');
        foreach($collated as $collection){
            $first = $collection->getFirst();
            $match = @$first->Customer->last_name;
            foreach($collection as $Order){
                $this->assertEqual(@$Order->Customer->last_name,$match);
            }
        }
        
    }
    function testCollateMultiple(){
        /*
         * You can collate on multiple fields to get a deeper array structure
         * of groups, just pass multiple arguments to collate.
         * Here I group products by the supplier id and then by their VAT band 
         */
        $product_model = new miResource(false,'Products');
        $Products = $product_model->find();
        $Products->pageSize(30);
        $Products->orderBy('supplier_id, vat DESC, id');
        $collated = $Products->collate('supplier_id','vat');
        $expected = array(1=>array(17.5),
                          2=>array(17.5),
                          3=>array(17.5,8.75,0),
                          4=>array(17.5,8.75),
                          5=>array(17.5),
                          6=>array(17.5));
        $i = 1;
        foreach($collated as $k=>$supplier_level){
            /* 
             * First grouped by suppliers. To see which supplier you need to
             * get the first product in the first collection at the supplier 
             * grouped level
             */
            $products = current($supplier_level);
            $first_product = $products->getFirst();
            $this->assertEqual($first_product->supplier_id,$i);
            $j = 0;
            foreach($supplier_level as $products){
                /*
                 * Now all the products in each collection should have
                 * the same VAT
                 */
                foreach($products as $product){
                    $this->assertEqual($product->vat,$expected[$i][$j]);
                }                
                $j++;
            }
            $this->assertEqual($j,count($expected[$i]),"Number of tax bands for supplier $i");
            $i++;
        }
    }
    function testCollateCallback(){
        /*
         * You can also define a callback function to apply to the field you 
         * want to collate on. Here we collate by created date, but specify a
         * callback function that returns just year and month, so orders are 
         * collated by that instead.
         * 
         * To specify a callback you need to pass an array for each argument. 
         * The first element is the field to group on and the second is the
         * PHP pseudo-type callback (see call_user_func for definition of 
         * callback pseudo-type).
         * 
         * In this example I use create_function to create an anonymous
         * function on the fly, but any style of callback will work.
         */
        $order_model = new miResource(false,'Orders');
        $Orders = $order_model->find();
        $Orders->pageSize(60);
        $Orders->orderBy('created_on');
        $collated = $Orders->collate(array('created_on',
                                create_function('$val',
                                    'return date("Y-m-d",strtotime($val));'
                                )
                            )
                        );
        // Check all orders in each collection share the same data month
        foreach($collated as $Orders){
            $first = $Orders->getFirst();
            $match = date('Y-m-d',strtotime($first->created_on));
            foreach($Orders as $Order){
                $this->assertEqual($match,date('Y-m-d',strtotime($Order->created_on)));
            }
        }
    }
}

$test = new testMiCollectionCollating();
$test->run(new HTMLReporter2());
