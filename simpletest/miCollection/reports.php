<?php

# simpletest
include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

class testMiCollectionReports extends UnitTestCase {
    
    function __construct(){
        $this->UnitTestCase('Test miCollection Reports');
        miDb::connect(
            json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test",
                          "username" : "root", 
                          "password" : "skippy", 
                          "no_cache" : 0, 
                          "warn_slow_query" : 50 }')
                    );
    }    

    function testReport(){
       
        /* 
         * You can get a report on aggregate data in a collection with 
         * the report() function.
         * First I set up a mock orders model with a formula to get the net 
         * total of each order (this would normally all be done in your class
         * which extends miResource) 
         */
        $order_model = new miResource(false,'Orders');
        $order_model->associate('Customer');
        $order_model->formulate('net_total','IFNULL((SELECT SUM(quantity*price)
                                             FROM orders_products 
                                             WHERE orders_products.order_id=orders.id),
                                             0)');
        $Orders = $order_model->find();
        $Orders->filter('Order.created_on','2006');
        $Orders->orderBy('Order.created_on');
        /*
        * Then call the report() function with two array arguments
        * The first contains the columns or SQL expression to group on and the
        * second contains the column names or SQL snippet results to retrieve.
        * 
        * In both cases the key is the alias for the column, if required. If
        * you don't require an alias (maybe because you're not using a SQL 
        * snippet) then you can leave out the key.
        * 
        * You can reference formulas in your SQL snippets and they will 
        * automatically be replaced with the formula.
        * 
        * This report gets the order count, net total and average order by
        * month. Below I also filter to orders paid in 2006 so only get
        * report for that year.
        * 
        * No SQL to get the full collection is executed, which means reporting
        * can be very fast when set up right and you can create multiple 
        * reports with minimal SQL queries.
        */
        $report = $Orders->report(
                        array('month_name'=>'MONTHNAME(orders.created_on)'),
                        array('order_count'=>'COUNT(orders.id)',
                              'net_total'=>'SUM(net_total)',
                              'average_order'=>'AVG(net_total)'));
        $this->assertEqual(count($report),12);
        $this->assertEqual($report[0]['net_total'],56547);
        $this->assertNotNull($report[0]['month_name']);
        $this->assertNotNull($report[0]['average_order']);
        
        /*
         * Here's another report that tells you how late late orders were paid.
         * This is surprisingly quick!
         */
        $order_model->formulate('paid_late','(paid_on>due_on)');
        $order_model->formulate('days_late',
        '(CEIL((UNIX_TIMESTAMP(paid_on)-UNIX_TIMESTAMP(due_on))/60/60/24))');
        $Orders = $order_model->find();
        $Orders->orderBy('Order.created_on'); 
        $Orders->filter('Order.created_on','2006');       
        $Orders->filter('paid_late');
        $report = $Orders->report(
                        array('month_name'=>'MONTHNAME(orders.created_on)'),
                        array('orders_paid_late'=>'COUNT(orders.id)',
                              'average_days_late'=>'AVG(days_late)',
                              'worst_case'=>'MAX(days_late)',
                              'best_case'=>'MIN(days_late)')
                        );
        // SHould really do some SQL queries here to verify these figures
        $this->assertNotNull($report[0]['month_name']);
        $this->assertNotNull($report[0]['orders_paid_late']);
        $this->assertNotNull($report[0]['average_days_late']);
        $this->assertEqual($report[0]['worst_case'],44);
        $this->assertEqual($report[0]['best_case'],1);
    }
}

$test = new testMiCollectionReports();
$test->run(new HTMLReporter2());
