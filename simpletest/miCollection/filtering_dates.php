<?php

# simpletest
include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

class testMiCollectionFilterDate extends UnitTestCase {
    
    function __construct(){
        $this->UnitTestCase('Test miCollection Filtering Dates');
        miDb::connect(
            json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test",
                          "username" : "root", 
                          "password" : "skippy", 
                          "no_cache" : 0, 
                          "warn_slow_query" : 50 }')
                    );
    }
    
    function testDateFilter(){
    
        $order_model = new miResource(false,'Order');
        $Orders = $order_model->find();
        
        /*
         * Full date
         * Note that I use miDate::strToTime which will convert UK format
         * dd/mm/yy to US format mm/dd/yy if miDate::$flip_date_month to
         * true. Mitaji tries to do the right thing by setting this flag
         * to true if you're in a non-US locale (as I think this is the only
         * locale that uses that format) but you may want to force it.
         * It may be made a MItaji configuration option at some point.
         */
        $Orders->filter('Order.created_on','20/02/2006');
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertPattern('/^2006-02-20.+/',$Order->created_on);
        }
        
        # Before date
        $d = '20 Feb 2006';
        $ts = strtotime($d);
        $Orders->filter('Order.created_on','before '.$d);
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertTrue(strtotime($Order->created_on)<$ts,
                    "Order->created_on is before $d");
        }
        
        # After date
        $Orders->filter('Order.created_on','after '.$d);
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertTrue(strtotime($Order->created_on)>$ts,
                        "Order->created_on is after $d");
        }
        
        # Over (same as before really, but allows more natural language)
        $d = '3 years ago';
        $ts = strtotime($d);
        $Orders->filter('Order.created_on','over '.$d);
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertTrue(strtotime($Order->created_on)<$ts,
                        "$Order->created_on is over $d");
        }
        
        # Date range - separate the 2 dates with a hyphen
        $d1 = '1 Feb 2006';
        $d2 = '28 Feb 2006';
        $ts1 = strtotime($d1.' 00:00:00');
        $ts2 = strtotime($d2.' 23:59:59');        
        $Orders->unfilter('Order.created_on');
        $Orders->filter('Order.paid_on',"$d1-$d2");
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $ts_paid_on = strtotime($Order->paid_on);
            $this->assertTrue($ts_paid_on>=$ts1 && $ts_paid_on<=$ts2,
                        "Date $Order->paid_on is within range $d1 to $d2");
        }
        
        # Year implied range
        $y = '2006';
        $d1 = '1 Jan '.$y;
        $d2 = '31 Dec '.$y;
        $ts1 = strtotime($d1);
        $ts2 = strtotime($d2);        
        $Orders->filter('Order.paid_on',$y);
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $ts_paid_on = strtotime($Order->paid_on);
            $this->assertTrue($ts_paid_on>=$ts1 && $ts_paid_on<=$ts2,
                "Date $Order->paid_on is in $y (within range $d1 to $d2)");
        }
        
        /*
         * Month implied range, optional year, defaults to current
         * The database is set up with orders that were paid at the extremes
         * of each month so this should catch any bugs
         */
        $d = 'Feb 2004';
        $ts1 = strtotime('1 '.$d);
        $ts2 = miDate::lastOfMonth($ts1);
        $d1 = date('d M Y H:i:s',$ts1);
        $d2 = date('d M Y H:i:s',$ts2);
        $Orders->filter('Order.paid_on',$d);
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $ts_paid_on = strtotime($Order->paid_on);
            $this->assertTrue($ts_paid_on>=$ts1 && $ts_paid_on<=$ts2,
                "Date $Order->paid_on is in $d (within range $d1 to $d2)");
        }
        // As above but year omitted
        $d = 'May';
        $ts1 = strtotime('1 '.$d);
        $ts2 = miDate::lastOfMonth($ts1);
        $d1 = date('d M Y H:i:s',$ts1);
        $d2 = date('d M Y H:i:s',$ts2);
        $Orders->filter('paid_on',$d);
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $ts_paid_on = strtotime($Order->paid_on);
            $this->assertTrue($ts_paid_on>=$ts1 && $ts_paid_on<=$ts2,
                "Date $Order->paid_on is in $d (within range $d1 to $d2)");
        }

        // Special keyword 'Never'
        $Orders->unfilter();
        $Orders->filter('Order.paid_on','never');
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertNull($Order->paid_on);
        }
        
        // Special keyword 'Anytime'
        $Orders->filter('Order.paid_on','anytime');
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertNotNull($Order->paid_on);
        }        
    }
}


$test = new testMiCollectionFilterDate();
$test->run(new HTMLReporter2());

?>
