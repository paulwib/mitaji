<?php

# simpletest
include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

class testMiCollectionIterable extends UnitTestCase {
    
    function __construct(){
        $this->UnitTestCase('Test miCollection Iterable functions');
        miDb::connect(
            json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test",
                          "username" : "root", 
                          "password" : "skippy", 
                          "no_cache" : 0, 
                          "warn_slow_query" : 50 }')
                    );
    }
    
    function testIterable(){
        $order_model = new miResource(false,'Orders');
        $Orders = $order_model->find();
        /*
         * When you iterate over a collection the resources primary key will
         * always be returned by key()
         */
        foreach($Orders as $primary_key=>$Order){
            $this->assertEqual($primary_key,$Order->id);
        }
    }
}

$test = new testMiCollectionIterable();
$test->run(new HTMLReporter2());
