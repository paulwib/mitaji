<?php

# simpletest
include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

class testMiCollectionFilterAssociates extends UnitTestCase {
    
    function __construct(){
        $this->UnitTestCase('Test miCollection Filtering Associates');
        miDb::connect(
            json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test",
                          "username" : "root", 
                          "password" : "skippy", 
                          "no_cache" : 0, 
                          "warn_slow_query" : 50 }')
                    );
        /*
         * Mock up some models and some associations
         * For more on setting up associations see miResource tests
         */
        $this->order_model = new miResource(false,'Order');
        $this->order_model->associate('Customer.Country',
                                'Consignee.Country');
        $this->customer_model = new miResource(false,'Customer');
    }
    
    function testFilterAssociate(){

        $Orders = $this->order_model->find();
        /*
         * You can filter on associate resources just like normal filters
         * but these need to always be qualified with the model name
         * 
         * (Error suppression is because accessing Customer property
         * when there isn't a Customer class throws a warning, but it works
         * fine for our purposes)
         */
        $Orders->filter('Customer.last_name','Jo*');
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertPattern('/Jo/i',@$Order->Customer->last_name);
        }
        
        /*
         * Ambiguities can be resolved by giving full path to field you want
         * to filter on. For example here we filter on Customer.Country.name
         * and Consignee.Country.name
         */
        $Orders->unfilter();
        $Orders->filter('Customer.Country.name','United Kingdom');        
        $Orders->filter('Consignee.Country.name','Albania');
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertEqual(@$Order->Customer->Country->name,'United Kingdom');
            $this->assertEqual(@$Order->Consignee->Country->name,'Albania');
        }
    }
    
    function testFilterHasManyAssociate(){
        /*
         * Filtering on has many associates is all worked out on the fly, 
         * no need to define the associations up front.
         * As an aside, the only reason other associates are defined as front
         * is because the data can all be got in one query, but need to 
         * specify what asociates you want to get.
         */ 
        $Customers = $this->customer_model->find();
        /*
         * Find customers who have unpaid orders.
         * The first filter makes sure they ahve at least one order and the
         * second one checks if any are late.
         * Without the first filter this would return customers who have no
         * orders at all!
         */
        $Customers->filter('Orders.id','NOT NULL');
        $Customers->filter('Orders.paid_on','never');
        $this->assertTrue($Customers->count());
        /*
         * The orders haven't actually been retrieved at this stage, but when
         * iterating over the customers I can confirm they have unpaid orders.
         * (For more on has many associations see Associates tests)
         */
        foreach($Customers as $Customer){
            $qry = "SELECT * FROM orders WHERE paid_on IS NULL AND customer_id=$Customer->id";
            $this->assertTrue(miDb::getResults($qry));
        }
        
        /*
         * This works even with through associations. For example, Orders have many
         * Products through OrdersProducts.
         * (see tests on Associates for more info on through associations)
         * These kind of filters can be kinda slow. Could be optimized by 
         * disassociating the model from other associates. Also you can look 
         * in miDb::queryLog to see the queries and right special functions
         * that optimize this. 
         */
        $Orders = $this->order_model->find();
        // Find all orders that contain a product worth more than 10 quid
        $Orders->filter('Products.retail_price','>10');
        $this->assertTrue($Orders->count());
        foreach($Orders as $id=>$Order){
            $qry = "SELECT products.retail_price 
                    FROM orders_products LEFT JOIN products 
                    ON orders_products.product_id=products.id 
                    WHERE orders_products.order_id=$id 
                    AND products.retail_price > 10";
            $this->assertTrue(miDb::getResults($qry));
        }
        $Orders->unfilter();
        /*
         * You can go pretty darn crazy with this kind of stuff. For example,
         * say a supplier has gone bust and you need to find all unpaid
         * orders for product from that supplier which contain high value
         * products. Yeah you can write a special query for this, or you 
         * can write three lines of code like this...
         */
        $Orders->filter('paid_on','never');
        $Orders->filter('Products.retail_price','>20');
        $Orders->filter('Products.Supplier.company','=Thirsty Dream');
        $this->assertTrue($Orders->count());
        foreach($Orders as $id=>$Order){
            $qry = "SELECT suppliers.company
                    FROM orders_products LEFT JOIN products 
                    ON orders_products.product_id=products.id 
                    LEFT JOIN suppliers ON 
                    products.supplier_id=suppliers.id 
                    WHERE orders_products.order_id=$id 
                    AND products.retail_price > 20
                    AND suppliers.company = 'Thirsty Dream'";
            $this->assertTrue(miDb::getResults($qry));
        }
        /*
         * For fun, restrict to orders going to customers in South London
         * THIS TEST NOT WORKING WITH THE CURRENT TEST DATABASE
         */
        $search_postcode_patterns = array('SE*','SW*');
        $Orders->filter('Customer.postcode',$search_postcode_patterns);
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $found = false;
            foreach($search_postcode_patterns as $p){
                if(stristr(@$Order->Customer->postcode,trim($p,'*'))){
                    $found = $p;
                    break;
                }
            }
            $this->assertTrue($found,"Found pattern '$p' in $Customer->postcode");
        }
        /*
         * You can remove filters and it should intelligently work out
         * what joins can be discarded and what can't. Let's remove the filter
         * on the Customer.postcode and Product.retail_price, but leave 
         * in Product.Supplier.company
         */
        $Orders->unfilter('Customer.postcode');
        $Orders->unfilter('Products.retail_price');
        $this->assertTrue($Orders->count());
        foreach($Orders as $id=>$Order){
            $qry = "SELECT suppliers.company
                    FROM orders_products LEFT JOIN products 
                    ON orders_products.product_id=products.id 
                    LEFT JOIN suppliers ON 
                    products.supplier_id=suppliers.id 
                    WHERE orders_products.order_id=$id 
                    AND suppliers.company = 'Thirsty Dream'";
            $this->assertTrue(miDb::getResults($qry));
        }
    }
}

$test = new testMiCollectionFilterAssociates();
$test->run(new HTMLReporter2());

?>
