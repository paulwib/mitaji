<?php

# simpletest
include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

class testMiCollectionFilterGroupConcat extends UnitTestCase {
    
    function __construct(){
        $this->UnitTestCase('Filter Group Concat (miCollection Tests)');
        miDb::connect(
            json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test",
                          "username" : "root", 
                          "password" : "skippy", 
                          "no_cache" : 0, 
                          "warn_slow_query" : 50 }')
                    );
    }    
    
    function testFilterGroupConcat(){
        
        /*
         * There are some special things you can do if you set up a GROUP
         * CONCAT based formula.
         * Here's an example where we use a GROUP CONCAT to get all the 
         * products in an order. 
         * 
         * As an aside, this is a quite neat way to collect
         * one field from multiple has many associates all in one field of
         * the parent, although could be very slow if your order contain 
         * a lot of products.
         * 
         * (This actually does not even need to be a subselect, except it does
         * require the additional JOINs. Maybe could look at improving that?)
         */
        $order_model = new miResource(false,'Orders');
        $order_model->formulate('products','(SELECT 
                                GROUP_CONCAT(DISTINCT products.description 
                                ORDER BY products.description SEPARATOR ", ") 
                                FROM products LEFT JOIN orders_products 
                                ON orders_products.product_id=products.id 
                                WHERE orders_products.order_id=orders.id)',
                        'string');
        
        // You can do a simple filter on the group concat result as a string
        $Orders = $order_model->find();
        $Orders->pageSize(3);
        $Orders->filter('products','Baseball Cap');
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $this->assertPattern('/Baseball Cap/i',$Order->products);
        }
        
        /*
         * Or you can use the special filterGroupCol
         * This first example will match orders that contain ANY of the 
         * product descriptions.
         * (This is probably better accomplished by just doing an 'in' filter
         * on Products.description, see filtering associates, but may be a 
         * better alterantive in some scenarios. One bonus is that you'll get 
         * back the matched values 
         */
        
        $Orders->unfilter();
        $products_to_find = array('Rude Snood','Winter Veil');
        $Orders->filterGroupConcat('products.description', $products_to_find);
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $order_products = explode(', ',$Order->products);
            $intersections = array_intersect($products_to_find,$order_products);
            $this->assertTrue($intersections);
            if(count($intersections) == 1){
                $this->assertTrue(true, 'One product matched');
            } else if(count($intersections) == 2){
                $this->assertTrue(true, 'Two products matched');
            } else {
                $this->assertTrue(false,'Weird number of intersections');
            }
        }
        /*
         * But perhaps the most compelling use case is to find a resource that
         * has ALL of the specified has many associates. I use the Orders
         * model below, but perhaps a better example would be to find all
         * Articles that has all the Tags in a group (not just any of them).
         * 
         * To match all set the third argument of filterGroupCol() to true.
         */
        $Orders->unfilter();
        $Orders->filterGroupConcat('products.description', $products_to_find, true);
        $this->assertTrue($Orders->count());
        foreach($Orders as $Order){
            $order_products = explode(', ',$Order->products);            
            $intersections = array_intersect($products_to_find,$order_products);
            $this->assertEqual($products_to_find,$intersections, 'Order contains *ALL* of the expected products');
        }
    }

}

$test = new testMiCollectionFilterGroupConcat();
$test->run(new HTMLReporter2());







