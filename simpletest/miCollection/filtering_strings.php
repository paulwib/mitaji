<?php

# simpletest
include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

class testMiCollectionFilterString extends UnitTestCase {
    
    function __construct(){
        $this->UnitTestCase('Test miCollection Filtering Strings');
        miDb::connect(
            json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test",
                          "username" : "root", 
                          "password" : "skippy", 
                          "no_cache" : 0, 
                          "warn_slow_query" : 50 }')
                    );
    }
    
    function testStringFilter(){
        
        $customer_model = new miResource(false,'Customer');
        $Customers = $customer_model->find();
        
        // A general match for a string ends up as LIKE '%string%'
        $Customers->filter('Customer.address','road');
        $this->assertTrue($Customers->count());
        foreach($Customers as $Customer){
            $this->assertPattern('/road/i',$Customer->address);
        }
        
        // Wildcard to begin with a string
        $Customers->unfilter();
        $Customers->filter('Customer.town','B*');
        $this->assertTrue($Customers->count());
        foreach($Customers as $Customer){
            $this->assertPattern('/^B/',$Customer->town);
        }
        
        // Wildcard to end with a string
        $Customers->unfilter();
        $Customers->filter('Customer.last_name','*son');
        $this->assertTrue($Customers->count());
        foreach($Customers as $Customer){
            $this->assertPattern('/son$/',$Customer->last_name);
        }
        /*
         * Remove all those address fields or won't get matches below as 
         * too tightly contrained.
         */
        $Customers->unfilter();
        
        // Match ANY of several values with `in value_1, value_2 ...`
        $postcodes = array('SW13 7XM', 'SW15 7ME');
        $Customers->filter('Customer.postcode','in '.implode(',',$postcodes));
        $this->assertTrue($Customers->count());
        foreach($Customers as $Customer){
            $this->assertTrue(in_array($Customer->postcode,$postcodes),'Postcode found');
        }
        
        // Match NONE of several values with `not in value_1, value_2 ...`
        $Customers->filter('Customer.postcode','not in '.implode(',',$postcodes));
        $this->assertTrue($Customers->count());
        foreach($Customers as $Customer){
            $this->assertFalse(in_array($Customer->postcode,$postcodes),'Postcode NOT found');
        }
        
        // Match EXACTLY one value with `= value`
        $Customers->filter('Customer.postcode','='.$postcodes[0]);
        $this->assertTrue($Customers->count());
        foreach($Customers as $Customer){
            $this->assertEqual($Customer->postcode,'SW13 7XM');
        }
        
        /* 
         * You can compare several fields to one value by passing an array
         * of fields and value. Internally these are joined together with
         * OR rather than AND.
         * (This isn't specific to filtering on strings, you can do it for any
         * type of filter, but probably most appropriate for strings)
         */
        $address_fields = array('address','town','postcode');
        $search_address_str = 'ta';
        $Customers->filter($address_fields,$search_address_str);
        $this->assertTrue($Customers->count());
        // Check string is present in at least one of the fields
        foreach($Customers as $Customer){
            $found = false;
            foreach($address_fields as $f){
                if(stristr($Customer->$f,$search_address_str)){
                    $found = $f;
                    break;
                }
            }
            // Highlight the match!
            $match = str_ireplace($search_address_str,
                                    '<b style="background:yellow;">'.
                                        strtoupper($search_address_str).
                                    '</b>',
                                    $Customer->$f);
            $this->assertTrue($found,"Found string '$search_address_str' in $f ($match)");
        }
        /*
         * You can also compare on field to multiple values. This is 
         * more flexible than an IN type filter as you can compare to
         * multiple patterns, for example find all customers in
         * South London by filltering on postcodes beginning SW and SE.
         * 
         * (Again not specific to strings)
         */
        $Customers->unfilter();
        $search_postcode_patterns = array('SE*','SW*');
        $Customers->filter('postcode',$search_postcode_patterns);
        $this->assertTrue($Customers->count());
        foreach($Customers as $Customer){
            $found = false;
            foreach($search_postcode_patterns as $p){
                if(stristr($Customer->postcode,trim($p,'*'))){
                    $found = $p;
                    break;
                }
            }
            $this->assertTrue($found,"Found pattern '$p' in $Customer->postcode");
        }
    }
}

$test = new testMiCollectionFilterString();
$test->run(new HTMLReporter2());

?>
