<?php

include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');
include_once('models/miJson.php');

class miJsonTest extends UnitTestCase {
    
    function __construct(){
        $this->UnitTestCase('Test miJson Database');
        file_put_contents('mijsons.json','[
        {"id":"54","name":"Steve","created_on":"2008-07-09"},
        {"id":"55","name":"Mike","created_on":"2008-07-10"}
        ]');
    }
    
    function testCrud(){
        
        // Find a collection
        $resources = miJson::find();
        $this->assertTrue($resources);
        $this->assertEqual($resources->count(),2);
        
        // The key will be the id of the resource
        foreach($resources as $k=>$resource){
            $this->assertEqual($k, $resource->id);
        }
        $this->assertEqual($resources->id, array(54,55));
        
        // Find a single resource
        $resource = miJson::findOne(array('id'=>54));
        $this->assertEqual($resource->id,54);
        
        // Iterate over it's properties
        foreach($resource as $k=>$v){
            $this->assertEqual($resource->$k,$v);
        }
        
        // Create a new resource
        $resource = new miJson(array('name'=>'Bob',
                                'created_on'=>date('Y-m-d')));
        $this->assertTrue($id = $resource->save());
        
        // Check it was saved
        $resources = miJson::find();
        $this->assertEqual($resources->count(),3);
        $resource2 = miJson::findOne(array('id'=>$id));
        $this->assertFalse(is_array($resource2->id));
        foreach($resource2 as $k=>$v){
            $this->assertEqual($v,$resource->$k);
        }
        
        // Change a property and re-save
        $resource2->name = 'Dave';
        $resource2->save();
        
        // Check the count is the same i.e. not saved as a new resource
        $resources = miJson::find();
        $this->assertEqual($resources->count(),3);
        
        // Also possible to change all members of a collection at once
        $resources->type = 'contact';
        $resources->save();
        
        // If you have a collection then you can't save individual members
        foreach($resources as $resource){
            if(in_array($resource->name, array('Steve','Mike'))){
                $resource->type = 'contact friend';
            } else {
                $resource->type = 'contact enemy';
            }
            // Resource does not have save method, collection does
            //$resource->save();
        }        
        // But what you can do is save them at the end
        $this->assertTrue($resources->save());
        
        // Similarly you can call delete on a collection or an individual
        $resources->filter(array('name'=>'Dave'));
        $this->assertTrue($resources->delete());
        $resources = miJson::find();
        $this->assertEqual($resources->count(),2);
        
        // Returns false if not found
        $this->assertFalse(miJson::findOne(array('name'=>'Joan')));
        
        // Also when you call save you can add a load of new data at once
        // Works for collections or individual resources
        $this->assertTrue($resource = miJson::findOne(array('name'=>'Steve')));
        $this->assertTrue($resource->save(array('name'=>'Stevie','job'=>'Chef')));
        $this->assertTrue($resource = miJson::findOne(array('job'=>'Chef')));
        $this->assertEqual($resource->name,'Stevie');
    }
// --------------------------------------------------------------------------
    function testInheritedCrud(){
        
        @unlink('products.json');
        $Product = new Product();
        $Product->description = 'Wooly Hat';
        $Product->price = 15.95;
        $this->assertTrue($Product->save());
        
        $Product = new Product();
        $Product->description = 'Scarf';
        $Product->price = 10.95;
        $this->assertTrue($Product->save());
        
        $Products = Product::find();
        $this->assertEqual($Products->count(), 2);
        // Check we got the right class
        $this->assertEqual(get_class($Products), 'Product');
        
        // Just check it has the right sort of proerties
        foreach($Products as $Product){
            $this->assertTrue($Product->id);
            $this->assertTrue($Product->price);
            $this->assertTrue($Product->description);
            $this->assertNull($Product->foo);
        }
        // Check findOne
        $Product2 = Product::findOne(array('id'=>$Product->id));
        $this->assertEqual($Product2->id,$Product->id);
        
        // Add multiple things to a collection, save once
        $Products = Product::find();
        $data = array(array('description'=>'Gloves','price'=>25.00),
                      array('description'=>'Coat','price'=>75.00),
                      array('description'=>'Boots','price'=>175.00));
        foreach($data as $d){
            $Products->add($d);
        }
        $Products->save();
        $Products = Product::find();
        $this->assertEqual($Products->count(), 5);
        unlink('products.json');
    }
}

// --------------------------------------------------------------------------
/**
 * A demonstration class that starts with no file.
 * Has to have it's own calls to find so it makes sure the right class is
 * used when static find and findOne methods are called.
 */
class Product extends miJson {
    static function find($filter=false){
        return parent::find($filter, __CLASS__);
    }
    static function findOne($filter=false){
        return parent::findOne($filter, __CLASS__);
    }
}

$test = new miJsonTest();
$test->run(new HTMLReporter2());
