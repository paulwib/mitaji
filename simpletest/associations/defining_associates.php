<?php

include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

class testDefiningAssociates extends UnitTestCase {
    
    function __construct(){
        $this->UnitTestCase('Test Associations ');
        miDb::connect(
            json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test",
                          "username" : "root", 
                          "password" : "skippy", 
                          "no_cache" : 0, 
                          "warn_slow_query" : 0 }')
                    );
    }
    
    function testDefining(){
        
        // This is a mock model, normally you create a subclass of miResource
        $order_model = new miResource(false,'Orders');        
        /*
         * Associates are defined by calling the models `associate()` method.
         * 
         * This can be done in the model constructor to avoid having to 
         * repeat it everywhere. It's done on a mock her just for testing
         * purposes.
         * 
         * No differentiation is made between has one and belongs to, this
         * will all be inferred from the table structure. But you do not
         * define has many associates like this. In fact you don't define 
         * has many associates at all (see below)
         * 
         * (The suppression of errors in tests below is because you get a 
         * warning about the classes being missing as we're doing everything
         * with mocks)
         */
        $order_model->associate('Customer');
        $Orders = new miCollection($order_model);
        $Order = $Orders->getFirst();
        $this->assertTrue(@$Order->Customer);
        
        /* 
         * The associates further one-to-one associates are not looked up
         * automagically as that could lead to retrieval of unneeded detail.
         * Instead associations can be chained together with dots (.).
         * 
         * It does not matter if the association is a has one or a belongs to,
         * this will be worked out automatically based on the table structure.
         * 
         * In this case they are belongs to relations, examples of has one to
         * be added.
         */
        $order_model->associate('Customer.Country');
        $Orders = new miCollection($order_model);
        $Order = $Orders->getFirst();
        $this->assertTrue(@$Order->Customer->Country);
        /*
         * You can safely access the same table in different one-to-one 
         * relationships.
         */
        $order_model->associate('Consignee.Country');
        $Orders = new miCollection($order_model);
        // Make sure we get orders with a consignee! (More in filter tests) 
        $Orders->filter('consignee_id','!0'); 
        $Order = $Orders->getFirst();
        $this->assertTrue(@$Order->Customer->Country);
        $this->assertTrue(@$Order->Consignee->Country);
        /* 
         * An associate can be linked more than once by using aliases e.g.
         * if you store a user id of a resource's creator and updater you can
         * add fields `creator_id` and `updater_id` then link twice to `Users`
         * by placing the alias name in brackets.
         * 
         * This also shows how you can add multiple associations at once by 
         * passing multiple arguments to `associate()`.
         */
        $order_model->associate('User(Creator).Country','User(Updater).Country');
        $Orders = new miCollection($order_model);
        $Order = $Orders->getFirst();
        $this->assertTrue(@$Order->Creator->Country);
        $this->assertTrue(@$Order->Updater->Country);
        
        /*
         * You can also `disassociate()` on the fly, here for example I remove
         * the association to the Creator
         */
        $order_model->disassociate('User(Creator)');
        $Orders = new miCollection($order_model);
        $Order = $Orders->getFirst();
        $this->assertNull(@$Order->Creator);
    }
    
    function testHasMany(){
        // This is a mock model, normally you create a subclass of miResource
        $customer_model = new miResource(false,'Customers');  
        /*
         * You don't actually define has many associates - there's no need
         * because they are not got up front (unless you filter on them
         * in which case some joins will be added automatically, but still it
         * doesn't get the resources up front)
         * 
         * Instead no queries will be executed until you actually attempt to
         * access the has many associate. You access them as a property
         * which is named after the plural of the related model e.g.
         * $resource->{has_many_model_in_plural}
         * This means you have to have your foreign keys set up correctly, no
         * way to override this.
         * In this first example we access the many orders of a customer, so
         * the `orders` table must have the foreign key `customer_id`
         */
        $Customers = $customer_model->find();
        foreach($Customers as $customer_id=>$Customer){
            $this->assertTrue(@$Customer->Orders);
            foreach($Customer->Orders as $Order){
                // Check orders belong to customer
                $this->assertEqual($Order->customer_id,$customer_id);
            }
        }
        /*
         * The has many property is an instance of miCollection so you can
         * filter it like any collection (see miCollection tests for more 
         * examples of filtering)
         */
        foreach($Customers as $customer_id=>$Customer){
            $Customer->Orders->filter('paid_on','anytime');
            foreach($Customer->Orders as $Order){
                // Check orders belong to customer and has not been paid
                $this->assertEqual($Order->customer_id,$customer_id);
                $this->assertNotNull($Order->paid_on);
            }
        }
        /*
         * A resource can also have many of something else through another
         * table. This is how many-to-many relationships work. The test
         * database has `orders` and `products` and the order are related
         * to products through the `orders_products` table , which also
         * stores the quantity ordered, the price paid and the date shipped.
         * 
         * Again there is no need to define anything up front, all these
         * associations will be detected automatically when you access the
         * property.
         * 
         * One query will be executed for accessing one property.
         */
        
        // This is a mock model, normally you create a subclass of miResource
        $order_model = new miResource(false,'Orders');
        $Orders = $order_model->find();
        $Orders->pageSize(5);
        $this->assertTrue($Orders->count());
        $qc = miDb::queryCount();
        foreach($Orders as $order_id=>$Order){
            // All orders have products so check the count
            $this->assertTrue(@$Order->Products->count());
            foreach($Order->Products as $Product){
                $this->assertEqual($Product->order_id,$order_id);
            }
        }
        // Five additional queries
        $this->assertEqual(miDb::queryCount(),$qc+5);
        
        /*
         * You can also access all the has many relations for a collection
         * by using the same technique on the collection e.g.
         * $collection->{has_many_model_in_plural}
         * Note this just return all products for all orders in one collection.
         * This can be quite useful when reporting (see tests on Reports)
         */
        $Orders = $order_model->find();
        $order_ids = miArray::extractByKey($Orders,'id');// Get these to check against
        foreach(@$Orders->Products as $Product){
            $this->assertTrue(in_array($Product->order_id,$order_ids));
        }
        /*
         * You can also get all has many associates for a collection and
         * have them automatically attached with the collate function e.g.
         * $collection->collate{has_many_model_in_plural}
         * This has the same end result as the first example, but will use far
         * less queries. However not always appropriate. Imagine your orders 
         * contain thousands of products each, and you just want to show the
         * first 5 with the order listing. It would make more sense to access
         * each property individually and load up the first 5, rather than 
         * load thousands up front, which you're not going to show anyway.
         * On the other hand, if your orders normally only contain a few
         * products this technique might be much more efficient.
         */
        $order_model = new miResource(false,'Orders');
        $Orders = $order_model->find();
        $Orders->pageSize(5);
        $this->assertTrue($Orders->count());
        
        $qc = miDb::queryCount();
        @$Orders->collateProducts();
        foreach($Orders as $order_id=>$Order){
            // All orders have products so check the count
            $this->assertTrue(@$Order->Products->count());
            foreach($Order->Products as $Product){
                $this->assertEqual($Product->order_id,$order_id);
            }
        }
        // Just one additional query
        $this->assertEqual(miDb::queryCount(),$qc+1);
    }
}

$test = new testDefiningAssociates();
$test->run(new HTMLReporter2());

?>
