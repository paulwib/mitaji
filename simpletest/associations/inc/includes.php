<?php

# simpletest
include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');

# Essential helpers
include_once('../../../helpers/AkInflector.php');
include_once('../../../helpers/miDate.php');
include_once('../../miDb.php');

# Testing
include_once('../../miCollection.php');
include_once('../../miResource.php');
?>
