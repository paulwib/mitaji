<?php



# simpletest
include_once('simpletest/unit_tester.php');
include_once('simpletest/htmlreporter2.php');


class testmiDb extends UnitTestCase {
    
    function __construct(){
        $this->UnitTestCase('Test miDb');
                miDb::connect(json_decode('{"dsn" : "mysql:host=localhost;dbname=mitaji_test","username" : "root","password" : "skippy","no_cache" : true}'));
    }
    
    function testQuery(){
        $var = miDb::getVar('SELECT customer_id FROM orders WHERE id=250;');
        $this->assertEqual($var,279,'Query: Got var');
        $row = miDb::getRow('SELECT * FROM orders WHERE id=250;');
        $this->assertEqual($row['customer_id'],279,'Query: Got row');
        $col = miDb::getCol('SELECT id FROM orders ORDER BY id LIMIT 0,10;');
        $this->assertEqual($col[0],12,'Query: Got col');
        $results = miDb::getResults('SELECT * FROM users ORDER BY id;');
        $this->assertEqual($results[0]['id'],1,'Query: Got result set');
    }
    
    function testTableInfo(){
        $tbl = miDb::tableInfo('orders');
        $this->assertTrue($tbl['customer_id']['required'],'TableInfo: Order customer_id is required');
    }
    
    function testPrimaryCol(){
        # Get the primary col
        $this->assertEqual(miDb::primaryKey('orders'),'id');
        
        # OK if on multiple columns
        $this->assertEqual(miDb::primaryKey('orders_products'),array('order_id','product_id'));
    }
    
    function testPerformance(){
        # When doing something like getting has many is there a really a performance boost
        # by doing it one query, or does the fact that miDb will use prepared queries
        # mean repeating for each thing is equally good?
        $i = 0;
        $params = array();
        while($i++ < 500){
            $params[':id_'.$i] = $i;
        }
        $base_qry = 'SELECT * FROM orders_products LEFT JOIN products ON orders_products.product_id=products.id ';
        $qry1 = $base_qry." WHERE order_id IN (".implode(',',array_keys($params)).")";
        miDb::getResults($qry1,$params);
        $t1 = miDb::lastQuery('milliseconds');
        $qry2 = $base_qry." WHERE order_id=:p";
        $t2 = 0;
        foreach($params as $k=>$v){
            miDb::getResults($qry2,array(':p'=>$v));
            $t2 += miDb::lastQuery('milliseconds');
        }
        $this->assertTrue($t2 > $t1,'One big query faster than many small queries');
        
        # It looks like there is very little in it for the table and data I'm using 
        # Occasionally the big query is actually slower
    }
    
}

$test = new testmiDb();
$test->run(new HTMLReporter2());





