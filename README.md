# Mitaji

Mitaji is a web application framework with an ORM, routers and views.

**WARNING: While this code is occasionally maintained, it is no longer being actively developed. Use at your own risk!**

## Command Line

There is a command line tools for helping with things like DB migrations. This should be run in the context of your application which has the mitaji dependency, like:

```
$ php vendor/bin/mitaji.php --help
```

Command line functions can be added for your own app using a similar approach. The basic idea is to configure your own options, then include the cli bootstrap script like:

```
// Make sure autoloader is present
require __DIR__ . '/../vendor/autoload.php';

// Set some options BEFORE requiring Mitaji's CLI bootstrap
$short_opts = 'm';
$long_opts = array('myopt=');
$normalize_opts = array('m'=>'myopt');

// Load the bootstrap
require __DIR__ . '/../vendor/mitaji/mitaji/bootstrap/cli2.php';

// Now should see passed options and can do whatever you need
print_r($_ENV['OPTS']);

// Will also parse any arguments (i.e. positional args without `--` flag)
print_r($_ENV['ARGS']);
```

You may want to do some additional wrapping of these scripts to do things like setup import paths or other application specific configuration.

## Tests

Depends on having `make` and `docker` installed:

```
$ make test
```

Note there are a bunch of unrunnable legacy tests in the `simpletest` directory that need to be migrated.
