<?php

/**
 * Run mitaji command-line methods, like:
 *
 *  php vendor/mitaji.php --help
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE);

$short_opts = 'h';
$long_opts = array('help');
$normalize_opts = array('h'=>'help');

// Run it from the root of your app to get local bootstrapping
require_once getcwd() . '/bootstrap/cli2.php';

// Add CLI paths so --help is able to list commands
$cli_paths = realpath(__DIR__ . '/../src/cli/');

// Start Mitaji
$m = new Mitaji($cli_paths);
$m->cliRun($_ENV['OPTS'], $_ENV['ARGS']);
