<?php

class miCollection implements Iterator {


    protected $page = 1;
    protected $page_size = 10;
    protected $resources_total = 0;
    protected $resources = null;
    protected $truncate_in_filter_to_page_size = true;
    protected $select_from_for_has_many_filter = array();
    protected $filters = array();
    protected $child_collections = array();
    protected $stop_filters = false;

    public $order_by = false;
    public $model = false;
    public $primary_key = false;
    public $tables_used = array();
    public $tables_aliased = array();
    public $select_cols = array();
    public $select_from = array();
    public $select_where = array();
    public $select_params = array();
    public $select_param_counter = 0;
    public $applied_filters = array();

    //----------------------------------------------------------------------------
    function __construct($model, $resources=false) {
        $this->model = $model;
        $this->primary_key = miDb::primaryKey($model->primary_table);
        $table_info = miDb::tableInfo($model->primary_table);
        $primary_fields = array_keys($table_info);
        foreach($primary_fields as $pf){
            $this->select_cols[$model->primary_table.'.'.$pf] = $pf;
        }
        /*
         * If a through table then also need to make sure those columns are
         * selected and you select from the through table then JOIN to the
         * primary table
         * This will only ever be set by a resource looking for has_many
         * relatives, do *not* set this in your own models
         */
        if($through_table = @$model->through_table){
            $table_info = miDb::tableInfo($through_table);
            $through_fields = array_keys($table_info);
            foreach($through_fields as $tf){
                /*
                 * If this table does have an id column then ignore it
                 * or it will overwrite the id of the thing you really want
                 */
                if($tf == 'id'){
                    continue;
                }
                $this->select_cols[$model->through_table.'.'.$tf] = $tf;
            }
            $foreign_col = AkInflector::singularize($model->primary_table).'_id';
            $this->select_from[] = $model->through_table;
            $this->select_from[] = "LEFT JOIN $model->primary_table
                ON $model->through_table.$foreign_col=$model->primary_table.id";
            // Need to add both to tables used to prevent aditional joins
            $this->tables_used[] = $model->through_table;
            $this->tables_used[] = $model->primary_table;

            // Join to associates of through model if there are any
            $through_class = AkInflector::modulize($through_table);
            $through_model = new $through_class();
            if($assoc = $through_model->associates()){
                foreach($assoc as $a){
                    $this->join($a,$through_table);
                }
            }
            // And get their formulas
            if($formulas = $through_model->formulas()){
                foreach($formulas as $k=>$f){
                    $model->formulate($k,$f);
                }
            }
        } else {
            $this->select_from[] = $model->primary_table;
        }
        $this->tables_used[] = $model->primary_table;

        // Use models associations to set up joins
        if($assoc = $model->associates()){
            foreach($assoc as $a){
                $this->join($a);
            }
        }
        // Use models default order by
        if($model->order_by){
            $this->orderBy($model->order_by);
        }
        if($resources){
            $this->setResources($resources);
        }
    }
    //----------------------------------------------------------------------------
    function setResources($resources){
        $this->resources = $resources;
    }
    //----------------------------------------------------------------------------
    function getData(){
        $data = array();
        foreach($this as $resource){
            $data[] = $resource->getData();
        }
        return $data;
    }
    //----------------------------------------------------------------------------
    // Iterator methods
    //----------------------------------------------------------------------------
    function rewind() {
        if($this->resources){
            reset($this->resources);
        }
    }
    //----------------------------------------------------------------------------
    function current() {
        if(is_null($this->resources)) {
            $this->find();
        }
        if($this->resources === false){
            return false;
        }
        return current($this->resources);
    }
    //----------------------------------------------------------------------------
    function key() {
        $resource = $this->current();
        return $resource->primaryKey();
    }
    //----------------------------------------------------------------------------
    function next() {
        return next($this->resources);
    }
    //----------------------------------------------------------------------------
    function valid() {
        return $this->current() !== false;
    }
    //----------------------------------------------------------------------------
    function get($index){
        if(is_null($this->resources)) {
            $this->find();
        }
        if($this->resources === false){
            return false;
        }
        return $this->resources[$index];
    }
    //----------------------------------------------------------------------------
    function getFirst(){
        if(is_null($this->resources)) {
            $this->find();
        }
        if(!$this->resources){
            return false;
        }
        $values = array_values($this->resources);
        if(!$values){
            return false;
        }
        return $values[0];
    }
    //----------------------------------------------------------------------------
    function count(){
        if(is_null($this->resources)) {
            $this->find();
        }
        return $this->resources == false ? false : count($this->resources);
    }
    //----------------------------------------------------------------------------
    function resetResources(){
        $this->resources_total = 0;
        $this->resources = null;
    }
    //----------------------------------------------------------------------------
    /**
     * Update all items in a collection with the same bits of data
     * @param array $data The data to add/update for all
     * @return bool True on success or false if failure. If any single update goes
     * wrong the update will fail and database changes will be rolled back.
     */
    function update($data){
        if(!is_array($data)){
            return false;
        }
        if(is_null($this->resources)) {
            $this->find();
        }
        if(!$this->resources){
            return false;
        }
        miDb::beginTransaction();
        $success = true;
        foreach($this->resources as $Resource){
            if(!$Resource->update($data)){
                $success = false;
            }
        }
        if($success){
            miDb::commit();
            return true;
        }
        miDb::rollback();
        return false;
    }
    //----------------------------------------------------------------------------
    /**
     * Get all unique errors for items in a collection (useful after a
     * failed update)
     */
    function errors(){
        if(!$this->resources){
            return false;
        }
        $tmp = array();
        foreach($this->resources as $Resource){
            if($errors = $Resource->errors()){
                foreach($errors as $e){
                    $tmp[] = $e;
                }
            }
        }
        if(!$tmp){
            return false;
        }
        return array_unique($tmp);
    }
    //----------------------------------------------------------------------------
    // Page control and information
    //----------------------------------------------------------------------------
    function orderById($ids){
        $this->order_by = 'FIELD('.$this->model->primary_table.'.id,'.implode(',',$ids).')';
    }
    //----------------------------------------------------------------------------
    function orderBy($ord){
        // If trying to order by a particular field then leave it as is
        if(miStr::beginsWith($ord,'FIELD')){
            $this->order_by = $ord;
            return $this;
        }
        // Check for an alias, but remove direction first
        $tmp = trim(preg_replace('/(asc|desc)$/','',strtolower($ord)));
        if($alias_ord = @$this->model->order_by_alias[$tmp]){
            $ord = str_replace($tmp,$alias_ord,$ord);
        }
        if($ord){
            $order_by = array();
            $tokens = explode(',',$ord);
            foreach($tokens as $t){
                $order_by[] = str_replace('.', '$', $t);
                continue;
                $t = trim($t);
                if(strpos($t,'.') === false){
                    $order_by[] = $t;
                } else {
                    $tmp = explode('.',$t);
                    /*
                     * If more distant than one table make sure table aliased
                     * correctly (should really be a consistent method for this)
                     */
                    if(count($tmp) > 2){
                        $col = array_pop($tmp);
                        $tmp2 = array();
                        foreach($tmp as $model){
                            $tmp2[] = AkInflector::tableize($model);
                        }
                        $tbl = implode('__',$tmp2);
                    } else {
                        list($model,$col) = $tmp;
                        $tbl = AkInflector::tableize($model);
                    }
                    $order_by[] = $tbl.'.'.$col;
                }
            }
            $this->order_by = implode(',',$order_by);
        }
        return $this;
    }
    //----------------------------------------------------------------------------
    function getOrderBy(){
        return $this->order_by;
    }
    //----------------------------------------------------------------------------
    function alias2realcol($col) {
        $a = str_replace('.', '$', $col);
        return array_search($a, $this->select_cols);
    }
    //----------------------------------------------------------------------------
    function page($p=null){
        if(!is_null($p) && $p >= 1){
            $this->resetResources();
            $this->page = $p;
        }
        return $this;
    }
    //----------------------------------------------------------------------------
    function getPage(){
        return $this->page;
    }
    //----------------------------------------------------------------------------
    // Set the page size to 0 to have unlimited results
    function pageSize($ps){
        if($ps != $this->page_size){
            $this->resetResources();
            $this->page_size = $ps;
        }
        return $this;
    }
    //----------------------------------------------------------------------------
    function getPageSize(){
        return $this->page_size;
    }
    //----------------------------------------------------------------------------
    function pagesTotal(){
        if(is_null($this->resources)) {
            $this->find();
        }
        if($this->resources === false){
            return false;
        }
        $ps = $this->getPageSize();
        $rt = $this->resourcesTotal();
        if($ps > 0 && $rt > $ps){
            return ceil($rt/$ps);
        }
        return 1;
    }
    //----------------------------------------------------------------------------
    function resourcesTotal(){
        if(is_null($this->resources)) {
            $this->find();
        }
        return $this->resources_total;
        return $this->resources_total == false ? false :
            $this->resources_total;
    }
    //----------------------------------------------------------------------------
    function resourcesStart(){
        if(is_null($this->resources)) {
            $this->find();
        }
        return $this->resources === false ? false :
            (($this->page - 1) * $this->page_size) + 1;
    }
    //----------------------------------------------------------------------------
    function resourcesEnd(){
        if(is_null($this->resources)) $this->find();
        return $this->resources === false ? false :
            $this->resourcesStart() + count($this->resources) - 1;
    }
    //----------------------------------------------------------------------------
    /**
     * Sort conditions so any subselects come last - this seems to make
     * the query faster
     */
    protected function sortSelectWhere($a,$b){
        if(strpos($b,'SELECT') !== false){
            return -1;
        }
        return strlen($a) < strlen($b) ? -1 : 1;
    }
    //----------------------------------------------------------------------------
    function find($ids=false,$return_qry=false) {
        // Select
        $select = array();
        foreach($this->select_cols as $col => $alias) {
            $select[] = "$col AS $alias";
        }
        if($formulas = $this->model->formulas()){
            foreach($formulas as $alias=>$formula){
                $select[] = "$formula AS $alias";
            }
        }
        $select = implode(",\n",$select);
        // From
        $from = implode("\n", $this->select_from);
        // If not trying to find specific ids add filters
        if(!$ids){
            if($this->select_from_for_has_many_filter){
                $all_joins = array();
                foreach($this->select_from_for_has_many_filter as $filter_id=>$joins){
                    $all_joins = array_merge($all_joins, $joins);
                }
                $from .= "\n ".implode("\n ", array_unique($all_joins));
                $group_by = $this->model->primary_table.'.id';
            }

            // Where
            if($this->select_where){
                uasort($this->select_where,array($this,'sortSelectWhere'));
                $where = implode("\n AND \n",$this->select_where);
            } else {
                $where = 1;
            }
        } else if(is_array($ids)){
            $where = $this->model->primary_table.'.id IN ('.implode(',',$ids).')';
        } else if(is_numeric($id)){
            $where  = $this->model->primary_table.'.id = '.$id;
        }
        $qry = "SELECT ";
        // Calculate rows if not looking for specific ids
        if (!$ids && $this->page_size > 1) {
            $qry .= " SQL_CALC_FOUND_ROWS ";
        }
        $qry .= "\n$select \nFROM \n$from \nWHERE \n$where";
        if($return_qry){
            return $qry;
        }
        // Add group by if set when doing select_from_for_has_many_filter
        if(@$group_by){
            $qry .= "\nGROUP BY $group_by";
        }
        // Order
        if ($this->order_by) {
            $qry.= " \nORDER BY " . $this->order_by;
        }
        // If not looking for particular ids add offset and limit
        if (!$ids && $this->page_size) {
            $offset = ($this->page - 1) * $this->page_size;
            if (is_numeric($offset) && is_numeric($this->page_size)) {
                $qry.= " LIMIT $offset, $this->page_size";
            }
        }
        $resources = miDb::getResults($qry, $this->select_params);

        if (empty($resources)) {
            $this->resources_total = 0;
            $this->resources = false;
            return false;
        }
        if(!$ids){
            $this->resources_total = $this->page_size > 1 ?
                miDb::getVar('SELECT FOUND_ROWS();') :
                count($resources);
        }
        $resources2 = array();
        foreach($resources as $rs){
            $class = get_class($this->model);
            $resource = new $class($rs, $this->model->primary_table);
            $resource->through_table = $this->model->through_table;
            // Copy associates as may be needed for determining what is aliased
            call_user_func_array(array($resource,'associate'),$this->model->associates());
            $resources2[] = $resource;
        }
        $this->resources = $resources2;
        return $this;
    }

    //----------------------------------------------------------------------------
    /**
     * The report func helps to limit what you get and group it etc.
     * @todo Really this is very similar to a find except for these two factors and
     * the fact it doesn't convert anything to classes. So maybe there should
     * be a separate func to put all the elements of the query together
     * and then find/report can do their own things with it.
     */
    function report($group_cols=false, $select_cols=false,
        $order_by = false, $page_size = false, $page = 1,
        $key_results_by = false){
        // Normalize arguments to array
        if(is_string($group_cols)){
            $group_cols = array($group_cols);
        }
        if(is_string($select_cols)){
            $select_cols = array($select_cols);
        }
        if(!$order_by){
            $order_by = 'id';
        }
        $old_order_by = $this->order_by;
        $this->orderBy($order_by);
        $group = array();
        $select = array();
        // Group columns, which will always be selected too
        if (@$group_cols) {
            foreach($group_cols as $k=>$v){
                if ($alias = $this->alias2realcol($v)) {
                    $tmp = $alias;
                } else {
                    $tmp = $this->model->injectFormulas($v);
                }
                $group[] = $tmp;
                if(is_numeric($k)){
                    $select[] = $tmp;
                } else {
                    $select[] = "$tmp AS $k";
                }
            }
        }
        // Select columns
        if($select_cols){
            foreach($select_cols as $k=>$v){
                if ($alias = $this->alias2realcol($v)) {
                    $tmp = $alias;
                } else {
                    $tmp = $this->model->injectFormulas($v);
                }
                if(is_numeric($k)){
                    $select[] = $tmp;
                } else {
                    $select[] = "$tmp AS $k";
                }
            }
        }
        // Build query
        $select = implode(",\n",$select);

        // From
        $from = implode("\n", $this->select_from);

        // Need to make sure got these in case of filters
        if($this->select_from_for_has_many_filter){
            $all_joins = array();
            foreach($this->select_from_for_has_many_filter as $filter_id=>$joins){
                $all_joins = array_merge($all_joins, $joins);
            }
            $from .= "\n ".implode("\n ", array_unique($all_joins));
            $group_by = $this->model->primary_table.'.id';
        }

        // Filters come from the collection
        if($this->select_where){
            uasort($this->select_where, array($this,'sortSelectWhere'));
            $where = implode("\n AND \n",$this->select_where);
        } else {
            $where = 1;
        }
        // Execute the query and return raw data
        $qry = "SELECT \n$select \nFROM \n$from \nWHERE \n$where";
        if ($group) {
            $group = implode(",\n",$group);
            $qry .= " GROUP BY $group";
        }

        if ($this->order_by) {
            if($this->order_by == 'id'){
                $this->order_by = $this->model->primary_table.'.id';
            }
            $qry .= " \nORDER BY " . $this->order_by;
        }

        // Offset, limit
        if ($page_size) {
            $offset = ($page - 1) * $page_size;
            if (is_numeric($offset) && is_numeric($page_size)) {
                $qry .= " LIMIT $offset, $page_size";
            }
        }
        if(@$old_order_by){
            $this->orderBy($old_order_by);
        }

        return miDb::getResults($qry.';',$this->select_params);
    }
    //----------------------------------------------------------------------------
    function collate(){
        if(is_null($this->resources)) {
            $this->find();
        }
        if($this->resources === false){
            return false;
        }
        $resources = $this->resources;
        /*
         * You can pass mutliple arguments if you want to collate on multiple
         * fields. Each call to this func shifts one of the arguments off
         * the stack
         */
        $collate_by = func_get_args();
        $field = array_shift($collate_by);
        /*
         * If the field is an array then the first element must be the field
         * name and the second is a callback (of the PHP pseudo-type)
         */
        $callback = false;
        if(is_array($field)){
            list($field,$callback) = $field;
        }

        // Need to use raw value as resource converted to classes
        $get_raw = false;
        if(strpos($field,'.')){
            $field = str_replace('.','$',$field);
            $get_raw = true;
        }
        // Initialise collated array
        $collated = array();
        foreach($resources as $resource){
            $k = $get_raw ? @$resource->getRawData($field) : @$resource->$field;
            if($k === false){
                trigger_error("[miCollection::collate] No value present for $field",E_USER_ERROR);
                return false;
            }
            // Do the callback if set
            if($callback){
                $k = call_user_func($callback,$k);
            }
            $collated[$k][] = $resource;
        }
        /*
         * Convert each array of objects into a collection, and if there are
         * still things to collate by recursively call this func on the
         * newly created collection, otherwise just return the array of
         * collections.
         */
        foreach($collated as $k=>$collation){
            $collection = new miCollection($this->model,$collation);
            if($collate_by){
                $collated[$k] = call_user_func_array(array($collection,'collate'),$collate_by);
            } else {
                $collated[$k] = $collection;
            }
        }
        return $collated;
    }
    //----------------------------------------------------------------------------
    function getFilter($field){
        return $this->getFilters($field);
    }
    //----------------------------------------------------------------------------
    function getFilters($field=false){
        if($field){
            if(strpos($field,'.') === false && !$this->model->formulas($field)){
                $field = AkInflector::modulize($this->model->primary_table).'.'.$field;
            }
            return @$this->filters[$field];
        }
        return $this->filters;
    }
    //----------------------------------------------------------------------------
    protected function disambiguateField($field){
        if($this->model->formulas($field)){
            return $field;
        }
        if(strpos($field,'.')){
            return $field;
        }
        foreach($this->tables_used as $table){
            if(miDb::colInfo($table,$field)){
                return AkInflector::modulize($table).'.'.$field;
            }
        }
        return false;
    }
    //----------------------------------------------------------------------------
    function unfilter($field=false){
        // Unset all
        if(!$field){
            $this->stop_filters = false;
            $this->filters = array();
            $this->applied_filters = array();
            $this->select_where = array();
            $this->select_params = array();
            $this->select_from_for_has_many_filter = array();
            $this->select_param_counter = 0;
            $this->resetResources();
            return;
        }

        // Unfilter an array of fields
        if(is_array($field)){
            foreach($field as $f){
                $this->unfilter($f);
            }
            return;
        }

        $field = $this->disambiguateField($field);

        // No filter? Then return false
        if(!@$this->filters[$field]){
            return false;
        }

        // Unset filter
        unset($this->applied_filters[$field]);
        $sql_snippet = @$this->select_where[$field];
        unset($this->filters[$field]);
        unset($this->select_where[$field]);
        // If a filter on a has many make sure you unset it's associated joins
        $tokens = explode('.',$field);
        if(AkInflector::is_plural($tokens[0])){
            $col = array_pop($tokens);
            $filter_id = implode('.',$tokens);
            unset($this->select_from_for_has_many_filter[$filter_id]);
        }

        // Extract paramaters from snippet and make sure they are unset
        preg_match_all('/\:param_[0-9]+/',$sql_snippet,$params,PREG_PATTERN_ORDER);
        if($params[0]){
            foreach($params[0] as $param){
                unset($this->select_params[$param]);
            }
        }

        // Reset resources as filters have been changed
        $this->resetResources();
        return true;
    }
    //----------------------------------------------------------------------------
    /*
     * Filter the collection to only contain resources with fields of a certain
     * value. The semantics of the value depend on the type - check filterString,
     * filterDate, filterNumber and filterIn for more info.
     *
     * @param mixed $field The name of the field to filter on. Can be an alias from
     * $model->filter_aliases or a formula from $model->formulas. Can also be an
     * array of filters with $fields for keys and $value for $values in which case
     * all will be applied at once and next two arguments will be ignored.
     * @param string|number The value to filter the field for. Can contain special
     * syntax to change comparison operators, @see  filterString,
     * filterDate, filterNumber and filterIn for more info.
     * @param bool $use_callback Whether to use a model callback or not, when
     * using a callback make sure this is set to false to prevent an infinite loop
     * @return bool|object A reference to this collection if successfully applied
     * or false if not
     */
    function filter($field, $value = 1, $use_callback=true){
        if($this->stop_filters){
            trigger_error('Filters are stopped, call unfilter()',E_USER_WARNING);
            return false;
        }
        // Handle multiple filters
        if(is_array($field)){
            foreach($field as $f=>$v){
                $this->filter($f, $v, $use_callback);
            }
            return;
        }
        // Disambiguate field
        if(!$real_field = $this->disambiguateField($field)){
            // Check for model callback
            if($use_callback && method_exists($this->model,'filterCallback')){
                if($filter = $this->model->filterCallback($this,$field,$value)){
                    $this->applied_filters[$field] = $value;
                    if(is_array($filter)){
                        $keys = array_keys($filter);
                        if(is_numeric($keys[0])){
                            list($field, $value) = $filter;
                            return $this->filter($field, $value, false);
                        } else {
                            $this->filter($filter);
                        }
                    }
                    return true;
                }
            }
            trigger_error("Filter field $field could not be disambiguated",E_USER_WARNING);
            return false;
        }
        // If filter already applied return
        if(isset($this->filters[$real_field]) && $this->filters[$real_field] === $value){
            return $this;
        }
        // Remove any existing filter on the field
        $this->unfilter($real_field);
        if(!$f = $this->__filter($real_field, $value, false)){
            return false;
        }
        // Now apply the filter
        list($col, $sql) = $f;
        $this->select_where[$col] = $sql;   // Store SQL for SELECT
        $this->filters[$real_field] = $value; // Store the filter itself
        // If filter not set by a callback then store
        // Generally the callback would have changed human readable filter into
        // machine readable
        if($use_callback){
            $this->applied_filters[$field] = $value;
        }
        $this->resetResources(); // Reset resources
        return $this;
    }
    //----------------------------------------------------------------------------
    /*
     * This stops filtering and forces collection to be empty until you call
     * unfilter() again
     */
    function filterStop(){
        $this->filter('id','=0',false);
        $this->resources = false;
        $this->stop_filters = true;
    }
    //----------------------------------------------------------------------------
    /**
     * filterReduce() applies the filter then gets back all matching ids
     * and uses that as a new filter. This allows filtering on the same column
     * multiple times which is useful for things like a has many relation.
     * For example if an article has many tags you can do like:
     *
     * $Articles->filterReduce('Tags.tag',array('politics','economics','toaster'));
     *
     * You also don't have to do it all at one like this. You could do:
     *
     * $Article->filterReduce('Tags.tag','politics');
     * $Article->filterReduce('Tags.tag','economics');
     * $Article->filterReduce('Tags.tag','toaster');
     *
     * Which will get articles that match all those tags.
     *
     * @param string $fld The name of the field
     * @param string|array $values Either a single value or an array of values.
     * @return int The number of resources matched (as does queries up front)
     */
    function filterReduce($fld,$values){
        $this->truncate_in_filter_to_page_size = false;
        if(is_array($values)){
            $value = array_shift($values);
        } else {
            $value = $values;
            $values = false;
        }
        $this->filter($fld, $value);
        // Get ids matching this filter (always order by primary key)
        $ids = $this->report(array($this->model->primary_table.'.id'),
            false,
            $this->model->primary_table.'.'.miDb::primaryKey($this->model->primary_table));
        if(!$ids){
            return false;
        }
        // Apply the ids as a filter
        $id_filter = miArray::extractByKey($ids,'id');
        $this->filter('id','in '.implode(',',$id_filter));

        // Remove filter on field as will now use id filter
        $this->unfilter($fld);

        // Move to the next filter if there is one
        if($values){
            return $this->filterReduce($fld,$values);
        }
        $this->truncate_in_filter_to_page_size = true;
        return count($ids);
    }
    //----------------------------------------------------------------------------
    function __filter($field, $value = 1, $compare_as_type = false) {

        if (!trim($field)) {
            trigger_error('[miCollection] No field provided to filter on',
                E_USER_WARNING);
            return false;
        }

        if (trim($value) === '') {
            trigger_error('[miCollection] Cannot filter on an empty string as
                value, if you really want to do that use EMPTY instead',
                E_USER_WARNING);
            return false;
        }

        $tokens = explode('.',$field);
        if($compare_to = $this->model->formulas($field)){
            $compare_as = $this->model->formulaReturnType($field);

        } else if(AkInflector::is_plural($tokens[0])){
            // Comparing to has many so need to add in some JOINs
            $col = array_pop($tokens);
            $last_tbl = $this->model->primary_table;
            $filter_id = implode('.',$tokens);
            foreach($tokens as $model){
                $current_tbl = AkInflector::tableize($model);
                if(AkInflector::is_plural($model)){
                    $foreign_col = AkInflector::singularize($last_tbl).'_id';
                    // Join to last table, maybe using through table
                    $foreign_exists = miDb::colInfo($current_tbl,$foreign_col);
                    if(!$foreign_exists && $through_tbl = miDb::detectThrough($last_tbl,$current_tbl)){
                        $right_foreign_col = AkInflector::singularize($current_tbl).'_id';
                        $join = "LEFT JOIN $through_tbl ON $through_tbl.$foreign_col=$last_tbl.id
                            LEFT JOIN $current_tbl ON $current_tbl.id=$through_tbl.$right_foreign_col";
                    } else if($foreign_exists) {
                        $join = "LEFT JOIN $current_tbl ON $last_tbl.id=$current_tbl.$foreign_col";
                    } else {
                        trigger_error("No way to join $last_tbl to $current_tbl ($field, $value)",E_USER_WARNING);
                        continue;
                    }
                } else {
                    $foreign_col = AkInflector::singularize($current_tbl).'_id';
                    $join = "LEFT JOIN $current_tbl ON $last_tbl.$foreign_col=$current_tbl.id";
                }
                $this->select_from_for_has_many_filter[$filter_id][] = $join;
                $last_tbl = $current_tbl;
            }
            $compare_as = miDb::colInfo($last_tbl, $col, 'type');
            $compare_to = $last_tbl.'.'.$col;

        } else {
            // Comparing to primary table or has one relation
            $col = array_pop($tokens);
            if(count($tokens) > 1){
                foreach($tokens as $t){
                    $tokens2[] = AkInflector::tableize($t);
                }
                $tbl = implode('__', $tokens2);
                if(!($real_tbl = @$this->tables_aliased[$tbl])){
                    trigger_error("[miCollection] Could not filter $field with $value", E_USER_WARNING);
                    return false;
                }
                $compare_as = @miDb::colInfo($real_tbl, $col, 'type');
            } else {
                $tbl = AkInflector::tableize($tokens[0]);
                if($real_tbl = @$this->tables_aliased[$tbl]){
                    $compare_as = @miDb::colInfo($real_tbl, $col, 'type');
                } else {
                    $compare_as = @miDb::colInfo($tbl, $col, 'type');
                }
            }
            $compare_to = $tbl.'.'.$col;
        }
        if(!$compare_as || !$compare_to){
            trigger_error("[miCollection] Could not filter '$field' with '$value'", E_USER_WARNING);
            return false;
        }

        // Now do the filtering
        // Watch out as zero lossely equals string in PHP! Need to use strict
        // equality to allow filtering on zero.
        // See https://www.hashbangcode.com/article/string-equals-zero-php
        if ($value === 'NULL') {
            $sql_snippet = "$compare_to IS NULL";
        } else if ($value === 'NOT NULL') {
            $sql_snippet = "$compare_to IS NOT NULL";
        } else if ($value === 'NOT EMPTY') {
            $sql_snippet = "($compare_to != '' AND $compare_to IS NOT NULL)";
        } else if ($value === 'EMPTY') {
            $sql_snippet = "($compare_to = '' OR $compare_to IS NULL)";
        } else {
            // Call filter function
            $fnc = 'filter' . $compare_as;
            if (method_exists($this, $fnc)) {
                if ($tmp = $this->$fnc($value)) {
                    list($sql_snippet, $tmp_params) = $tmp;
                    $sql_snippet = $compare_to . ' ' . $sql_snippet;
                    if (is_array($tmp_params)) {
                        $this->select_params = array_merge($this->select_params, $tmp_params);
                        $this->select_param_counter++;
                    } else if (is_string($tmp_params) && $tmp_params !== false) {
                        $this->select_params[':param_' . $this->select_param_counter++] = $tmp_params;
                    }
                } else {
                    trigger_error("[miCollection] Filter '$field' on '$value' was
                        not understood as '$compare_as'", E_USER_WARNING);
                    return false;
                }
            } else {
                trigger_error("[miCollection] No filter func for
                    $compare_as", E_USER_WARNING);
                return false;
            }
        }
        return array($field, $sql_snippet);
    }
    //----------------------------------------------------------------------------
    protected function filterString($value) {
        if(preg_match('/^(not )?in (.+)/', $value)){
            return $this->filterIn($value);
        }
        if (strstr($value, '*')) {
            $tmp_value = str_replace('*', '%', $value);
            return array("LIKE :param_{$this->select_param_counter}", $tmp_value);
        }
        if ($value{0} == '=') {
            return array("= :param_{$this->select_param_counter}", substr($value, 1));
        }
        if ($value{0} == '!') {
            return array("!= :param_{$this->select_param_counter}", substr($value, 1));
        }
        return array("LIKE :param_{$this->select_param_counter}", "%$value%");
    }
    //----------------------------------------------------------------------------
    protected function filterNumber($value) {
        if(preg_match('/^(not )?in (.+)/', $value)){
            return $this->filterIn($value);
        }

        // Remove commas and spaces from value
        $value = trim(preg_replace('/[\s,]/', '', $value));

        // Within range
        if (preg_match('/[0-9]+-[0-9]+/',$value)) {
            list($min, $max) = explode('-', $value);
            $min = trim($min);
            $max = trim($max);
            if (is_numeric($min) && is_numeric($max) && $max > $min) {
                $param_a = ':param_' . ($this->select_param_counter++);
                $param_b = ':param_' . $this->select_param_counter;
                return array(" BETWEEN $param_a AND $param_b ", array($param_a => $min, $param_b => $max));
            } else {
                return false;
            }
        }
        // More than
        if (strpos($value, '>') === 0 || strstr($value, 'morethan')) {
            $tmp_value = trim(str_replace('>', '', str_replace('morethan', '', $value)));
            if (is_numeric($tmp_value)) {
                return array(' > :param_' . $this->select_param_counter, $tmp_value);
            }
        }
        // Less than
        if (strpos($value, '<') === 0 || strstr($value, 'lessthan')) {
            $tmp_value = trim(str_replace('<', '', str_replace('lessthan', '', $value)));
            if (is_numeric($tmp_value)) {
                return array(' < :param_' . $this->select_param_counter, $tmp_value);
            }
        }
        // Not equal to
        if (strpos($value, '!') === 0 || strstr($value, 'not')) {
            $tmp_value = trim(str_replace('!', '', str_replace('not', '', $value)));
            if (is_numeric($tmp_value)) {
                return array(' != :param_' . $this->select_param_counter, $tmp_value);
            }
        }
        // Equal to
        if($value{0} == '='){
            return array(' = :param_' . $this->select_param_counter, trim($value,'='));
        }
        // Default if numeric is equal to
        if(is_numeric($value)){
            return array(' = :param_' . $this->select_param_counter, $value);
        }
        return false;
    }
    //----------------------------------------------------------------------------
    public function filterDate($value) {
        // Never
        if ($value == 'never') {
            return array(' IS NULL', null);
        }
        // Anytime
        if (strtolower($value) == 'anytime') {
            return array(' IS NOT NULL', null);
        }
        // Last whenever
        if (strpos($value, 'last') === 0) {
            $tmp_value = str_replace('last ', '', $value);
            $d = $this->isSqlDateTimeValue($tmp_value) ?
                $tmp_value :
                date('Y-m-d 23:59:59',miDate::strToTime($tmp_value . ' ago'));
            if ($d) {
                return array(' > :param_' . $this->select_param_counter, $d);
            }
        }
        // Before a certain date
        if (strpos($value, 'before') === 0 || strpos($value, 'over') === 0) {
            $tmp_value = str_replace('before ', '', str_replace('over ', '', $value));
            $d = $this->isSqlDateTimeValue($tmp_value) ?
                $tmp_value :
                date('Y-m-d 00:00:00',miDate::strToTime($tmp_value));
            if ($d) {
                return array(' < :param_' . $this->select_param_counter,$d);
            }
        }
        // After a certain date
        if (strpos($value, 'after') === 0) {
            $tmp_value = str_replace('after ', '', $value);
            $d = $this->isSqlDateTimeValue($tmp_value) ?
                $tmp_value :
                date('Y-m-d 23:59:59', miDate::strToTime($tmp_value));
            if ($d) {
                return array(' > :param_' . $this->select_param_counter, $d);
            }
        }
        // Date range, but make sure does not conflict with a normal SQL date
        if (preg_match('/^[^\-]+-[^\-]+$/', $value)) {
            list($start, $end) = explode('-', $value);
            $start = miDate::strToTime(trim($start));
            $end = miDate::strToTime(trim($end));
            if ($start && $end) {
                $param_a = ':param_' . ($this->select_param_counter++);
                $param_b = ':param_' . $this->select_param_counter;
                return array(" BETWEEN $param_a AND $param_b ",
                    array($param_a => date('Y-m-d 00:00:00', $start),
                    $param_b => date('Y-m-d 23:59:59', $end)));
            }
        }
        // Exact SQL date
        if (preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/', $value)) {
            return array(' = :param_' . $this->select_param_counter, $value);
        }
        // Year range in YYYY format
        if (preg_match('/^[0-9]{4}$/',$value)){
            return $this->filterDate("1 Jan $value-31 Dec $value");
        }
        // Month range in YYYY-MM format
        if (preg_match('/^[0-9]{4}-[0-9]{2}$/',$value) && $ts = strtotime($value.'-01')){
            $f = date('1 M Y',$ts).'-'.date('d M Y',miDate::lastOfMonth($ts));
            return $this->filterDate($f);
        }
        // Month range - word followed by 4 numbers e.g. May 2009
        if (preg_match('/^[a-zA-Z]+( [0-9]{4})?$/',$value) && $ts = strtotime('1 '.$value)){
            $f = date('1 M Y',$ts).'-'.date('d M Y',miDate::lastOfMonth($ts));
            return $this->filterDate($f);
        }
        // Default is just to compare as a date
        if ($d = miDate::strToTime(trim($value))){
            return array(' LIKE :param_' . $this->select_param_counter, date('Y-m-d', $d) . '%');
        }
        return false;
    }
    //----------------------------------------------------------------------------
    /**
     * Test to see if a string is an exact SQL date time value - used in `filterDate`
     * to decide whether to try and parse or treat as an exact date
     * @param string $value The string to check
     * @return bool True if is a full date time value or false if not
     */
    protected function isSqlDateTimeValue($value){
        return preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/', $value);
    }
    //----------------------------------------------------------------------------
    protected function filterIn($value) {
        preg_match('/^(not )?in (.+)/', $value, $matches);
        $not = trim($matches[1]);
        $options = explode(',', $matches[2]);
        /*if($this->truncate_in_filter_to_page_size
        && count($options) > $this->getPageSize() && $this->getPageSize() > 0){
            $options = array_slice($options,0,$this->getPageSize());
        }*/
        if (count($options) == 1) {
            return array(($not ? ' !' : ' ').'= :param_' . $this->select_param_counter,
                array(':param_' . $this->select_param_counter => $options[0]));
        }
        $params = array();
        foreach($options as $opt) {
            $params[':param_' . $this->select_param_counter++] = trim($opt);
        }
        return array(($not ? ' NOT' : ' ') .
            ' IN (' . implode(',', array_keys($params)) . ')',
            $params);

    }
    //----------------------------------------------------------------------------
    protected function join($rel,$start_table=false){

        $all_tables = array();
        $models = explode('.', $rel);
        $left_table = $start_table ? $start_table : $this->model->primary_table;
        $left_alias_table = false;
        foreach($models as $i => $model) {
            $right_table = false;
            $right_alias_table = false;
            $foreign_key = false;

            // Aliased model e.g. Model(Alias)
            if (preg_match('/([a-zA-Z]+)\(([a-zA-Z]+)\)/',$model,$matches)) {
                $right_table = AkInflector::tableize($matches[1]);
                $right_alias_table = AkInflector::tableize($matches[2]);
                $foreign_key = AkInflector::singularize($right_alias_table).
                    '_id';
                if ($i > 0) {
                    $right_alias_table = $all_tables[count($all_tables)-1].
                        '__'.$right_alias_table;
                }

                // Literal model
            } else {
                $right_table = AkInflector::tableize($model);
                $foreign_key = AkInflector::singularize($right_table).
                    '_id';
                if ($i > 0) {
                    $right_alias_table = $all_tables[count($all_tables)-1].
                        '__'.$right_table;
                }
            }

            /*
             * Work out what table names to use for the actual joins
             * depending on if either aliased
             */
            $join_left_table = $left_alias_table ?
                $left_alias_table : $left_table;
            $join_right_table = $right_alias_table ?
                $right_alias_table : $right_table;
            /*
             * Only actually need to do the join and get data is this table
             * (or alias) has not already been joined
             */
            if(!in_array($join_right_table, $this->tables_used)
                && !in_array($join_right_table, array_keys($this->tables_aliased))) {
                // Make the JOIN
                $join = " LEFT JOIN $right_table";
                if ($right_alias_table) {
                    $join .= " AS $right_alias_table";
                }
                // Work out what ON
                $left_table_info = miDb::tableInfo($left_table);
                $right_table_info = miDb::tableInfo($right_table);
                $reverse_foreign_key = AkInflector::singularize($left_table).'_id';
                if(@$left_table_info[$foreign_key]){
                    $join .= " ON $join_left_table.$foreign_key = $join_right_table.id";
                } else if (@$right_table_info[$reverse_foreign_key]){
                    $join .= " ON $join_left_table.id = $join_right_table.$reverse_foreign_key";
                } else if ($through_table = miDb::detectThrough($left_table,$right_table)){
                    trigger_error("Don't know how to do through $through_table yet
                        $join_left_table $join_right_table",
                        E_USER_ERROR);
                } else {
                    trigger_error("[miCollection] Cannot JOIN $left_table to
                        $right_table on $foreign_key or $reverse_foreign_key in either direction",
                        E_USER_ERROR);
                    return false;
                }
                $this->select_from[] = $join;

                // Normalize column so can sort out data later
                $col_prefix = $right_alias_table ? $right_alias_table : $right_table;
                $col_prefix = preg_split('/__/', $col_prefix, -1, PREG_SPLIT_NO_EMPTY);
                foreach($col_prefix as $k=>$prefix) {
                    $col_prefix[$k] = AkInflector::singularize(AkInflector::modulize($prefix));
                }
                $col_prefix = implode('$', $col_prefix) . '$';
                $tmp_cols = array();
                $store_table = $right_alias_table ? $right_alias_table : $right_table;
                $ti = miDb::tableInfo($right_table);
                foreach($ti as $k => $v) {
                    $tmp_cols["$store_table.$k"] = $col_prefix.$k;
                }
                $this->select_cols = array_merge($this->select_cols, $tmp_cols);
            }

            // Right becomes left for next loop
            $left_table = $right_table;
            $left_alias_table = $right_alias_table;

            // Store right table used
            $this->tables_used[] = $right_table;

            // If aliased store that and set what goes in local all_tables
            if($right_alias_table){
                $this->tables_aliased[$right_alias_table] = $right_table;
                $all_tables[] = $right_alias_table;
            } else {
                $all_tables[] = $right_table;
            }
        }
    }
    //----------------------------------------------------------------------------
    /*
     * This allows you to load a has many associate for *all* resources in a
     * collection with *one* query. Note this is not always faster than accessing
     * them through miResource->{HasMany} and you also cannot limit the results,
     * it will always get everything.
     *
     * Call like miCollection->collate{HasMany} to load everything and collate
     * to all objects. Call without 'collate' to just return everything
     * uncollated, which can be useful for reports.
     */
    function __call($func, $args){

        if(@$this->child_collections[$func]){
            return $this->child_collections[$func];
        }

        if(miDb::primaryKey($this->model->primary_table) != 'id'){
            trigger_error("[miCollection] Cannot get has may $func unless the
                table {$this->model->primary_table} has a primary key named 'id'",
                E_USER_WARNING);
            return false;
        }

        $foreign_key = AkInflector::singularize($this->model->primary_table).'_id';

        // Check we have something (also forces load)
        if($this->count() === false){
            return false;
        }

        // See if this is a call to collate associates, or just to load them up
        if(substr($func,0,7) == 'collate'){
            $func = substr($func,7);
            $collate = true;
        } else {
            $collate = false;
        }

        if(!AkInflector::is_plural($func)){
            trigger_error("[miCollection] Cannot get has many $func for" .
                " {$this->model->primary_table} - did you " .
                " forget to write it in the plural?",
                E_USER_WARNING);
            return false;
        }

        /*
         * Create model using generic miResource or stdClass depending on
         * what actually exists
         */
        $model_name = AKInflector::modulize($func);
        if(!@class_exists($model_name)){
            $table = AkInflector::tableize($model_name);
            trigger_error("[miResource] Has many subclass $model is missing
                so using mock miResource", E_USER_WARNING);
            if(!@class_exists('miResource')){
                $model = new stdClass();
                $model->primary_table = $table;
            } else {
                $model = new miResource(false,$table);
            }
        } else {
            $model = new $model_name();
            $table = $model->primary_table;
            // Remove any belongs_to associates that point back to this
            if($assoc = $model->associates()){
                $this_model = AkInflector::modulize($this->model->primary_table);
                foreach($assoc as $a){
                    if($model->initialRel($a) == $this_model){
                        $model->disassociate($a);
                    }
                }
            }
        }

        // Check related table info
        $ti = miDb::tableInfo($table);
        if(!$ti){
            trigger_error("[miCollection] {$this->model->primary_table} related table
                $table does not exist", E_USER_ERROR);
            return false;
        }

        // If foreign key does not exist in the table look for a through table
        $through_table = false;
        if(!isset($ti[$foreign_key]) &&
            !$through_table = miDb::detectThrough($this->model->primary_table,$table)){
            trigger_error("[miResource] {$this->model->primary_table} has no
                through table $through_table to $table", E_USER_ERROR);
            return false;
        }

        // Add through table to the model
        if($through_table){
            $model->through_table = $through_table;
            $filter_on = AkInflector::modulize($through_table).'.'.$foreign_key;
        } else {
            $filter_on = $foreign_key;
        }

        // Get primary keys from the current collection
        $ids = array();
        foreach($this->resources as $resource){
            $ids[] = $resource->id;
        }

        // Use that as the input filter to the has many model
        $collection = new miCollection($model);
        $this->child_collections[$func] = $collection;

        // Results are unlimited to ensure we get everything
        $collection->pageSize(0);
        $collection->filter($filter_on,'in '.implode(',',$ids));

        // No collation? Then return the collection as is
        if(!$collate){
            return $collection;
        }

        // If collating check there is something in the collection
        if(!$collection->count()){
            return false;
        }

        // Collate the collection by the foreign col
        $collated = $collection->collate($foreign_key);
        /*
         * @todo This appears to be broken, at least I can't get it to work
         * without generating a lot more queries than it should
         */
        // Now add back to our resources
        foreach($this->resources as $k=>$resource){
            $coll = @$collated[$resource->id];
            // Some resources might not have anything
            if(!$coll){
                continue;
            }
            $this->resources[$k]->{$func} = $coll;
            /*
             * Add the filter to the collection, but false as the third
             * argument means do not reset the resources
             */
            $coll->filter($filter_on, $resource->id, false);
            /*
             * Make sure the resource knows to keep this filter on when
             * ever the has many associate is accessed
             */
            $this->resources[$k]->hackHasManyFilter($func, $filter_on);
        }
        return true;
    }
    // ----------------------------------------------------------------------------
    /**
     * Magic method
     *
     * Using a property name of another model in the plural will invoke the __call
     * method. @see __call for further explanation.
     *
     * Using a property name that corresponds to a property or column of the model
     * will return an array of all values of that property for every resource in
     * the current collection.
     */
    function __get($prop){
        if(AkInflector::is_plural($prop) && strtolower($prop) != $prop){
            return $this->$prop();
        }
        if($resource = $this->getFirst()){
            if(isset($resource->$prop)){
                return miArray::extractByKey($this,$prop);
            }
        }
        return null;
    }
}
