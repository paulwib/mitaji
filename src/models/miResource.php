<?php

class miResource {

    public $aliased_as = false;
    public $primary_table = '';
    public $assoc = array();
    public $formulas = array();
    public $rules = array();
    public $delete_cascade = false;
    public $through_table = false;
    public $save_new_through = false;
    public $order_by = false;
    public $onchange_events = array();
    public $order_by_alias = array();
    protected $__associates = array();
    protected $__formulas = array();
    protected $__formula_return_types = array();
    protected $__denormalized_formulas = array();
    protected $__data = array();
    protected $__raw_data = array();
    protected $__has_many_filters = array();
    protected $__rules = array();
    protected $__locked_props = array();
    protected $__warnings = array();
    protected $__errors = array();
    protected static $__object_cache = array();
    protected static $__force_reload = false;
    protected static $suspend_formulas = false;
    public static $save_has_many_index = false;

// ----------------------------------------------------------------------------
    static function flushObjectCache(){
        self::$__object_cache = array();
    }

// ----------------------------------------------------------------------------
// Sets a flag that forces reload after save even if already within a transaction
    static function setForceReload($b){
        self::$__force_reload = $b;
    }
// ---------------------------------------------------------------------------
    function __construct($raw_data = false, $primary_table = false, $aliased_as = false) {

        $this->aliased_as = $aliased_as;

        if(is_array($raw_data)){
            $this->__raw_data = $raw_data;
        }
        if(!$this->primary_table){
            $this->primary_table = $primary_table ?
                    AkInflector::tableize($primary_table) :
                        AkInflector::tableize(get_class($this));
        }
        if($this->assoc){
            foreach($this->assoc as $a){
                $this->associate($a);
            }
        }
        if($this->formulas){
            foreach($this->formulas as $field=>$formula){
                $this->formulate($field, $formula);
            }
        }
        if($this->rules){
            foreach($this->rules as $fld=>$rule){
                $keywords = explode(' ',$rule);
                foreach($keywords as $keyword){
                    $this->addRule($fld, $keyword);
                }
            }
        }
    }
// -------------------------------------------------------------------
/**
 * Default permissionsToFind callback returns true. Override this
 * in your own models to handle permissions.
 * @param object $Collection The curent miCollection object - allows you to
 * set granular filters on collections based on permissions
 */
    function permissionsToFind(&$Collection){
        return true;
    }
// -------------------------------------------------------------------
/**
 * Default permissionsToSave callback returns true. Override this
 * in your own models to handle permissions.
 * Passes in data so can check at granular level whether a user is able
 * to save partciular data.
 */
    function permissionsToSave($data){
        return true;
    }
// -------------------------------------------------------------------
/**
 * Default permissionsToDelete callback returns true. Override this
 * in your own models to handle permissions.
 */
    function permissionsToDelete(){
        return true;
    }
// -------------------------------------------------------------------
    function associates(){
        return array_unique($this->__associates);
    }
// -------------------------------------------------------------------
    function associate(){
        $args = func_get_args();
        if($args){
            foreach($args as $a){
                $this->__associates[] = $a;
            }
        }
        return $this->__associates;
    }
// -------------------------------------------------------------------
    function disassociate(){
        $args = func_get_args();
        if($args){
            $new_a = array();
            foreach($args as $a){
                $a = preg_quote($a);
                foreach($this->__associates as $ca){
                    if(!preg_match("/^$a/",$ca)){
                        $new_a[] = $ca;
                    }
                }
            }
            $this->__associates = $new_a;
        } else {
            $this->__associates = array();
        }
        return $this->__associates;
    }
// ---------------------------------------------------------------------------
    function formulas($alias=false){
        if($alias){
            return @$this->__formulas[$alias];
        }
        return $this->__formulas;
    }
// ---------------------------------------------------------------------------
    function formulaReturnType($alias){
        return @$this->__formula_return_types[$alias];
    }
// ---------------------------------------------------------------------------
    function formulate($alias,$formula,$return_type='number'){
        // Inject other formulas
        $formula = $this->injectFormulas($formula);
        // If column exists then a denormalized formula
        if(miDb::colInfo($this->primary_table,$alias)){
            $this->__denormalized_formulas[$alias] = $formula;
            return;
        }
        // Otherwise store as formula
        $this->__formulas[$alias] = $formula;
        // Set type if not already
        if (!isset($this->__formula_return_types[$alias])) {
            $this->__formula_return_types[$alias] = $return_type;
        }
    }
// ---------------------------------------------------------------------------
    function deformulate($alias=false){
        if($alias){
            unset($this->__formula_return_types[$alias]);
            unset($this->__formulas[$alias]);
            return;
        }
        $this->__formulas = array();
    }
// ---------------------------------------------------------------------------
    function suspendFormulas($suspend=true) {
        miResource::$suspend_formulas = true;
    }
// ---------------------------------------------------------------------------
/**
 * Update and denormalized formulas (i.e. columns that really exist for a
 * formula) for this resource
 */
    function updateDenormalizedFormulas(){
        //echo "Updating formulas for " . get_class($this) . "." . $this->id . "\n";
        // Suspended formulas
        if (miResource::$suspend_formulas) {
            return true;
        }
        // If not actually saved yet just return true
        if (!$this->primaryKey()) {
            return true;
        }
        if ($this->__denormalized_formulas) {
            foreach ($this->__denormalized_formulas as $field=>$formula) {
                list($sql, $params) = $this->primaryKeyParams();
                $c = get_class($this);
                // SELECT and UPDATE in 2 queries quicker than one
                $qry = "SELECT $formula FROM $this->primary_table WHERE $sql";
                $val = miDb::getVar($qry, $params);
                $params[':val'] = $val;
                $qry = "UPDATE $this->primary_table SET $field=:val WHERE $sql;";
                if (!miDb::getResults($qry, $params)) {
                    $e = "Could not update $field with formula";
                    $this->errors($e, $field);
                    trigger_error($e, E_USER_WARNING);
                    return false;
                }
            }
        }
        return $this->updateDenormalizedAssociateFormulas();
    }
// ---------------------------------------------------------------------------
    function updateDenormalizedAssociateFormulas(){
        // Suspended formulas
        if (miResource::$suspend_formulas) {
            return true;
        }
        // Update formulas on associates
        if ($this->assoc_formulas) {
            foreach ($this->assoc_formulas as $assoc) {
                $c = get_class($this);
                if (AkInflector::is_plural($assoc) && $this->$assoc
                    && $this->$assoc->count()) {
                    foreach ($this->$assoc as $ob) {
                        if(!$ob->updateDenormalizedFormulas()){
                            return false;
                        }
                    }
                }
                elseif ($this->$assoc && $this->$assoc->primaryKey()) {
                    if(!$this->$assoc->updateDenormalizedFormulas()){
                        return false;
                    }
                }
            }
        }
        return true;
    }
// ----------------------------------------------------------------------------
/**
 * This takes a string and will look for any formula aliases in it which will
 * be replaced with their actual formulas. The aliases must be surrounded in
 * brackets.
 */
    function injectFormulas($str){
        if(!$formulas = $this->formulas()){
            return $str;
        }
        foreach($formulas as $alias=>$formula){
            if(strpos($str,"($alias)") !== false){
                $str = str_replace("($alias)","($formula)",$str);
            }
        }
        return $str;
    }
// ---------------------------------------------------------------------------
    function find($reload=false) {
        // Create new collection with this as referenced model
        $c = new miCollection($this);
        /*
         * Callback to setFindPermissions - passes the created collection
         * so filters can be set on it for granular permissions
         */
        if (!$reload) {
            if (!$this->permissionsToFind($c)) {
                return false;
            }
        }
        // Return the collection
        return $c;
    }
// ---------------------------------------------------------------------------
/**
 * findRandom to return random resource(s)
 * This is probably not that efficient if you have complex models.
 * It requires your model to have an id column.
 * @params int $n The number of resources to find, which will be in a sequence
 */
    function findRandom($n=1,$where=false) {
        $qry = "SELECT id FROM $this->primary_table ";
        if($where){
            $qry .= " WHERE $where";
        }
        $qry .= ';';
        $ids = miDb::getCol($qry);
        $id_keys = array_rand($ids,min($n,count($ids)));
        if($n == 1){
            $id = $ids[$id_keys];
            return $this->findOne($id);
        }
        $filter_ids = array();
        foreach($id_keys as $idk){
            $filter_ids[] = $ids[$idk];
        }
        $coll = $this->find();
        $coll->filter('id','in '.implode(',',$filter_ids));
        return $coll;
    }

// ---------------------------------------------------------------------------
/**
 * findOne will find the first exact match of field and value
 * @params string $val The value to search for
 * @params string $field The filed to serach on, default to id
 */
    function findOne($val,$field='id') {
        $coll = new miCollection($this);
        $coll->filter($field,'='.$val);
        $coll->pageSize(1);
        if(!$coll->count()){
            $return_value = false;
        } else {
            $return_value = $coll->getFirst();
        }
        unset($coll);
        return $return_value;
    }
// ---------------------------------------------------------------------------
/**
 *
 * @params string|int This should be the value of the primary key. If the
 * table has multiple primary keys these should be passed joined with a
 * hyphen (-) in the order the columns appear in the table.
 * @params bool $cached Whether to get from cache if present, defaults to true
 * @returns object|bool Returns an instance of the model or false if not found
 */
    function findByPrimaryKey($val, $cached=true, $reload=false){
        if($cached && ($resource = @miResource::$__object_cache[get_class($this)][$val])){
            return $resource;
        }
        $primary_key = miDb::primaryKey($this->primary_table);
        if(!$primary_key) return false;
        if (!$coll = $this->find($reload)) {
            return false;
        }
        $coll->pageSize(1);
        if(is_array($primary_key)){
            $values = explode('-',$val);
            if(count($values) != count($primary_key)){
                trigger_error("[miResource] Not enough values provided to match primary key", E_USER_ERROR);
                return false;
            }
            $coll->filter(array_combine($primary_key, $values));
        } else {
            $coll->filter($primary_key, $val);
        }
        if(!$coll->count()){
            $return_value = false;
        } else {
            $resource = $coll->getFirst();
            if($resource->primaryKey() != $val){
                $return_value = false;
            } else {
                $return_value = $resource;
                miResource::$__object_cache[get_class($this)][$resource->primaryKey()] = $resource;
            }
        }
        unset($coll);
        return $return_value;
    }
// ---------------------------------------------------------------------------
/**
 * @return string The value of the primary key for this resource. If it has
 * multiple primary keys they will be returned joined with hyphens (-) in the
 * order the columns appear in the table
 */
    function primaryKey(){
        $primary_key = miDb::primaryKey($this->primary_table);
        if(!$primary_key) return false;
        if(is_array($primary_key)){
            $tmp = array();
            foreach($primary_key as $key){
                if(isset($this->{$key})){
                    $tmp[] = $this->{$key};
                }
            }
            // If a value is missing from primary key return false
            if(count($primary_key) != count($tmp)){
                return false;
            }
            return implode('-',$tmp);
        }
        return isset($this->{$primary_key}) ? $this->{$primary_key} : false;
    }
// ---------------------------------------------------------------------------
/**
 * @return string The value of the through primary key for this resource if
 * it has a through table.
 */
    function primaryKeyThrough(){
        if(!$this->through_table) return false;
        $primary_key = miDb::primaryKey($this->through_table);
        if(!$primary_key) return false;
        if(is_array($primary_key)){
            $tmp = array();
            foreach($primary_key as $key){
                if(isset($this->{$key})){
                    $tmp[] = $this->{$key};
                }
            }
            // If a value is missing from primary key return false
            if(count($primary_key) != count($tmp)){
                return false;
            }
            return implode('-',$tmp);
        }
        return isset($this->{$primary_key}) ? $this->{$primary_key} : false;
    }
// ---------------------------------------------------------------------------
/**
 * Get the SQL needed to find a resource by its primary key
 * @returns array The first element is a SQL snippet suitable to use in a
 * WHERE condition and the second is the paramaters for miDb::getResults
 */
    function primaryKeyParams(){
        $primary_key = miDb::primaryKey($this->primary_table);
        if(!$primary_key) return false;
        $sql = array();
        $params = array();
        if(is_array($primary_key)){
            foreach($primary_key as $key){
                if(!isset($this->{$key})){
                    return false;
                }
                $params[':'.$key] = $this->{$key};
                $sql[] = "$key = :$key";
            }
            return array(implode(' AND ',$sql), $params);
        }
        if(!isset($this->{$primary_key})){
            return false;
        }
        return array("$primary_key = :$primary_key",
                        array(':'.$primary_key=>$this->{$primary_key}));
    }
// ---------------------------------------------------------------------------
/**
 * Get the initial associate in an associate relation string. For example,
 * given Product.Supplier it would return Product
 * @returns string
 */
    function initialRel($rel){
        $a = explode('.', $rel);
        return $a[0];
        //preg_match('/^([^ \+]+)/', $rel, $matches);
        //return $matches[1];
    }
// ---------------------------------------------------------------------------
/**
 * Save the resource to the database. This is used for both new resources
 * (INSERT) and existing (UPDATE). Which one depends on whether the resource
 * has a primary key defined and returned by primaryKey()
 * It will also save/update/delete belongs to, hasone and has many associates.
 * All foreign keys etc. are worked out automatically, just put the relevant
 * info in data (more info in tests).
 *
 * Uses transactions so an all or nothing operation. If not everything in
 * data is saved successfully eveything wwill be rolled back.
 *
 * @params array $data The data to overwrite any existing data (required)
 * @params bool $ignore_missing Won't validate stuff that is missing,
 * only used internally for updates
 * @returns bool The primary key on success, false on failure. If failure
 * you can get error messages using errors(). If you override this you
 * should return the same.
 */
    function save($data, $ignore_missing=false) {
        // Callback to permissionsToSave
        if(!$this->permissionsToSave($data)){
            return false;
        }
        // Reset errors
        $this->__errors = array();
        // Store values for onchange events
        $onchange_event_start_values = array();
        if($this->onchange_events){
            foreach($this->onchange_events as $field=>$method){
                $onchange_event_start_values[$field] = @$this->$field;
            }
        }
        // All done in a transaction
        if ($trans_started = miDb::beginTransaction()) {
            miResource::$save_has_many_index = false;
        }
        $rollback = false;
        $primary_key = $this->primaryKey();
        $primary_key_name = miDb::primaryKey($this->primary_table);
        $save_has_many = array();
        $save_has_one = array();
        $save_belongs_to = array();
        $primary_table_info = miDb::tableInfo($this->primary_table);

        // Automatically add timestamps
        if (isset($primary_table_info['created_on']) && !@$data['created_on']) {
            $data['created_on'] = @$this->created_on ?
                                    $this->created_on : date('Y-m-d H:i:s', miDate::now());
        }
        if (isset($primary_table_info['updated_on']) && !@$data['updated_on']) {
            $data['updated_on'] = date('Y-m-d H:i:s', miDate::now());
        }

        // Work out how to process the data
        $rollback_fields = array();
        foreach($data as $field => $value) {
            // Normal field
            if(!is_array($value)){
                $rollback_fields[$field] = $value;

            // Save has many
            } else if(strtolower($field{0}) != $field{0} &&
            AkInflector::is_plural($field) &&
            class_exists(AkInflector::modulize($field))){
                // Below it is checked if this can actually be saved
                $save_has_many[$field] = $value;
                unset($data[$field]);

            // Save has one / belongs to
            } else if(strtolower($field{0}) != $field{0} &&
            AkInflector::is_plural($field) == false){
                if(!@class_exists($field) && !($real_class = $this->getAssociateRealClass($field))){
                    trigger_error("[miResource->save] 1. No support for processing $field", E_USER_WARNING);
                    unset($data[$field]);
                }
                $assoc_table = AkInflector::tableize($field);
                $has_one_foreign_key = AkInflector::singularize($this->primary_table).'_id';
                $belongs_to_local_key = AkInflector::singularize($assoc_table).'_id';

                // Has one associate?
                if(@miDb::colInfo($assoc_table,$has_one_foreign_key)){
                    $save_has_one[$field] = $value;
                    unset($data[$field]);

                // Belongs to associate?
                } else if(@miDb::colInfo($this->primary_table,$belongs_to_local_key)){
                    $field = @$real_class ? $real_class : $field;
                    $save_belongs_to[$field] = array($belongs_to_local_key,$value);
                    unset($data[$field]);

                // Not understood
                } else {
                    trigger_error("[miResource->save] 2. No support for processing $field", E_USER_WARNING);
                    unset($data[$field]);
                }
            } else {
                trigger_error("[miResource->save] 3. No support for processing $field", E_USER_WARNING);
                unset($data[$field]);
            }
        }
        /*
         * Saving belongs to associates *before* the main resource is saved
         */
        if($save_belongs_to){
            foreach($save_belongs_to as $model=>$local_key_and_resource){
                list($local_key,$resource) = $local_key_and_resource;
                $aliased_as = AkInflector::modulize(preg_replace('/_id$/','',$local_key));
                // Look for existing to update or create new - key must be passed in data
                $class = get_class($this->$model);
                if (get_class($this->$model) == $model) {
                    $stub = $this->$model;
                } else {
                    $stub = new $model(false, false, $aliased_as);
                }
                if(is_numeric(@$data[$local_key])){
                    // Find resource currently referenced
                    $ob = $stub->findByPrimaryKey($this->$local_key);
                    // If an id was set but doesn't exist unset and continue
                    if(!$ob){
                        unset($data[$local_key]);
                        trigger_error("[miResource->save] Bad local key for $local_key",E_USER_NOTICE);
                        continue;
                    }
                } else {
                    $ob = $stub;
                }

                // Attempt save
                $result = $ob->save($resource);
                if(!$result){
                    $save_has_one_errors[] = $ob->errors();
                    $rollback = true;

                // If succesful make sure using the correct local key
                } else {
                    $data[$local_key] = $ob->primaryKey();
                }

                // Store as prop in case of rollback and no reload
                $this->$model = $ob;
            }
        }
        // Saving of the main resource
        if ($this->validate($data, $ignore_missing)) {
            // Get paramaters for query
            list($cols, $params) = $this->getParams($data);

            // Nothing to update? This should never happen so an internal error
            if(!$cols || !$params){
                trigger_error('[miResource->save] Weird, no paramaters to save',E_USER_ERROR);
                return false;
            }
            // Use UPDATE if already have a primary key
            if($primary_key){
                $qry = "UPDATE $this->primary_table SET \n";
                $updates = array();
                foreach($cols as $col) {
                    $updates[] = $col . '=' . ':' . $col;
                }
                $qry.= implode(",\n", $updates);
                list($where_sql, $where_params) = $this->primaryKeyParams();
                $params = array_merge($params, $where_params);
                $qry.= ' WHERE '.$where_sql;

            // Otherwise INSERT
            } else {
                $qry = "INSERT INTO $this->primary_table (" .
                   implode(",\n", $cols) . ")\n" .
                   " VALUES (" . implode(",\n", array_keys($params)) . ");";
            }

            // Execute the query
            $primary_saved = false;
            if(miDb::getResults($qry, $params)){
                /*
                 * Set auto_increment primary key to lastInsertId if present
                 * and no primary key already set. This needs to be unset
                 * if the rest of the save does not succeed.
                 */
                $primary_saved = true;
                $new_pk_assigned = false;
                if(!$this->primaryKey()){
                    $pk_inf = miDb::primaryKeyInfo($this->primary_table);
                    $pk_name = @$pk_inf['field_name'];
                    // Auto increment primary key
                    if(@$pk_inf['auto_increment']){
                        $this->{$pk_name} = miDb::lastInsertId();
                        $new_pk_assigned = true;

                    // Primary key set in data
                    // Could be multiple fields, in which case they must
                    // all be in data for the save to be successful
                    } else {
                        $pk_flds = miDb::primaryKey($this->primary_table);
                        if(is_array($pk_flds)){
                            foreach($pk_flds as $pk_fld){
                                $this->{$pk_fld} = $data[$pk_fld];
                            }
                        } else {
                            $this->{$pk_flds} = $data[$pk_flds];
                        }
                    }
                }
            } else {
                $rollback = true;
                trigger_error("[miResource] Resource could not be saved but
                    this was not caught by validation, very unusual.",
                    E_USER_WARNING);
            }
        } else {
            $rollback = true;
        }

        // Add errors from saving has one *after* main save so messages in a sensible order
        if(@$save_has_one_errors){
            foreach($save_has_one_errors as $errors){
                $this->errors($errors);
            }
        }
        /*
         * Saving has one associates
         */
        if($save_has_one){
            foreach($save_has_one as $model=>$resource){
                $foreign_key = AkInflector::singularize($this->primary_table).'_id';
                // Look for existing to update or create new
                $mock = new $model();
                if(!$ob = $mock->findOne($this->primaryKey(),$foreign_key)){
                    $ob = $mock;
                }
                // Make sure foreign key assigned to resource data
                $resource[$foreign_key] = $this->primaryKey();
                // Attempt save
                if(!$ob->save($resource)){
                    $this->errors($ob->errors());
                    $rollback = true;
                }
                // Store as prop in case of rollback and no reload
                $this->$model = $ob;
            }
        }

        /*
         * Saving has many associates
         */
        if($save_has_many){
            if(miDb::primaryKey($this->primary_table) != 'id'){
                // This primary table must have primary key 'id'
                $e = "Cannot save has many $func unless the table
                        $this->primary_table has a primary key named 'id'";
                $this->errors($e,'associates');
                trigger_error("[miResource->save] $e", E_USER_WARNING);
                $rollback = true;
                $save_has_many = array();
            }
            // Storage for rollback info
            $rollback_has_many = array();
            foreach($save_has_many as $model=>$resources){
                $model = AkInflector::modulize($model);
                $prop = AkInflector::pluralize($model);
                $table = AkInflector::tableize($model);
                $primary_key = miDb::primaryKey($table);
                $foreign_key = AkInflector::singularize($this->primary_table).'_id';
                $through_foreign_key = false;
                $real_model = false;
                $real_prop = false;

                // Add this primary key to the data as foreign key
                if($this->primaryKey()){
                    foreach($resources as $k=>$d){
                        $resources[$k][$foreign_key] = $this->primaryKey();
                    }
                }
                // Check model has correct foreign key
                $assoc_table_info = miDb::tableInfo($table);
                if(!$assoc_table_info || !isset($assoc_table_info[$foreign_key])){
                    // If not, check through table
                    if($through_table = miDb::detectThrough($this->primary_table, $table)){
                        // If exists switch to using that for model, prop and primary key
                        $real_model = $model;
                        $real_prop = $prop;
                        $model = AkInflector::modulize($through_table);
                        $prop = AkInflector::pluralize($model);
                        $primary_key = miDb::primaryKey($through_table);
                        $through_foreign_key = AkInflector::singularize($table).'_id';

                    } else {
                        $e = "$table has no association with $this->primary_table";
                        $this->errors($e,'associates');
                        trigger_error($e, E_USER_WARNING);
                        $rollback = true;
                        continue;
                    }
                }
                // The actual save
                $errors = array();
                if($this->primaryKey()){
                    // Get current instances of associate model, keyed by primary key
                    $current = $this->{$prop} ? miArray::keyBy($this->{$prop},$primary_key) : array();
                    $saved_primary_keys = array();
                    $j = 0;
                    $i = 0;
                    miResource::$save_has_many_index = -1;
                    foreach($resources as $resource_data){
                        miResource::$save_has_many_index++;
                        /*
                         * Automatically add 'ord' column value (will be
                         * ignored if not present in table)
                         */
                        if(!@$resource_data['ord']){
                            $resource_data['ord'] = ++$j;
                        }
                        /*
                         * Check current for existing by creating a mock from
                         * the passed data and see if it has a primary key
                         * which is present in current. Also tells us if new
                         * or not so knows whether to save or update below.
                         */
                        $mock = new $model($resource_data);
                        if($mock->primaryKey() && $current_ob = @$current[$mock->primaryKey()]){
                            $is_new = false;
                            $ob = $current_ob;
                        /*
                         * Otherwise it's new so save with this primary key for the
                         * child's foreign key (all other data would have been
                         * assigned when instantiated above)
                         */
                        } else {
                            $resource_data[$foreign_key] = $this->primaryKey();
                            $is_new = true;
                            $ob = new $model();
                        }
                        /*
                         * If this is a through relationship and no key for
                         * the resource on the other side of the relationship
                         * has been supplied then try to create a new resource
                         * on the other side with the data supplied if this
                         * has been enabled by setting $save_new_through = true
                         * on the through model
                         */
                        if($through_foreign_key && !@$resource_data[$through_foreign_key]){
                            // Check this is allowed (default is not)
                            if(!$ob->save_new_through){
                                $rollback = true;
                                $this->errors('No support for through processing '.$real_model,$real_model);
                                continue;
                            }
                            $new_ob = new $real_model();
                            if(!$new_id = $new_ob->save($resource_data)){
                                $this->errors($new_ob->errors());
                                $rollback = true;
                                continue;
                            }
                            $resource_data[$through_foreign_key] = $new_id;
                        }
                        // Save the object with the new data
                        if(!($pk = $ob->{$is_new ? 'save' : 'update'}($resource_data))){
                            $this->errors($ob->errors());
                            $errors[$i] = $ob->errors();
                            $rollback = true;
                        }
                        // Transfer any warnings to this parent object
                        if($ob->warnings()){
                            $this->warnings($ob->warnings());
                        }
                        /*
                         * Store primary key whether saved or not, but any
                         * not in data will be deleted below
                         */
                        $saved_primary_keys[] = $pk;
                        $i++;
                    }
                    // Delete anything from current that was not encountered above
                    if($current){
                        foreach($current as $ob){
                            // Skip rubbish that may have come in on a previous attempt to save
                            if(!$ob->primaryKey()) {
                                continue;
                            }
                            if(!in_array($ob->primaryKey(),$saved_primary_keys)){
                                if(!$ob->delete()){
                                    $rollback = true;
                                    $this->errors($ob->errors());
                                    $this->errors('Could not delete associated '.get_class($ob),
                                                    'associates');
                                    $errors[$i] = $ob->errors();
                                }
                            }
                        }
                    }
                } else {
                    $rollback = true;
                }
                /*
                 * Store information now in case we need to rollback below
                 * Adds a bit of overhead, but doesn't execute any queries
                 * unless something goes wrong
                 */
                if($resources){
                    $rollback_data = array('collection'=>false,
                                           'filter'=>false,
                                           'tmp_data'=>false,
                                           'errors'=>false);
                    if(@$real_model) {
                        /*
                         * With through associates prepare a collection to
                         * get them back if we need to rollback below
                         */
                        $rollback_prop = $real_prop;
                        // Suppress errors in case foreign key is missing (happens sometimes)
                        $tmp_data = @miArray::keyBy($resources, $through_foreign_key);
                        // Copy any properties in the existing item that were not POSTed
                        foreach($tmp_data as $k=>$td){
                            $tmp = new $model($td);
                            if(@$current[$tmp->primaryKey()]){
                                $tmp_data[$k] = array_merge($current[$tmp->primaryKey()]->getData(),$td);
                            }
                        }
                        // Create a new collection to show back
                        if($ids = array_diff(array_keys($tmp_data), array(0,''))){
                            $tmp_model = new $real_model();
                            $rollback_data['collection'] = $tmp_model->find();
                            $rollback_data['filter'] = array('id'=>'in '.implode(',',$ids));
                            $rollback_data['tmp_data'] = $tmp_data;
                        }
                    }
                    /*
                     * If no through association or above didn't produce a
                     * useful then create a collection of mock resources based
                     * on input data
                     */
                    if(!$rollback_data['collection']) {
                        $tmp_objects = array();
                        foreach($resources as $resource_data){
                            $tmp_objects[] = new $model($resource_data);
                        }
                        $rollback_data['collection'] = new miCollection(new $model(), $tmp_objects);
                        $rollback_prop = $prop;
                    }
                    // Save errors
                    if($errors){
                        $rollback_data['errors'] = $errors;
                    }
                    $rollback_has_many[$rollback_prop] = $rollback_data;
                }
            }
        }
        // Something went wrong so roll it all back
        if($rollback){
            /*
             * As called recursively with has many and has one, only rollback
             * if this call started the transaction
             */
            if($trans_started) {
                miDb::rollback();
            }
            if($rollback_fields){
                foreach($rollback_fields as $field=>$value){
                    $this->{$field} = $value;
                }
            }
            if(@$rollback_has_many){
                foreach($rollback_has_many as $prop=>$rollback_data){
                    $collection = $rollback_data['collection'];
                    // Set the filter (this only exists if through association)
                    if($rollback_data['filter']){
                        $collection->filter($rollback_data['filter']);
                        /*
                         * Make sure additional data applied back to collection
                         * Again, only applies if a through association
                         */
                        foreach($collection as $ob){
                            foreach($rollback_data['tmp_data'][$ob->id] as $k=>$v){
                                $ob->$k = $v;
                            }
                        }
                    }
                    // Copy errors to the new objects
                    if($errors = $rollback_data['errors']){
                        $i = 0;
                        foreach($collection as $ob){
                            if($e = @$errors[$i]){
                                $ob->errors($e);
                            }
                            $i++;
                        }
                    }
                    $this->$prop = $collection;
                    // Lock the collection so don't acidentally search for it again
                    $this->lockProp($prop);
                }
            }
            // Unset newly assigned primary key (can only ever be on one col)
            if(@$new_pk_assigned){
                $pk = miDb::primaryKey($this->primary_table);
                unset($this->$pk);
            }
            return false;
        }
        /*
         * As called recursively with has many and has one, only commit,
         * reload and update denorm formulas if this call started
         * the transaction
         */
        if($trans_started) {
            miDb::commit();
            $this->reload(@$new_pk_assigned);
            $this->updateDenormalizedFormulas();
        }
        // Force reload flag reloads even if this didn't start transaction -
        // hack for all or nothing batch operations
        elseif (miResource::$__force_reload) {
            $this->reload(@$new_pk_assigned);
            $this->updateDenormalizedFormulas();
        }
        /*
         * If not reloading then make sure the data is assigned to this
         * object, which is like a mock reload
         */
        else {
            foreach($rollback_fields as $field=>$value){
                $this->{$field} = $value;
            }
        }

        // Fire on change events
        if($this->onchange_events){
            foreach($this->onchange_events as $field=>$method){
                $start_value = $onchange_event_start_values[$field];
                if($start_value != $this->$field){
                    $this->$method($start_value, $this->$field, $field);
                }
            }
        }
        return $this->primaryKey();
    }
// ---------------------------------------------------------------------------
/**
 * Whilst save() requires an entire representation of a resource update()
 * allows setting of specific fields to specific values. In HTTP terms this
 * is a PATCH, while save is more like POST and PUT.
 *
 * This is pretty much the same as a save except the validator is told to
 * ignore missing values. May get unusual results if you try to update with
 * no values at all - best to make sure there is something or your table
 * has an `updated_on` column as that will always be automatically updated.
 *
 * Previously this func would only update the primary resource and ignore
 * associates (by generating it's own query). But this seemed like an
 * unneccesary constraint so removed. The only thing to remember is that it is
 * only missing values in the inital resource that are ignored, associates
 * must be fully valid.
 *
 * Another gotcha is that if you override the save() method in your model
 * remember to pass through the `ignore_missing` argument.
 *
 * @param array $data The data to save to the resource
 * @return The primary key of the resource on success, or false on failure.
 */
    function update($data) {
        if(!$this->primaryKey()){
            $this->errors('Cannot update resource that has not been saved');
            return false;
        }
        $result = $this->save($data, true);
        return $result;
    }
// ---------------------------------------------------------------------------
/**
 * Deletes a resource from the database. Note the object itself isn't
 * destroyed so could be cached somewhere for trash/undo functionality
 */
    function delete() {

        if(!$this->primaryKey()){
            $this->errors('Cannot delete as not saved');
            return false;
        }
        if(!$this->permissionsToDelete()){
            return false;
        }
        // Store values for onchange events
        $onchange_event_start_values = array();
        if($this->onchange_events){
            foreach($this->onchange_events as $field=>$method){
                $onchange_event_start_values[$field] = @$this->$field;
            }
        }
        $rollback = false;
        $trans_started = miDb::beginTransaction();

        // Delete associates; these need to be explicitly defined in the model
        if($this->delete_cascade){
            foreach($this->delete_cascade as $prop){
                if(!$this->$prop) {
                    continue;
                }
                if(AkInflector::is_plural($prop)){
                    foreach($this->$prop as $resource){
                        if(!$resource->delete()){
                            $this->errors("Could not delete $prop",$prop);
                            $rollback = true;
                        }
                    }
                } else {
                    if(!$this->$prop->delete()){
                        $this->errors("Could not delete $prop",$prop);
                        $rollback = true;
                    }
                }
            }
        }
        list($where_sql, $where_params) = $this->primaryKeyParams();
        if(!$where_sql || !$where_params){
            $e = "No primary key to delete from $this->primary_table";
            $this->errors($e);
            trigger_error("[miResource] $e",E_USER_WARNING);
            $rollback = true;
        }
        $qry = "DELETE FROM $this->primary_table WHERE ".$where_sql;
        if(!miDb::getResults($qry, $where_params)){
            $c = get_class($this);
            $e = "Could not delete $c although no idea why";
            $this->errors($e,$c);
            trigger_error("[miResource] $e",E_USER_WARNING);
            $rollback = true;
        }
        if(!$this->updateDenormalizedAssociateFormulas()){
            $rollback = true;
        }

        if($rollback){
            if($trans_started) {
                miDb::rollback();
            }
            return false;
        }
        if($trans_started){
            miDb::commit();
        }
        if($this->onchange_events){
            foreach($this->onchange_events as $field=>$method){
                if($onchange_event_start_values[$field]){
                    if(!$this->$method($onchange_event_start_values[$field], null, $field)){
                        // Just continue - thing has been deleted anyway so probably shouldn't worry
                        continue;
                    }
                }
            }
        }
        return true;
    }
// ---------------------------------------------------------------------------
/**
 * Force reload of the data in the resource to current state of database
 * @param $new_pk Passed internally from save to reload, so if you want to
 * override to do something special when a new thing saved, so it in reload
 * and check for a new pk (like update a fulltext index or something)
 * @return bool
 */
    function reload($new_pk=false) {
        // Can only reload if saved
        if(!$pk = $this->primaryKey()){
            return false;
        }

        // Clear cache
        unset(miResource::$__object_cache[get_class($this)][$pk]);

        // Get data
        $model_class = get_class($this);
        $model = new $model_class(false, $this->primary_table);
        $tmp = $model->findByPrimaryKey($pk, false, true);
        if(!$tmp){
            trigger_error("[miResource] Could not find ".get_class($this)." with primary key ".
                            $pk." to reload!", E_USER_ERROR);
            return false;
        }
        // Assign data to this
        $this->__data = array();
        if($data = $tmp->getRawData()){
            $this->__raw_data = $data;
        } else {
            $this->__raw_data = array();
        }
        // Clear locked props
        $this->__locked_props = array();
    }
// ---------------------------------------------------------------------------
/**
 */
    private function setMessage($type, $value, $key) {
        if (is_array($value)) {
            foreach($value as $value2) {
                $this->setMessage($type, $value2[0], $value2[1]);
            }
        } else if ($value) {
            /*if (is_string($key)) {
                $this->{$type}[$key] = $value;
            } else {*/
                $this->{$type}[] = array($value, $key);
            //}
        }
        return $this->$type;
    }
// ---------------------------------------------------------------------------
/**
 */
    function errors($value = false, $key = false) {
        return $this->setMessage('__errors', $value, $key);
    }
// ---------------------------------------------------------------------------
/**
 */
    function getError($id) {
        if (!stristr($id, '.')) {
            $id = get_class($this).'.'.$id;
        }
        $id = str_replace('.','_',$id);
        $tmp = array();
        foreach($this->__errors as $e){
            if($e[1] == $id){
                $tmp[] = $e[0];
            }
        }
        return $tmp;
    }
// ---------------------------------------------------------------------------
/**
 */
    function errorFields() {
        return array_keys($this->__errors);
    }
// ---------------------------------------------------------------------------
/**
 */
    function resetErrors() {
        $this->__errors = array();
    }
// ---------------------------------------------------------------------------
/**
 */
    function warnings($value = false, $key = false) {
        return $this->setMessage('__warnings', $value, $key);
    }
// ---------------------------------------------------------------------------
/**
 */
    function getWarning($id) {
        $tmp = array();
        foreach($this->__warnings as $e){
            if($e[1] == $id){
                $tmp[] = $e[0];
            }
        }
        return $tmp;
    }
// ---------------------------------------------------------------------------
/**
 */
    function warningFields($value = false, $key = false) {
        return array_keys($this->__warnings);
    }
// ---------------------------------------------------------------------------
/**
 */
    function resetWarnings() {
        $this->__warnings = array();
    }

// ---------------------------------------------------------------------------
// Validation
// ---------------------------------------------------------------------------
    function validate($data, $ignore_missing=false){
        // Add rules based on table info
        $this->tableInfoToRules();
        $bad_flds = array();
        $human_ob = AkInflector::humanize(get_class($this));
        if (miResource::$save_has_many_index > 0){
            $err_index = '_' . miResource::$save_has_many_index;
        }
        else {
            $err_index = '';
        }
        // Do required first
        if(@$this->__rules['Required']){
            foreach($this->__rules['Required'] as $fld){
                // Human names
                $human_fld = AkInflector::humanize($fld);
                // Error id
                $err_id = get_class($this) . '_' . $fld . $err_index;
                // If col not present and OK to ignore missing then continue
                if(!array_key_exists($fld, $data) && $ignore_missing){
                    continue;
                }
                // Required fields must exist and not be an empty string
                if (!array_key_exists($fld,$data) || trim($data[$fld]) === '' || $data[$fld] === null) {
                    $bad_flds[] = $fld;
                    $msg_tpl = $this->getErrorMessageTemplate('Required',get_class($this),$fld);
                    $msg = sprintf($msg_tpl,$human_ob,$human_fld);
                    $this->errors($msg,$err_id);
                }
            }
            // Don't want to go over these again
            unset($this->__rules['Required']);
        }
        // Now everything else
        foreach($this->__rules as $keyword => $flds){
            $fun = 'validate'.$keyword;
            if(!method_exists($this, $fun)){
                trigger_error('Cannot validate '.$fun,E_USER_WARNING);
                return false;
            }
            if(is_string($flds)){
                $flds = (array)$flds;
            }
            foreach($flds as $fld){
                // Skip fields that have already failed validation
                if(in_array($fld,$bad_flds)){
                    continue;
                }
                /*
                 * If the field is not set or empty then skip - if it was
                 * required it would already have been caught above,
                 * no point validating an empty string or null value
                 */
                $v = @$data[$fld];
                if(!array_key_exists($fld, $data) || $v === '' || $v === null){
                    continue;
                }
                // Human field name
                $human_fld = AkInflector::humanize($fld);
                // Error id
                $err_id = get_class($this) . '_' . $fld . $err_index;
                // Call validation func passing the value, column info and the rest of the data
                $res = call_user_func(array($this,$fun),
                            $v, miDb::colInfo($this->primary_table, $fld), $data);
                // If res is strictly true then OK
                if(true === $res){
                    continue;
                }
                // Arguments to sprintf
                $args = array();
                // If is a string returned use it for error message template
                if(is_string($res)){
                    $args[] = $res;
                // Otherwise get the error message using conventions
                } else {
                    $args[] = $this->getErrorMessageTemplate($keyword,
                                    get_class($this), $fld);
                }
                // Add generic paramaters
                $args[] = $human_ob;
                $args[] = $human_fld;
                $args[] = $v;
                // If returned array then add as extra paramaters
                if(is_array($res)){
                    $args = array_merge($args, $res);
                }
                // sprintf the message
                $msg = call_user_func_array('sprintf', $args);
                // Store error message
                $this->errors($msg, $err_id);
            }
        }
        return $this->errors() ? false : true;
    }
// ---------------------------------------------------------------------------
    function addRule($fld, $keyword){
        $this->__rules[$keyword][] = $fld;
    }
// ---------------------------------------------------------------------------
    function getErrorMessageTemplate($keyword,$model_name,$fld_name){
        $fld = strtoupper($fld_name);
        $model = strtoupper(AkInflector::underscore($model_name));
        $keyword = strtoupper(AkInflector::underscore($keyword));
        $try = array($model.'_'.$fld.'_ERR_FLD_'.$keyword,
                     $model.'_ERR_FLD_'.$keyword,
                     'ERR_FLD_'.$keyword);
        if($this->aliased_as){
            $alias = strtoupper(AkInflector::underscore($this->aliased_as));
            $try = array_merge(array($alias.'_'.$fld.'_ERR_FLD_'.$keyword,
                         $alias.'_ERR_FLD_'.$keyword), $try);
        }
        foreach($try as $t){
            if(defined($t)){
                return constant($t);
            }
        }
        trigger_error("No error message defined in {$try[0]}, {$try[1]}, {$try[2]} or $keyword",E_USER_NOTICE);
        return'%1$s %2$s is not valid';
    }
// ---------------------------------------------------------------------------
    function validateNumeric($v){
        return is_numeric($v);
    }
// ---------------------------------------------------------------------------
    function validateAlphaNumeric($v){
        return preg_match('/^[A-Za-z0-9 ]+$/',$v);
    }
// ---------------------------------------------------------------------------
    function validateAlphaNumericNoSpaces($v){
        return preg_match('/^[A-Za-z0-9]+$/',$v);
    }
// ---------------------------------------------------------------------------
    function validateAlpha($v){
        return preg_match('/^[A-Za-z]+$/',$v);
    }
// ---------------------------------------------------------------------------
    function validateInteger($v){
        return (int)$v === $v;
    }
// ---------------------------------------------------------------------------
    function validateGtZero($v){
        return $v > 0;
    }
// ---------------------------------------------------------------------------
    function validateLtZero($v){
        return $v < 0;
    }
// ---------------------------------------------------------------------------
    function validateEmail($v){
        $rx = '/\S+@\S+\.\S{2,}/';
        //$rx = '/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i';
        return preg_match($rx,$v) ? true : false;
    }
// ---------------------------------------------------------------------------
    function validateDate($v){
        return miDate::strToTime($v) !== false;
    }
// ---------------------------------------------------------------------------
    function validateFuture($v){
        return miDate::strToTime($v) >= miDate::now();
    }
// ---------------------------------------------------------------------------
    function validatePast($v){
        return miDate::strToTime($v) <= miDate::now();
    }
// ---------------------------------------------------------------------------
    function validateLength($v, $col_info){
        if (strlen($v) > $col_info['maxlen']) {
            return array($col_info['maxlen']);
        }
        return true;
    }
// ---------------------------------------------------------------------------
    function validateUnique($v, $col_info){
        $fld = $col_info['field_name'];
        $qry = "SELECT id FROM $this->primary_table WHERE $fld=:$fld";
        $params = array(":$fld"=>$v);
        if (@$this->id) {
            $qry .= ' AND id != :id';
            $params[':id'] = $this->id;
        }
        $qry .= ' LIMIT 0,1';
        return miDb::getVar($qry, $params) ? false : true;
    }
// ---------------------------------------------------------------------------
    function tableInfoToRules(){
        $table_info = miDb::tableInfo($this->primary_table);
        foreach($table_info as $fld=>$info){
            if($info['auto_increment']){
                continue;
            }
            if(@$info['required']){
                $this->addRule($fld,'Required');
            }
            if($info['type'] == 'number'){
                $this->addRule($fld,'Numeric');
            }
            if($info['type'] == 'date'){
                $this->addRule($fld,'Date');
            }
            if($info['maxlen']){
                $this->addRule($fld,'Length');
            }
            if($info['unique']){
                $this->addRule($fld,'Unique');
            }
        }
    }
// ---------------------------------------------------------------------------
    private function getParams($data) {
        $primary_table_info = miDb::tableInfo($this->primary_table);
        $i = 0;
        foreach($primary_table_info as $col => $info) {
            if (array_key_exists($col,$data)) {
                $cols[] = $col;
                if ($info['sql_null_allowed'] && $data[$col] === '') {
                    $params[':' . $col] = null;
                } else if ($info['type'] == 'date' && $data[$col]) {
                    $params[':' . $col] = date('Y-m-d H:i:s',
                                            miDate::strToTime($data[$col])
                                        );
                } else {
                    $params[':' . $col] = $data[$col];
                }
                $i++;
            }
        }
        return array($cols, $params);
    }
// ---------------------------------------------------------------------------
    function getNonEmpty($values) {
        $tmp = array();
        foreach($values as $v) {
            if ($this->$v) {
                $tmp[$v] = $this->$v;
            }
        }
        return $tmp;
    }
// ---------------------------------------------------------------------------
    function getData() {

        // Access all associates so there data is parsed out of __raw_data
        foreach($this->__raw_data as $k=>$v){
            $doll_pos = strpos($k,'$');
            if($doll_pos !== false){
                $r = substr($k,0,$doll_pos);
                if(!isset($this->$r)){
                    $make_it_so = $this->$r;
                }
            } else {
                $make_it_so = $this->$k;
            }
        }
        // Put it all in a regular array
        $data = array();
        $ignore_has_many = array();
        foreach($this->__data as $k=>$v){
            if(@get_class($v) && method_exists($v,'getData')){
                // Recursively call getData
                $data[$k] = $v->getData();
            } else if (@get_class($v) == 'miCollection'){
                foreach($v as $v2){
                    $data[$k][] = $v2->getData();
                    $ignore_has_many[] = $k;
                }
            } else {
                $data[$k] = $v;
            }
        }
        /*
         * Get has many associates. If not sure if they're present because
         * may not have been accessed this can be forced by passing the model
         * names as arguments to this function
         */
        $has_many = func_get_args();
        if($has_many && is_array($has_many)){
            foreach($has_many as $hm){
                if(in_array($hm, $ignore_has_many)){
                    continue;
                }
                foreach($this->{$hm}() as $v){
                    $data[$hm][] = $v->getData();
                }
            }
        }

        return $data;
    }
// ---------------------------------------------------------------------------
    function getRawData($field=false) {
        if($field){
            return @$this->__raw_data[$field];
        }
        return $this->__raw_data;
    }
// ---------------------------------------------------------------------------
    function lockProp(){
        $args = func_get_args();
        $this->__locked_props = array_merge($this->__locked_props,$args);
    }
// ---------------------------------------------------------------------------
    function unlockProp(){
        $args = func_get_args();
        if(!$args){
            $this->__locked_props = array();
        }
        $this->__locked_props = array_diff($this->__locked_props,$args);
    }
// ---------------------------------------------------------------------------
/**
 * If the model has an aliased class, like $assoc = array('Foo(Bar)') where
 * Foo is the real class name and Bar is the alias, this will return the
 * real class name e.g. getAssociateRealClass('Bar') will return 'Foo'.
 * @param string $alias_class
 * @return string The real class name
 */
    function getAssociateRealClass($alias_class){
        $assoc = preg_grep("/(^|\.)[A-Za-z]+\($alias_class\)($|\.)/",$this->__associates);
        if($assoc && $assoc_str = array_shift($assoc)){
            list($real_class,$junk) = explode('(',$assoc_str);
            if(class_exists($real_class)){
                return $real_class;
            }
        }
        return false;
    }
// ---------------------------------------------------------------------------
    function __set($prop, $value) {
        if(in_array($prop,$this->__locked_props)){
            trigger_error("[miResource] Cannot set locked property '$prop'",E_USER_NOTICE);
            return false;
        }
        $this->__data[$prop] = $value;
    }
// ---------------------------------------------------------------------------
    function __get($prop) {
        /*
         * If the property accessed is a plural and is not all in
         * lowercase then assumed attempt at accessing a has many
         * associate, so invoked as a method to provoke __call below
         */
        if(AkInflector::is_plural($prop) && strtolower($prop) != $prop){
            return $this->$prop();
        }

        // Already set and put into data
        if (array_key_exists($prop, $this->__data)) {
            return $this->__data[$prop];
        }

        // If in raw data, copy to data and return
        if(array_key_exists($prop, $this->__raw_data)){
            $this->__data[$prop] = $this->__raw_data[$prop];
            return $this->__data[$prop];
        }

        // If a formula not already set, lazily evaluate it
        if(($formula = $this->formulas($prop)) && $this->primaryKey()){
            list($where, $params) = $this->primaryKeyParams();
            $qry = "SELECT $formula FROM $this->primary_table WHERE $where";
            $value = miDb::getVar($qry,$params);
            $this->__data[$prop] = $value;
            return $value;
        }
        /*
         * Otherwise probably trying to access a sub-resource...
         * Look for matching field names in raw data i.e. if the $prop
         * was 'Customer' then look for all raw data fields
         * beginning 'Customer$' - but quick check first to make sure
         * correctly cased
         */
        if(strtolower($prop) == $prop){
            return null;
        }
        $trans_data = array();
        // Look for data already loaded
        $at_least_one_non_null_value = false;
        foreach($this->__raw_data as $k => $v) {
            if (preg_match('/^' . $prop . '\$(.+)/', $k, $matches)) {
                if(!is_null($v)){
                    $at_least_one_non_null_value = true;
                }
                $trans_data[$matches[1]] = $v;
            }
        }
        // Find a model that matches the $prop
        if (!@class_exists($prop)){
            if($real_prop = $this->getAssociateRealClass($prop)) {
                $prop = $real_prop;
            } else {
                trigger_error("No class for $prop", E_USER_WARNING);
                return null;
            }
        }
        $this->__data[$prop] = new $prop($trans_data);
        /*
         * Return the instance - if there are further implied __gets
         * they will be called on the returned instance e.g.
         * $this->Order->Customer->Country->printable_name
         * ...would invoke this method on Order, then Customer, then Country
         */
        return $this->__data[$prop];
    }
// ---------------------------------------------------------------------------
    function __isset($prop) {
        if(isset($this->__data[$prop]) || isset($this->__raw_data[$prop])){
            return true;
        }
        return false;
    }
// ---------------------------------------------------------------------------
    function __unset($prop) {
        if(@$this->__data[$prop]){
            unset($this->__data[$prop]);
        }
        if(@$this->__raw_data[$prop]){
            unset($this->__raw_data[$prop]);
        }
    }
// ---------------------------------------------------------------------------
    function __call($func, $args) {

        // Won't work unless primary table has a primary key called 'id'
        if(miDb::primaryKey($this->primary_table) != 'id'){
            trigger_error("[miResource] Cannot get has many $func unless the
                table $this->primary_table has a primary key named 'id'",
                E_USER_WARNING);
            return false;
        }
        // Create collection if not already stored
        if(!($coll = @$this->__data[$func])){
            // Nothing if this doesn't already have a primary key
            if(!$this->primaryKey()){
                return false;
            }
            if(!AkInflector::is_plural($func)){
                trigger_error("Method missing ".get_class($this).'::'.$func,
                    E_USER_ERROR);
                return false;
            }
            $model_name = AkInflector::modulize($func);
            // Create the model, will use miResource model if not defined
            if(!class_exists($model_name)){
                trigger_error("[miResource] Has many subclass $model is
                        missing", E_USER_ERROR);
                return false;
            } else {
                $model = new $model_name();
                $table = $model->primary_table;
            }
            $foreign_key = AkInflector::singularize($this->primary_table).'_id';
            $through_table = false;

            // Check has many associate is valid
            $ti = miDb::tableInfo($table);
            if(!$ti){
                trigger_error("[miResource] $this->primary_table related table
                                $table does not exist", E_USER_ERROR);
                return false;
            }
            // If no foreign key look for through table
            if(!isset($ti[$foreign_key])
            && !$through_table = miDb::detectThrough($this->primary_table,
                                     $table)){
                trigger_error("[miResource] $this->primary_table has no
                     through table $through_table to $table", E_USER_ERROR);
                return false;
            }

            // Remove any belongs_to associates that point back to this parent class
            /*$reassoc_this = false;
            if($assoc = $model->associates()){
                $this_model = AkInflector::modulize($this->primary_table);
                foreach($assoc as $a){
                    if($this->initialRel($a) == $this_model){
                        $model->disassociate($a);
                        $reassoc_this = true;
                    }
                }
            }*/
            // Add through table to the model
            if($through_table){
                $model->through_table = $through_table;
                $filter_on = AkInflector::modulize($through_table).'.'.$foreign_key;
            } else {
                $filter_on = $foreign_key;
            }
            /*
             * Store the column to filter on so can make sure it is always
             * applied when you retrieve this collection (so you can't
             * accidentally remove it with unfilter()
             */
            $this->__has_many_filters[$func] = $filter_on;
            $coll = $model->find();
            $coll->pageSize(0);
            // Reassociate to this parent if needs be
            /*if ($reassoc_this && $coll && $coll->count()) {
                foreach ($coll as $r) {
                    $r->{$this_model} = $this;
                }
            }  */
            $this->__data[$func] = $coll;
        }
        // Make sure saved filter is set
        if(@$this->__has_many_filters[$func] &&
        !in_array($func,$this->__locked_props) &&
        !$coll->getFilters($this->__has_many_filters[$func])){
            $coll->filter($this->__has_many_filters[$func], $this->id);
        }
        return $coll;
    }
// ---------------------------------------------------------------------------
/*
 * This is used by miCollection->loadHasMany to make sure that when it
 * collates collections it is able to preset the filter (see that function
 * for more details)
 */
    function hackHasManyFilter($has_many, $filter_on){
        $this->__has_many_filters[$has_many] = $filter_on;
    }
}
