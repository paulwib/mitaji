<?php


class miJson implements Iterator {

    const MODE_COLLECTION = 0, MODE_RESOURCE = 1;
    public $db_path = '';
    private $db = array(), $resources = array(),
            $file = '', $mode = miJson::MODE_RESOURCE;
// -----------------------------------------------------------------------
/**
 * When invoking constructor to create a new resource you can optionally
 * pass an array of data to populate the new resource.
 * @param array $data An array of data to populate a new instance, defaults
 * to an empty array (no data)
 * @param int $mode Whether to treat the instance as a collection or
 * individual resource, defaults to resource. This can be considered a
 * private argument as only used by the find() static method.
 */
    function __construct($data=array(), $mode=1){
        if(!$this->openDb()){
            return;
        }
        $this->mode = $mode;
        $this->resources = $this->db;
        // Create new resource
        if($mode == 1 && is_array($data)){
            array_unshift($this->db, (object) $data);
            $this->resources = array($this->db[0]);
        }
    }
// -----------------------------------------------------------------------
/**
 * Internal function to check can connect to the DB i.e. file can be opened,
 * read and written to.
 */
    private function openDb(){
        $file = $this->db_path.AkInflector::tableize(
                        strtolower(get_class($this))
                    ).'.json';
        if(!is_file($file)){
            if(!is_writable(dirname($file))){
                trigger_error("[miJson] Cannot read/write to file: $file",
                    E_USER_ERROR);
                return false;
            }
            touch($file);
        } else if (!is_writable($file)){
            trigger_error("[miJson] Cannot write to file: $file",
                    E_USER_WARNING);
        }
        $str = trim(file_get_contents($file));
        $json = json_decode($str);
        // If file not empty and json not an array, must be syntax problem
        if($str && !is_array($json)){
            trigger_error("[miJson] File does not contain JSON array: $file",
                    E_USER_ERROR);
                return false;
        }
        $this->file = $file;
        $this->db = $json ? $json : array();
        return true;
    }
// -----------------------------------------------------------------------
/**
 * Save the object to the database
 * @param array $data New data for the resource(s), like a shortcut to set
 * a load of new properties at once from post data for example.
 * @return mixed The objects id(s) if successful, or false if not successful
 */
    function save($data=false){
        if($data){
            foreach($data as $k=>$v){
                $this->$k = $v;
            }
        }
        foreach($this->resources as $resource){
            if(!$resource->id){
                $resource->id = uniqid();
                $resource->created_on = time();
            }
            $resource->updated_on = time();
        }
        if($this->saveDb()){
            return $this->id;
        }
        return false;
    }
// -----------------------------------------------------------------------
    function saveDb() {
        $s = str_replace('},',"},\n", json_encode(array_values($this->db)));
        return file_put_contents($this->file, $s);
    }
// -----------------------------------------------------------------------
/**
 * Add new resources to a collection. Will not be saved until you call
 * save() on the collection
 */
    function add($data){
        $ob = (object) $data;
        $this->resources[] = $ob;
        $this->db[] = $ob;
    }
// -----------------------------------------------------------------------
/**
 * Find a collection of resources
 * @param mixed $filter An array or object containing filter paramaters. This is
 * optional and if omitted will just find all objects. Can be filtered further
 * using the filter() method
 * @param string $c The class to create - this is just because PHP < 5.3.0
 * doesn't support late static binding, so you have to define the child classes
 * like:
    static function find($filter=false) {
        return parent::find($filter, __CLASS__);
    }
 * @return object A collection of resources
 */
    static function find($filter=false, $c=__CLASS__){
        $coll = new $c(false, miJson::MODE_COLLECTION);
        if($filter){
            $coll->filter($filter);
        }
        return $coll;
    }
// -----------------------------------------------------------------------
/**
 * Find a single resource
 * @param mixed $filter An array or object containing filter paramaters. This is
 * optional and if omitted will just find the first object.
* @param string $c The class to create - this is just because PHP < 5.3.0
 * doesn't support late static binding, so you have to define the child classes
 * like:
    static function findOne($filter=false) {
        return parent::findOne($filter, __CLASS__);
    }
 * @return mixed The object if found, or false if not found
 */
    static function findOne($filter=false, $c=__CLASS__){
        $coll = new $c(false, miJson::MODE_RESOURCE);
        if($filter){
            $coll->filter($filter);
        }
        return $coll->count() == 1 ? $coll : false;
    }
// -----------------------------------------------------------------------
/**
 * Filter a collection of resources by given paramaters
 * @param mixed $filter An array or object containing filter paramaters.
 * @return int The number of matched resources
 */
    function filter($filter){
        if(!$this->db) return false;
        $i = 0;
        $matched = array();
        foreach($this->resources as $ob){
            $match = true;
            foreach($filter as $prop=>$value){
                if($ob->$prop != $value){
                    $match = false;
                    break;
                }
            }
            if($match){
                $matched[] = $ob;
                $i++;
            }
        }
        $this->resources = $matched;
        return $i;
    }
// -----------------------------------------------------------------------
/**
 * Order a collection by a particular field
 * @param string $field The name of the field to order by
 * @param int $dir The direction to order by 0 for ascending, 1 for descending
 */
    function orderBy($field, $dir=0){
        miArray::mdsort($this->resources, $field, $dir);
    }
// -----------------------------------------------------------------------
/*
 * Delete all the current resources
 * @return bool Delete success
 */
    function delete(){
        $delete_ids = miArray::extractByKey($this->resources,'id');
        foreach($this->db as $k=>$resource){
            if(in_array($resource->id, $delete_ids)){
                unset($this->db[$k]);
            }
        }
        return $this->saveDb();
    }
// -----------------------------------------------------------------------
    function count(){
        return count($this->resources);
    }
// -----------------------------------------------------------------------
/**
 * Validate all the current resources against a given criteria
 */
    function validate(){

    }
// -----------------------------------------------------------------------
/**
 * Iterator rewind
 */
    function rewind() {
        if($this->mode == miJson::MODE_RESOURCE){
            reset($this->resources[0]);
        } else {
            reset($this->resources);
        }
    }
// -------------------------------------------------------------------------
/**
 * Iterator current
 */
    function current() {
        if($this->mode == miJson::MODE_RESOURCE){
            return current($this->resources[0]);
        } else {
            return current($this->resources);
        }
    }
// -------------------------------------------------------------------------
/**
 * Iterator key
 */
  function key() {
        if($this->mode == miJson::MODE_RESOURCE){
            return key($this->resources[0]);
        } else {
            $current = $this->current();
            return $current->id;
        }
    }
// --------------------------------------------------------------------------
/**
 * Iterator next
 */
    function next() {
        if($this->mode == miJson::MODE_RESOURCE){
            return next($this->resources[0]);
        } else {
            return next($this->resources);
        }
    }
// -------------------------------------------------------------------------
/**
 * Iterator valid
 */
    function valid() {
        return $this->current() !== false;
    }
// ----------------------------------------------------------------------------
/**
 * Iterator get
 */
    function get($index){
        if($this->mode == miJson::MODE_RESOURCE){
            return $this->resources[0]->$index;
        } else {
            return $this->resources[$index];
        }
    }
// -----------------------------------------------------------------------
    function __get($prop){
        if($this->mode == miJson::MODE_RESOURCE){
            return @$this->resources[0]->$prop ? $this->resources[0]->$prop : null;
        }
        return miArray::extractByKey($this->resources, AkInflector::singularize($prop));
    }
// -----------------------------------------------------------------------
    function __set($prop, $value){
        foreach($this->resources as $resource){
            $resource->$prop = $value;
        }
    }
// -----------------------------------------------------------------------
    function __isset($prop){
        if($this->mode == miJson::MODE_RESOURCE){
            return isset($this->resources[0]->$prop);
        }
        $a = miArray::extractByKey($this->resources, AkInflector::singularize($prop));
        return empty($a) ? false : true;
    }
// -----------------------------------------------------------------------
    function __unset($prop){
        if($this->mode == miJson::MODE_RESOURCE){
            unset($this->resources[0]->$prop);
        }
        foreach($this->resources as $resource){
            unset($resource->$prop);
        }
    }
// -----------------------------------------------------------------------
    function __toString(){
        if($this->mode == miJson::MODE_RESOURCE){
            return json_encode($this->resources[0]);
        }
        return json_encode($this->resources);
    }
// -----------------------------------------------------------------------
}

