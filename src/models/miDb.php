<?php

// General error messages
define('MIDB_ERR_NO_CONFIG', 'No configuration, cwd is %s');
define('MIDB_ERR_BAD_CONFIG', 'Configuration %s could not be parsed as JSON');
define('MIDB_ERR_CONNECT', 'Connection error: %s');
define('MIDB_ERR_SQL', 'SQL Query error: %3$s (%1$s %2$s)');
define('MIDB_ERR_NO_PREPARE', 'Could not prepare query');
define('MIDB_ERR_TABLE_MISSING', 'Table "%s" does not exist');
define('MIDB_ERR_UNKNOWN_DRIVER', 'Database driver "%s" is not known');
define('MIDB_ERR_UNKNOWN_TYPE', 'Field type "%s" is not known');
define('MIDB_ERR_NO_MIGRATIONS', 'No migrations available');
define('MIDB_ERR_ALL_MIGRATIONS_RUN', 'All migrations have been run');
define('MIDB_ERR_ADD_TABLE_EXIST', 'Cannot add table "%s" as already exists ' .
    'Use db::alter if you want to change it');
define('MIDB_ERR_MIGRATION_EXIST', 'There is already a migration to %s "%s" ' .
    'Either run the migration with "db::migrate" or delete it ' .
    '(possibly using db::unmigrate if it was the last thing you did)');
define('MIDB_ERR_COL_EXIST', 'Cannot %s %s as column already exists');
define('MIDB_ERR_COL_NOT_EXIST', 'Cannot %s %s as column does not exist');
define('MIDB_ERR_MIGRATE_FAILED', 'Migration failed on file "%s"');

// Warning messages
define('MIDB_WARN_SLOW_QUERY', "Slow query, took %s milliseconds \n%s");
define('MIDB_WARN_NO_CACHE', 'Could not create cache "%s" for database');

// DSN Templates
define('MIDB_DSN_MYSQL_NO_DB', 'mysql:host=%s;');
define('MIDB_DSN_MYSQL', 'mysql:host=%s;dbname=%s');

// Shell commands and errors
define('MIDB_DUMP_MYSQL', 'mysqldump -u%1$s -p%2$s %3$s');
define('MIDB_DUMP_NO_PASS_MYSQL', 'mysqldump -u%1$s %2$s');
define('MIDB_DUMP_TABLE_MYSQL', 'mysqldump -u%1$s -p%2$s %3$s %4$s');
define('MIDB_DUMP_TABLE_WHERE_MYSQL', 'mysqldump -u%1$s -p%2$s %3$s %4$s ' .
    '--where="%5$s" --add-drop-table=false --no-create-info=true');
define('MIDB_LOAD_MYSQL', 'mysql -u%1$s -p%2$s %3$s < %4$s');
define('MIDB_ERR_NO_DUMP', 'No dump program for driver "%s"');
define('MIDB_ERR_NO_LOAD', 'No load program for driver "%s"');

// CLI help
define('MIDB_CREATE_CLI_HELP', 'Create a database');
define('MIDB_MIGRATE_CLI_HELP', 'Run all migrations on your database');
define('MIDB_UNMIGRATE_CLI_HELP', 'Undo the last migration on your database ' .
    '(may have to call multiple times to undo a lot of migrations)');

// Query templates
define('MIDB_CREATE', 'CREATE DATABASE IF NOT EXISTS %1$s CHARACTER SET %2$s;');
define('MIDB_MIGRATE_PHP', '<?php
class Migration_%1$s {
    protected $apply_qry = "%2$s";
    protected $revert_qry = "%3$s";

    // Apply changes
    public function apply(){
        miDb::beginTransaction();
        if (miDb::getResults($this->apply_qry)) {
            miDb::commit();
            return true;
        }
        else {
            miDb::rollback();
            return false;
        }
    }

    // Revert changes
    public function revert(){
        miDb::beginTransaction();
        if (miDb::getResults($this->revert_qry)) {
            miDb::commit();
            return true;
        }
        else {
            miDb::rollback();
            return false;
        }
    }
}
');
define('MIDB_ADD_TABLE', 'CREATE TABLE IF NOT EXISTS `%1$s`
    (
        %2$s
    )
    ENGINE=InnoDB;');
define('MIDB_DROP_TABLE', 'DROP TABLE %1$s;');
define('MIDB_ADD_COLUMN_AFTER', 'ALTER TABLE %1$s ADD %2$s AFTER %3$s;');
define('MIDB_ADD_COLUMN', 'ALTER TABLE %1$s ADD %2$s;');
define('MIDB_ALTER_COLUMN', 'ALTER TABLE %1$s CHANGE `%2$s` %3$s;');
define('MIDB_DROP_COLUMN', 'ALTER TABLE %1$s DROP COLUMN %2$s;');
define('MIDB_ADD_INDEX', 'ALTER TABLE %1$s ADD INDEX (%2$s);');
define('MIDB_DROP_INDEX', 'ALTER TABLE %1$s DROP INDEX %2$s;');
/**
 * miDb
 * Manages the database connection, executes query and abstracts
 * retrieval of database meta-data
 */

class miDb {

    public static $connected = false;
    public static $connection_error = false;
    public static $config = false;
    private static $dbh = false;
    private static $tables = array();
    private static $table_info = array();
    private static $primary_keys = array();
    private static $current_db = false;
    private static $query_cache = array();
    private static $log = array();
    private static $debug_log = array();
    private static $query_log = array();
    private static $query_time = 0;
    private static $query_count = 0;
    private static $trans_in_progress = false;
    private static $type_map = array(
        'mysql'=>array(
            'primary_key'=>'MEDIUMINT UNSIGNED NOT NULL',
            'id'=>'MEDIUMINT UNSIGNED',
            'integer'=>'INT(11)',
            'string'=>'VARCHAR(255)',
            'float'=>'FLOAT',
            'text'=>'TEXT',
            'datetime'=>'DATETIME',
            'timestamp'=>'DATETIME',
            'date'=>'DATE',
            'time'=>'TIME',
            'boolean'=>'TINYINT(1)',
            'status'=>'TINYINT(3) UNSIGNED',
            'currency'=>'DECIMAL(10,2)',
            'percentage'=>'DECIMAL(4,3) UNSIGNED',
        ));
    //----------------------------------------------------------------------------
    /**
     * Connect to the database
     * @param object $config A configuration object with properties: driver,
     * encoding, host, database, username, password and optionally cache_table_info
     * @param bool $no_db If do not want to connect immediately to given database,
     * or you know it's missing from the config, set this to true (defaults to
     * false, so immediately connects)
     * @return bool True on success, false on failure
     */
    static function connect($config, $no_db=false) {
        // Only connects once
        if (self::$connected) {
            return true;
        }
        // Temp directory for caching table info, check $_ENV first
        if (!$tmp_dir = @$_ENV['TMPDIR']) {
            $tmp_dir = sys_get_temp_dir();
        }
        $tmp_dir = rtrim($tmp_dir, '/') . '/midb/';
        if (!file_exists($tmp_dir) && !@mkdir($tmp_dir, 0777, true)) {
            $config->temp_dir = false;
        } else {
            $config->temp_dir = $tmp_dir;
        }
        // Store config statically
        self::$config = $config;

        // Check for a DSN for the driver
        $const = 'MIDB_DSN_' . strtoupper($config->driver);
        if($no_db){
            $const .= '_NO_DB';
        }
        if (!defined($const)) {
            trigger_error(sprintf(MIDB_ERR_UNKNOWN_DRIVER, $config->driver),
                E_USER_ERROR);
            return false;
        }
        $dsn = sprintf(constant($const),$config->host,$config->database);

        // Connect
        try {
            self::$dbh = new PDO($dsn, $config->username, $config->password);
            // Temporary hack to emulate prepares
            if (@$config->pdo_attr_emulate_prepares) {
                self::$dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
            }
            // Always throw exceptions, caught internally to show debug info
            self::$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // Convert empty strings to null, as HTML froms will POST empty values
            // and easier to do this than manually filter every request
            self::$dbh->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
            self::$connected = true;
        }
        catch(PDOException $e) {
            self::$connection_error = $e;
        }
        // Fail on error
        if (@get_class(self::$connection_error) == 'PDOException') {
            trigger_error(sprintf(MIDB_ERR_CONNECT,
                self::$connection_error->getMessage()),
            E_USER_ERROR);
            return false;
        }
        // Set connection encoding
        if (@$config->encoding) {
            miDb::getResults('SET NAMES '.$config->encoding);
        }
        return true;
    }
    //----------------------------------------------------------------------------
    /**
     * Autoconnect will look for configuration JSON file called 'database.json'
     * in the include path.
     * @param bool $no_db Set to true to not connect immediately to database
     * named in config (useful when creating new), defaults to false
     * @return bool True on success, false on failure
     */
    static function autoConnect($no_db=false) {
        if(self::$connected){
            return true;
        }
        $path = 'config';
        if (isset($_ENV['MITAJI'])) {
            $path .= '/' . $_ENV['MITAJI'];
        }
        $config_json = file_get_contents($path . '/database.json', true);
        if (!$config_json) {
            trigger_error(sprintf(MIDB_ERR_NO_CONFIG, getcwd() . "\nInclude path: " . get_include_path()),
                E_USER_ERROR);
            return false;
        }
        $config = json_decode($config_json);
        if (!is_object($config)) {
            trigger_error(MIDB_ERR_BAD_CONFIG, E_USER_ERROR);
            return false;
        }
        return self::connect($config, $no_db);
    }
    //----------------------------------------------------------------------------
    /**
     * Quote a string as appropriate for the DB driver, should not really use this
     * better to use named paramaters instead, but handy in some situations, like
     * converting to a SQL query that can be expanded (@see miDb::expandQuery)
     *
     * @param string $str The string to quote
     * @returns string The quited string
     */
    static function quote($str){
        self::autoConnect();
        return self::$dbh->quote($str);
    }
    //----------------------------------------------------------------------------
    /**
     * Given aan array that represents a row like:
     * array('id' => 1, 'name' => 'foobar')
     * This will convert to a SQL query using named paramaters to safely insert
     * (doesn't actually perfomr the insert)
     *
     * @param string $table
     * @param array $row    Columns for keys array
     * @return array        String query for first element, second array paramaters
     */
    static function array2insert($table, $row) {
        $qry = "INSERT INTO $table ("
            . (implode(', ', array_keys($row)))
            . ') VALUES (';
        $params = array();
        foreach ($row as $k => $v) {
            $params[':' . $k] = $v;
        }
        $qry .= implode(',', array_keys($params)) . ')';
        return array($qry, $params);
    }
    //----------------------------------------------------------------------------
    /**
     * Given a query with named paramaters and the array of paramaters this will
     * expand to a full query with everything quoted appropriately for the current
     * driver
     * @params string $qry The SQL query
     * @params array $params The array of paramaters
     */
    static function expandQuery($qry, $params=array()){
        if (!$params) {
            return $qry;
        }
        foreach ($params as $k=>$v) {
            $qry = preg_replace('/'.$k.'(\W|$)/', self::quote($v).'$1', $qry);
        }
        return rtrim($qry,';').';';
    }
    //----------------------------------------------------------------------------
    /**
     * Start a transaction, will fail if a transaction is already in progress
     * @return bool True on success, false on failure
     */
    static function beginTransaction(){
        self::autoConnect();
        if (self::$trans_in_progress == true) {
            return false;
        }
        self::$dbh->beginTransaction();
        self::$trans_in_progress = true;
        return true;
    }
    //----------------------------------------------------------------------------
    /**
     * Check wehther a transaction is currently in progress
     * @returns bool True if transaction has been started and not committed or
     * rolled back, otherwise false
     */
    static function transInProgress(){
        return self::$trans_in_progress;
    }
    //----------------------------------------------------------------------------
    /**
     * Roll back the current transaction so database returns to state before
     * transaction was started
     * @returns bool True if a transaction was in progress and successfully rolled
     * back, otherwise false
     */
    static function rollback(){
        if (self::$trans_in_progress === false) {
            return false;
        }
        self::$dbh->rollback();
        self::$trans_in_progress = false;
        return true;
    }
    //----------------------------------------------------------------------------
    /**
     * Commits the current transaction
     * @returns bool True if transaction was in progress and successfully
     * committed, otherwise false
     */
    static function commit(){
        if(self::$trans_in_progress === false){
            return false;
        }
        $result = self::$dbh->commit();
        self::$trans_in_progress = false;

        return $result;
    }
    //----------------------------------------------------------------------------
    /**
     * Convert a simple type to a SQL type
     * @param string $type The simple type
     * @returns mixed The SQL equivalent of the simple type, or false if no type
     * mapping found, or no types for current driver
     */
    static function type2sql($type){
        $types = @self::$type_map[self::$config->driver];
        if (!$types) {
            trigger_error(sprintf(MIDB_ERR_UNKNOWN_DRIVER,
                self::$config->driver),
            E_USER_ERROR);
            return false;
        }
        return @$types[$type];
    }
    //----------------------------------------------------------------------------
    /**
     * Convert a SQL type into the simple type
     * @param string $type The SQL type
     * @returns mixed The simple equivalent of the SQL type, or false if no type
     * mapping found, or no types for current driver
     */
    static function sql2type($sql){
        $types = @self::$type_map[self::$config->driver];
        if (!$types) {
            trigger_error(sprintf(MIDB_ERR_UNKNOWN_DRIVER,
                self::$config->driver),
            E_USER_ERROR);
            return false;
        }
        foreach ($types as $type=>$sql_type) {
            if (miStr::beginsWith($sql, $sql_type)) {
                return $type;
            }
        }
        return false;
    }

    //----------------------------------------------------------------------------
    /**
     * Execute a query that returns just one value
     *
     * @params string $qry The SQL query
     * @params array $params Optional query paramaters
     * @returns mixed The value as a string, or false if nothing found
     */
    static function getVar($qry, $params = false) {
        $resource = self::getResults($qry, $params, PDO::FETCH_BOTH);
        return @$resource[0][0];
    }
    //----------------------------------------------------------------------------
    /**
     * Execute a query that returns one row
     * @params string $qry The SQL query
     * @params array $params Optional query paramaters
     * @returns mixed The row as an array, or false if nothing found
     */
    static function getRow($qry, $params = false) {
        $resources = self::getResults($qry, $params);
        if ($resources) {
            return $resources[0];
        }
        return false;
    }
    //----------------------------------------------------------------------------
    /**
     * Execute a query that returns a column of values
     * @params string $qry The SQL query
     * @params array $params Optional query paramaters
     * @returns mixed The column values as an array, or false if nothing found
     */
    static function getCol($qry, $params = false) {
        return self::getResults($qry, $params, PDO::FETCH_COLUMN);
    }
    //----------------------------------------------------------------------------
    /**
     * Execute a query that returns multiple rows
     * @params string $qry The SQL query
     * @params array $params Optional query paramaters
     * @params mixed $fetch_mode The PDO constant for fetch mode
     * @params string $fetch_class The class name to fetch results into, or false
     * to just get an array of results
     * @returns mixed The results found as an array, or false if nothing found
     */
    static function getResults($qry, $params=false,
        $fetch_mode=PDO::FETCH_ASSOC, $fetch_class=false) {
            $start_time = microtime(true);
            self::autoConnect();
            self::$query_count += 1;
            // Initiate log for this query
            $log = array('query'=>$qry, 'params'=>$params, 'prepared'=>false,
                'success'=>0, 'error'=>false, 'milliseconds'=>0,
                'column_count'=>0,'row_count'=>0);
            if (is_array($params) && $params) {
                $cache_key = md5(preg_replace('/\s/', '', $qry));
                if (isset(self::$query_cache[$cache_key])) {
                    $log['prepared'] = true;
                    $rs = self::$query_cache[$cache_key];
                } else {
                    $rs = self::$dbh->prepare($qry);
                    self::$query_cache[$cache_key] = $rs;
                }
                if (!$rs) {
                    $err_str = MIDB_ERR_NO_PREPARE;
                    $log['error'] = $err_str;
                    self::log($log);
                    trigger_error($err_str, E_USER_ERROR);
                    return;
                }
                try {
                    $rs->execute($params);
                } catch (Exception $e) {
                    if ($rs->errorCode()) {
                        $err = $rs->errorInfo();
                        $err_str = sprintf(MIDB_ERR_SQL,$err[0],$err[1],$err[2]);
                    } else {
                        $err_str = 'Unknown error cause by execuing prepared query';
                    }
                    $log['error'] = $err_str;
                    self::log($log);
                    trigger_error($err_str, E_USER_ERROR);
                    return;
                }
            } else {
                $rs = self::$dbh->query($qry);
                if ($rs === false) {
                    $err = self::$dbh->errorInfo();
                    $err_str = sprintf(MIDB_ERR_SQL,$err[0],$err[1],$err[2]);
                    $log['error'] = $err_str;
                    self::log($log);
                    trigger_error($err_str, E_USER_ERROR);
                    return false;
                }
            }
            // If a SELECT then fetch all the rows
            $return_rows = false;
            if(in_array(substr(strtoupper($qry),0,4), array('SELE','DESC','SHOW'))){
                $return_rows = true;
            }

            if(!$return_rows){
                // Log row count and return true
                $log['row_count'] = $rs->rowCount();
                $return_value = true;
            } else {
                if ($fetch_mode == PDO::FETCH_COLUMN) {
                    $resources = $rs->fetchAll(PDO::FETCH_COLUMN, 0);
                } else if ($fetch_mode == PDO::FETCH_CLASS
                    && class_exists($fetch_class)) {
                        $resources = $rs->fetchAll(PDO::FETCH_CLASS, $fetch_class);
                    } else {
                        $resources = $rs->fetchAll($fetch_mode);
                    }
                // Row count is the number of actual resources returned
                if (is_array($resources) && $resources) {
                    $log['row_count'] = count($resources);
                    $log['column_count'] = $rs->columnCount();
                }
                $return_value = $resources;
            }
            $seconds = microtime(true)-$start_time;
            $ms = $seconds * 1000;
            $log['success'] = true;
            $log['milliseconds'] = $ms;
            self::$query_time += $ms;
            if (@self::$config->warn_slow_query
                && $ms > self::$config->warn_slow_query) {
                    trigger_error(sprintf(MIDB_WARN_SLOW_QUERY, $ms, $qry),
                        E_USER_NOTICE);
                }
            self::log($log);
            return $return_value;
        }
    //----------------------------------------------------------------------------
    /**
     * Get the last insert id
     * @returns int Last insert id
     */
    static function lastInsertId(){
        return self::$dbh->lastInsertId();
    }
    //----------------------------------------------------------------------------
    /**
     * Set the database to use
     * @returns boll True on success or false on failure
     */
    static function useDatabase($db_name){
        if(self::getResults('USE '.$db_name)){
            self::$current_db = $db_name;
            self::$table_info = array();
            self::$tables = array();
            return true;
        }
        return false;
    }
    //----------------------------------------------------------------------------
    /**
     * Get the name of the current database
     * @returns string The name of current database
     */
    static function getDatabase(){
        if(!self::$current_db){
            self::$current_db = self::getVar('SELECT database()');
        }
        return self::$current_db;
    }
    // ---------------------------------------------------------------------------
    /**
     * Get a simple array of resources using one column for the key and another
     * for the value. Handy for form selects on small collections.
     * @param string $resource The resource or table name
     * @param string $key The column name to use for the key
     * @param string $value The column name to use for the value
     * @param string $empty Inserts $empty at the beginning of the array with an
     * index of 0, useful if choosing nothing is an option, if you see what I mean
     * @return array The key/value pairs or an empty array if nothing found
     */
    static function options($tbl, $key, $value, $empty=false){
        $tbl = AkInflector::tableize($tbl);
        $rows = miDb::getResults("SELECT $key, $value FROM $tbl ORDER BY $value;");
        if ($rows) {
            $keys = miArray::extractByKey($rows, $key);
            $values = miArray::extractByKey($rows, $value);
            if ($empty) {
                array_unshift($keys, 0);
                array_unshift($values, $empty);
            }
            return array_combine($keys, $values);
        }
        return array();
    }
    //----------------------------------------------------------------------------
    // Meta data
    //----------------------------------------------------------------------------
    /**
     * List all tables in the current database
     * @returns array Multi-dimensional array of table info, where table name is
     * key and info is value
     */
    static function allTables() {
        $tables = self::getCol('SHOW TABLES');
        if (!$tables) {
            return false;
        }
        $tmp = array();
        foreach ($tables as $tbl) {
            $tmp[$tbl] = self::tableInfo($tbl);
        }
        return $tmp;
    }
    //----------------------------------------------------------------------------
    /**
     * Delete cached table info
     * @param string $tbl Optional, name of table to delete cached info for, if
     * false will delete infor for all tables
     * @returns bool True if files deleted, false if not files deleted
     */
    static function deleteCachedTableInfo($tbl=false) {
        $db = self::getDatabase();
        $cache_pattern = self::$config->temp_dir . $db . '/*';
        if($tbl){
            $cache_pattern .= $tbl.'.json';
        }
        $files = glob($cache_pattern);
        if ($files) {
            foreach($files as $f){
                unlink($f);
            }
            return true;
        }
        return false;
    }
    //----------------------------------------------------------------------------
    /**
     * Quick check to see if table exists, unlike tableInfo will not trigger an
     * error if the table does not exist.
     *
     * @param string $tbl The name of the table to check exists
     * @returns boll True if exists, false if not
     */
    static function tableExists($tbl){
        if(!self::$tables){
            self::$tables = self::getCol('SHOW tables;');
        }
        return in_array($tbl, self::$tables);
    }
    //----------------------------------------------------------------------------
    static function tableInfo($tbl) {

        // Check not passed an empty string
        if (!trim($tbl)) {
            trigger_error("Table name cannot be empty string",E_USER_ERROR);
            return false;
        }
        // Check table exists
        if(!self::tableExists($tbl)){
            trigger_error(sprintf(MIDB_ERR_TABLE_MISSING,$tbl),E_USER_ERROR);
            return false;
        }
        // If already set just return it
        if (isset(self::$table_info[$tbl])) {
            return self::$table_info[$tbl];
        }
        // Check cached version
        $cache_path = false;
        if (@self::$config->temp_dir && @self::$config->cache_table_info) {
            // Files stored in folder for current database so need to find that
            // (as may be multiple apps on one server with conflicting table names)
            $db = self::getDatabase();
            $cache_path = self::$config->temp_dir . $db . '/';
            $cache_file = $cache_path . 'ti_' . $tbl . '.json';
            if (file_exists($cache_file)) {
                self::$table_info[$tbl] = json_decode(file_get_contents($cache_file), true);
                return self::$table_info[$tbl];
            }
        }
        // Here's the SQL to reflect on the table and cache
        $table_info = array();
        if (!($fields = self::getResults("DESCRIBE $tbl;"))) {
            trigger_error(sprintf(self::DB_TABLE_MISSING, $tbl), E_USER_ERROR);
            return false;
        }
        foreach($fields as $f) {
            $key = $f['Field'];
            if (isset($f['Attributes'])) {
                $atts = strtolower($f['Attributes']);
            }
            $table_info[$key] = array('field_name' => $key,
                'unique' => 0,
                'type' => '',
                'sql_create' => '',
                'sql_type' => '',
                'sql_null_allowed' => false,
                'sql_extra' => '',
                'sql_default' => false,
                'auto_increment'=>false,
                'required' => 0,
                'maxlen'=>false);
            if (!is_null($f['Default'])) {
                $table_info[$key]['sql_default'] = $f['Default'];
            }
            $table_info[$key]['sql_type'] = $sql_type = strtoupper(
                trim(
                    preg_replace('/(\(.+\)|unsigned)/', '', $f['Type'])
                )
            );
            if (strtolower($f['Null']) == 'yes') {
                $table_info[$key]['sql_null_allowed'] = 1;
            }
            if ($f['Extra']) {
                $table_info[$key]['sql_extra'] = $f['Extra'];
                if(strtolower($f['Extra']) == 'auto_increment'){
                    $table_info[$key]['auto_increment'] = true;
                }
            }
            if ($table_info[$key]['sql_null_allowed'] === false &&
                $table_info[$key]['sql_default'] === false &&
                $key != 'created_on'
                && $key != 'updated_on'
                && $f['Extra'] != 'auto_increment') {
                    $table_info[$key]['required'] = 1;
                }
            $table_info[$key]['type'] = $type = self::getColumnType($sql_type);
            preg_match('/\(([0-9]+)\)/', $f['Type'], $matches);
            if ($type == 'string' && isset($matches[1])) {
                $table_info[$key]['maxlen'] = $matches[1];
            }
            if ($sql_type == 'ENUM') {
                $tmp = preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $f['Type']);
                $options = explode("','", $tmp);
                $table_info[$key]['enum_options'] = $options;
            }
            // Creation string (if need to copy or rename)
            $sql_create = strtoupper($f['Type']);
            if (strtolower($f['Null']) == 'no') {
                $sql_create.= ' NOT NULL';
            } else {
                $sql_create.= ' NULL';
            }
            if ($f['Default']) {
                $sql_create.= " DEFAULT '{$f['Default']}'";
            }
            $table_info[$key]['sql_create'] = $sql_create;
        }
        $indexes = self::getResults("SHOW INDEXES FROM $tbl;");
        foreach($indexes as $index) {
            if (strtolower($index['Key_name']) == 'primary') {
                $table_info[$index['Column_name']]['primary_key'] = true;
                // Not sure about this way of checking unique keys. Also doesn't help
                // if you have primary key on more than one column, but in that case
                // you would like to check that the two combined are unique
            } else if ($index['Non_unique'] == '0') {
                $table_info[$index['Column_name']]['unique'] = 1;
            }
        }
        // Always cache in memory
        self::$table_info[$tbl] = $table_info;
        // Cache to file if possible
        if ($cache_path && $cache_file) {
            if (!is_dir($cache_path)) {
                if(!@mkdir($cache_path)){
                    trigger_error(sprintf(MIDB_WARN_NO_CACHE,$cache_path), E_USER_WARNING);
                }
                @chmod($cache_path,0777);
            }
            file_put_contents($cache_file, json_encode($table_info));
            @chmod($cache_file,0777);
        }
        return $table_info;
    }
    //----------------------------------------------------------------------------
    static function colInfo($tbl, $col, $prop = false) {
        $tbl_info = self::tableInfo($tbl);
        if (!$tbl_info || !isset($tbl_info[$col])) {
            return false;
        }
        if ($prop && !isset($tbl_info[$col][$prop])) {
            return false;
        }
        if ($prop) {
            return $tbl_info[$col][$prop];
        }
        return $tbl_info[$col];
    }
    //----------------------------------------------------------------------------
    static function primaryKey($table){

        if(!@self::$primary_keys[$table]){
            $ti = self::tableInfo($table);
            if(!$ti){
                return false;
            }
            $primary_key = array();
            foreach($ti as $col=>$info){
                if(@$info['primary_key']){
                    $primary_key[] = $col;
                }
            }
            if(!$primary_key){
                return false;
            }
            if(count($primary_key) == 1){
                $primary_key = $primary_key[0];
            } else {
                sort($primary_key);
            }
            self::$primary_keys[$table] = $primary_key;
        }
        return self::$primary_keys[$table];
    }
    //----------------------------------------------------------------------------
    static function primaryKeyInfo($table) {

        if(!($primary_key = self::primaryKey($table))){
            return false;
        }
        if(is_array($primary_key)){
            $tmp = array();
            foreach($primary_key as $col){
                $tmp[] = self::colInfo($table, $col);
            }
            return $tmp;
        }
        return self::colInfo($table, $primary_key);
    }
    //----------------------------------------------------------------------------
    static function colOptions($tbl, $col, $force_null = false) {
        $col_info = self::colInfo($tbl, $col);
        if (!$options = @$col_info['enum_options']) {
            return false;
        }
        $select_options = array();
        if ($col_info['sql_null_allowed'] || $force_null) {
            $select_options['NULL'] = 'None';
        }
        foreach($options as $opt) {
            $select_options[$opt] = $opt;
        }
        return $select_options;
    }
    //----------------------------------------------------------------------------
    private static function getColumnType($sql_type) {
        switch ($sql_type) {
        case 'TEXT':
        case 'TINYTEXT':
        case 'MEDIUMTEXT':
        case 'LONGTEXT':
        case 'VARCHAR':
        case 'CHAR':
        case 'ENUM':
        case 'SET':
            return 'string';
        case 'TINYINT':
        case 'SMALLINT':
        case 'MEDIUMINT':
        case 'BIGINT':
        case 'INT':
        case 'DECIMAL':
        case 'FLOAT':
        case 'DOUBLE':
        case 'TIME':
            return 'number';
        case 'YEAR':
        case 'MONTH':
        case 'DATE':
        case 'DATETIME':
        case 'TIMESTAMP':
            return 'date';
        default:
            return false;
        }
    }

    //----------------------------------------------------------------------------
    static function detectThrough($left_tbl, $right_tbl){
        $tables = array($left_tbl, $right_tbl);
        sort($tables);
        $through_table = implode('_',$tables);
        return self::tableExists($through_table) ? $through_table : false;
    }
    //----------------------------------------------------------------------------
    // Debugging and logging
    //----------------------------------------------------------------------------
    private static function log($log){
        self::$log[] = $log;
    }
    //---------------------------------------------------------------------------
    static function resetQueryLog(){
        self::$log = array();
    }
    //----------------------------------------------------------------------------
    static function queryLog($n=false, $prop = false){
        if($n){
            $log = array_slice(self::$log,-$n);
        } else {
            $log = self::$log;
        }
        if($prop){
            $tmp = array();
            foreach($log as $l){
                $tmp[] = @$l[$prop];
            }
            return $tmp;
        }
        return $log;
    }
    //----------------------------------------------------------------------------
    /**
     * Get the last query, or some property of the last query
     * @params string $prop The property to get which can be one of query, params,
     * prepared, success, error, milliseconds, column_count, row_count
     * @returns mixed An array of all properties or the specific property requested
     */
    static function lastQuery($prop=false) {
        $qry = self::queryLog(1);
        return $prop ? @$qry[0][$prop] : $qry[0];
    }
    //----------------------------------------------------------------------------
    static function queryCount() {
        return self::$query_count;
    }
    //----------------------------------------------------------------------------
    static function queryTime() {
        return self::$query_time;
    }

    //----------------------------------------------------------------------------
    /**
     * Load data from a SQL file using command line mysql - note could just call
     * miDb::query($qry) with the contents of the file as the query, but this may
     * be quicker when loading a lot of data
     *
     * @params $f The path to the file
     * @returns bool True on success, false on failure
     */
    static function load($f){
        miDb::autoConnect();
        $sql = file_get_contents($f);
        miDb::getResults($sql);
        return;
        $const = 'MIDB_LOAD_' . strtoupper(miDb::$config->driver);
        if (!defined($const)) {
            trigger_error(sprintf(MIDB_ERR_NO_LOAD, miDb::$config->driver),
                E_USER_ERROR);
            return false;
        }
        $cmd = sprintf(constant($const),
            miDb::$config->username,
            miDb::$config->password,
            miDb::$config->database,
            $f);
        exec($cmd, $output, $return_var);
        if ($return_var == 0) {
            return true;
        }
        return false;
    }
    //----------------------------------------------------------------------------
    /**
     * Dumps all data in the current database and returns as SQL, using
     * the command line program for speed, currently only supports mysqldump
     *
     * @returns mixed a SQL string which would recreate the entire database
     * structure and data, or false if cannot be exported
     */
    static function dump(){
        miDb::autoConnect();
        if (!miDb::$config->password) {
            $const = 'MIDB_DUMP_NO_PASS_' . strtoupper(miDb::$config->driver);
        }
        else {
            $const = 'MIDB_DUMP_' . strtoupper(miDb::$config->driver);
        }
        if (!defined($const)) {
            trigger_error(sprintf(MIDB_ERR_NO_DUMP, miDb::$config->driver) . " '$const'",
                E_USER_ERROR);
            return false;
        }
        if (!miDb::$config->password) {
            $cmd = sprintf(constant($const),
                miDb::$config->username,
                miDb::$config->database);
        }
        else {
            $cmd = sprintf(constant($const),
                miDb::$config->username,
                miDb::$config->password,
                miDb::$config->database);
        }
        exec($cmd, $output, $return_var);
        if ($return_var == 0) {
            return implode("\n", $output)."\n";
        }
        return false;
    }
    //----------------------------------------------------------------------------
    /**
     * Dumps all data in the given table - note no create table is added, just
     * pure data. Only works for mysql.
     *
     * @params string $tbl The name of the table
     * @returns mixed A string of SQL or false if cannot be dumped
     */
    static function dumpTableData($tbl, $f=false){
        miDb::autoConnect();
        $const = 'MIDB_DUMP_TABLE_' . strtoupper(miDb::$config->driver);
        if (!defined($const)) {
            trigger_error(sprintf(MIDB_ERR_NO_DUMP, miDb::$config->driver),
                E_USER_ERROR);
            return false;
        }
        $cmd = sprintf(constant($const),
            miDb::$config->username,
            miDb::$config->password,
            miDb::$config->database,
            $tbl);
        exec($cmd, $output, $return_var);
        if ($return_var == 0) {
            return implode("\n", $output)."\n";
        }
        return false;
    }
    //----------------------------------------------------------------------------
    /**
     * Dumps all data in the given table - that matches the $where clause
     * Note no create table is added, just pure data. Only works for mysql.
     *
     * @params string $tbl The name of the table
     * @params string $where A where clause, like "id IN (1, 2, 3)"
     * @returns mixed A string of SQL or false if cannot be dumped
     */
    static function dumpTableDataWhere($tbl, $where){
        miDb::autoConnect();
        $const = 'MIDB_DUMP_TABLE_WHERE_' . strtoupper(miDb::$config->driver);
        if (!defined($const)) {
            trigger_error(sprintf(MIDB_ERR_NO_DUMP, miDb::$config->driver),
                E_USER_ERROR);
            return false;
        }
        $cmd = sprintf(constant($const),
            miDb::$config->username,
            miDb::$config->password,
            miDb::$config->database,
            $tbl,
            $where);
        exec($cmd, $output, $return_var);
        if ($return_var == 0) {
            return implode("\n", $output)."\n";
        }
        return false;
    }
    //----------------------------------------------------------------------------
}
