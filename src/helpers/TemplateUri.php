<?php

/* 
 * This is an implementation of uri template 01
 * Also allows comparison of real URLs to templates to see if they match.
*/

class TemplateUri {
    
	/*
	 * Constant regex templates. Default match is one or more characters that
	 * is not a slash, so will not match across path components.
	 */
	const MATCH_DEFAULT = '([^\/]+)';
	
	/*
	 * Special to override default so will match across path components
	 */ 
	const MATCH_ANY = '(.*)';
	/*
	 * Numeric must match one or more numbers
	 */
    const NUMERIC = '([0-9]+)';
	/*
	 * Alpha must match one or more lowercase letters
	 */
    const ALPHA = '([a-z]+)';
    /* 
	 * Characters allowed in template variable names
	 * That colon is for my own purposes, see below
	 */
    var $nameRegexp = '/\{([a-zA-Z0-9\-\._~:]+)\}/';
    var $varRegexp = array();
// --------------------------------------------------------------------------
// Constructor
    function __construct($template,$varRegexp=array()){
        $this->template = $template;
        $names = $this->getNames();
        foreach($names as $name){
            if(isset($varRegexp[$name])){
                $this->setVarRegexp($name,$varRegexp[$name]);
            } else if(strstr($name,':')){
                list($new_name,$constraint) = explode(':',$name);
                $this->template = str_replace('{'.$name.'}','{'.$new_name.'}',$this->template);
                $this->setVarRegexp($new_name,constant("TemplateUri::$constraint"));
			
			// Default regex if none specified - will *not* match across path components
            } else {
                $this->setVarRegexp($name,TemplateUri::MATCH_DEFAULT);
            }
        }
    }
// --------------------------------------------------------------------------   
// Checks all variable names contain rfc3986 unreserved characters
// @todo check it is a valid uri
    function validate(){
        preg_match_all('/(\{.+\})/',$this->template,$varNames);
        $varNames = $varNames[1];
        foreach($varNames as $name){
            if(!preg_match($this->nameRegexp,$name)){
                return false;
            }
        }
        return true;
    }
// -------------------------------------------------------------------------- 
/**
 * @return array Returns all the names in the template as an array in the order
 * they appear in the template
 */
    function getNames(){
        preg_match_all($this->nameRegexp,$this->template,$varNames);
        return $varNames[1];
    }
// -------------------------------------------------------------------------- 
/**
 * Returns the template populated with $data which is an array
 * of values keyed by template variable name
 * @param mixed $data a (keyed) array or object containing values for names
 * @return string An expanded URL
 */
    function expand($data){
        $varNames = array();
        $varValues = array();

        // Extract varNames and find matching values
		$varNames = $this->getNames();
		foreach($varNames as $name){
			$value = is_object($data) ? @$data->$name : @$data[$name];
			if($value !== false && preg_match('/'.$this->getVarRegexp($name).'/',$value)){
				$varValues['{'.$name.'}'] = $value;
			}
		}
        $uri = str_replace(array_keys($varValues),array_values($varValues),$this->template);
        // Any undefined variables are replaced with an empty string
        $uri = preg_replace($this->nameRegexp,'',$uri);
        return $uri;
    }
// --------------------------------------------------------------------------     
/*
 * Add a regular expression constraint for a variable 
 * @param string $name The name of the variable
 * @parm string $regexp A regular expression constraint
 */
    function setVarRegexp($name,$regexp){
        $this->varRegexp[$name] = $regexp;
    }
// --------------------------------------------------------------------------     
/**
 * Read a regex constraint
 * @param string $name The name of the variable
 * @return string The reqular expression
 */
    function getVarRegexp($name){
        return isset($this->varRegexp[$name]) ? $this->varRegexp[$name] : false;
    }
// --------------------------------------------------------------------------     
/**
 * Remove a regex constraint
 * @param string $name The name of the variable
 * @return bool True if removed, or false if nothing was defined for $name
 */
    function removeVarRegexp($name){
        if(isset($this->varRegexp[$name])){
            $this->setVarRegexp($name,'(.*)');
            return true;
        }
        return false;
    }
// -------------------------------------------------------------------------- 
/**
 * @return string Return the complete regular expression for matching
 */
    function getFullVarRegexp(){
        
        /*
		 * Escape regex syntax characters, except the template URI variable 
		 * delimiters '{' and '}' (they'll be removed below)
		 */
        $regexp = preg_quote($this->template,'/');
        $regexp = str_replace('\{','{',$regexp);
        $regexp = str_replace('\}','}',$regexp);
        
        // Replace var names with regular expressions
        $varNames = preg_replace('/^.+$/','{\\0}',
                        array_keys($this->varRegexp));
        $varValues = preg_replace('/^[^\(].+[^\)]$/','(\\0)',
                        array_values($this->varRegexp));
        return str_replace($varNames,$varValues,$regexp);
    }
// -------------------------------------------------------------------------- 
/**
 * Check if a given URI matches the template
 * @param string $uri The URI to compare to
 */
    function matchUri($uri){
        
        $regexp = $this->getFullVarRegexp();
        if(preg_match('/^'.$regexp.'$/',$uri,$uriValues)){
            // Remove the whole string match
            array_shift($uriValues);

            // If anything in uriValues map to template names
            if($uriValues){            
                $varNames = $this->getNames();
                /*
				 * If there aren't as many values as names e.g. if you'd added
				 * an optional constraint like '(\d+)?', pad the values array 
				 * with NULL
				 */
                $uriValues = array_pad($uriValues,count($varNames),NULL);
                $tmp = array_combine($varNames,$uriValues);
                // Trim slashes off matched vars
                foreach($tmp as $k=>$v){
                    $tmp[$k] = trim($v,'/');
                }
				// Return values
                return $tmp;
            }

            // No values? Just return true
            return true;
        }

        // No match, return false
        return false;
    }
// --------------------------------------------------------------------------
/**
 * Batch process an array of template URIs and expand with given vars
 * @param array $templates An array of string template URIs
 * @param array $vars The variables to expand them with
 * @return array An array of expanded URLs
 */
    static function batchExpand($templates, $vars){
        if(!$templates || !is_array($templates)){
            return false;
        }
        $expanded = array();
        foreach($templates as $k=>$template){
            $tpluri = new self($template);
            $expanded[$k] = $tpluri->expand($vars);
        }
        return $expanded;
    }
// --------------------------------------------------------------------------
}

?>
