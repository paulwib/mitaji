<?php

class miMath {

    public static function scale($size=array(0,0),
				$max_size=array(0,0)){
	$w = $size[0];
	$h = $size[1];
	$max_w = $max_size[0];
	$max_h = $max_size[1];
	if($max_w && $w > $max_w){
	    $scale = (100/$w)*$max_w;
	    $h = ($h/100)*$scale;
	    $w = $max_w;
	} 
	if($max_h && $h > $max_h){
	    $scale = (100/$h)*$max_h;
	    $w = ($w/100)*$scale;
	    $h = $max_h;
	} 
	return array((int)round($w),(int)round($h));			
    }
#-----------------------------------------------------------------------------    
    public static function parseEquation($str,$vars) {    
    
	extract($vars);
	$str = str_replace(' ','',$str);
	$str = preg_replace('/(^|\+|\-|\*|\()([a-z])/','$1\$$2',$str);
	eval('$result='.$str.';');
	return $result;
    }
#-----------------------------------------------------------------------------
# Calculate a percentage of n
    public static function percent($n,$pc,$f=2){
	return round(($n/100)*$pc,$f);
    }
#-----------------------------------------------------------------------------
# Find what n1 is as a percentage of n2
    public static function percentOf($n1,$n2,$f=2){
	if($n1 && $n2){
	    return round(($n1/$n2)*100,$f);
	}
	return 0;
    }
#-----------------------------------------------------------------------------
# Find what percentage of n1 would need to be added
# to make it equal to n2 (useful for reverse calculating mark up)
    public static function percentDiff($n1,$n2,$f=2){
	if($n1 && $n2){
	    return round((($n2/$n1)-1)*100,$f);
	}
	return false;
    }
#-----------------------------------------------------------------------------

}


?>