<?php

/**
 * miHtml provides helper methods for generating common block of HTML,
 * mainly for forms. All methods are called statically.
 * 
 * @author Paul Willoughby
 * @copyright Copyright (c) 2008, Paul Willoughby
 * @license http://opensource.org/licenses/gpl-2.0.php
 */

class miHtml {
    const CONDITIONAL_COMMENT = "<!--[if %s]>\n%s\n<![endif]-->";
    public static $self_closing_tags = array('input','link','img');
    private static $builder = false;
    
//---------------------------------------------------------------------------
/**
 * Link in a CSS file
 * @param string $href The href of the CSS file relative to public directory
 * @param string $media The CSS media attribute (screen, print etc)
 * @param string $ie An Internet Explorer version number to target, which will
 *                   wrap the link in an IE specific conditional comment
 * @param bool $embed If true then will return a <style> tag to embed CSS
 * directly in an HTML page, otherwise will return a <link> which is default
 * @return string The full CSS link which will also have the file modified
 *                  time appended as a query string to bust caches.
 */
    static function cssLink($href, $media = 'screen', $ie = false, $embed=false) {
		
		$real_file = ltrim($href,'/');
		
		// If not embedding make sure local file exists
		if(!$embed && !file_exists($real_file)){
			trigger_error("Cannot link to file $href as does not exist relative to doc root",E_USER_WARNING);
			return false;
		
		// If embedding look to see if file exists...
		} else if ($embed && !file_exists($real_file)){
			// ...if it doesn't try the whole include path
			if (!$real_file = file_exists_include_path($real_file)) {
				trigger_error("Cannot embed file $href as does not exist in path",E_USER_WARNING);
				return false;
			}
		}
		$builder = self::builder();
		if(!$embed){
			$href = miLink::setUrlVar($href,'v',filemtime($real_file));
			$html = $builder->link(array('rel'=>'stylesheet',
										'href'=>$href,
										'media'=>$media));
			return $ie ? sprintf(miHtml::CONDITIONAL_COMMENT, $ie, $html) : $html;
		}
		$html = $builder->style(array('type'=>'text/css',
                                    'charset'=>'utf8',
                                    'media'=>$media),
                               file_get_contents($real_file));
        return $ie ? sprintf(miHtml::CONDITIONAL_COMMENT, $ie, $html) : $html;
    }
//---------------------------------------------------------------------------
/**
 * Exactly the same as above but with embed set to true regardless of other
 * arguments
 * @see miHtml::cssLink
 */
    static function cssEmbed($href, $media = 'screen', $ie = false) {
		return self::cssLink($href, $media, $ie, true);
    }
//---------------------------------------------------------------------------
/**
 * Link in a Javascript file
 * @param string $href The href of the JS file relative to public directory
 * @param string $ie An Internet Explorer version number to target, which will
 *                   wrap the link in an IE specific conditional comment
 * @return string The full JS script tag which will also have the file modified
 *                  time appended as a query string to bust caches.
 */
    static function jsLink($href, $ie = false, $embed = false) {
		
        $real_file = ltrim($href,'/');
		
		// If not embedding make sure local file exists
		if(!$embed && !file_exists($real_file)){
			trigger_error("Cannot link to file $href as does not exist relative to doc root",E_USER_WARNING);
			return false;
		
		// If embedding look to see if file exists...
		} else if ($embed && !file_exists($real_file)){
			// ...if it doesn't try the whole include path
			if (!$real_file = file_exists_include_path('public/'.$real_file)) {
				trigger_error("Cannot embed file $href as does not exist in path",E_USER_WARNING);
				return false;
			}
		}
		$builder = self::builder();
		if(!$embed){
			$href = miLink::setUrlVar($href,'v',filemtime($real_file));
			$html = $builder->script(array('type'=>'text/javascript',
										'charset'=>'utf-8',
										'src'=>$href));
			return $ie ? sprintf(miHtml::CONDITIONAL_COMMENT, $ie, $html) : $html;
		}
		$html = $builder->script(array('type'=>'text/javascript',
                                    'charset'=>'utf-8'), 
                        file_get_contents($real_file));
        return $ie ? sprintf(miHtml::CONDITIONAL_COMMENT, $ie, $html) : $html;
    }
//---------------------------------------------------------------------------
/**
 * Exactly the same as above but with embed set to true regardless of other
 * arguments
 * @see miHtml::jsLink
 */
    static function jsEmbed($href, $ie = false) {
        return self::jsLink($href, $ie, true);
    }

// --------------------------------------------------------------------------
/**
 * @return object An instance of miHtml to operate as a builder (see __call)
 */
    static function builder(){
        if(!self::$builder){
            self::$builder = new self();
        }
        return self::$builder;
    }
//---------------------------------------------------------------------------
/**
 * Generate a UL menu with first and last classes for LI elements, and
 * additional class names based on current URI and customizable UL and
 * LI class prefix.
 * 
 * @param array $links An array of links where the link text to use is the key
 * and each elements is a URL or an array of link attributes (make sure to 
 * include an element named `href` or `url` for the actual link). Allowed 
 * attributes are href, id, class, rel and title.
 * @param string $current_uri The current URL, optional. If supplied any link
 * with a URL that matches $current_uri will be given the class `selected`. 
 * Any that partially match the path will be given class `in-path`.
 * @param string $ul_class A class name for the UL element
 * @param string $li_class_prefix If supplied this will be used as a prefix for
 * a class name based on the link text, so if it was 'item' and your link had 
 * the text 'About Us' it would end up with the class 'item-about-us'.
 * @return string an HTML string
 * 
 */
    static function menuUl($links, $current_uri=null, $ul_class=false, $li_class_prefix=false){

        $builder = self::builder();
        $html = '';
		$i = 0;
		$count = count($links)-1; 
        foreach($links as $label=>$link){
			// Make sure you can use `href` or `url` for the URL
			if(is_array($link) && @$link['url']){
				$link['href'] = $link['url'];
				unset($link['url']);
			}
			$a_atts = is_array($link) ? $link : array('href'=>$link);
			$li_atts = array();
			if($i == 0){
				$li_atts['class'][] = 'first';
			}
			if($i == $count){
				$li_atts['class'][] = 'last';
			}
			if($a_atts['href'] == $current_uri){
				$li_atts['class'][] = 'selected';
			} else if($current_uri && miStr::beginsWith($current_uri,rtrim($a_atts['href'],'/'))){
				$li_atts['class'][] = 'in-path';
			}
			if($li_class_prefix){
				$li_atts['class'][] = $li_class_prefix.'-'.miStr::sanitize($label);
			}
			// Make sure only allowed attributes on link
			$a_atts = array_intersect_assoc($a_atts,array('href'=>@$a_atts['href'],
														  'title'=>@$a_atts['title'],
														  'rel'=>@$a_atts['rel'],
														  'class'=>@$a_atts['class'],
														  'id'=>@$a_atts['id']));
            $html .= $builder->li($li_atts, $builder->a($a_atts,$label));
			$i++;
        }
		if($ul_class){
        	return $builder->ul(array('class'=>$ul_class),$html);
		} else {
			return $builder->ul($html);
		}
    }
// --------------------------------------------------------------------------
/**
 * Magic __call method allows you to write HTML in a "builder" style with the
 * first argument optiuonally defining attributes and the rest being text
 * which allows you to construct the code in a similar way to how the end
 * HTML would look e.g.
 * 
 *   $builder = miHtml::builder();
 *   echo $builder->div(array('class'=>'foo'), $builder->p('Hello World'));
 * 
 * Would give you:
 *     
 *     <div class="foo"><p>Hello World!</p></div>
 * 
 * @param string $func The name of the HTML tag is the function name
 * @param mixed $args1, $args2, ... $argsN If the first argument is an array
 * it will be used as attribute pairs. If not it and all subsequent arguments
 * will be treated as strings to concatenate within the HTML tag.
 * 
 * Attribute pais should be in an array where the key is the attribute name
 * and the value is the value. If the value is an array the elements will
 * be joined with a space, handy for class and rel attributes etc.
 * 
 * @todo Improve handling of self closing tags
 * @todo Add an XHTML switch
 */
    function __call($func, $args){
        $func = strtolower($func);
        $str = "<$func";
        if(is_array($args[0])){
			$atts = array_shift($args);
			ksort($atts);
            foreach($atts as $k=>$v){
				if(is_null($v) || $v === false){
					continue;
				}
				// Multiple values for attribute
				if(is_array($v)){
					$v = implode(' ',$v);
				}
                //$v = htmlspecialchars($v);
                $str .= ' '.$k.'="'.$v.'"';
            }
        }
        $str .= ">";
        if($args && in_array($func, miHtml::$self_closing_tags) === false){
            foreach($args as $a){
                // Newline before each opening tag
                if($a && $a{0} == '<'){
                    $str .= "\n";
                }
                $str .= $a;
            }
        }
        if(in_array($func, miHtml::$self_closing_tags)){
            return $str;
        } else {
            $str .= "</$func>\n";
        }
        return $str;
    }
}
