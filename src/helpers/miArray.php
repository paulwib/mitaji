<?php

/**
 * miArray
 * 
 * Some array helpers
 */

class miArray {

/**
 * Key an array by a property of one or more of it's elements. The elements 
 * must neccessarily be objects or arrays so they have properties.
 * If the input array has elements with keys of the same value they will
 * overwrite each other so only the last would survive.
 * @param array $a The array to act on
 * @param mixed $prop The property of each element to use. Can be a single 
 * string/numeric key in which case a single prop will be used for the key. 
 * Or it can be an array of strings/numeric keys in which case multiple 
 * properties will be used, joined by a hyphen.
 * @return array The array keyed by prop
 */ 
    static function keyBy($a, $prop){
        $b = array();
        foreach($a as $r) {
            if(is_array($prop)){
                $k = array();
                foreach($prop as $f){
                    $k[] = is_object($r) ? $r->$f : $r[$f];
                }
                $k = implode('-',$k);
            } else {
                $k = is_object($r) ? $r->$prop : $r[$prop];
            }
            $b[$k] = $r;
        }
        return $b;
    }
//-------------------------------------------------------------------------   
/**
 * Extract values from a 2d array where each element is an object or an array
 * @param array $a The array to act on
 * @param str $prop The name of the property to extract from each array
 * element
 * @param mixed $preserve_keys Boolean true preserves original keys, false 
 * reindexes numerically and a string will assume it's a key of the element to
 * use as a key
 * @return array A single dimensional array containing the extracted values
 */
    static function extractByKey($a, $prop, $preserve_keys=false){
        $b = array();
        if($preserve_keys){
            foreach($a as $k=>$r) {
				if(is_string($preserve_keys)){
					$key = is_object($r) ? $r->{$preserve_keys} : $r[$preserve_keys];
				} else {
					$key = $k;
				}
                $b[$key] = is_object($r) ? $r->{$prop} : $r[$prop];
            }
        } else {
            foreach($a as $r) {
                $b[] = is_object($r) ? $r->{$prop} : $r[$prop];
            }
        }
        return $b;
    }
//---------------------------------------------------------------------------- 
/**
 * Strip empty strings from an array recursively
 * @param array $a The array to strip empty values from
 * @return array The input array stripped of empty values 
 */
    static function stripEmpty($a){
        $b = array();
        foreach($a as $k=>$v){
            if($v == ''){
                continue;
            }
            if(is_array($v)){
                $b[$k] = self::stripEmpty($v);
            } else {
                $b[$k] = $v;
            }
        }
        return $b;
    }
//--------------------------------------------------------------------------    
/**
 * Search a multi-dimensional array for a value
 * @param array $a The array to search
 * @param string $prop The property of each element to check
 * @param mixed $value The value to match
 * @param bool $strict Whether to use strict equality (===) or not
 * @return mixed The matched element
 */
    static function mdsearch($a, $prop, $value, $strict=true){
        foreach($a as $r){
            $val = is_object($r) ? @$r->$prop : @$r[$prop];
			if($strict){
				if($val === $value){
					return $r;
				}
			} else {
				if($val == $value){
					return $r;
				}
			}
        }
        return false;
    }
//--------------------------------------------------------------------------
/** 
 * Sort a multi-dimensional array by a deep property 
 * e.g. miArray::mdsort($a, 'x.y.z')
 * @param array $a The array to sort, done in place like other sort functions
 * @param mixed $prop The property name to sort on
 * @param int $dir The direction to sort in, 0 for ascending, 1 (or anything
 * @param bool $maintain_keys Whether to retain keys or not (like difference 
 * between sort and asort), defaults to true
 */
    static function mdsort(&$a, $prop, $dir=0, $maintain_keys=true){
        $dir = $dir == 0 ? '<' : '>';
        $test = array_values($a);
        if(is_array($test[0])){
            $prop = '["'.implode('"]["',explode('.',$prop)).'"]';
        } else if(is_object($test)) {
            $prop = '->'.implode('->',explode('.',$prop));
        } else {
            trigger_error('Cannot mdsort', E_USER_WARNING);
            return 0;
        }
        
        $fnc = 'return $a'.$prop.' '.$dir.' $b'.$prop.' ? -1 : 1;';
        $fnc = create_function('$a, $b',$fnc);
        return $maintain_keys ? uasort($a, $fnc) : usort($a, $fnc);
    }
//--------------------------------------------------------------------------
/**
 * Group a multi-dimensional array by one of it's elements properties and
 * preserves the original keys.
 * 
 * @param array $a The array to act on
 * @param string $prop The property to group on
 * @return array A multi-dimensional array, each containing all the elements
 * who share the same $prop value, keyed by $prop
 */
    static function mdgroup($a, $prop) {
        $b = array();
        foreach($a as $k=>$el){
            $ob = (object)$el;
            $b[$ob->$prop][] = $el;
        }
        return $b;
    }

//--------------------------------------------------------------------------
/**
 * Sorts an array by keys in a case insensitive way
 */
	static function knatcasesort(&$a){
		$keys = array_keys($a);
		natcasesort($keys);
		$tmp = array();
		foreach($keys as $k){
			$tmp[$k] = $a[$k];
		}
		$a = $tmp;
		return;
	}
//---------------------------------------------------------------------------
/**
 * Filter an array so only elements with a certain value are returned. Works
 * with Iterator objects too, but in that case it's assumed that each element 
 * is also an object.
 */
	static function filter($a,$prop,$val,$strict=false){
		$tmp = array();
		foreach($a as $el){
			$match = is_object($el) ? $el->$prop : $el[$prop];
			if($strict && $match === $val){
				$tmp[] = $el;
			} else if($match == $val){
				$tmp[] = $el;
			}
		}
		return $tmp;
	}
//---------------------------------------------------------------------------
/**
 * Add all the numbers in an array with $prop > 0
 */
	static function sumMoreThanZero($a,$prop){
		$vals = miArray::extractByKey($a,$prop);
		$total = 0;
		foreach($vals as $v){
			if($v > 0){
				$total += $v;
			}
		}
		return $total;
	}
//---------------------------------------------------------------------------
/**
 * This takes a flat array and converts it to a 2d array, where in each element 
 * the key is assigned to $key_label and the value is assigned to $val_label e.g.
 * 
 * $foo = array(10=>'Fred',20=>'bar','30'=>'Twenty');
 * $foo = expand($foo,'id','name');
 * print_r($foo);
 * array(
 * 	array('id'=>10,'name'=>'Fred'),
 * 	array('id'=>20,'name'=>'bar'),
 * 	array('id'=>30,'name'=>'Twenty')
 * )
 */
	static function expand($a,$key_label,$val_label){
		$tmp = array();
		foreach($a as $k=>$v){
			$tmp[] = array($key_label=>$k,$val_label=>$v);
		}
		return $tmp;
	}
//---------------------------------------------------------------------------
}
