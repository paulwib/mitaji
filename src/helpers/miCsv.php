<?php

/**
 * miCSV
 * 
 * Some helpers for working with CSV data and files
 */
class miCsv {

/**
 * Convert a CSV file to an array structure
 * @param string $file The path to the file
 * @param bool $header_row Whether the CSV file has a header row. If true
 * the column headings will be used for the array keys of each element.
 * @param string $dl The field delimiter in the CSV file @see fgetcsv
 * @param string $enc The character enclosing fields @see fgetcsv
 * @return array AN array structure representing the files contents
 */
    static function file2array($file, $header_row=true, $dl=',',$enc='"'){
        if (!is_file($file)) {
            return false;
        }
        if (!is_readable($file)) {
            return false;
        }
        // Find the longest line in the file
        $len = miStr::longest(file($file));
        $handle = fopen($file, 'r');
        if($header_row){
            $headers = fgetcsv($handle, $len, $dl, $enc);
            $line = 1;
        } else {
            $line = 0;
            $headers = false;
        }
        $out_arr = array();
        while (($data = fgetcsv($handle, $len)) !== false) {
            $line++;
            if(!$data) continue;
            if($headers && count($headers) != count($data)){
                trigger_error('[miCsv::file2array] Data has different number 
                                of columns to header in file '.$file.' on line '
                                .$line.': "'.implode(',',$data).'"',
                                E_USER_WARNING);
                continue;
            }
            $out_arr[] = $headers ? array_combine($headers, $data) : $data;
        }
        fclose($handle);
        return $out_arr;
    } 
// -------------------------------------------------------------------------
/**
 * Convert a multi-dimensional array to a CSV string as per RFC 4180
 * The advantage over fputcsv is that does not require any file I/O.
 *
 * @param array $a The array of data
 * @param array $props Optionally specify the properties of each array
 * element to include, defaults to include all
 * @param bool $header Optionally whether to write the column headers,
 * defaults to true
 * @param string $br The character to use for line break, defaults to \r\n
 * @return string A string of CSV data, with a line break at the end
 */ 
    static function array2string($a, $props=array(), $header=true, 
                                    $dl=',', $enc='"', $br="\n"){
        $b = array();        
        if(!$props){
            $props = array_keys($a[0]);
        }
        if($header){
            foreach($props as $k=>$v){
                $header_line[] = $enc.addslashes(is_numeric($k) ? $v : $k).$enc;
            }
            $b[] = implode($dl,$header_line);
        }
        foreach($a as $r){
            $line = array();    
            foreach($props as $col){
                $v = $r[$col];
                if (preg_match('/(,|\n|\r)/', $v)) {
                    if (strstr($v, '"')) {
                        $v = str_replace('"', '""', $v);
                    }
                    $v = '"' . $v . '"';
                }
                $line[] = $v;
            }
            $b[] = implode($dl,$line);
        }
        return implode($br, $b) . $br;
    }
}
