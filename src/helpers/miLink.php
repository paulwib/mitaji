<?php

class miLink {
	
	protected static $__response = false;
//---------------------------------------------------------------------------
/**
 * When this is included from miResponder with $responder->addHelper() this 
 * method is automatically called so functions below that need access to the 
 * request can get at it.
 */ 
	static function setResponse(&$response){
		self::$__response = $response;
	}
//---------------------------------------------------------------------------
/**
 * Get a URL from a template defined in urls.json
 * @param string $url_tpl_ref A refernce to the URL template given like
 * model.link_name e.g. 'Thing.list'
 * @param mixed $vars An object or array containing the variables to expand 
 * the URL with. Not all templates require variables so defaults to false
 * @return string The expanded URL 
 */
	static function link($url_tpl_ref, $vars=false){
		if(!self::$__response){
			trigger_error('miLink::link needs access to the responder',E_USER_ERROR);
			return false;
		}
		list($c,$name) = explode('.',$url_tpl_ref);
		$url_tpl = self::$__response->request->config->urls->{$c}->{$name};
		
		// Trigger error if a problem as may be doing a redirect or sumink
		if(!$url_tpl){
			trigger_error('No link available for '.$url_tpl_ref,E_USER_ERROR);
			return false;
		}
		// URL variables to expand
		if($vars){
			$tpl_url = new TemplateUri($url_tpl);
			$url = $tpl_url->expand($vars);
		
		// Simple URL
		} else {
			$url = $url_tpl;
		}

		// Deal with non document_root root
		if(self::$__response->request->rel_root){
			$url = '/'.trim(self::$__response->request->rel_root,'/').$url;
		}
		return $url;
	}
// ------------------------------------------------------------------------
/**
 * Get a URL from a template defined in urls.json wrapped in an HTML
 * anchor with text
 * @param string $txt The text of the link
 * @param string $url_tpl_ref A refernce to the URL template given like
 * model.link_name e.g. 'Thing.list'
 * @param mixed $vars An object or array containing the variables to expand 
 * the URL with. Not all templates require variables so defaults to false
 * @return string The HTML anchor tag
 */ 
	static function linkTo($txt,$url_tpl_ref,$vars=false){
		return sprintf('<a href="%s">%s</a>',
					   self::link($url_tpl_ref,$vars),
					   $txt);
	}
// ------------------------------------------------------------------------
/**
 * Generate a UL menu of top level links from urls.json. Will only include
 * URLs that have one path token or less and do not contain 
 * template vars so '/things/' would be included but '/things/reports/'
 * '/things/{id}/' and '/{foo}' would not. Also only includes one URL per
 * model to avoid repeating URLs
 * @param array $ul_atts Attributes to apply to wrapper UL, like a class name
 * or id or something, optional
 * @return string an HTML unordered list of links (will be in order they
 * appear in urls.json)
 */
	static function linkMenu($ul_atts=array(), 
                        $starts_with_url_path_token=false, $exclude=array()){
		$urls = self::$__response->request->config->urls;
		if(!$urls) { 
			return false;
		}
		$menu = array();
		foreach($urls as $model=>$url_tpls){
			if(is_string($url_tpls)){
				$url_tpls = array($url_tpls);
			}
			foreach($url_tpls as $url_tpl_name=>$url_tpl){
                if (in_array("$model.$url_tpl_name", $exclude)) {
                    continue;
                }
				// Get all items below a certain path
				if($starts_with_url_path_token){
					$rx = '/^\/'.$starts_with_url_path_token.'\//';
					if(!preg_match($rx,$url_tpl)){
						continue;
					}
					$tmp_url = preg_replace($rx,'',$url_tpl);
				} else {
					$tmp_url = $url_tpl;
				}
				// Work out title for link
				$tokens = preg_split('/\//',$tmp_url,-1,PREG_SPLIT_NO_EMPTY);
				if(count($tokens) <= 1 && !strstr($url_tpl,'{')){
					$const = strtoupper(AkInflector::underscore($model)).'_TITLE_'.strtoupper($url_tpl_name);
					if(!$txt = @constant($const)){						
						if('/' == $tmp_url){
							$txt = AkInflector::humanize($model);
						} else {
							$txt = AkInflector::pluralize(
										AkInflector::titleize($model)
									);
						}
					}
					$menu[$txt] = $url_tpl;
					break;
				}
			}
		}
		return self::linkUl($menu,false,$ul_atts);
	}
//---------------------------------------------------------------------------
/**
 * Generate a UL menu with first and last classes for LI elements, and
 * additional class names based on current URI and customizable UL and
 * LI class prefix.
 * 
 * @param array $links An array of links where the link text to use is the key
 * and each elements is a URL or an array of link attributes (make sure to 
 * include an element named `href` or `url` for the actual link). Allowed 
 * attributes are href, id, class, rel and title.
 * @param string $current_uri The current URL, optional. If supplied any link
 * with a URL that matches $current_uri will be given the class `selected`. 
 * Any that partially match the path will be given class `in-path`.
 * @param string $atts An array of attribute for the UL
 * @return string an HTML string
 * 
 */
    static function linkUl($links, $current_uri=false, $atts=array()){
		if($current_uri === false){
			$current_uri = self::$__response->request->uri;
		}
        $builder = miHtml::builder();
        $html = '';
		$i = 0;
		$count = count($links)-1; 
        foreach($links as $label=>$link){
			// Make sure you can use `href` or `url` for the URL
			if(is_array($link) && @$link['url']){
				$link['href'] = $link['url'];
				unset($link['url']);
			}
			$a_atts = is_array($link) ? $link : array('href'=>$link);
			$li_atts = array();
			foreach($a_atts as $k=>$v){
				if(preg_match('/^li_/',$k)){
					$li_atts[preg_replace('/^li_/','',$k)] = $v;
					unset($a_atts[$k]);
				}
			}
			if(array_key_exists('class',$li_atts)){
				$li_atts['class'] = @(array)$li_atts['class'];
			}
			if($i == 0){
				$li_atts['class'][] = 'first';
			}
			if($i == $count){
				$li_atts['class'][] = 'last';
			}
			if($a_atts['href'] == $current_uri){
				$li_atts['class'][] = 'selected';
			} else if($current_uri && miStr::beginsWith($current_uri,rtrim($a_atts['href'],'/'))){
				$li_atts['class'][] = 'in-path';
			}
			// Make sure only allowed attributes on link
			$a_atts = array_intersect_assoc($a_atts,array('href'=>@$a_atts['href'],
														  'title'=>@$a_atts['title'],
														  'rel'=>@$a_atts['rel'],
														  'class'=>@$a_atts['class'],
														  'id'=>@$a_atts['id']));
            $html .= $builder->li($li_atts, $builder->a($a_atts,$label));
			$i++;
        }
		return $builder->ul($atts,$html);
    }
// ----------------------------------------------------------------------------
/**
 * Create a sort link
 * @param string $val The value to sort on
 * @param string $url The URL to add sort value to, if omitted uses current
 * @param string $o_name The name of the sort URL param, defaults to 'o'
 * @param string $p_name The name of the page URL param, defaults to 'p'
 * @return string The new URL
 */    
    static function linkSort($val, $url=false, $o_name='o', $p_name='p'){
		if(!$url){
			$url = self::$__response->request->uri;
			$get = self::$__response->request->get_vars; 
		} else if($get_str = parse_url($url,PHP_URL_QUERY)){
			parse_str($get_str,$get);
		} else {
			$get = array();
		}
		$c_order = @$get[$o_name];
        $order = urlencode($val);
        if (@$c_order && $c_order == $val.' ASC') {
            $order .= '+DESC';
        } else {
            $order .= '+ASC';
        }
		// Unset page and current order by
        $url = self::unsetUrlVar($url, $p_name);
        $url = self::unsetUrlVar($url, $o_name);
		// Set page to 1 and add new order by
        $url = self::setUrlVar($url, $p_name, 1);
        $url = self::setUrlVar($url, $o_name, $order);
        return $url;
    }
// ----------------------------------------------------------------------------
/**
 * As linkSort, except puts the link in an anchor tag and adds a class if
 * it is the current sort URL paramater of sort-asc or sort-desc depending.
 * @see miLink::linkSort
 */
	static function linkToSort($val, $label=false, $url=false, $o_name='o', $p_name='p'){
		// Get the URL
		$sort_url = self::linkSort($val, $url, $o_name, $p_name);
		if(!$url){
			$get = self::$__response->request->get_vars; 
		} else if($get_str = parse_url($url,PHP_URL_QUERY)){
			parse_str($get_str,$get);
		} else {
			$get = array();
		}
		// Set up link atts
		$atts = array('href'=>$sort_url,'class'=>array('sort'));
		
		// Get current order
		if($c_order = @$get[$o_name]){
			// See if it is the same as $val and capture direction
			if(preg_match("/^$val( ASC| DESC)?$/i",$c_order,$matches)){
				$atts['class'][] = 'sort-'.trim(strtolower($matches[1]));
			}
		}
		// Build the link
        $builder = miHtml::builder();
		return $builder->a($atts, $label ? $label : AkInflector::humanize($val));
	}
//---------------------------------------------------------------------------
    static function linkToPages($pages, $cpage, $abridge=true, $url=false, $p_name='p') {
        
		if ($pages <= 1) return '';
		
		if(!$url){
			$url = self::$__response->request->uri;
			$get = self::$__response->request->get_vars; 
		} else if($get_str = parse_url($url,PHP_URL_QUERY)){
			parse_str($get_str,$get);
		} else {
			$get = array();
		}
		
        $builder = miHtml::builder();

        $html = '';
        if ($cpage > 1) {
            $tmp_url = self::setUrlVar($url, $p_name, ($cpage-1));
            $html .= $builder->li(array('class'=>'prev'), 
                        $builder->a(array('href'=>$tmp_url,
                                          'title'=>'Previous Page', 
                                          'rel'=>'prev'),
                                    '&laquo;Prev')
                    );
        }
        for ($i = 0; $i < $pages; $i++) {
            $is_start = $i < 5;
            $is_finish = $i > ($pages - 5);
            $min_range = ($cpage - 6);
            $max_range = ($cpage + 6);
            $in_range = $i > ($min_range) && $i < ($max_range);
            $in_range_2 = $cpage < 5 && $i < 10;
            $in_range_3 = $cpage > ($pages - 5) && $i > ($pages - 10);
            if (!$abridge || $is_start || $is_finish || $in_range || $in_range_2 || $in_range_3) {
                $hellip = false;
                $page = $i + 1;
                if ($i == $cpage - 1) {
                    $html .= $builder->li(array('class'=>'selected',
                                            'title'=>'Current Page'),
                                        $page);
                } else {
                    $tmp_url = self::setUrlVar($url, $p_name, $page);
                    $html .= $builder->li(
                                $builder->a(array('href'=>$tmp_url,
                                          'title'=>"Page $page of $pages"),
                                    $page));
                }
            } else if (!$hellip) {
                $html .= $builder->li(array('class'=>'hellip',
                                            'title'=>'Skipped...'),
                                        '&hellip;');
                $hellip = true;
                continue;
            }
        }
        if ($cpage < $pages) {
            $tmp_url = self::setUrlVar($url, $p_name, ($cpage+1));
            $html .= $builder->li(array('class'=>'next'), 
                        $builder->a(array('href'=>$tmp_url,
                                          'title'=>'Next Page', 
                                          'rel'=>'next'),
                                    'Next &raquo;')
                    );
        }
        return $builder->ul($html);
    }
// ----------------------------------------------------------------------------
/**
 * Sets a variable in the query string of the given URL
 * @param string $url The URL to add the variable to
 * @param string $name The name of the variable
 * @param string $value The value of the variable
 * @return string The new URL
 * @todo Use the current URL if none set
 */
   static function setUrlVar($url,$name,$value){
        $purl = parse_url(self::unsetUrlVar($url,$name));
        if(isset($purl['query'])){        
            return $purl['path'].'?'.$purl['query']."&amp;$name=$value";
        } else {
            return $purl['path']."?$name=$value";
        }
    }
// ----------------------------------------------------------------------------
/**
 * Unsets a variable in the query string of the given URL
 * @param string $url The URL to add the variable to
 * @param string $name The name of the variable
 * @return string The new URL, any domain name will be lost
 * @todo Use the current URL if none set
 */
    static function unsetUrlVar($url,$name){
        $purl = parse_url($url);
        if(!isset($purl['query'])){
            return $url;
        }
        $arr = array();
        parse_str($purl['query'],$arr);
        if(!isset($arr[$name])){
            return $url;
        }
        unset($arr[$name]);
        if(count($arr)){
            return $purl['path'].'?'.http_build_query($arr);
        } else {
            return $purl['path'];
        }
    }
// ----------------------------------------------------------------------------
	static function copyVarsToUrl($url,$keys,$get=false){
		if(false == $get){
			if(!$get = self::$__response->request->get_vars){
				return $url;
			}
		}
		if(!is_array($keys)){
			return $url;
		}
		$get_keys = array_keys($get);
		foreach($keys as $k){
			if(in_array($k,$get_keys) && $get[$k] != ''){
				$url = miLink::setUrlVar($url,$k,$get[$k]);
			}
		}
		return $url;
	}
// ----------------------------------------------------------------------------
/**
 * Appends a token to the path of the given URL 
 * @param string $url The URL to append to
 * @param $token The token to append 
 * @return string the new URL (any domain name will be lost)
 */
	static function appendToPath($url,$token){
		$purl = parse_url($url);
		return rtrim($purl['path'],'/').'/'.$token.'/'.(@$purl['query'] ? '?'.$purl['query'] : '');
	}
// ----------------------------------------------------------------------------
}


?>
