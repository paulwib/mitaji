<?php

class miFormHelper {
    // Cache for looked up options
	private static $__lookup_opts_cache = array();
    // Store context 
    private static $__context           = false;
    private static $__context_has_many  = false;
    private static $__context_iterator  = 0;
    private static $__context_iterators = array();

    /**
     * Set context which will get pre-pended to form names, used when
     * for has one/has many relations
     * @param string $context   The context name
     * @param mixed $has_many   If has many relation give class name of has many
     *                          and field names/ids will be made a sub-array until
     *                          name/id requested for a different resource type
     */
    static function setFormContext($context, $has_many=false){
        self::$__context = $context;
        self::$__context_iterator = 0;
        self::$__context_iterators = array();
        self::$__context_has_many = $has_many;
    }
// ---------------------------------------------------------------------	
	static function attId($Resource, $fld, $alias=false, $id=false){
        if (!($name = self::attName($Resource, $fld, $alias, $id))) {
            return false;
        }
        return trim(
            str_replace(']_', '_', 
                str_replace('[', '_', $name)
        ), ']');
	}
// ---------------------------------------------------------------------
	static function attName($Resource, $fld, $alias=false, $id=false){
        // Negating yo' self? 
        if ($alias) {
            return $alias;
        }
		if (is_object($Resource)) {
            $r = get_class($Resource);
            $id = $Resource->primaryKey();
		}
        elseif (is_string($Resource)) {
            $r = $Resource;
        }
        else {
			trigger_error("miFormHelper::attName: Resource must be string or object", E_USER_WARNING);
            return false;
        }
        if (self::$__context && $r != self::$__context) {
            $c = self::$__context;
            if (self::$__context_has_many) {
                $name = $c .'[' . AkInflector::pluralize($r) . ']';
                if ($id) {
                    $i_key = $r . '_' . $id;
                    if (!array_key_exists($i_key, self::$__context_iterators)) {
                        self::$__context_iterators[$i_key] = self::$__context_iterator++;
                    }
                    $i = self::$__context_iterators[$i_key];
                }
                else {
                    $i = 'new';
                }
                $name .= "[$i]";
            }
            else {
                $name = $c .'[' . $r . ']';
            }
            $name .= "[$fld]";
        }
        else {
            $name = $r . '[' . $fld . ']';
        }
        return $name;
	}
// ---------------------------------------------------------------------
	static function standardAtts($Resource, $fld, $atts){
		$alias = false;
		if(@$atts['alias']){
			$alias = $atts['alias'];
			unset($atts['alias']);
		}
		// If no id set...
		if(!array_key_exists('id', $atts)){
			// If custom name used then convert to id string
			if(@$atts['name']){
				$atts['id'] = str_replace(array('[',']'), array('_',''),$atts['name']);
			// Otherwise based on the resource
			} else {
				$atts['id'] = self::attId($Resource,$fld,$alias);
			}
		}
		// If no name set get default
		if(!@$atts['name']){
			$atts['name'] = self::attName($Resource,$fld,$alias);
		}
		// If no value set use resource's current value
		if(!array_key_exists('value', $atts)){
			$atts['value'] = htmlspecialchars((string)$Resource->$fld);
		}
		return $atts;
	}	
// ---------------------------------------------------------------------
	static function label($Resource,$fld,$txt=false,$atts=array()){
		$builder = miHtml::builder();
		$atts['for'] = self::attId($Resource,$fld);
		return $builder->label($atts,($txt ? $txt : AkInflector::titleize($fld)));
	}
// ---------------------------------------------------------------------
	static function lookupOpts($Resource,$fld,$use_flds,$filter=array()){
		$options = array();
		$class = AkInflector::modulize(preg_replace('/_id$/','',$fld));
		$cache_key = $class.md5(print_r($use_flds,true).print_r($filter,true));
		$found_alias = false;
		if(!@class_exists($class) && !($class = $Resource->getAssociateRealClass($class))){
			trigger_error('Cannot look up options for '.$fld,E_USER_ERROR);
		}
		if(array_key_exists($cache_key,self::$__lookup_opts_cache)){
			return self::$__lookup_opts_cache[$cache_key];
		}
		$model = new $class();
		$resources = $model->find();
        if(!$resources || !$resources->count()){
            return false;
        }
		if($filter){
			$resources->filter($filter);
		}
		// Always find everything
		$resources->pageSize(0);
		if(!$resources->count()){
			trigger_error('No options for '.$fld,E_USER_NOTICE);
			return false;
		}
		foreach($resources as $rs){
			$str = '';
			foreach($use_flds as $u_fld){
				if($rs->$u_fld){
					$str .= $rs->$u_fld . ' ';
				} else {
                    $str .= $u_fld . ' ';
                }
			}
			$options[$rs->id] = trim($str);
		}
		self::$__lookup_opts_cache[$cache_key] = $options;
		return $options;
	}
// ---------------------------------------------------------------------	
	static function hiddenfield($Resource,$fld,$atts=array()){
		$builder = miHtml::builder();
		$atts = self::standardAtts($Resource,$fld,$atts);
		$atts['type'] = 'hidden';
		return $builder->input($atts);
	}
// ---------------------------------------------------------------------	
	static function textfield($Resource,$fld,$atts=array()){
		$builder = miHtml::builder();
		$atts = self::standardAtts($Resource,$fld,$atts);
		$atts['type'] = 'text';
		if(!array_key_exists('size',$atts)){
			$atts['size'] = '28';
		}
		return $builder->input($atts);
	}
// ---------------------------------------------------------------------	
	static function textarea($Resource,$fld,$atts=array()){
		$builder = miHtml::builder();
		$atts = self::standardAtts($Resource,$fld,$atts);
		if(!array_key_exists('cols',$atts)){
			$atts['cols'] = '32';
		}
		if(!array_key_exists('rows',$atts)){
			$atts['rows'] = '8';
		}
		$text = $atts['value'];
		unset($atts['value']);
		return $builder->textarea($atts,$text);
	}
// ---------------------------------------------------------------------	
	static function checkbox($Resource,$fld,$atts=array()){
		$builder = miHtml::builder();
		// If no value supplied, assume a boolean
		if(!array_key_exists('value',$atts)){
			$atts['value'] = '1';
		}
		$atts = self::standardAtts($Resource,$fld,$atts);
		$atts['type'] = 'checkbox';
		// See if checked by comparing resource value to input value
		if(@$Resource->$fld == $atts['value']){
			$atts['checked'] = 'checked';
		}
		return $builder->input($atts);
	}
// ---------------------------------------------------------------------
/**
 * Make a select field, normally for some associate referenced by a foreign key
 * @param object $Resource An instance of a model
 * @param string $fld The name of the field
 * @param mixed $options Either an array of options with values for keys and text
 * for array values, or a string which means look up the values in this model
 * and using ids for values and field names in string forms a template for
 * each option e.g. select($Entry,'category_id','description') and you can also
 * start with an empty option by changing it to something like 
 * 'Please select:description', or you can also pass a number and it will make
 * options that represent each integer up to that value.
 * @param array $atts any additional HTML attributes. In this context 'value'
 * means the selected value.  
 */
	static function select($Resource,$fld,$options,$filter=array(),$atts=array()){
		$builder = miHtml::builder();
		$option_tags = array();
		$atts = self::standardAtts($Resource,$fld,$atts);
		// Look up options from another model
		if(is_string($options)){
			$empty_option = false;
			if(strstr($options,':')){
				$tmp = explode(':',$options);
				$empty_option = $tmp[0];
				$options = $tmp[1];
			}
			if(!$options = self::lookupOpts($Resource,$fld,explode(' ',$options),$filter)){
				trigger_error('No options available for '.$fld,E_USER_WARNING);
				return false;
			}
			if($empty_option){
				$option_tags[] = $builder->option(array('value'=>' '),$empty_option);
			}
		// Numeric range of options from 0 to $options
		} else if(is_int($options)){
			$options = range(0,$options);
		}
		// Build options
		foreach($options as $value=>$text){
            $opt_atts = array();
            $opt_atts['value'] = $value;
            if($atts['value'] == $value){
				$opt_atts['selected'] = 'selected';
			}
            $option_tags[] = $builder->option($opt_atts, $text);
        }		
		unset($atts['value']); // Unset meaningless attribute
		return $builder->select($atts, implode("\n", $option_tags)); // Wrap in the select
	}
// ---------------------------------------------------------------------
/*
 * General select field, just pass attributes and options. The attributes
 * can contain a value which will select the option with that value.
 */
	static function selectField($atts,$options,$text_value=false){
		$builder = miHtml::builder();
		foreach($options as $value=>$text){
			if($text_value){
				$opt_atts = array('value'=>$text);
			} else {
            	$opt_atts = array('value'=>$value);
			}
            if((string)@$atts['value'] == (string)$value){
				$opt_atts['selected'] = 'selected';
			}
            $option_tags[] = $builder->option($opt_atts, $text);
        }
        unset($atts['value']);
        return $builder->select($atts, implode("\n", $option_tags));
	}
// ---------------------------------------------------------------------
/**
 * As above, in this context att['value'] means the selected value
 */
	static function radioset($Resource,$fld,$options,$filter=array(),$atts=array()){
		$builder = miHtml::builder();
		$atts = self::standardAtts($Resource,$fld,$atts);
		// Look up options from another model
		if(is_string($options)){
			if(!$options = self::lookupOpts($Resource,$fld,explode(' ',$options),$filter)){
				trigger_error('No options available for '.$fld,E_USER_WARNING);
				return false;
			}
		}
		// Build options
		$i = 0;
		foreach($options as $value=>$text){
            $radio_atts = array();
			$radio_atts['type'] = 'radio';
			$radio_atts['id'] = $atts['id'].'_'.($i++);
			$radio_atts['name'] = $atts['name'];
            $radio_atts['value'] = $value;
            if(@$atts['value'] == $value){
				$radio_atts['checked'] = 'checked';
			}
			$label_atts['for'] = $radio_atts['id'];
            $li_tags[] = $builder->li($builder->input($radio_atts).
									  $builder->label($label_atts,$text));
        }
		unset($atts['value'],$atts['name']); // Unset meaningless attributes
		return $builder->ul($atts,implode("\n", $li_tags)); // Wrap in UL
	}
// ---------------------------------------------------------------------
	static function checkboxes($Resource, $fld, $options, $filter=array(), $atts=array(), $start_index=0){
		$builder = miHtml::builder();
		$atts = self::standardAtts($Resource, $fld, $atts);
		$base_name = self::attName($Resource, $fld);
		if(!array_key_exists('id',$atts)){
			$base_id = self::attId($atts['name']);
		} else {
			$base_id = $atts['id'];
		}
		// Look up options from another model
        if(is_string($options)){
            if(!$options = self::lookupOpts($Resource,$fld,explode(' ',$options),$filter)){
                trigger_error('No options available for '.$fld, E_USER_WARNING);
                return false;
            }
        }
        /*
         * If value supplied that will assume to be values checked, otherwise 
         * extracts id from property
         */
        if(isset($atts['value'])){
            $checked =  is_array($atts['value']) ? $atts['value'] : array();
        }
        else {
		    $checked = miArray::extractByKey($Resource->$fld, 'id');
        }
		// Build options
		$i = $start_index;
		foreach($options as $value=>$text){
            $cb_atts = array();
			$cb_atts['type'] = 'checkbox';
			$cb_atts['id'] = $atts['id'].'_'.($i);
			$cb_atts['name'] = $atts['name'].'['.$i.']['.AkInflector::foreignKey($fld).']';
            $cb_atts['value'] = $value;
            if(in_array($value, $checked)){
				$cb_atts['checked'] = 'checked';
			}
			$label_atts['for'] = $cb_atts['id'];
            $li_tags[] = $builder->li($builder->input($cb_atts).
									  $builder->label($label_atts,$text));
			$i++;
        }
		unset($atts['value'],$atts['name']); // Unset meaningless attributes
		return $builder->ul($atts,implode("\n", $li_tags));// Wrap in the UL
	}
// ---------------------------------------------------------------------
}
