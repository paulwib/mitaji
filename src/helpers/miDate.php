<?php

/**
 * This class provides a number of helpers for formatting dates and a couple
 * of other bits and pieces
 */
class miDate {
    
	const SECONDS_IN_DAY = 86400;
    public static $flip_date_month = false;
    private static $now = false;
// ---------------------------------------------------------------------------
// Returns current time or some value you hacked with seNow() for tests
    public static function now(){
        return self::$now !== false ? self::$now : time();
    }
// ---------------------------------------------------------------------------
// Set $now so can move back and forward in time, useful for testing
// Diable in production!
    public static function setNow($now){
        self::$now = miDate::strToTime($now);
    }
// ---------------------------------------------------------------------------
/**
 * Smart(ish) formatting
 */
    public static function smartDate($d){
        $ts = self::strToTime($d);
        if($ts === -1 || $ts === false) return $d;
        // If today, just print time
        if(date('d M Y',$ts) == date('d M Y')){
            return 'Today at '.date('H:i',$ts);
        }
        // This year, just print date and month
        if(date('Y',$ts) == date('Y')){
            return date('j M',$ts);
        }
		// Otherwise full date
        return date('j M Y',$ts);
    }
// ---------------------------------------------------------------------------
/**
 * As @see smartDate, but also displays time
 */
    public static function smartDateTime($d){    
        $ts = self::strToTime($d);
        if($ts === -1 || $ts === false) return $d;
        // If today, just print time
        if(date('d M',$ts) == date('d M')){
            return 'Today '.date('H:i',$ts);
        }
        // This year, just print date, month and time
        if(date('Y',$ts) == date('Y')){
            return date('d M H:i',$ts);
        }
		// Otherwise full date and time
        return date('d M Y H:i',$ts);
    }
// ---------------------------------------------------------------------------
/**
 * Format a date with full day, month and year
 */
    public static function smartDateYear($d){
        $ts = self::strToTime($d);
        if($ts === -1 || $ts === false) return $d;
        return date('d M Y',$ts);
    }
// ---------------------------------------------------------------------------
/**
 * Format a date with full day, month and year
 */
    public static function fullDate($d){
        $ts = self::strToTime($d);
        if($ts === -1 || $ts === false) return $d;
        return date('d M Y',$ts);
    }
// ---------------------------------------------------------------------------
/**
 * Format a date in a long way
 */
    public static function longDate($d,$empty=false){
        $ts = self::strToTime($d);
        if($ts === -1 || $ts === false) return $empty ? $empty : $d;
        return date('l jS F Y',$ts);
    }
// ---------------------------------------------------------------------------
/**
 * Format a date in a way useable in a form input
 */
    public static function formDate($d){
        $ts = self::strToTime($d);
        if($ts === -1 || $ts === false) return $d;
        return date('j M Y',$ts);
    }
// ---------------------------------------------------------------------------
/**
 * Format a date and time in a way useable in a form input
 */
    public static function formDateTime($d){
        $ts = self::strToTime($d);
        if($ts === -1 || $ts === false) return $d;
        return date('j M Y H:i:s',$ts);
    }
// ---------------------------------------------------------------------------
	public static function smartDateRange($d1,$d2,$fmt='j M Y',$dl=' - '){
        if (!$d1 && $d2) {
            return miDate::smartDate($d2);
        }
        if (!$d2 && $d1) {
            return miDate::smartDate($d1);
        }
		$ts1 = self::strToTime($d1);
		$ts2 = self::strToTime($d2);
		if(!$ts1 || !$ts2){
			return false;
		}
		$str1 = array();
		$str2 = array();
		$tokens = array_reverse(explode(' ',$fmt));
		$last_equal = false;
		$str = '';
		foreach($tokens as $token){
			$str .= $token;
			if(date($str,$ts1) == date($str,$ts2)){				
				$str2[] = date($token,$ts2);
			} else {
				$str1[] = date($token,$ts1);
				$str2[] = date($token,$ts2);
			}
		}
		if($str1){
			return implode(' ',array_reverse($str1)).$dl.implode(' ',array_reverse($str2));
		}
		return implode(' ',$str2);
	}
// ---------------------------------------------------------------------------
/**
 * Convert a date to SQL format
 */
    public static function sqlDate($d){
        $ts = self::strToTime($d);
        if($ts === -1 || $ts === false) return $d;
        return date('Y-m-d H:i:s',$ts);
    }
// ---------------------------------------------------------------------------
	public static function days($format='d',$ts=false){
		if($ts == false){
			$ts = time();
		}
		if(!$ts){
			return false;
		}
		// Start on 1st regardless of where $ts starts
		$start = strtotime(date('Y-m-01 12:00:00',$ts));
		$max = date('d',self::lastOfMonth($ts));
		if(!$max || $max>31){
			return false;
		}
		$use_strftime = strstr($format,'%');
		$i = 0;
		$days = array();
		while($i < $max){
			$ts = $start+($i*miDate::SECONDS_IN_DAY);
			$days[] = $use_strftime ? strftime($format, $ts) : date($format,$ts);
			$i++;
		}
		return $days;
	}
// ---------------------------------------------------------------------------
	public static function months($format='M',$ts=false){
		if($ts == false){
			$ts = strtotime('1st Jan');
		} else {
			$ts = strtotime(date('Y-01-01',$ts));
		}
		if(!$ts){
			return false;
		}
		$use_strftime = strstr($format,'%');
		$i = 0;
		$months = array();
		while($i<12){
			$months[] = $use_strftime ? strftime($format, $ts) : date($format,$ts);
			$ts = strtotime('+1 month',$ts);
			$i++; 
		}
		return $months;
	}
// ---------------------------------------------------------------------------
/**
 * Compute the difference in days between two dates
 */
    public static function diffDays($start,$end){
	    return ceil((self::strToTime($end)-self::strToTime($start)) / miDate::SECONDS_IN_DAY);
    }
// ---------------------------------------------------------------------------
/**
 * Format a number of seconds as a duration
 * @param int $secs The duration in seconds (required)
 * @param str $u_secs The unit to use for seconds. If evaluates to false
 * the seconds unit will not be displayed.
 * @param str $u_mins The unit to use for minutes. If evaluates to false
 * the minutes unit will not be displayed.
 * @param str $u_hours The unit to use for hours. If evaluates to false
 * the hours unit will not be displayed.
 * @param str $u_days The unit to use for days. If evaluates to false
 * the days unit will not be displayed.
 * @param bool $conditional_plural If true will attempt to pluralize the 
 * units using AkInflector
 */
    public static function durationFormat($secs,
                            $u_secs='s', 
                            $u_mins='m',
                            $u_hours='h',
                            $u_days='d',
                            $conditional_plural=false){
            $d = array();
            if($u_days){
                $days = floor($secs / miDate::SECONDS_IN_DAY);
                $secs -= $days * miDate::SECONDS_IN_DAY;
                if($days){
                    $d[] = $days.($conditional_plural ? 
						AkInflector::conditionalPlural($days,$u_days) :
						$u_days);
                }
            }
            if($u_hours){
                $hours = floor($secs / 3600);
                $secs -= $hours * 3600;
                if($hours){
                    $d[] = $hours.($conditional_plural ? 
						AkInflector::conditionalPlural($hours,$u_hours) :
						$u_hours);
                }
            }
            if($u_mins){
                $mins = floor($secs / 60);
                $secs -= $mins * 60;
                if($mins){
                    $d[] = $mins.($conditional_plural ? 
						AkInflector::conditionalPlural($mins,$u_mins) :
						$u_mins);
                }
            }
            if($u_secs){
                if($secs){
                    $d[] = $secs.($conditional_plural ? 
						AkInflector::conditionalPlural($secs,$u_secs) :
						$u_secs);
                }
            }
            return implode(' ',$d);
        }
// ---------------------------------------------------------------------------
/**
 * Convert string to time. 
 * The only difference from native strtotime is that if the current timezone 
 * is not in America the date and month will be flipped as native strtotime
 * expects short dates to be in US format mm/dd/yy.
 * @param string $str The string to parse
 * @param int $now The timestamp which is used as a base for the calculation 
 * of relative dates like "+1 day"
 * @param bool $force_non_us Forces date to be considered in non-US style
 * regardless of value returned by date_default_timezone_get
 * @return A timestamp or false if the date cannot be parsed (normalizes the 
 * behaviour prior to PHP 5.1.0 which would return -1 if date could not be 
 * parsed)  
 */ 
    public static function strToTime($str,$now=false,$force_non_us=false){
		if(is_int($str)){
			return $str;
		}
		if($force_non_us || !stristr(date_default_timezone_get(),'America')) {
            $str = preg_replace(
               '/^\s*([\d]{1,2})[\/\. -]+([\d]{1,2})[\/\. -]*([\d]{0,4})/',
               '\\2/\\1/\\3', 
               $str);
        	$str = trim($str,'/.-');
        }
        $ts = $now !== false ? strtotime($str,$now) : strtotime($str);
		if($ts == -1){
			$ts = false;
		}
		return $ts;
    }
// ---------------------------------------------------------------------------
/**
 * Return the timestamp for the last second of the month that the provided 
 * timestamp is in
 */
    public static function lastOfMonth($ts){
        return strtotime('-1 day 23:59:59',
                    strtotime('+1 month',
                            strtotime(date('1 M Y 00:00:00',$ts))
                    )
                );
    }
// ---------------------------------------------------------------------------
}
?>
