<?php

class miStr {

    // ----------------------------------------------------------------------------
    /**
     * Checks if $str begins with a certain string
     * @param string $str The subject string
     * @param string $begins_with The string to test it begins with
     * @return bool
     */
    static function beginsWith($str, $begins_with){
        return $begins_with == substr($str, 0, strlen($begins_with));
    }
    // ----------------------------------------------------------------------------
    /**
     * Checks if $str ends with a certain string
     * @param string $str The subject string
     * @param string $ends_with The string to test it ends with
     * @return bool
     */
    static function endsWith($str, $ends_with){
        return $ends_with == substr($str, strlen($ends_with)*-1);
    }
    // ----------------------------------------------------------------------------
    /**
     * Checks if $str begins with a capital letter
     * @param string $str The subject string
     * @return bool
     */
    static function isCapitalized($str){
        return strtolower($str{0}) != $str{0};
    }
    // ----------------------------------------------------------------------------
    /**
     * Returns the length of longest string in array of strings
     * @param array $strs An array of strings
     * @return int
     */
    static function longest($strs=array()){
        $len = 0;
        foreach($strs as $str){
            $len = max($len, strlen($str));
        }
        return $len;
    }
    // ----------------------------------------------------------------------------
    /**
     * Sanitize a string so it is safe to use in URLs
     * @param string $str The string to sanitize
     * @param string $space The character to use for spaces, defaults to hyphen '-'
     * @return string
     */
    static function sanitize($str, $space='-'){
        // Kill entities
        $str = preg_replace('/&.+?;/', '', $str);
        // Remove non alpha-numerics
        $str = preg_replace('/[^a-zA-Z0-9 _-]/', '', $str);
        // Condense white space
        $str = preg_replace('/\s+/', ' ', $str);
        // Replace spaces with $space
        $str = str_replace(' ', $space, $str);
        // Remove excessive $space characters
        $str = preg_replace('|-+|', $space, $str);
        // Lowercase and trim space
        $str = trim(strtolower($str),$space);
        return $str;
    }
    // ----------------------------------------------------------------------------
    /**
     * Truncate
     * @param string $str The string to truncate
     * @param int $length The length to truncate too
     * @param string $etc A string to append if truncated
     * @param bool $break_words Whether to break words or not
     * @params bool $span If set to true the string will be wrapped in a span with
     *                    the original text as it's title attriute.
     * @return string
     */

    static function truncate($str, $length=80, $etc='&hellip;',$break_words=true, $span=false) {
        if ($length == 0){
            return '';
        }
        if (strlen($str) <= $length) {
            return $str;
        }
        $original = $str;
        $length -= preg_match('/^&.+;/',$etc) ? 1 : strlen($etc);
        if (!$break_words) {
            $str = preg_replace('/\s+?(\S+)?$/', '', substr($str, 0, $length+1));
        } else {
            $str = substr($str, 0, $length);
        }
        if($span) {
            return "<span title=\"$original\">".$str.$etc.'</span>';
        }
        return $str.$etc;
    }
    // ----------------------------------------------------------------------------
    /**
     * Return a random string of characters, useful for passwords etc.
     * @param int $len The length of the required string
     * @param string $chars A string of characters to use for the source
     * @return string
     */
    static function random($len=8,
        $chars='23456789abcdfghjkmnpqrstvwxyz*^-~!'){
        $str = '';
        while($len--){
            $r = rand(0,strlen($chars));
            $char = substr($chars,$r,1);
            $str .= $char;
            // Remove the character so it isn't used twice
            $chars = str_ireplace($char,'',$chars);
        }
        return $str;
    }
    // ----------------------------------------------------------------------------
    /**
     * Concatenates strings with delimiter whilst skipping empty strings
     * @param string $dl The glue string
     * @params string All other arguments should be the strings to join together, any
     * number of arguments allowed.
     * @return string
     */
    static function concatNoEmpty(){
        $args = func_get_args();
        $dl = array_shift($args);
        if(!$args) {
            return '';
        }
        if(count($args) == 1 && is_array($args[0])){
            $args = $args[0];
        }
        $no_empty = array();
        foreach($args as $a){
            if($trimmed = trim($a)){
                $no_empty[] = $trimmed;
            }
        }
        return implode($dl,$no_empty);
    }
    // ----------------------------------------------------------------------------
    /**
     * Format a number like a currency with currency character. Will add a minus
     * character to the beginning of the string if less than zero.
     *
     * @param int $n The number to format
     * @param string $char The currency character
     * @param int $decimals Number of decimal places
     * @return string
     */
    static function currency($n, $char=false, $decimals=2,$truncate_zero_decimals=false){
        if(!is_numeric($n)){
            $n = 0;
        }
        $str = number_format($n,$decimals);
        if($truncate_zero_decimals){
            $str = preg_replace('/\.00$/','',$str);
        }
        if($n >= 0){
            return $char.$str;
        }
        // Move the neg before the currency symbol
        $str = str_replace('-','',$str);
        return '-'.$char.$str;
    }
    # ----------------------------------------------------------------------------
    /**
     * Percentage
     * @param $n The number to convert
     * @return string
     */
    public static function percentage($n){
        return sprintf('%01.2f',$n).'%';
    }
    // ----------------------------------------------------------------------------
    /*
     * Convert a string to Ascii entities, useful for stuff like encoding
     * email addresses so can't be read by stupid spiders
     * @param string $str The string to encode
     * @return string
     */
    public static function asciiEntities($str) {

        $str = trim($str);
        $outstr = '';

        for ($i=0;$i<strlen($str);$i++) {
            $char = substr($str,$i);
            if($char != ' '){
                $outstr .= "&#".ord($char).";";
            } else {
                $oustr .= ' ';
            }
        }
        return $outstr;
    }
    // ----------------------------------------------------------------------------
    //original Title Case script © John Gruber <daringfireball.net>
    //javascript port © David Gouch <individed.com>
    //PHP port of the above by Kroc Camen <camendesign.com>

    static function titleCase ($title) {
        //remove HTML, storing it for later
        //       HTML elements to ignore    | tags  | entities
        $regx = '/<(code|var)[^>]*>.*?<\/\1>|<[^>]+>|&\S+;/';
        preg_match_all ($regx, $title, $html, PREG_OFFSET_CAPTURE);
        $title = preg_replace ($regx, '', $title);

        //find each word (including punctuation attached)
        preg_match_all ('/[\w\p{L}&`\'‘’"“\.@:\/\{\(\[<>_]+-? */u', $title, $m1, PREG_OFFSET_CAPTURE);
        foreach ($m1[0] as &$m2) {
            //shorthand these- "match" and "index"
            list ($m, $i) = $m2;

            //correct offsets for multi-byte characters (`PREG_OFFSET_CAPTURE` returns *byte*-offset)
            //we fix this by recounting the text before the offset using multi-byte aware `strlen`
            $i = mb_strlen (substr ($title, 0, $i), 'UTF-8');

            //find words that should always be lowercase…
            //(never on the first word, and never if preceded by a colon)
            $m = $i>0 && mb_substr ($title, max (0, $i-2), 1, 'UTF-8') !== ':' &&
                !preg_match ('/[\x{2014}\x{2013}] ?/u', mb_substr ($title, max (0, $i-2), 2, 'UTF-8')) &&
                preg_match ('/^(a(nd?|s|t)?|b(ut|y)|en|for|i[fn]|o[fnr]|t(he|o)|vs?\.?|via)[ \-]/i', $m)
                ?   //…and convert them to lowercase
                mb_strtolower ($m, 'UTF-8')

                //else: brackets and other wrappers
                : ( preg_match ('/[\'"_{(\[‘“]/u', mb_substr ($title, max (0, $i-1), 3, 'UTF-8'))
                ?   //convert first letter within wrapper to uppercase
                mb_substr ($m, 0, 1, 'UTF-8').
                mb_strtoupper (mb_substr ($m, 1, 1, 'UTF-8'), 'UTF-8').
                mb_substr ($m, 2, mb_strlen ($m, 'UTF-8')-2, 'UTF-8')

                //else: do not uppercase these cases
                : ( preg_match ('/[\])}]/', mb_substr ($title, max (0, $i-1), 3, 'UTF-8')) ||
                preg_match ('/[A-Z]+|&|\w+[._]\w+/u', mb_substr ($m, 1, mb_strlen ($m, 'UTF-8')-1, 'UTF-8'))
                ?   $m
                //if all else fails, then no more fringe-cases; uppercase the word
                :   mb_strtoupper (mb_substr ($m, 0, 1, 'UTF-8'), 'UTF-8').
                mb_substr ($m, 1, mb_strlen ($m, 'UTF-8'), 'UTF-8')
                ));

            //resplice the title with the change (`substr_replace` is not multi-byte aware)
            $title = mb_substr ($title, 0, $i, 'UTF-8').$m.
                mb_substr ($title, $i+mb_strlen ($m, 'UTF-8'), mb_strlen ($title, 'UTF-8'), 'UTF-8')
                ;
        }

        //restore the HTML
        foreach ($html[0] as &$tag) $title = substr_replace ($title, $tag[0], $tag[1], 0);
        return $title;
    }
    // ----------------------------------------------------------------------------
}

