<?php

class miEmail {
    
    # Set this to a writable path rather than sending
    # Useful for testing
    public static $write_to_path = false;
    
    public static function send($to,$from,$subject,
				$content,$html=false,$write_to_path=false){	
	# General headers
	$headers = '';
	$headers .= "Return-Path: $from\n";
	$headers .= "Sender: $from\n";
	$headers .= "X-Sender: $from\n";
	$headers .= "From: $from\n";
	$headers .= "Reply-To: $from\n";
	
	# HTML or plain text?
	if($html){
	    $headers .= "MIME-Version: 1.0\n";
	    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
	
	} else {
	    $headers .= "Content-type: text/plain; charset=iso-8859-1\n";
	}

	# Send the message
	if($write_to_path){
	    $new_to = str_replace('@','-at-',$to);
	    $new_to = str_replace('.','-dot-',$new_to);
	    $fname = date('Y-m-d_H:i:s__').$new_to.'__'.str_replace(' ','_',$subject).'.eml';
	    file_put_contents($write_to_path.'/'.$fname,$headers."Subject:$subject\n\n$content\n\n");
	} else {
	    mail($to,$subject,$content,$headers,'-f '.$from);
	}

	return true;
    }
}


?>