<?php

class ErrorResponder extends miResponder {
    
    const ERROR_TPL = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" 
    "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>Error %s</title>
        <meta name="generator" value="Mitaji">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>                                    
        <h1>%s</h1>
    </body>
</html>';
// --------------------------------------------------------------------------    
    function get($status, $error_str=false, $context=false){

        $this->status = $status;
		$config = $this->request->config;
        $layout_tpl = @$config->mitaji->error_layout ? 
                            $config->mitaji->error_layout : 'error.html.php';
        $this->layout = $this->request->ajax ? false : $layout_tpl;
        $this->page_title = $this->status.' '.$this->request->httpReasonPhrase($this->status);
        $this->body_class = 'http-error http-status-'.$status;
        $this->error_context = $context;
        
        // Errors
        $this->error_str = $error_str;
        $this->recent_errors = $this->request->getErrors(10);
        $this->errors = $this->request->getErrors();
        
        // Debug backtrace
		$backtrace = debug_backtrace();
		$backtrace = array_slice($backtrace,2);
		$mybacktrace = array();
		foreach($backtrace as $bt){
			$tmp['file'] = $bt['file'];
			$tmp['line'] = $bt['line'];
			$msg = '';
			if($c = @$bt['class']){
				$msg .= $bt['class'].$bt['type'];
			}
			if($fun = @$bt['function']){
				if($fun == 'trigger_error'){
					continue;
				}
				$msg .= $fun.'(';
				if($args = @$bt['args']){
					$tmp_args = array();
					foreach($args as $i=>$arg){
						if(is_object($arg)){
							$tmp_args[] = '<span class="_e_type">object</span> '.get_class($arg);
						} else if (is_array($arg)){
							$tmp_args[] = '<span class="_e_type">array</span> '.print_r($arg,true);
						} else if (is_bool($arg)){
							$tmp_args[] = '<span class="_e_type">bool</span> '.($arg ? 'true' : 'false');
						} else if (is_string($arg)){
							$tmp_args[] = '<span class="_e_type">string</span> '."'".$arg."'";
						} else {
							$tmp_args[] = '<span class="_e_type">'.gettype($arg).'</span> '.$arg;
						}
					}
					$msg .= implode(', ',$tmp_args);
				}
				$msg .= ')';
			}
			$tmp['str'] = $msg;
			$mybacktrace[] = $tmp;
		}
		$this->mybacktrace = $mybacktrace;
        
        // Get debug HTML by parsing template
        $debug_html = $this->parseTemplate('errors/debug.php');
        
        // Email error
        if ($email_to = $config->mitaji->email_errors) {
            /*
             * Only email error if one of this type has not been sent 
             * recently. "Recently" defaults to 'email_error_interval' 
             * in config/mitaji.json
             */
            $cache_flag_file = $this->request->getTempDir() . 'last_' . $this->status . '_error_emailed.txt';
            $last_error_time = @filemtime($cache_flag_file);
            $next_error_time = $last_error_time + (@$this->config->mitaji->email_error_interval ? 
                    $this->config->mitaji->email_error_interval : 900);
            if (!$last_error_time || time() > $next_error_time) {
                $Email = new miResponder($this->request, $this->config);
                $Email->headers['To'] = $email_to;
                $Email->headers['From'] = @$this->config->mitaji->email_errors_from ?  
                        $this->config->mitaji->email_errors_from : 
                        $email_to;
                $Email->headers['Subject'] = $this->page_title.':'.$this->request->uri;
                $Email->headers['Content-Type'] = 'text/html; charset=utf-8';
                // Set body to just contain debug html
                $Email->body = '<html><body>'.$debug_html.'</body></html>';
                if($Email->email()){
                    // Remember when the last email was sent
                    touch($cache_flag_file);
                }
            }
        }
        // Output error page, or use built-in template if no error template
        $tpl = 'errors/' . $this->status.'.php';
        if (file_exists_include_path('templates/'.$tpl)) {
            $this->body = $this->parseTemplate($tpl);
        } else {
            $this->body = sprintf(ErrorResponder::ERROR_TPL, 
                $this->request->httpReasonPhrase($this->status), 
                $this->request->httpReasonPhrase($this->status));
        }
        // Append error HTML to body if configured
        if($config->mitaji->display_errors){
            $this->body .= $debug_html;
        }
        $this->output();
        return $this;
    }
// --------------------------------------------------------------------------     
}
