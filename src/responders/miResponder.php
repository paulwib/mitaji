<?php
/*
 * miResponder
 *
 */
class miResponder {

    public $status = 200,
           $headers = array(),
           $body = '',
           $layout = 'default.html.php',
           $template = false,
		   $request,
           $page_size = 30;
    private $config_whitelist = array('page_size');
	private $redirect_state = false;
    protected $context = array(),
			  $__helpers = array();
            
//---------------------------------------------------------------------------
    function __construct(&$request) {        
        $this->request = $request;
		// Add default helpers
		$this->addHelpers('miFormHelper','miLink');
		// Get any redirect state
        $this->redirect_state = $this->__redirectState();
    }
//---------------------------------------------------------------------------
/**
 * Looks for config vars based on a file named same as current url template
 * with a json extension. This can ovverride normal class vars. Can set up
 * a whitelist to let it only override certain vars.
 */
    function viewConfig($config){
        if (!is_object($config)) {
            $f = 'templates/' . $config;
            if (!file_exists_include_path($f)) {
                return false;
            }
            $data = file_get_contents($f, FILE_USE_INCLUDE_PATH);
            if (!$data) {
                trigger_error("Config $f could not be read", E_USER_ERROR);
                return false;
            }
            $config = json_decode($data);
            if (!is_object($config)) {
                trigger_error("Config $f is not JSON", E_USER_ERROR);
                return false;
            }
        }
        // Read extra variables
        foreach ($this->config_whitelist as $name) {
            if ($config->{$name}) {
                $this->{$name} = $config->{$name};
            }
        }
        // Variables can be specific to current URL
        if (@$config->url_config) {
            foreach ($config->url_config as $url_config) {
                $rx = '/^' . str_replace('*', '.*', 
                            str_replace('/', '\/', $url_config->url)) . '$/';
                if (preg_match($rx, $this->request->url_path)) {
                    unset($url_config->url);
                    $this->viewConfig($url_config);
                    return;
                } else {
                }
            }
        }
    }
//---------------------------------------------------------------------------
/**
 * Get & Set Magic Methods  
 * Setting properties actually stores them in context. 
 * All variables assigned to a view will be available in the template 
 * without using $this.
 */
    function __set($prop, $value) {
        $this->context[$prop] = $value;
    }
    function __get($prop) {
        if (!isset($this->context[$prop])) {
            return null;
        }
        return $this->context[$prop];
    }
// ------------------------------------------------------------------------
/*
 * Adds a header which will force the response to be downloaded
 * @param string $f The name of the file to download as
 * @return void
 */
    function setDownloadHeader($f, $content_type=false){
        $this->headers['Content-Disposition'] = 'attachment; filename='.$f;
        if ($content_type) {
            $this->headers['Content-Type'] = $content_type;
        }
    }
// ------------------------------------------------------------------------
/*
 * Attempts to work out file mime type based on files extension. It first 
 * checks a defined constant. Then if finfo_file is defined it will try that
 * and as a last resort it will try deprecated mime_content_type.
 * @param string The name of the file
 * @return string The mime type of the file
 */
    function getFileMimeType($f){
        $ext = pathinfo($f, PATHINFO_EXTENSION);;
        if(!($mime_type = constant('MIME_TYPE_'.$ext))){
            if(function_exists('finfo_open')){
                $rs = finfo_open(FILEINFO_MIME);
                $mime_type = finfo_file($rs, $f,FILEINFO_MIME);
            } else {
                $mime_type = mime_content_type($f);
            }
        }
        return $mime_type;
    }
//---------------------------------------------------------------------------
/** 
 * HTTP Methods 
 * These functions are mapped from HTTP request method. By default they all 
 * return false, which will trigger a Method Not Allowed response. 
 * It's up to you to configure what they do.
 */
    public function get() {
        return false;
    }
    public function post() {
        return false;
    }
    public function put() {
        return false;
    }
    public function delete() {
        return false;
    }
// Default head method is just to get() and unset the body
    function head() {
        if($this->get()){
        	$this->body = '';
        	return $this;
		}
		return false;
    }
//---------------------------------------------------------------------------
/**
 * This copies any variables assigned to the current instance into the local
 * context and includes the template, so it has access to all those variables.
 * It will also have access to all methods and members of miResponder.
 * @param string $template The name of a file in the templates folder.
 * @return string The parsed template
 */
    function parseTemplate($template){
        // Start output buffer
        ob_start();
        // Copy context to local scope
        if ($this->context) {
            extract($this->context);
        }
        // Include content files
        if(!include('templates/' . $template)){
            trigger_error('[miResponder::parseTemplate] templates/'.
                           $template. ' not found in path '.get_include_path(), E_USER_ERROR);
            return false;
        }
        // Get parsed contents of buffer
        $parsed_template = ob_get_contents();
        ob_end_clean();
        return $parsed_template;
    }
//---------------------------------------------------------------------------
/**
 * This is a shortcut to parse a number of templates at once and automatically
 * parses the layout template last.
 * @param string $templates, ... Any number of template file names can be 
 * passed and they will each be parsed, one after the other.
 * Because templates are parsed in the context of $this you can include 
 * the current body in a template with $this->body.
 * This argument can be empty in which case the current body is just wrapped in 
 * the current layout (if set)
 * @return object Returns a reference to $this i.e. the current instance so
 * your responsers can just do things like:
 *  return $this->output('MyTemplate.html.php');
 */
    function output() {
        // Process passed templates
        $templates = func_get_args();
        if($templates){
            foreach($templates as $template){
                $this->body = $this->parseTemplate($template);
            }
        }
        // Always process any layout template last
        if($this->layout){
            $this->body = $this->parseTemplate('layout/' . $this->layout);
        }
        return $this;
    }
//---------------------------------------------------------------------------
/**
 * Email the current value of $this->body reading headers from $this->headers
 * @param string $to_path A valid directory path to write the email to
 * rather than sedning it (handy for testing)
 * @return bool Whether the message was sent successfully or not (or written 
 * to path if reuqested)
 */
    function email($to_path=false) {
        $content = $this->body;
        if (!$content) {
            trigger_error('[miResponder::email] No content for email',E_USER_WARNING);
            return false;
        }
        if(!$to = @$this->headers['To']){
            trigger_error('[miResponder::email] No "To:" header set',E_USER_WARNING);
            return false;
        }
        if(!$subject = @$this->headers['Subject']){
            trigger_error('[miResponder::email] No "Subject:" header set',E_USER_WARNING);
            return false;
        }
        unset($this->headers['To']);
        unset($this->headers['Subject']);
        
        $headers = '';
        // Add mime header if current content type is HTML
		if (stristr($this->headers['Content-Type'],'html')) {
            $headers .= "MIME-Version: 1.0\n";
        }
        if($this->headers){
            $headers = '';
            foreach($this->headers as $k=>$v){
                $headers .= "$k: $v\n";
            }
        }
        // Send the message
        if ($to_path) {
            if (is_writable($to_path)) {
                $new_to = str_replace('@', '-at-', $to);
                $new_to = str_replace('.', '-dot-', $new_to);
                $fname = date('Y-m-d_H:i:s__').$new_to.'__'.
                                miStr::sanitize($subject).'.eml';
                return file_put_contents($to_path.'/'.$fname, 
                            $headers . "To:$to\nSubject:$subject\n\n$content\n\n");
            }
            trigger_error('[miResponder::email] Cannot write email to path '.$to_path, E_USER_WARNING);
            return false;
        } 
        return mail($to, $subject, $content, $headers);
    }
//---------------------------------------------------------------------------
/**
 * Redirect to a URL as new, mainly used as response to a successful POST.
 * The reason to use this is that it means the next time the URL is accessed
 * isNewUrl will return true, but not a 100% is somehow the new URL is
 * accessed before the redirect completed (pretty unlikely)
 * @param string $url The URL to redirect to
 */
    public function redirectNewUrl($url){
		$this->redirectWithState($url,'saved');
    }
//---------------------------------------------------------------------------
	public function redirectUpdatedUrl($url){
		$this->redirectWithState($url,'updated');
    }
//---------------------------------------------------------------------------
	public function redirectDeletedUrl($url){
		$this->redirectWithState($url,'deleted');
    }
//---------------------------------------------------------------------------
	public function redirectWithState($url, $state) {
		$f = $this->request->getTempDir().md5($url.'-state');
		if(!file_put_contents($f,$state)){
			trigger_error('State could not be saved to '.$f,E_USER_ERROR);
		}
		$this->request->redirect($url, 303);
	}
//---------------------------------------------------------------------------
	private function __redirectState(){
		$f = $this->request->getTempDir().md5($this->request->uri.'-state');
		if(!file_exists($f)){
			return false;
		}
		$state = file_get_contents($f);
		unlink($f);
		return $state;
	}
//---------------------------------------------------------------------------
	public function redirectState(){
		return $this->redirect_state;
	}
//---------------------------------------------------------------------------
/**
 * Checks to see if this is the first time the current URL has been accessed.
 * Works in tandem with redirectNewUrl above. This can only be called once so
 * it's done in the constructor above and stored in a protected variable
 * accessed through isNewUrl().
 * @return bool Whether the current URL in this->request is new.
 */
    private function __isNewUrl(){
        $f = $this->request->getTempDir().md5($this->request->uri);
        $res = file_exists($f);
        if($res){
            unlink($f);
        } 
        return $res;
    }
//---------------------------------------------------------------------------
/**
 * Public method to get at the is_new_url variable
 * @return bool Whether the current URL in this->request is new.
 */
    public function isNewUrl(){
        return $this->redirectState() == 'saved';
    }

// ------------------------------------------------------------------------
	function error(){
		return $this->status == 422;
	}
// ------------------------------------------------------------------------
	function saved(){
		return $this->isNewUrl();
	}
// ------------------------------------------------------------------------
	function updated(){
		return $this->request->method == 'POST' && 
				$this->status == 200;
	}
// ------------------------------------------------------------------------
	function deleted(){
		return $this->request->method == 'DELETE' && 
			   $this->status == 200;
	}
// ------------------------------------------------------------------------
/*
 * This checks if current user has a particular permission - prbably shouldn't
 * be hera as implementatin specific, but needed for now.
 */
    function permission(){
        if (!$this->request->user) {
            return false;
        }
        if (!method_exists($this->request->user, 'hasPermission')) {
            return false;
        }
        $args = func_get_args();
        return call_user_func_array(array($this->request->user, 'hasPermission'), $args);
    }
// ------------------------------------------------------------------------
	function addHelpers(){
		$args = func_get_args();
		foreach($args as $helper){
			if(class_exists($helper)){
				$this->__helpers[] = $helper;
				// Initialize link back to this
				if(method_exists($helper,'setResponse')){
					call_user_func_array(array($helper,'setResponse'), array(&$this));
				}
			} else {
				trigger_error("Helper $helper does not exist", E_USER_WARNING);
			}
		}
	}
// ------------------------------------------------------------------------
	function getHelpers(){
		return $this->__helpers;
	}
// ------------------------------------------------------------------------
/**
 * The magic __call method is used to access helpers
 */
	function __call($fun, $args){
		
		// Otherwise try other mixed in helpers
		foreach($this->__helpers as $helper){
			if(method_exists($helper,$fun)){
				return call_user_func_array(array($helper,$fun),$args);
			}
		}
		trigger_error("Method missing $fun", E_USER_ERROR);
	}
// ------------------------------------------------------------------------
}

