<?php

define('ERR_FLD_REQUIRED','%2$s is required');
define('ERR_FLD_ALPHA','%2$s can only contain letters');
define('ERR_FLD_ALPHANUMERIC','%2$s can only contain letters, numbers and spaces');
define('ERR_FLD_ALPHANUMERICNOSPACES','%2$s can only contain letters and numbers, no spaces allowed');
define('ERR_FLD_DATE','%2$s is not a valid date');
define('ERR_FLD_EMAIL','%2$s is not a valid email address');
define('ERR_FLD_FUTURE','%2$s must be a date in the future');
define('ERR_FLD_GT_ZERO','%2$s must be greater than zero');
define('ERR_FLD_INTEGER','%2$s must be a round number');
define('ERR_FLD_LENGTH','%2$s cannot be longer than %4$s characters');
define('ERR_FLD_LT_ZERO','%2$s must be less than zero');
define('ERR_FLD_NUMERIC','%2$s must be a number');
define('ERR_FLD_PAST','%2$s must be a date in the past');
define('ERR_FLD_UNIQUE','%1$s %2$s %3$s  is not unique');
