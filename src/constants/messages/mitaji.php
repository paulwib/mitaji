<?php

// --------------------------------------------------------------------------
// Errors
define('ERR_NO_AUTOLOAD','Could not __autoload class %s');
define('ERR_EMPTY_CONFIG','Config is empty');
define('ERR_CONFIG_NOT_JSON','Config file %s could not be parsed as json');
define('ERR_CLI_PATH_NOT_WRITABLE','Cannot write to path');
define('ERR_CLI_DATABASE_NOT_SUPPORTED','No support for database "%s"');
define('ERR_CLI_BAD_METHOD','Command listed as valid but method does not exist');
define('ERR_CLI_NO_HELP','Sorry no help available for %s');
define('ERR_CLI_NO_COMMAND','Unknown command');
define('ERR_CLI_NO_TESTS','No tests available');
define('ERR_CLI_TEST_NOT_FOUND','No file for test "%s"');

// --------------------------------------------------------------------------
// Command line help
define('CLI_BASE_COMMAND', 'php bin/mitaji.php');
define('CLI_HELP_PREAMBLE',
'Defined commands:
(type "' . CLI_BASE_COMMAND . ' <subcommand> --help" to find out more on that command)');

// --------------------------------------------------------------------------
// HTTP
define('HTTP_REASON_PHRASE_100','Continue');
define('HTTP_REASON_PHRASE_101','Switching Protocols');
define('HTTP_REASON_PHRASE_200','OK');
define('HTTP_REASON_PHRASE_201','Created');
define('HTTP_REASON_PHRASE_202','Accepted');
define('HTTP_REASON_PHRASE_203','Non-Authoritative Information');
define('HTTP_REASON_PHRASE_204','No Content');
define('HTTP_REASON_PHRASE_205','Reset Content');
define('HTTP_REASON_PHRASE_206','Partial Content');
define('HTTP_REASON_PHRASE_300','Multiple Choices');
define('HTTP_REASON_PHRASE_301','Moved Permanently');
define('HTTP_REASON_PHRASE_302','Found');
define('HTTP_REASON_PHRASE_303','See Other');
define('HTTP_REASON_PHRASE_304','Not Modified');
define('HTTP_REASON_PHRASE_305','Use Proxy');
define('HTTP_REASON_PHRASE_307','Temporary Redirect');
define('HTTP_REASON_PHRASE_400','Bad Request');
define('HTTP_REASON_PHRASE_401','Unauthorized');
define('HTTP_REASON_PHRASE_403','Forbidden');
define('HTTP_REASON_PHRASE_404','Not Found');
define('HTTP_REASON_PHRASE_405','Method Not Allowed');
define('HTTP_REASON_PHRASE_406','Not Acceptable');
define('HTTP_REASON_PHRASE_407','Proxy Authentication Required');
define('HTTP_REASON_PHRASE_408','Request Timeout');
define('HTTP_REASON_PHRASE_409','Conflict');
define('HTTP_REASON_PHRASE_410','Gone');
define('HTTP_REASON_PHRASE_411','Length Required');
define('HTTP_REASON_PHRASE_412','Precondition Failed');
define('HTTP_REASON_PHRASE_413','Request Entity Too Large');
define('HTTP_REASON_PHRASE_414','Request-URI Too Long');
define('HTTP_REASON_PHRASE_415','Unsupported Media Type');
define('HTTP_REASON_PHRASE_416','Requested Range Not Satisfiable');
define('HTTP_REASON_PHRASE_417','Expectation Failed');
define('HTTP_REASON_PHRASE_422','Unprocessable Entity');
define('HTTP_REASON_PHRASE_500','Internal Error');
define('HTTP_REASON_PHRASE_501','Not Implemented');
define('HTTP_REASON_PHRASE_502','Bad Gateway');
define('HTTP_REASON_PHRASE_503','Service Unavailable');
define('HTTP_REASON_PHRASE_504','Gateway Timeout');
define('HTTP_REASON_PHRASE_505','HTTP Version Not Supported');
