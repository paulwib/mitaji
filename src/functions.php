<?php

// Define sys_get_temp_dir if not defined
if ( !function_exists('sys_get_temp_dir')) {
    function sys_get_temp_dir() {
        if (!empty($_ENV['TMP'])) {
            return realpath($_ENV['TMP']);
        }
        if (!empty($_ENV['TMPDIR'])) {
            return realpath( $_ENV['TMPDIR']);
        }
        if (!empty($_ENV['TEMP'])) {
            return realpath( $_ENV['TEMP']);
        }
        $tempfile = tempnam(uniqid(rand(),TRUE),'');
        if (file_exists($tempfile)) {
            $ret = realpath(dirname($tempfile));
            unlink($tempfile);
            return $ret;
        }
    }
}

// Check a file exists in the include path
function file_exists_include_path($f) {
    $path = explode(PATH_SEPARATOR, get_include_path());
    foreach ($path as $p) {
        $test = rtrim($p, '/') . '/' . $f;
        if (is_file($test)) {
            return $test;
        }
    }
    return false;
}



