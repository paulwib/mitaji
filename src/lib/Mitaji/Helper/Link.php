<?php

/**
 * Mitaji Link Helpers
 * @author Paul Willoughby <paul@fivetide.com>
 */
class Mitaji_Helper_Link {
    
    private $_urls;
    private $_current_urls;
    private $_rel_root;

    /**
     * Constructor
     * @param $urls         List of URLs in the application
     * @param $current_url  The current URL
     */
    public function __construct($urls, $current_url=false, $rel_root=false){
        $this->_urls = $urls;
        $this->_current_url = $current_url;
        $this->_rel_root = $rel_root;
    }
    /* 
     * Exports 
     * @return  List of helper methods (useful for mixins)
     */
    public function exports(){
        return array(
            'link',
            'linkMenu',
            'linkSort',
            'linkTo',
            'linkToPages',
            'linkToSort',
            'linkUl',
            'setUrlVar',
            'unsetUrlVar',
        );
    }
    /**
     * Find URL template and expand with given vars
     *
     * @param string $tpl_path      Path to the template in $_urls using dot notation e.g. 
     *                              Product.list
     * @param mixed $vars           An object or array containing the variables to expand 
     *                              the URL with, default: false
     * @return string               The expanded URL 
     */
	public function link($tpl_path, $vars=false){
		list($c, $name) = explode('.', $tpl_path);
		$url_tpl = $this->_urls->{$c}->{$name};
		
		// Trigger error if a problem as may be doing a redirect or sumink
		if(!$url_tpl){
			trigger_error('No link available for ' . $tpl_path, E_USER_ERROR);
			return false;
		}
		// URL variables to expand
		if ($vars) {
			$tpl_url = new TemplateUri($url_tpl);
			$url = $tpl_url->expand($vars);
		
		} 
		// Simple URL
        else {
			$url = $url_tpl;
		}
		// Deal with non document_root root
        // @todo Is this really needed?
		if($this->_rel_root){
			$url = '/' . trim($this->_rel_root, '/') . $url;
		}
		return $url;
	}

    /**
     * Find URL template and expand with given vars and wrap in an HTML anchor
     *
     * @param string $txt           The text of the link
     * @param string $tpl_path      Path to the template in $_urls using dot notation e.g. 
     *                              Product.list
     * @param mixed $vars           An object or array containing the variables to expand 
     *                              the URL with, default: false
     * @return string               The HTML anchor tag
     */ 
	public function linkTo($txt, $tpl_path, $vars=false){
		return sprintf('<a href="%s">%s</a>', $this->link($tpl_path, $vars), $txt);
	}

    /**
     * Generate a UL menu of top level links from $_urls. Will only include
     * URLs that have one path token or less and do not contain 
     * template vars so '/things/' would be included but '/things/reports/'
     * '/things/{id}/' and '/{foo}' would not. Also only includes one URL per
     * model to avoid repeating URLs
     *
     * @param array $ul_atts    Attributes to apply to wrapper UL, like a class name
     *                          or id or something, optional
     * @param string $starts_with_url_path_token    Get everything below a certain level
     * @param array $exclude    List of items to exclude 
     *                          @todo How does that exclusion work?
     * @return string           An HTML unordered list of links (will be in order they
     *                          appear in $_urls)
     */
	public function linkMenu($ul_atts=array(), $starts_with_url_path_token=false, $exclude=array()){
		$menu = array();
		foreach ($this->_urls as $model=>$url_tpls) {
			if (is_string($url_tpls)) {
				$url_tpls = array($url_tpls);
			}
			foreach ($url_tpls as $url_tpl_name => $url_tpl) {
                if (in_array("$model.$url_tpl_name", $exclude)) {
                    continue;
                }
				// Get all items below a certain path
				if ($starts_with_url_path_token) {
					$rx = '/^\/'.$starts_with_url_path_token.'\//';
					if (!preg_match($rx, $url_tpl)) {
						continue;
					}
					$tmp_url = preg_replace($rx, '', $url_tpl);
				} 
                else {
					$tmp_url = $url_tpl;
				}
				// Work out title for link
				$tokens = preg_split('/\//', $tmp_url, -1, PREG_SPLIT_NO_EMPTY);
				if (count($tokens) <= 1 && !strstr($url_tpl,'{')) {
					$const = strtoupper(AkInflector::underscore($model)) . '_TITLE_' . strtoupper($url_tpl_name);
					if (!$txt = @constant($const)) {						
						if ('/' == $tmp_url) {
							$txt = AkInflector::humanize($model);
						} 
                        else {
							$txt = AkInflector::pluralize(AkInflector::titleize($model));
						}
					}
					$menu[$txt] = $url_tpl;
					break;
				}
			}
		}
		return $this->linkUl($menu, false, $ul_atts);
	}

    /**
     * Generate a UL menu with first and last classes for LI elements, and
     * additional class names based on current URI and customizable UL and
     * LI class prefix.
     * 
     * @param array $links  An array of links where the link text to use is the key
     *                      and each elements is a URL or an array of link attributes (make sure to 
     *                      include an element named `href` or `url` for the actual link). Allowed 
     *                      attributes are href, id, class, rel and title.
     * @param string $current_url   The current URL, optional. If supplied any link
     *                              with a URL that matches $current_url will be given the class `selected`. 
     *                              Any that partially match the path will be given class `in-path`.
     * @param string $atts  An array of attribute for the UL
     * @return string       An HTML string
     * 
     */
    public function linkUl($links, $current_url=false, $atts=array()){
		if($current_url === false){
			$current_url = $this->current_url;
		}
        $builder = miHtml::builder();
        $html = '';
		$i = 0;
		$count = count($links)-1; 
        foreach ($links as $label => $link) {
			// Make sure you can use `href` or `url` for the URL
			if (is_array($link) && @$link['url']) {
				$link['href'] = $link['url'];
				unset($link['url']);
			}
			$a_atts = is_array($link) ? $link : array('href' => $link);
			$li_atts = array();
			foreach ($a_atts as $k => $v) {
				if (preg_match('/^li_/',$k)) {
					$li_atts[preg_replace('/^li_/','',$k)] = $v;
					unset($a_atts[$k]);
				}
			}
			if (array_key_exists('class', $li_atts)) {
				$li_atts['class'] = @(array)$li_atts['class'];
			}
			if ($i == 0) {
				$li_atts['class'][] = 'first';
			}
			if ($i == $count) {
				$li_atts['class'][] = 'last';
			}
			if ($a_atts['href'] == $current_url) {
				$li_atts['class'][] = 'selected';
			} 
            elseif($current_url && miStr::beginsWith($current_url, rtrim($a_atts['href'], '/'))){
				$li_atts['class'][] = 'in-path';
			}
			// Make sure only allowed attributes on link
			$a_atts = array_intersect_assoc($a_atts, array(
                'href'  => @$a_atts['href'],
				'title' => @$a_atts['title'],
                'rel'   => @$a_atts['rel'],
				'class' => @$a_atts['class'],
				'id'    => @$a_atts['id']
            ));
            $html .= $builder->li($li_atts, $builder->a($a_atts,$label));
			$i++;
        }
		return $builder->ul($atts, $html);
    }

    /**
     * Create a sort link
     * @param string $val The value to sort on
     * @param string $url The URL to add sort value to, if omitted uses current
     * @param string $o_name The name of the sort URL param, defaults to 'o'
     * @param string $p_name The name of the page URL param, defaults to 'p'
     * @return string The new URL
     */    
    public function linkSort($val, $url=false, $o_name='o', $p_name='p'){
        if (!$url) {
            $url = $this->current_url;
        }
        // Get url query string as an array
        $get = array();
        if ($url) {
            $qs = parse_url($url, PHP_URL_QUERY);
			parse_str($get_str, $get);
        }
		$c_order = @$get[$o_name];
        $order = urlencode($val);
        if ($c_order && $c_order == $val.' ASC') {
            $order .= '+DESC';
        } 
        else {
            $order .= '+ASC';
        }
		// Unset page and current order by
        $url = $this->unsetUrlVar($url, $p_name);
        $url = $this->unsetUrlVar($url, $o_name);
		// Set page to 1 and add new order by
        $url = $this->setUrlVar($url, $p_name, 1);
        $url = $this->setUrlVar($url, $o_name, $order);
        return $url;
    }

    /**
     * As linkSort, except puts the link in an anchor tag and adds a class if
     * it is the current sort URL paramater of sort-asc or sort-desc depending.
     * @see miLink::linkSort
     */
	public function linkToSort($val, $label=false, $url=false, $o_name='o', $p_name='p'){
		// Get the URL
		$sort_url = $this->linkSort($val, $url, $o_name, $p_name);
        if (!$url) {
            $url = $this->current_url;
        }
        // Get url query string as an array
        $get = array();
        if ($url) {
            $qs = parse_url($url, PHP_URL_QUERY);
			parse_str($get_str, $get);
        }
		// Set up link atts
		$atts = array('href'=>$sort_url,'class'=>array('sort'));
		
		// If current order set
		if($c_order = @$get[$o_name]){
			// See if it is the same as $val and capture direction
			if (preg_match("/^$val( ASC| DESC)?$/i", $c_order, $matches)) {
				$atts['class'][] = 'sort-' . trim(strtolower($matches[1]));
			}
		}
		// Build the link
        $builder = miHtml::builder();
		return $builder->a($atts, $label ? $label : AkInflector::humanize($val));
	}

    /**
     * Get an HTML UL menu of pages
     * @todo This hasn't been looked at for ages, could probably be considerably improved
     * basically want a way to get a menu of numebrs given a min and a max with different
     * ways of abridging, like scaling increment, abridged etc.
     */
    public function linkToPages($pages, $cpage, $abridge=true, $url=false, $p_name='p') {
        
		if ($pages <= 1) return '';
		
        if (!$url) {
            $url = $this->current_url;
        }
        // Get url query string as an array
        $get = array();
        if ($url) {
            $qs = parse_url($url, PHP_URL_QUERY);
			parse_str($get_str, $get);
        }
        $builder = miHtml::builder();

        $html = '';
        if ($cpage > 1) {
            $tmp_url = $this->setUrlVar($p_name, ($cpage-1), $url);
            $html .= $builder->li(array('class'=>'prev'), 
                        $builder->a(array('href'=>$tmp_url,
                                          'title'=>'Previous Page', 
                                          'rel'=>'prev'),
                                    '&laquo;Prev')
                    );
        }
        for ($i = 0; $i < $pages; $i++) {
            $is_start = $i < 5;
            $is_finish = $i > ($pages - 5);
            $min_range = ($cpage - 6);
            $max_range = ($cpage + 6);
            $in_range = $i > ($min_range) && $i < ($max_range);
            $in_range_2 = $cpage < 5 && $i < 10;
            $in_range_3 = $cpage > ($pages - 5) && $i > ($pages - 10);
            if (!$abridge || $is_start || $is_finish || $in_range || $in_range_2 || $in_range_3) {
                $hellip = false;
                $page = $i + 1;
                if ($i == $cpage - 1) {
                    $html .= $builder->li(array(
                        'class' => 'selected',
                        'title' => 'Current Page'
                    ), $page);
                } else {
                    $tmp_url = $this->setUrlVar($p_name, $page, $url);
                    $html .= $builder->li(
                                $builder->a(array(
                                    'href'  => $tmp_url,
                                    'title' => "Page $page of $pages"
                                ), $page)
                            );
                }
            } else if (!$hellip) {
                $html .= $builder->li(array(
                    'class' => 'hellip',
                    'title' => 'Skipped...'
                ), '&hellip;');
                $hellip = true;
                continue;
            }
        }
        if ($cpage < $pages) {
            $tmp_url = $this->setUrlVar($p_name, ($cpage+1), $url);
            $html .= $builder->li(array('class'=>'next'), 
                        $builder->a(array('href'=>$tmp_url,
                                          'title'=>'Next Page', 
                                          'rel'=>'next'),
                                    'Next &raquo;')
                    );
        }
        return $builder->ul($html);
    }

    /**
     * Sets a variable in the query string of the given URL
     *
     * @param string $name The name of the variable
     * @param string $value The value of the variable
     * @param string $url The URL to add the variable to, defaults to current
     * @return string The new URL
     */
    public function setUrlVar($name, $value, $url=false){
        if (!$url) {
            $url = $this->current_url;
        }
        // Unset
        $purl = parse_url($this->unsetUrlVar($url, $name));
        // Set
        if (isset($purl['query'])) {        
            return $purl['path'] . '?' . $purl['query'] . "&amp;$name=$value";
        } 
        return $purl['path'] . "?$name=$value";
    }

    /**
     * Unsets a variable in the query string of the given URL
     * @param string $name The name of the variable
     * @param string $url The URL to add the variable to, defaults to current
     * @return string The new URL, any domain name will be lost
     */
    public function unsetUrlVar($name, $url=false) {
        if (!$url) {
            $url = $this->current_url;
        }
        $purl = parse_url($url);
        if(!isset($purl['query'])){
            return $url;
        }
        $arr = array();
        parse_str($purl['query'], $arr);
        if (!isset($arr[$name])) {
            return $url;
        }
        unset($arr[$name]);
        if (count($arr)) {
            return $purl['path'] . '?' . http_build_query($arr);
        }
        return $purl['path'];
    }
}
