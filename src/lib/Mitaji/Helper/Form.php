<?php

/**
 * Mitaji Form Helpers
 *
 * @author Paul Willoughby <paul@fivetide.com>
 */
class Mitaji_Helper_Form {
    // Cache for looked up options
	private $__lookup_opts_cache = array();

    // Store context 
    private $__context              = false;
    private $__context_has_many     = false;
    private $__context_counters     = array();
    
    /**
     * Constructor
     */
    public function __construct(){
    }
    /**
     * Set context which will get pre-pended to form names, used when
     * for has one/has many relations
     * @param string $context   The context name
     * @param mixed $has_many   If has many relation give class name of has many
     *                          and field names/ids will be made a sub-array until
     *                          name/id requested for a different resource type
     */
    public function setContext($context, $has_many=false){
        $this->__context = $context;
        $this->__context_has_many = $has_many;
        $this->__context_counters = array();
    }

    /**
     * Exports 
     * @return  List of helper methods (useful for mixins)
     */
    public function exports(){
        return array(
            'checkbox',
            'checkboxes',
            'hiddenfield',
            'hiddeninput'   => 'hiddenfield',
            'label',
            'radioset',
            'select',
            'selectField',
            'setContext',
            'textarea',
            'textfield',
            'textinput'     => 'textfield',
        );
    }

    /**
     * Get a unique id attribute for a resource property
     * based on attName
     * @see Mitaji_Helper_Form::attName
     */
	public function attId($Resource, $fld, $context_caller='attId'){
        if (!($name = $this->attName($Resource, $fld, $context_caller))) {
            return false;
        }
        return str_replace(array('[', ']'), array('_', ''), $name);
	}
    /**
     * Get a unique name attribute for a resource property
     * based on attName. Will be nested according to context.
     * @param mixed $Resource           Name of a class or instance
     * @param string $fld               Name of the field
     * @param string $context_caller    A name to qualify the field to prevent
     *                                  context has many increment being 
     *                                  increased wehne getting label and attId
     */
	public function attName($Resource, $fld, $context_caller=''){
		if (is_object($Resource)) {
            $r = get_class($Resource);
		}
        elseif (is_string($Resource)) {
            $r = $Resource;
        }
        else {
			trigger_error("miFormHelper::attName: Resource must be string or object", E_USER_WARNING);
            return false;
        }
        if ($this->__context && $r != $this->__context) {
            $c = $this->__context;
            if ($this->__context_has_many) {
                $name = $c .'[' . $this->__context_has_many . ']';
                $i_key = $context_caller .'_' . $r . '_' . $fld;
                if (!array_key_exists($i_key, $this->__context_counters)) {
                    $this->__context_counters[$i_key] = 0;
                }
                else {
                    $this->__context_counters[$i_key]++; 
                }
                $i = (int)@$this->__context_counters[$i_key];
                $name .= "[$i]";
            }
            else {
                $name = $c .'[' . $r . ']';
            }
            $name .= "[$fld]";
        }
        else {
            $name = $r . '[' . $fld . ']';
        }
        return $name;
	}

    /**
     * Return standard attributes needed for all form elements
     * @param mixed $Resource   Name of a class or instance
     * @param string $fld       Name of the field
     * @param array $atts       Additional attributes
     */
	public function standardAtts($Resource, $fld, $atts){
		// If no id set...
		if (!array_key_exists('id', $atts)) {
			// If custom name used then convert to id string
			if (@$atts['name']) {
				$atts['id'] = str_replace(array('[', ']'), array('_', ''), $atts['name']);
			} 
			// Otherwise based on the resource
            else {
				$atts['id'] = $this->attId($Resource, $fld);
			}
		}
		// If no name set get default
		if (!@$atts['name']) {
			$atts['name'] = $this->attName($Resource, $fld);
		}
		// If no value set use resource's current value
		if (!array_key_exists('value', $atts) && is_object($Resource)) {
			$atts['value'] = @$Resource->$fld;
		}
        elseif (!array_key_exists('value', $atts)) {
            $atts['value'] = false;
        }
        // If false use an empty string so attribute will exists
        if ($atts['value'] === false || $atts['value'] === null) {
            $atts['value'] = '';
        }
		return $atts;
	}	
    /**
     * Get a label HTML element for Resource->fld - makes sure it has correct "for"
     * attribute matching attId
     *
     * @param mixed $Resource   Name of a class or instance
     * @param string $fld       Name of the field
     * @param string $txt      Text for label, optional: defaults to titleized version of field name 
     * @param array $atts       Additional attributes
     */
	public function label($Resource, $fld, $txt=false, $atts=array()){
		$builder = miHtml::builder();
		$atts['for'] = $this->attId($Resource, $fld, 'label');
		return $builder->label($atts, ($txt ? $txt : AkInflector::titleize($fld)));
	}
    /**
     * Look up options for a resources field when it is a foreign key
     *
     * @param mixed $Resource   Name of a class or instance
     * @param string $fld       Name of the field
     * @param array $use_flds   Fields to use to make the label of the option
     * @param array $filter     Additional filters to apply to the foreign
     *                          resource list
     *                          
     */
	public function lookupOpts($Resource, $fld, $use_flds, $filter=array()){
		$options = array();
		$class = AkInflector::modulize(preg_replace('/_id$/','',$fld));
		$cache_key = $class . md5(print_r($use_flds, true) . print_r($filter, true));
		$found_alias = false;
		if (!@class_exists($class) && !($class = $Resource->getAssociateRealClass($class))) {
			trigger_error('Cannot look up options for ' . $fld, E_USER_ERROR);
		}
		if (array_key_exists($cache_key, $this->__lookup_opts_cache)) {
			return $this->__lookup_opts_cache[$cache_key];
		}
		$model = new $class();
		$resources = $model->find();
        if (!$resources || !$resources->count()) {
            return false;
        }
		if ($filter) {
			$resources->filter($filter);
		}
		// Always find everything
		$resources->pageSize(0);
		if (!$resources->count()) {
			trigger_error('No options for ' . $fld, E_USER_NOTICE);
			return false;
		}
		foreach ($resources as $rs) {
			$str = '';
			foreach ($use_flds as $u_fld) {
				if ($rs->$u_fld) {
					$str .= $rs->$u_fld . ' ';
				} else {
                    $str .= $u_fld . ' ';
                }
			}
			$options[$rs->id] = trim($str);
		}
		$this->__lookup_opts_cache[$cache_key] = $options;
		return $options;
	}

    /**
     * Hidden field
     * @param mixed $Resource   Name of a class or instance
     * @param string $fld       Name of the field
     * @param array $atts       Additional attributes
     */
	public function hiddenfield($Resource, $fld, $atts=array()){
		$builder = miHtml::builder();
		$atts = $this->standardAtts($Resource, $fld, $atts);
		$atts['type'] = 'hidden';
		return $builder->input($atts);
	}
    /**
     * Text field
     * @param mixed $Resource   Name of a class or instance
     * @param string $fld       Name of the field
     * @param array $atts       Additional attributes
     */
	public function textfield($Resource, $fld, $atts=array()){
		$builder = miHtml::builder();
		$atts = $this->standardAtts($Resource, $fld, $atts);
		$atts['type'] = 'text';
		if (!array_key_exists('size', $atts)) {
			$atts['size'] = '28';
		}
		return $builder->input($atts);
	}

    /**
     * Text area
     * @param mixed $Resource   Name of a class or instance
     * @param string $fld       Name of the field
     * @param array $atts       Additional attributes
     */
	public function textarea($Resource, $fld, $atts=array()) {
		$builder = miHtml::builder();
		$atts = $this->standardAtts($Resource, $fld, $atts);
		if (!array_key_exists('cols',$atts)) {
			$atts['cols'] = '32';
		}
		if (!array_key_exists('rows',$atts)) {
			$atts['rows'] = '8';
		}
		$text = $atts['value'];
		unset($atts['value']);
		return $builder->textarea($atts,$text);
	}

    /**
     * Checkbox
     * @param mixed $Resource   Name of a class or instance
     * @param string $fld       Name of the field
     * @param array $atts       Additional attributes
     */
	public function checkbox($Resource, $fld, $atts=array()){
		$builder = miHtml::builder();
		// If no value supplied, assume a boolean
		if (!array_key_exists('value',$atts)) {
			$atts['value'] = '1';
		}
		$atts = $this->standardAtts($Resource, $fld, $atts);
		$atts['type'] = 'checkbox';
		// See if checked by comparing resource value to input value
		if (@$Resource->$fld == $atts['value']) {
			$atts['checked'] = 'checked';
		}
		return $builder->input($atts);
	}

    /**
     * Make a select field, normally for some associate referenced by a foreign key
     * @param object $Resource An instance of a model
     * @param string $fld The name of the field
     * @param mixed $options Either an array of options with values for keys and text
     * for array values, or a string which means look up the values in this model
     * and using ids for values and field names in string forms a template for
     * each option e.g. select($Entry,'category_id','description') and you can also
     * start with an empty option by changing it to something like 
     * 'Please select:description', or you can also pass a number and it will make
     * options that represent each integer up to that value.
     * @param array $atts any additional HTML attributes. In this context 'value'
     * means the selected value.  
     */
	public function select($Resource, $fld, $options, $filter=array(), $atts=array()){
		$builder = miHtml::builder();
		$option_tags = array();
		$atts = $this->standardAtts($Resource, $fld, $atts);
		// Look up options from another model
		if (is_string($options)) {
			$empty_option = false;
			if (strstr($options, ':')) {
				$tmp = explode(':', $options);
				$empty_option = $tmp[0];
				$options = $tmp[1];
			}
			if (!$options = $this->lookupOpts($Resource, $fld, explode(' ', $options), $filter)){
				trigger_error('No options available for ' . $fld, E_USER_WARNING);
				return false;
			}
			if ($empty_option) {
				$option_tags[] = $builder->option(array('value'=>' '), $empty_option);
			}
		} 
		// Numeric range of options from 0 to $options
        elseif (is_int($options)) {
			$options = range(0, $options);
		}
		// Build options
		foreach ($options as $value => $text) {
            $opt_atts = array();
            $opt_atts['value'] = $value;
            if ($atts['value'] == $value) {
				$opt_atts['selected'] = 'selected';
			}
            $option_tags[] = $builder->option($opt_atts, $text);
        }		
		unset($atts['value']); // Unset meaningless attribute
		return $builder->select($atts, implode("\n", $option_tags)); // Wrap in the select
	}

    /**
     * General select field, just pass attributes and options. The attributes
     * can contain a value which will select the option with that value.
     */
	public function selectField($atts, $options, $text_value=false){
		$builder = miHtml::builder();
		foreach ($options as $value=>$text) {
			if ($text_value) {
				$opt_atts = array('value' => $text);
			} else {
            	$opt_atts = array('value' => $value);
			}
            if ((string)@$atts['value'] == (string)$value) {
				$opt_atts['selected'] = 'selected';
			}
            $option_tags[] = $builder->option($opt_atts, $text);
        }
        unset($atts['value']);
        return $builder->select($atts, implode("\n", $option_tags));
	}

    /**
     * As above, in this context att['value'] means the selected value
     */
	public function radioset($Resource, $fld, $options, $filter=array(), $atts=array()){
		$builder = miHtml::builder();
		$atts = $this->standardAtts($Resource, $fld, $atts);
		// Look up options from another model
		if (is_string($options)) {
			if (!$options = $this->lookupOpts($Resource, $fld, explode(' ', $options), $filter)){
				trigger_error('No options available for ' . $fld, E_USER_WARNING);
				return false;
			}
		}
		// Build options
		$i = 0;
		foreach ($options as $value => $text) {
            $radio_atts = array();
			$radio_atts['type'] = 'radio';
			$radio_atts['id'] = $atts['id'].'_'.($i++);
			$radio_atts['name'] = $atts['name'];
            $radio_atts['value'] = $value;
            if (@$atts['value'] == $value) {
				$radio_atts['checked'] = 'checked';
			}
			$label_atts['for'] = $radio_atts['id'];
            $li_tags[] = $builder->li($builder->input($radio_atts).
									  $builder->label($label_atts, $text));
        }
		unset($atts['value'], $atts['name']); // Unset meaningless attributes
		return $builder->ul($atts, implode("\n", $li_tags)); // Wrap in UL
	}

    /**
     * As above, in this context att['value'] means (multiple) selected value(s)
     */
	public function checkboxes($Resource, $fld, $options, $filter=array(), $atts=array(), $start_index=0){
		$builder = miHtml::builder();
		$atts = $this->standardAtts($Resource, $fld, $atts);
		$base_name = $this->attName($Resource, $fld);
		if (!array_key_exists('id',$atts)) {
			$base_id = $this->attId($atts['name']);
		} else {
			$base_id = $atts['id'];
		}
		// Look up options from another model
        if (is_string($options)) {
            if(!$options = $this->lookupOpts($Resource,$fld,explode(' ',$options),$filter)){
                trigger_error('No options available for '.$fld, E_USER_WARNING);
                return false;
            }
        }
        // If supplied value it's checked, otherwise extracts id from property
        if (isset($atts['value'])) {
            $checked =  is_array($atts['value']) ? $atts['value'] : array();
        }
        else {
		    $checked = miArray::extractByKey($Resource->$fld, 'id');
        }
		// Build options
		$i = $start_index;
		foreach ($options as $value => $text){
            $cb_atts = array();
			$cb_atts['type'] = 'checkbox';
			$cb_atts['id'] = $atts['id'] . '_' . ($i);
			$cb_atts['name'] = $atts['name'] . '[' . $i .'][' . AkInflector::foreignKey($fld) . ']';
            $cb_atts['value'] = $value;
            if (in_array($value, $checked)) {
				$cb_atts['checked'] = 'checked';
			}
			$label_atts['for'] = $cb_atts['id'];
            $li_tags[] = $builder->li($builder->input($cb_atts) . $builder->label($label_atts, $text));
			$i++;
        }
		unset($atts['value'], $atts['name']); // Unset meaningless attributes
		return $builder->ul($atts, implode("\n", $li_tags)); // Wrap in the UL
	}
// ---------------------------------------------------------------------
}
