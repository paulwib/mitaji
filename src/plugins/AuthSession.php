<?php

include_once('Auth.php');

// Start session as soon as included
if(!session_id()){
    session_start();
}

class AuthSession extends Auth {
    
    private static $login_url = '/login/';
    const SESS_USER_KEY = 'authsession_user';
    const SESS_REDIRECT_KEY = 'authsession_redirect';
// -------------------------------------------------------------------------    
    public static function before($config, &$request){
        self::$config = $config;
        
        // Default login url
        if(!@$config->login_url){
            $config->login_url = self::$login_url;
        }
        $auth_function = self::parseAuthFunction();      
        
        // Always retrieve the user if logged in
        if(isset($_SESSION[self::SESS_USER_KEY])){
            // Unset if empty
            if(empty($_SESSION[self::SESS_USER_KEY])){
                unset($_SESSION[self::SESS_USER_KEY]);
                return;
            }
            $user = call_user_func($auth_function,
                        $_SESSION[self::SESS_USER_KEY], $request,'AuthSession');
            if($user){
                $request->user = $user;
                return;
            }
        }

        // No auth if current request URL doesn't match a defined pattern
        if(!($url = self::checkProtected($request->url_path))  && $request->uri != $config->login_url){
            return;
        }

        // If URL protected and no authenticated user redirect to login page 
        if($url && !$user && $request->uri != $config->login_url){
            // Store requested URL in session so can redirect when authenticated
            unset($_SESSION[self::SESS_USER_KEY]);
            $_SESSION[self::SESS_REDIRECT_KEY] = $request->uri;
			$url = $config->login_url;
			// Add Ajax to redirect URL if requested with Ajax
			if($request->ajax){
				$url .= '?X-Requested-With=xmlhttprequest';
			}
            return $request->redirect($url);
        }
        
        /*
         * If this is a POST to the login URL then authenticate 
         * The POST must contain the fields 'username' and 'pass'.
         * The 'username' is passed to configured auth function, but not the
         * password as that will be checked against the ha1 of any returned user
         * below
         */
        if($request->uri == $config->login_url && $request->method == 'POST'){
            $post = $request->post_vars;
            if(!$post['username'] || !$post['pass']){
                self::$error = self::ERROR_LOGIN_EMPTY;
                return;
            }
            
            // Call authentication function to return user
            $username = strip_tags($post['username']);
            $pass = strip_tags($post['pass']);
            $user = call_user_func($auth_function, $username, $request, 'AuthSession');
            if(!$user){
                self::$error = self::ERROR_LOGIN_FAIL;
                return;
            }
            
            // Convert user to object (so can return an array if you like)
            $user = (object)$user;
            $user->auth_method = 'Session';
            if(!$user || !@$user->ha1 || $user->ha1 != md5($username.':'.$config->realm.':'.$pass)){
                self::$error = self::ERROR_LOGIN_FAIL;
                return;
            }
            
            // Check required user
            if(!self::checkRequiredUser($username, @$url)){
                trigger_error('[AuthSession] User OK, but not required user', E_USER_NOTICE);
                trigger_error(403, E_USER_ERROR);
                return;
            }
            
            // Check required group
            if(!self::checkRequiredGroup(@$user->group, @$url)){
                trigger_error('[AuthSession] User OK,  but not in required group', E_USER_NOTICE);
                trigger_error(403, E_USER_ERROR);
                return;
            }

            // Regenerate id for security
            session_regenerate_id();
            
            // Store user name
            $_SESSION[self::SESS_USER_KEY] = $username;
            
            // Redirect if required
            if(@$_SESSION[self::SESS_REDIRECT_KEY]){
                return $request->redirect($_SESSION[self::SESS_REDIRECT_KEY]);
            }
            
            // Attach user to request
            $request->user = $user;         
        }
    }
// --------------------------------------------------------------------------  
    /**
     * Force a particular user to be logged in, only to be used
     * when creating new accounts
     */
     public static function forceUser($username) {
        $_SESSION[self::SESS_USER_KEY] = $username;
        session_regenerate_id();
     }
// --------------------------------------------------------------------------  
    /**
     * redirectUrl
     * Return the URL to be redirected to after login successful.
     * useful for making decsions about what kind of login form
     * to go to.
     */
    public static function redirectUrl() {
        return @$_SESSION[self::SESS_REDIRECT_KEY];
    }
// --------------------------------------------------------------------------  
    /**
     * error
     * Return last error
     */
    public static function error(){
        return self::$error;
    }
// -------------------------------------------------------------------------  
}
