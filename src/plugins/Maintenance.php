<?php

class Maintenance {

  private static $default_template = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
    "http://www.w3.org/TR/html4/strict.dtd">
    <head>
    <meta http-equiv="Content-type"
    content="text/html; charset=utf-8">
    <title>Site unavailable due to maintenance</title>
    </head>
    <body>
    <p>Sorry, we\'re currently closed, please try again later.<p>
    </body>
    </html>';
  private static $default_login_form = '
  <form action="" method="get">
  <p>
  <label for="password">Password</label>
  <input type="text" name="password" id="password" value="">
  <button type="submit">Log in</button>
  </p>
  </form>
  ';
  // -------------------------------------------------------------------------
  public static function before($config, &$request){
    if(isset($config->on) && !$config->on){
      return;
    }
    // Check for password to bypass maintanance
    if(@$config->password && @$config->password_in){
      if(!session_id()){
        session_start();
      }
      if(@$_SESSION['maintenance_password_ok']){
        return;
      }
      foreach($config->password_in as $in){
        if(@$request->input[$in] == $config->password){
          $_SESSION['maintenance_password_ok'] = true;
          return;
        }
      }
      $_SESSION['maintenance_password_ok'] = false;
    }
    $response = new miResponder($request);
    if(!@$config->status){
      $response->status = 503;
    }
    if(!@$config->page_title){
      @$response->page_title = 'Site unavailable due to maintenance';
    }
    if(@$config->tpl){
      if(@$config->layout){
        $response->layout = $config->layout;
      } else {
        $response->layout = false;
      }
      $response->output($config->tpl);
    } else {
      $response->body = self::$default_template;
      if(@$config->password){
        $response->body = str_replace('</body>',self::$default_login_form.'</body>',$response->body);
      }
    }
    return $response;
  }
}

