<?php

/*
    
    This plugin will hack the request method from POST vars. So if you want to 
    try and be a bit more RESTful you can add a field to you form like 
    <input type="hidden" name="<?=RequestMethodHack::postName();?>" value="PUT">

    This value will be extracted from the POST data and injected into 
    $request->method.
    
    Note that you can only hack POST requests as hacking GET requests could 
    lead to making a GET non-idempotent.

    Hackable methods can be set in configuration json, these should be an array like:
    
    "methods" : ["PUT","DELETE","PATCH"]

    This plugin could be completely abused to make up your own request methods.
    YOU'RE STRONGLY ADVISED NOT TO DO THAT!

    You can also configure the name of the post var if you don't like the default
    (or it conflicts with something) MI_METHOD
*/

class RequestMethodHack {
    
    public static $post_name = 'MI_METHOD';
    public static function before($config,&$request){

        $post_name = @$config->post_name ? $config->post_name : self::$post_name;
        
        // Store post name so templates can access it
        self::$post_name = $post_name;
        
        $hackable_methods = @$config->methods ? $config->methods : array('PUT','DELETE','PATCH');
        
        // Only the POST method can be hacked, and the hacked method must be explicitly allowed
        if($request->method != 'POST' || !isset($request->post_vars[$post_name])){
            return;
        }
        $hacked_method = strtoupper($request->post_vars[$post_name]);
        
        if(!in_array($hacked_method,$hackable_methods)){
            trigger_error(405,E_USER_ERROR);
            return;
        }
        $request->cloaked_method = $request->method;
        $request->method = $hacked_method;
    }
    
    public static function postName(){
            return self::$post_name;
    }

    
}

?>
