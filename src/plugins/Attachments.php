<?php
/**
 * Attachments Mitaji plug-in
 *
 * This allows attaching files to any resource and adding meta data about the
 * files. Resources are not by default searchable on this meta data so if
 * that is required of an application it would have to be stored in a
 * database table.
 *
 * Most methods don't know about classes or models or anything so must be
 * specified as a string $resource_name. This is used for the folder name
 * where the attachments are stored.
 *
 * Also the id is fairly arbritary, but should always use the resource id.
 * Each resource will have a folder in the resource name directory.
 *
 * Having an arbriatry resource name and id keeps this decoupled from a
 * particular type of model.
 *
 * You end up with a structure like:
 *
 * /products/1/
 * /products/2/
 *
 * Each attachment gets a file_id used internally based on the inode number.
 * This will prevent one attachment overwriting another.
 *
 * Configuration options:
 *
 * media_dir
 * :Path to media directory to store attachments, if relative
 * should be to relative to index.php
 *
 * base_url
 * :The base URL for attachments. If none specified the last part
 * of the media_dir path is used.
 *
 * transformations
 * :Transformations to apply to attachments based on suffix. These are
 * specified by transformation name with a series of method calls,
 * for example:
 *
 * "transformations" : {
 *           "product_image" : {
 *               "thumb" : {
 *                   "resize" : [68, false, 90]
 *               },
 *               "small" : {
 *                   "resize" : [false, 180, 90]
 *               },
 *               "large" : {
 *                   "resize" : [false, 800, 90]
 *               }
 *           }
 *       }
 *
 * ...would mean that if you saved an attachment with suffix product_image
 * the original image would be saved and three different sizes would also
 * be saved e.g.
 *
 * Attachments::saveUploadedFile('products',65,array('name'=>'foo.jpg'),'product_image');
 *
 * Would result in something like:
 *
 * /products/65/frg6_product_image.jpg <-- Original image
 * /products/65/frg6_product_image_thumb.jpg <-- "thumb" transform applied
 * /products/65/frg6_product_image_small.jpg <-- "small" transform applied
 * /products/65/frg6_product_image_large.jpg <-- "large" transform applied
 *
 * You could then get a particular transformation like:
 *
 * Attachments::get('products',65,'product_image','small');
 *
 * This would return all the small images. Generally the id is only used
 * internally so you don't fetch by id, you do thing like get all the small
 * images and display the first one.
 *
 */

class Attachments {

    static $config = false;
    private static $meta_cache = array();
    private static $sort_order_arr = false;

// -------------------------------------------------------------------------
/**
 * The before method is instigated by running mitaji and just sets the config
 */
    static function before($config){
        self::$config = $config;
        self::$config->media_dir = realpath(rtrim(self::$config->media_dir,'/')).'/';
    }
// -------------------------------------------------------------------------
/**
 * Create a directory writable by all system users
 */
    static function createWritableDir($d){
        if(file_exists($d)){
            if(!is_writable($d)){
                trigger_error("[Attachments::createWritableDir] Cannot write to directory $d",
                        E_USER_ERROR);
                return false;
            }
            return $d;
        }
        $old_umask = umask(0);
        if(!is_dir($d) && !mkdir($d,0777,true)){
             trigger_error("[Attachments::createWritableDir] Could not create directory $d",
                        E_USER_ERROR);
            return false;
        }
        umask($old_umask);
        return rtrim(realpath($d),'/').'/';
    }
// -------------------------------------------------------------------------
/**
 * Put contents to a file and make sure writable by everyone
 */
    static function putWritableFile($f,$contents){
        if(!file_put_contents($f,$contents)){
            trigger_error("[Attachments::putWritableFile] Cannot write file $f",
                        E_USER_ERROR);
            return false;
        }
        if(!chmod($f,0777)){
            trigger_error("[Attachments::putWritableFile] Could not change permissions of $f",
                        E_USER_ERROR);
        }
        return realpath($f);
    }
// -------------------------------------------------------------------------
/**
 * getDir will give a path to the media directory for a resource
 * @param string $resource_name The name of the resource
 * @param string|number The id of the resource
 * @param bool $create Whether to create the folder if it does not exist
 * @return string The path to the resource's folder
 */
    static function getDir($resource_name, $id, $create=false){
        $media_dir = self::$config->media_dir;
        $resource_name = AkInflector::tableize($resource_name);
        $d = $media_dir.$resource_name.'/'.$id.'/';
        if(file_exists($d)){
            return $d;
        }
        if(!$create){
            return false;
        }
        // Make resource directory
        if(!$d = self::createWritableDir($d)){
            return false;

        }
        return $d;
    }
// -------------------------------------------------------------------------
/**
 * Get attachments for the given resource
 * @param string $resource_name The name of the resource
 * @param string|number The id of the resource
 * @param string $suffix The file's suffix given when originally saved
 *                       @see Attachments::put
 * @param string $transform A particular transformation of the file applied
 *                          when saved @see Attachments::put
 * @param string $prop A particular property of the file. Can be one of:
 *                     file_id, path, url, width, height, extension
 * @return array An array of attachments. If a prop sepcified then just an
 *               array of that property otherwise a 2d array of files where
 *               each file has all properties. If meta data has been set they
 *               will be sorted based on that order.
 */
    static function get($resource_name, $id, $suffix=false, $transform=false, $prop=false){
        if(!$dir_name = self::getDir($resource_name,$id)){
            return false;
        }
        /*
         * Base URL is assumed to be the last part of the path to the media
         * directory. Override this by specifying base_url in config.
         */
        if(isset(self::$config->base_url)){
            $base_url = self::$config->base_url;
        } else {
            $tmp = preg_split('/\//',self::$config->media_dir,-1,PREG_SPLIT_NO_EMPTY);
            $base_url = '/'.$tmp[count($tmp)-1].'/';
        }
        $glob = $dir_name.'*';
        if($suffix){
            $glob .= $suffix;
        }
        if($transform){
            $glob .= '_'.$transform;
        }
        $glob .= '*';
        $files = glob($glob);
        $return_files = array();
        foreach($files as $f){
            $tmp = pathinfo($f);
            $file_parts = explode('_',$tmp['filename']);
            if($file_parts[0] == 'meta'){
                continue;
            }
            $tmp['file_id'] = $file_parts[0];
            $tmp['path'] = $f;
            $tmp['url'] = $base_url.$resource_name.'/'.$id.'/'.$tmp['filename'].'.'.$tmp['extension'];
            if(@self::$config->bust_cache){
                $tmp['url'] .=  '?'.filemtime($f);
            }
            if(in_array(strtolower($tmp['extension']),array('jpg','jpeg','gif','png','bmp'))){
                $img = getimagesize($f);
                $tmp['width'] = $img[0];
                $tmp['height'] = $img[1];
            }
            $return_files[] = $tmp;
        }
        // Sort if there is an order file
        if($suffix && $meta = self::getMeta($resource_name,$id,$suffix)){
            $merge_meta = false;
            if(is_array(current($meta))){
                self::$sort_order_arr = miArray::extractByKey($meta,'file_id');
                $merge_meta = true;
            } else {
                self::$sort_order_arr = $meta;
            }
            usort($return_files, array('Attachments', 'sortOrder'));
            if($merge_meta){
                $meta = miArray::keyBy($meta,'file_id');
                foreach($return_files as $i=>$rf){
                    if(!is_array(@$meta[$rf['file_id']])){
                        continue;
                    }
                    $return_files[$i] = array_merge($rf,$meta[$rf['file_id']]);
                }
            }
            self::$sort_order_arr = false;
        }
        if($prop){
            return miArray::extractByKey($return_files,$prop);
        }
        return $return_files;
    }
// -------------------------------------------------------------------------
/**
 * Find attachments for a particular resource. A bit like Attachments::get
 * except will work out $resource_name and $id from an instance of a class.
 * The $resource_name is assumed to be a tableized version of the objects
 * class name and the id will be got from objects primaryKey() method if it
 * exists, or by it's id property. If neither is found will return false.
 * @param object $resource An instance of a class
 * @param string $suffix The file's suffix given when originally saved
 *                       @see Attachments::put. Optional.
 * @param string $transform A particular transformation of the file applied
 *                          when saved @see Attachments::put. Optional.
 * @param string $prop A particular property of the file. Can be one of:
 *                     file_id, path, url, width, height, extension. Optional.
 * @return array An array of attachments. If a prop sepcified then just an
 *               array of that property otherwise a 2d array of files where
 *               each file has all properties. If meta data has been set they
 *               will be sorted based on that order.
 */
    static function find($resource, $suffix=false, $transform=false, $prop=false) {
        if(!is_object($resource)){
            return false;
        }
        $resource_name = AkInflector::tableize(get_class($resource));
        if(method_exists($resource,'primaryKey')){
            $id = $resource->primaryKey();
        } else if (@$resource->id){
            $id = $resource->id;
        } else {
            return false;
        }
        return self::get($resource_name, $id, $suffix, $transform, $prop);
    }
// -------------------------------------------------------------------------
/**
 * Exactly the same as Attachments::find, but will only return the first
 * file found.
 * @see Attachments::find
 */
    static function findOne($resource, $suffix=false, $transform=false, $prop=false) {
        $files = self::find($resource, $suffix, $transform, $prop);
        if(is_array($files)){
            return array_shift($files);
        }
        return false;
    }
// -------------------------------------------------------------------------
/**
 * Save an uploaded file.
 * @param string $resource_name The name of the resource
 * @param string|number The id of the resource
 * @param array $upload An uploaded file as parsed into miRequest->uploads
 * @param string $suffix @see Attachments::put
 * @param string $force_file_id @see Attachments::put
 * @param object $transform @see Attachments::put
 * @return bool true on success or false on fail
 */
    static function saveUploadedFile($resource_name, $id, $upload, $suffix=false, $force_file_id=false, $transform=null){
        // Make sure submitted extension appended to file
        $tmp = $upload['tmp_name'];
        $new = $tmp.'.'.pathinfo($upload['name'],PATHINFO_EXTENSION);
        // Use move_uploaded_file to be extra safe
        if(move_uploaded_file($tmp,$new)){;
            return self::put($resource_name, $id, $new, $suffix, $force_file_id, $transform);
        }
        return false;
    }
// -------------------------------------------------------------------------
    static function saveString($resource_name, $id, $str, $suffix, $ext){
        // Save to tmp directory
        if(!$d = self::createWritableDir(self::$config->media_dir.'.tmp/')){
            return false;
        }
        // Filename based on md5 of args and time
        $tmp_file = $d.md5($resource_name.$id.$suffix.$ext.time()).'.'.$ext;
        if(!self::putWritableFile($tmp_file,$str)){
            return false;
        }
        // Now normal put of file
        $res = self::put($resource_name, $id, $tmp_file, $suffix, false, false);
        // Remove tmp file
        unlink($tmp_file);
        // Remove tmp directory
        rmdir($d);
        return $res;
    }
// -------------------------------------------------------------------------
/**
 * The core behaviour for saving an attachment is in Attachments::put. As well
 * as saving the file to the correct place it will apply any transformations
 * defined in the config.
 * @param string $resource_name The name of the resource
 * @param string|number The id of the resource
 * @param string $f The path to a file on the local system
 * @param string $suffix A suffix to append to the file to distinguish it from
 * other types of attachment. This is also a hook for applying transformations
 * defined in config.
 * @param string $force_file_id Forces file id to a particular value, useful
 * for reprocessing existing attachments
 * @param object $transform Overrides any configured transforms for this suffix.
 * Useful if tranfromations need to be conditional e.g. different processing
 * for landscape and portrait images.
 * @return bool True on success or false on failure
 */
    static function put($resource_name, $id, $f, $suffix=false, $force_file_id=false, $transform = null){

        // Get directory, third argument means create if does not exist
        if(!$dir_name = self::getDir($resource_name,$id,true)){
            return false;
        }
        $src_f = realpath($f);
        if(!file_exists($src_f)){
            trigger_error("[Attachments::put] $f does not exist",
                            E_USER_WARNING);
            return false;
        }
        if(!$blob = file_get_contents($src_f)){
            trigger_error("[Attachments::put] $f cannot be read",
                            E_USER_WARNING);
            return false;
        }
        // Unique base filename based on fileinode, but can be overridden
        $file_id = $force_file_id ? $force_file_id : base_convert(fileinode($f),10,36);
        $ext = strtolower(pathinfo($src_f,PATHINFO_EXTENSION));
        if($suffix){
            $base_file = $dir_name.$file_id.'_'.$suffix;
        } else {
            $base_file = $dir_name.$file_id;
        }
        $base_file .= '.'.$ext;
        $file_count = 0;
        // Save the original file if doesn't already exist
        //if(!file_exists($base_file)){
            if(!self::putWritableFile($base_file,$blob)){
                trigger_error("[Attachments::put] Could not save $base_file",E_USER_WARNING);
                return false;
            }
            $file_count++;
        //}
        // Apply transformations if there are any
        $trans = is_object($transform) ? $transform : @self::$config->transformations->{$suffix};
        if($suffix && $trans){
            foreach($trans as $k=>$tran){
                $in_file = $base_file;
                $out_file = preg_replace("/\.$ext$/", '', $base_file) . "_$k.$ext";
                $i = 0;
                foreach($tran as $fnc=>$args){
                    $i++;
                    $tmp_out_file = $out_file.'.tmp'.$i;
                    $args[] = $in_file;
                    $args[] = $tmp_out_file;
                    call_user_func_array(array('Attachments',$fnc),$args);
                    $in_file = $tmp_out_file;
                }
                // Rename to correct out file name
                rename($tmp_out_file, $out_file);
                // Clean up tmp files
                $tmp_files = glob($dir_name.'*.tmp*');
                if($tmp_files){
                    foreach($tmp_files as $tf){
                        unlink($tf);
                    }
                }
            }
        }
        // Add info to meta file
        self::addToMeta($resource_name,$id,$suffix,$file_id);
        return $file_id;
    }
// -------------------------------------------------------------------------
/**
 * Get the name of the meta file associated witha resources attachments.
 * @param string $resource_name The name of the resource
 * @param string|number The id of the resource
 * @param string $suffix The file suffix. Unlike some other methods this is
 *                       required as meta data is always associated with a
 *                       particular suffix.
 * @param bool $create_folder Whether to create the rsource folder if it
 *                             doesn't already exist
 */
    public static function getMetaFile($resource_name, $id, $suffix, $create_folder=false){
        if(!$dir_name = self::getDir($resource_name, $id)){
            return false;
        }
        $meta_file = $dir_name.'meta';
        if($suffix){
            $meta_file .= '_'.md5($suffix);
        }
        $meta_file .= '.json';
        return $meta_file;
    }
// -------------------------------------------------------------------------
/**
 * This will add a file to the meta file if it isn't already mentioned.
 */
    public static function addToMeta($resource_name, $id, $suffix, $file_id){
        if(!$meta = self::getMeta($resource_name,$id,$suffix,false)){
            $meta = array();
        }
        $file_ids = miArray::extractByKey($meta,'file_id');
        if(!in_array($file_id,$file_ids)){
            array_unshift($meta,array('file_id'=>$file_id));
            self::setMeta($resource_name,$id,$suffix,$meta);
        }
        return true;
    }
// -------------------------------------------------------------------------
/**
 * Set the meta data associated with a particular resource's attachments based
 * on suffix. This all ends up in a JSON file in the resource's directory.
 * @param string $resource_name The name of the resource
 * @param string|number The id of the resource
 * @param string $suffix The file suffix. Unlike some other methods this is
 *                       required as meta data is always associated with a
 *                       particular suffix.
 * @param array $meta The meta data to associate with the files. This should
 *                    be a 2d array where each element has at least one
 *                    property, the file_id. Any other arbritary properties
 *                    may be added. If just an object with a file id property
 *                    it will be put in an array, but all handled internally, no
 *                    need to worry about this.
 * @param bool $delete_missing Whether to delete files who's ids are not in
 *                             the passed $meta.
 * @return bool True on success false otherwise
 */
    static function setMeta($resource_name, $id, $suffix, $meta, $delete_missing=false){
        $meta_cache_key = md5($resource_name.$id.$suffix);
        $meta_file = self::getMetaFile($resource_name, $id, $suffix, true);
        // If just one item put it into an array
        if(@$meta['file_id']){
            $meta = array($meta);
        }
        // Validate meta data and at same time ensures that it's *not* an associative array
        $tmp = array();
        foreach($meta as $m){
            if(!$m['file_id']){
                trigger_error('Attachments::setMeta: Data is invalid as file id not present',E_USER_WARNING);
                return false;
            }
            $tmp[] = $m;
        }
        $meta = $tmp;

        if($delete_missing && ($current_meta = self::getMeta($resource_name, $id, $suffix))){
            $new_file_ids = miArray::extractByKey($meta,'file_id');
            $current_file_ids = miArray::extractByKey($current_meta, 'file_id');
            $delete_file_ids = array_diff($current_file_ids,$new_file_ids);
            foreach($delete_file_ids as $delete_file_id){
                self::delete($resource_name, $id, $delete_file_id);
            }
        }
        if ($meta) {
            $meta_json = json_encode($meta);
            if(self::putWritableFile($meta_file,$meta_json)){
                self::$meta_cache[$meta_cache_key] = $meta;
                return true;
            }
            return false;
        }
        // Mo meta so delete any existing file
        unlink($meta_file);
        return true;
    }
// -------------------------------------------------------------------------
/**
 * Get the meta data associated with a particular resource's attachments based
 * on suffix.
 * @param string $resource_name The name of the resource
 * @param string|number The id of the resource
 * @param string $suffix The file suffix. Unlike some other methods this is
 *                       required as meta data is always associated with a
 *                       particular suffix.
 * @return array A 2d array of meta data. each element will have at least a
 *               'file_id' property and anything else added with
 *                Attachments::setMeta
 */
    static function getMeta($resource_name, $id, $suffix, $cache=true){
        $meta_cache_key = md5($resource_name.$id.$suffix);
        if($cache && $meta = @self::$meta_cache[$meta_cache_key]){
            return $meta;
        }
        $meta_file = self::getMetaFile($resource_name, $id, $suffix);
        if(!$meta_file || !file_exists($meta_file)){
            return false;
        }
        if($meta = json_decode(file_get_contents($meta_file),true)){
            if($cache){
                self::$meta_cache[$meta_cache_key] = $meta;
            }
            return $meta;
        }
        return false;
    }
// -------------------------------------------------------------------------
/**
 * Private function for getting image in the same order as their meta data
 */
    private static function sortOrder($a, $b){
        $a_index = array_search($a['file_id'], self::$sort_order_arr);
        $b_index = array_search($b['file_id'], self::$sort_order_arr);
        if($a_index == $b_index){
            return 0;
        }
        return $a_index < $b_index ? -1 : 1;
    }
// -------------------------------------------------------------------------
/**
 * Delete one or several attachments
 * @param string $resource_name The name of the resource
 * @param string|number The id of the resource
 * @param string $file_id Optional. The id of the file to delete. If not
 * specified all attachments for this resource will be deleted.
 */
    static function delete($resource_name, $id, $file_id=false){
        $resource_name = strtolower($resource_name);
        if(!$dir_name = self::getDir($resource_name,$id)){
            return;
        }
        if($file_id){
            $glob = $dir_name.$file_id.'_*';
        } else {
            $glob = $dir_name.'*';
        }
        $files = glob($glob);
        if($files){
            foreach($files as $f){
                unlink($f);
            }
        }
        // Clean up directories
        if(!glob($dir_name.'*')){
            rmdir($dir_name);
            // Remove resource directory if empty
            if(!glob(self::$config->media_dir.$resource_name.'/*')){
                rmdir(self::$config->media_dir.$resource_name.'/');
            }
        }
        return true;
    }
// -------------------------------------------------------------------------
/**
 * Transformation function to resize an image. Requires miMath to do the
 * scaling. The new width and height are treated as maximums and the image
 * will be resized without changing aspect ratio. So if the original was
 * 600x400 and you try to resize to 400x300 you'll end up with 400x200.
 * @param int $w The maximum width of the image
 * @param int $h The maximum height of the image
 * @param int $quality The quality of the resized image (for jpegs)
 * @param string $src Path to source image
 * @param string $dest Path to destination image - must not be same as $src
 */
    static function resize($w,$h,$quality,$src,$dest){

        if($src == $dest){
            trigger_error("Attachments::resize: source
                          file cannot be same as destination file ($src)",
                          E_USER_ERROR);
            return false;
        }
        // Get src image info
        list($width, $height, $type, $attr) = getimagesize($src);

        // Check GIF support
        if(IMAGETYPE_GIF == $type && self::gifSupport() == false){
            trigger_error('[Attachments] GIF support is not available on this server',E_USER_ERROR);
            return false;
        }

        // If correct size already, just copy
        if($width <= $w && $height <= $h){
            copy($src,$dest);
            return $dest;
        }
        // Get new dimensions
        list($new_width,$new_height) = miMath::scale(array($width,$height),array($w,$h));

        // Convert the old image to a resource
        $old_image = self::getImageResource($type, $src);

        // Create a new image resource at the target size
        $new_image = self::createImageResource($type, $new_width, $new_height);

        // Copy and resample
        imagecopyresampled($new_image,$old_image,0,0,0,0,$new_width,$new_height,$width,$height);

        // Save the final new image
        self::writeImageResource($type,$new_image,$dest,$quality);

        // Clean up
        imagedestroy($old_image);
        imagedestroy($new_image);

        return $dest;
    }
# ----------------------------------------------------------------------------
/*
 * This will crop out part of an image.
 *
 * @param mixed $origin An object or array with properties x and y. This is where
 * the top left corner of the crop will start. Can also use keywords. For
 * the X axis can use 'left', 'right' and 'center', for the Y axis 'top',
 * 'bottom' and 'center'. The 'center' keyword means the space around the
 * crop will be even. If both were 'center' it's like punching out the
 * central part of the image at width and height which indicate the amount
 * from the origin point to crop. Will never be more than orignal width and
 * height.
 * @param mixed $size An object or array with properties w(idth) and h(eight)
 * @param int $quality The quality of the resized image (for jpegs only)
 * @param string $src Path to source image
 * @param string $dest Path to destination image - must not be same as $src
 * @return mixed The destination file name or false
 */
    static function crop($origin, $size, $quality, $src, $dest){
        if($src == $dest){
            trigger_error("Attachments::crop: source file cannot be sames as destination file ($src)",
                          E_USER_ERROR);
            return false;
        }

        // Get src image info
        list($width, $height, $type, $attr) = getimagesize($src);

        // Check GIF support
        if(IMAGETYPE_GIF == $type && self::gifSupport() == false){
            trigger_error('[Attachments] GIF support is not available on this server',E_USER_ERROR);
            return false;
        }

        list($origin_x, $origin_y) = (array)$origin;
        list($new_width, $new_height) = (array)$size;

        // If the image is already smaller than the crop size no need to do anything but copy
        if($width <= $new_width && $height <= $new_height){
            copy($src, $dest);
            return $dest;
        }

        // X origin
        $keyword_x = array('left'=>0, 'right'=>$width, 'center'=>max($width-$new_width,0)/2);
        if(array_key_exists($origin_x,$keyword_x)){
            $origin_x = $keyword_x[$origin_x];
        }
        if(!is_numeric($origin_x)){
            trigger_error("Attachments::crop: Origin X position $origin->x not understood",
                          E_USER_ERROR);
            return false;
        }
        // Y origin
        $keyword_y = array('top'=>0, 'bottom'=>$height, 'center'=>max($height-$new_height,0)/2);
        if(array_key_exists($origin_y,$keyword_y)){
            $origin_y = $keyword_y[$origin_y];
        }
        if(!is_numeric($origin_y)){
            trigger_error("Attachments::crop: Origin Y position $origin->y not understood",
                          E_USER_ERROR);
            return false;
        }
        // Convert the old image to a resource
        $old_image = self::getImageResource($type, $src);

        // Create a new image resource at the target size
        $new_image = self::createImageResource($type, $new_width, $new_height);

        // Copy the relevant bit of the src image to the new image
        imagecopyresampled($new_image,
                           $old_image,
                           0,0,
                           $origin_x,$origin_y,
                           $new_width,$new_height,
                           $new_width,$new_height);

        // Save the final new image
        self::writeImageResource($type,$new_image,$dest,$quality);

        // Clean up
        imagedestroy($old_image);
        imagedestroy($new_image);

        // Return destination
        return $dest;
    }
// -------------------------------------------------------------------------
    private static function createImageResource($type, $width, $height){
        switch($type){
            case IMAGETYPE_JPEG:
                return imagecreatetruecolor($width,$height);
            case IMAGETYPE_PNG:
                return imagecreatetruecolor($width,$height);
            case IMAGETYPE_GIF:
                // True color GIFs are not supported by GD
                return imagecreate($width,$height);
        }
        return false;
    }
// -------------------------------------------------------------------------
    private static function getImageResource($type,$src){
        switch($type){
            case IMAGETYPE_JPEG:
                return imagecreatefromjpeg($src);
            case IMAGETYPE_PNG:
                return imagecreatetruecolor($new_width,$new_height);
            case IMAGETYPE_GIF:
                return imagecreatefromgif($src);
                break;
        }
        return false;
    }
// -------------------------------------------------------------------------
    private static function writeImageResource($type,$img,$dest,$quality=100){
        switch($type){
            case IMAGETYPE_JPEG:
                imagejpeg($img,$dest,$quality);
                break;
            case IMAGETYPE_PNG:
                imagepng($img,$dest);
                break;
            case IMAGETYPE_GIF:
                imagegif($img,$dest);
                break;
        }
        return false;
    }
// -------------------------------------------------------------------------
    private static function gifSupport(){
        return imagetypes() & IMG_GIF;
    }
// -------------------------------------------------------------------------
}
