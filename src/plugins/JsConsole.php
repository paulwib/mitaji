<?php

/**
* JsConsole Mitaji Plug-in
* Print errors and debug messages to javascript console
*
* @author Paul Willoughby <paul@fivetide.com>
* @copyright Copyright (c) 2007, Paul Willoughby http://www.fivetide.com
* @license GNU Lesser General Public License <http://www.gnu.org/copyleft/lesser.html>
*/


class JsConsole {

    public static $log_to_file = false;
    private static $js_lines = array();
    private static $js_fun = 'log';
    private static $dir_fun = 'print_r';
    private static $timers = array();
    private static $config = false;
    private static $off = false;
    private static $counters = array();
// -------------------------------------------------------------------------
/**
 * Switch off while code is running
 */
    static function off(){
        self::$off = true;
    }
// -------------------------------------------------------------------------
/**
 * Switch on while code is running
 */
    static function on(){
        self::$off = false;
    }
// -------------------------------------------------------------------------
/**
 * Switch to logging messages to a file rather than to the console
 */
    static function logToFile($f){
        if(self::$off){
            return;
        }
        // Create file if it does not exist
        if(!file_exists($f) && !touch($f)){
            trigger_error("[JsConsole] Cannot create log file '$f'", E_USER_ERROR);
            return false;
        }
        // Check file is (still) writable
        if(is_writable($f)){
            self::$log_to_file = $f;
            return true;
        } else {
            trigger_error("[JsConsole] Cannot write to log file '$f'", E_USER_ERROR);
            return false;
        }
    }
// -------------------------------------------------------------------------
/**
 * Private log function
 */
    private static function __log(){
        if(self::$off){
            return;
        }
        $args = func_get_args();
        $js_args = array();
        foreach($args as $v){
            if(is_array($v) || is_object($v)){
                $str = self::dir($v);
                $js_args[] = $str;
            } else {
                if(is_null($v)){
                    $js_args[] = "'null'";
                } else if(is_bool($v)){
                    $js_args[] = $v ? 'true' : 'false';
                } else if(is_int($v) || is_float($v)){
                    $js_args[] = $v;
                } else {
                    $js_args[] = "'".self::escapeStr($v)."'";
                }
            }
        }
        if(self::$log_to_file){
            file_put_contents(self::$log_to_file,date('Y-m-d H:i:s').' '.
                                trim(implode(',',str_replace('\n',"\n",$js_args)))."\n",
                                FILE_APPEND);
            return;
        }
        self::$js_lines[] = 'console.'.self::$js_fun.'('.implode(',',$js_args).')';
    }
// -------------------------------------------------------------------------
/**
 * Log a message or variable of any type, maps to console.log()
 * @param mixed $var,... Mutliple variables to log, can be strings, numbers,
 * objects, arrays etc.
 */
    static function log(){
        if(self::$off){
            return;
        }
        self::$js_fun = 'log';
        $args = func_get_args();
        call_user_func_array(array('JsConsole','__log'),$args);
    }
// -------------------------------------------------------------------------
/**
 * Log a message and keep a count of it
 * @param string $msg The message to count, every time called with this message
 * one will be added to the count
 */
    static function count($msg){
        if(self::$off){
            return;
        }
        self::$js_fun = 'log';
        if(!array_key_exists($msg,self::$counters)){
            self::$counters[$msg] = 1;
        }
        $c = self::$counters[$msg]++;
        self::__log($msg,' ---> count:',$c);
    }
// -------------------------------------------------------------------------
/**
 * Maps to console.info()
 * @see JsConsole::log
 * @param mixed $var,... Mutliple variables to log, can be strings, numbers,
 * objects, arrays etc.
 */
    static function info(){
        self::$js_fun = 'info';
        $args = func_get_args();
        call_user_func_array(array('JsConsole','__log'),$args);
        self::$js_fun = 'log';
    }
// -------------------------------------------------------------------------
/**
 * Maps to console.warn()
 * @see JsConsole::log
 * @param mixed $var,... Mutliple variables to log, can be strings, numbers,
 * objects, arrays etc.
 */
    static function warn(){
        if(self::$off){
            return;
        }
        self::$js_fun = 'warn';
        $args = func_get_args();
        call_user_func_array(array('JsConsole','__log'),$args);
        self::$js_fun = 'log';
    }
// -------------------------------------------------------------------------
/**
 * Maps to console.error()
 * @see JsConsole::log
 * @param mixed $var,... Mutliple variables to log, can be strings, numbers,
 * objects, arrays etc.
 */
    static function error(){
        if(self::$off){
            return;
        }
        self::$js_fun = 'error';
        $args = func_get_args();
        call_user_func_array(array('JsConsole','__log'),$args);
        self::$js_fun = 'log';
    }
// -------------------------------------------------------------------------
/**
 * Maps to console.dir()
 * @see JsConsole::log
 * @param mixed $var The variable to print, as ooposed to other log functions
 * this will only accept one argument
 */
    static function dir($var){
        if(self::$off){
            return;
        }
        switch(self::$dir_fun){
            case 'json_encode':
                return json_encode($var);
            case 'var_dump':
                ob_start();
                var_dump($var);
                $tmp = ob_get_contents();
                ob_end_clean();
                return "'".self::escapeStr($tmp)."'";
            case 'var_export':
                return "'".self::escapeStr(var_export($var,true))."'";
            case 'print_r':
            default:
                return "'".self::escapeStr(print_r($var,true))."'";
        }
    }
// -------------------------------------------------------------------------
/**
 * Private function for converting a PHP string into a Javascript one
 * @param string $s The string to escape
 * @return string The escaped string
 */
    private static function escapeStr($s){
        $s = str_replace("\n",'\n',$s);
        $s = str_replace("\r",'\n',$s);
        $s = str_replace("'","\'",$s);
        $s = str_replace('\\\\','\\',$s);
        return $s;
    }
// -------------------------------------------------------------------------
/**
 * Start a timer
 * @param string $name A name for the timer
 * @return void
 */
    static function time($name){
        if(self::$off){
            return;
        }
        self::$timers[$name] = microtime(true);
    }
// -------------------------------------------------------------------------
/**
 * End a timer. Will call JsConsole:::log with the result so the result
 * will be printed to the javascript console.
 * @param string $name The name of the timer passed to JsConsole::time()
 * @return float $seconds The number of seconds elapsed or false if no timer
 * exists with $name.
 */
    static function timeEnd($name,$begin=false){
        if(self::$off){
            return;
        }
        if(!isset(self::$timers[$name]) && $begin !== false && is_numeric($begin)){
            self::$timers[$name] = $begin;
        }
        if(!isset(self::$timers[$name])){
            self::warn('Cannot do timer as no start time for '.$name);
            return false;
        }
        $seconds = number_format((microtime(true)-self::$timers[$name]),4,'.','');
        self::log(preg_replace('/\s+/',' ',$name).': ',$seconds.' seconds ('.$seconds*1000,' ms)');
        return $seconds;
    }
// -------------------------------------------------------------------------
/**
 * The function called by Mitaji BEFORE the request has been processed and a
 * response returned. Passed in config plugins->JsConsole, options below:
 * @param object $config The JsConsole configuration which has the
 * following options:
 *  - on boolean - Whether JsConsole is on or not. This can be also be set at
 *    runtime with JsConsole::on() and JsConsole::off()
 * - error_reporting int - The errors to report, like PHP error_reporting()
 * - dir_fun string - The function to use to print objects and arrays. Can be one
 *   of json_encode, var_dump, var_export or print_r. Defaults to json_encode.
 * - log_to_file string - a file to write log to instead of JS console.
 */
    static function before($config, &$request){
        if(isset($config->dir_fun)){
            self::$dir_fun = $config->dir_fun;
        }
        if(isset($config->log_to_file)){
            self::logToFile($config->log_to_file);
        }
    }
// ------------------------------------------------------------------------
/**
 * The function called by Mitaji AFTER the request has been processed and a
 * response returned. Writes javascript into the body of the page so messages
 * can be viewed in javascript console.
 * @see JsConsole::before for config options
 */
    static function after($config, &$response, &$request){
        if(self::$off || !$config->on){
            self::$off = true;
            return;
        }
        // HTML closing </body> tag must be present
        $body = $response->body;
        if(!strstr($body,'</body>')){
            return;
        }
        if(class_exists('miDb')){
            $queries = miDb::queryLog(0,'milliseconds');
            self::log(count($queries).' DB queries in '.array_sum($queries).'ms');
        }
        // Error reporting
        if(isset($config->error_reporting)){
            if(!is_numeric($config->error_reporting)){
                $error_reporting = constant($config->error_reporting);
            } else {
                $error_reporting = $config->error_reporting;
            }
        }

        if($config->error_reporting && $es = $request->getErrors()){
            foreach($es as $e){
                if(!($config->error_reporting & $e['no'])){
                    continue;
                }
                $level = $request->getErrorLevelString($e['no']);
                $str = "{$e['str']} (line {$e['line']} of {$e['file']}, severity $level)";
                switch($e['no']) {
                    case E_ERROR:
                    case E_USER_ERROR:
                        self::warn($str);
                        break;
                    case E_WARNING:
                    case E_USER_WARNING:
                        self::warn($str);
                        break;
                    case E_NOTICE:
                    case E_USER_NOTICE:
                        self::log($str);
                        break;
                    default:
                        self::log($str);
                }
            }
        }

        // Write javascript before closing HTML body tag
        $js = "\n";
        $js .= '<script type="text/javascript">';
        $js .= "\n<!--\n";
        $js .= "if(typeof(console) != 'undefined' && typeof(console.log) == 'function'){\n";
        if(self::$js_lines){
            $js .= implode(";\n",self::$js_lines)."\n\n";
        };
        $js .= "\n// Read generated time from meta tag as that is most accurate\n";
        $js .= 'var metas = document.getElementsByTagName("meta");
                if(null != metas){
                    for(var i=0,l=metas.length;i<l;i++){
                        if(metas[i].name == "generated_in"){
                            console.log("Page generation time: "+metas[i].content);
                            break;
                        }
                    }
                 }';
        $js .= "\n}\n//-->\n</script>\n";
        $response->body = str_replace('</body>',$js."\n</body>",$body);
    }
# -------------------------------------------------------------------------
}

?>
