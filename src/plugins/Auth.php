<?php

/**
 * Auth plugin
 * 
 * Doesn't really do anything on it's own, more of a helper. See 
 * AuthDigest and AuthSession for examples of plugins that extend the
 * this class to do something useful
 */

class Auth {
    protected static $error = false;
    protected static $config = false;
    const ERROR_LOGIN_EMPTY = 1;
    const ERROR_LOGIN_FAIL = 2;
// -------------------------------------------------------------------------
    static function parseAuthFunction(){
        // Sort out authentication function
        if(!isset(self::$config->auth_function)){
            trigger_error('[AuthSession] No user authentication function defined',E_USER_ERROR);
        }
        if(strstr(self::$config->auth_function,'::')){
            $auth_function = explode('::',self::$config->auth_function);
        } else {
            $auth_function = self::$config->auth_user_func;
        }
        if (!is_callable($auth_function)) {
            trigger_error("Auth function not callable", E_USER_ERROR);
            return;
        }
        return $auth_function; 
    }
// -------------------------------------------------------------------------    
    static function checkProtected($check_url){
        if(!$urls = @self::$config->urls){
            return false;
        }
        // Get protected URLs and convert to regex patterns
        $rxs = array();
        foreach($urls as $url){
            $rxs['/^'.str_replace('\*','.*', preg_quote($url->pattern,'/')).'$/'] = $url;
        }
        foreach($rxs as $rx=>$url){
            if(preg_match($rx, $check_url)){
                return $url;             
            }
        }
        return false;       
    }
// ------------------------------------------------------------------------- 
    static function checkRequiredUser($url, $username){
        if(!@$url->require_user) return true;
        return in_array($username, explode(' ',$url->require_user));
    }
// ------------------------------------------------------------------------- 
    static function checkRequiredGroup($url, $usergroup){
        // No required group
        if(!$require_group = @$url->require_group){
            return true;
        }
        // User has no group
        if(!$usergroup){
            return false;
        }
        // Compare user's groups and required groups
        $usergroup = explode(' ',$usergroup);
        $require_group = explode(' ', $require_group);
        return array_intersect($require_group, $usergroup);
    }
}
