<?php

require dirname(__FILE__) . '/Auth.php';

class AuthDigest extends Auth {
    
    private static $ie_compat = true;

// -------------------------------------------------------------------------    
    public static function before($config,&$request){
        self::$config = $config;
        
        // Need apache_request_headers() to work
        if(!function_exists('apache_request_headers')){
            trigger_error('[AuthDigest] PHP not running as Apache module,
                           Digest Authentication not possible',E_USER_ERROR);
            return;
        }
        
        // No auth if current request URL doesn't match a defined pattern
        if(!($url = self::checkProtected($request->url_path))){
            return;
        }

        // Make the authentication header to send to the client
        $time = time();
        $salt = md5($request->client_ip.':'.
                    $request->host.':'.
                    $request->headers['User-Agent']);
        $nonce_options = array(
            md5(date('Y-m-d H:i',$time).':'.$salt.':'.$config->realm),
            md5(date('Y-m-d H:i',$time-60).':'.$salt.':'.$config->realm),
            md5(date('Y-m-d H:i',$time-120).':'.$salt.':'.$config->realm)
            );

        // Auth header
        $auth_header = 'Digest ';
        $auth_header .= 'qop="auth", '; // No auth-int for now
        $auth_header .= 'realm="'.$config->realm.'", ';
        $auth_header .= 'nonce="'.$nonce_options[0].'", ';
        $auth_header .= 'opaque="'.md5($config->opaque).'" ';

        // Get auth header received - PHP MUST be running as Apache module       
        $request_auth_header = @$request->headers['Authorization'];
        
        // No auth header received? Request one...    
        if(!$request_auth_header){ 
            return self::authResponder($auth_header,$request);
        }

        // Convert received auth header to an array
        $auth_str = str_replace('Digest ','',$request_auth_header);
        $tmp = explode(',',$auth_str);
        $auth = array();
        foreach($tmp as $str){
            $i = strpos($str,'=');
            $key = trim(substr($str,0,$i));
            $value = trim(substr($str,$i+1),'"\'');
            $auth[$key] = $value;
        }

        /*
         * IE compatibility - IE doesn't include query string in the URI 
         * in the auth header so we help it out by adding it
         */
        $request_uri = $request->uri;
        if(@$config->ie_compat && strpos($auth['uri'], '?') === false 
            && strpos($request_uri, '?') !== false) { 
                $request_uri = $request->url_path;
        }

        // Check URIs match        
        if($auth['uri'] != $request_uri){
            $err_str = "[AuthDigest] Authentication URI {$auth['uri']} does 
                                        not match request URI $request_uri";
            if(@$config->ie_compat){
                $err_str .= ' (running in IE compatibility mode)';
            }
            trigger_error($err_str, E_USER_NOTICE);
            trigger_error(400,E_USER_ERROR);
            return;
        }

        // If opaque is used, check it is correct
        if(isset($auth['opaque']) && 
                 $auth['opaque'] != md5($config->opaque)){
            return self::authResponder($auth_header,$request,true);
        }

        // Stale nonce
        if(!in_array($auth['nonce'],$nonce_options)) {
            return self::authResponder($auth_header,$request,true);
        }     

        /* 
         * Call authentication function should return object/array containing 
         * at minimum a ha1 hash, which is md5 of `username:realm:password` 
         */
        $username = $auth['username'];
        $user = call_user_func(self::parseAuthFunction(), $username, $request, 'AuthDigest');
        if(!$user){
            return self::authResponder($auth_header,$request);
        }
        $user = (object)$user;
        $user->auth_method = 'HTTP Digest';

        /**
         * @todo: To support auth-int the body of the request would have to 
         * be added to the hash e.g. md5('request_method:request_uri:body')
         * Currently only Opera supports auth-int and it won't work unless the 
         * form has Content-Type: application/x-www-form-urlencoded
         */
        $ha2 = md5($_SERVER['REQUEST_METHOD'].':'.$request_uri);
        $combined = $user->ha1.':'.
                    $auth['nonce'].':'.
                    $auth['nc'].':'.
                    $auth['cnonce'].':'.
                    $auth['qop'].':'.
                    $ha2;
                    
        // User name and password OK
        if($auth['response'] == md5($combined)) {
            
            // Check required user
            if(!self::checkRequiredUser($username, @$url)){
                trigger_error('[AuthDigest] User OK, but not required user', E_USER_NOTICE);
                trigger_error(403, E_USER_ERROR);
                return;
            }
            
            // Check required group
            if(!self::checkRequiredGroup(@$user->group, @$url)){
                trigger_error('[AuthDigest] User OK,  but not in required group', E_USER_NOTICE);
                trigger_error(403, E_USER_ERROR);
                return;
            }
            $request->user = $user;
            return;
        }
        // Password must be incorrect
        return self::authResponder($auth_header,$request);
    }
// -------------------------------------------------------------------------  
    private static function authResponder($auth_header, $request, $stale=false){
        if($stale){
            $auth_header .= ', stale="true"';
        }
        $response = new miResponder($request);
        $response->headers['WWW-Authenticate'] = $auth_header;
        $response->status = 401;
        $response->layout = 'error.html.php';
        return $response->output('errors/401.php');
    }
}

