﻿<?php

/*
    Attempts to convert all non-ascii utf-8 entities into 
    equaivalent HTML hex codes.

    Weirdly this doesn't work on my Windows box or Dreamhost 
    unless the file is saved as ANSI.

    Consider this just a demo of an after plugin - don't use 
    without extensive testing on your own setup.

    Also I find most of these characters render fine if the 
    page is served as UTF-8.

    Below is another way to do this by testing if the ord is over 160.
    That seems to give better results.

    Both methods don't convert < and > so you'd have to do this 
    elsewhere if you wanted to print those characters.

    Also it seems to depend on the encoding of the file that contains the 
    string that you're trying to output. So bit of a nightmare really.

    Actually, might have something to do with simpletest. Although I ask it 
    to use UTF-8 I'm not sure if it does.
*/
class i18nEntities {

    public static function after($config,&$response){
        $translate = array(array('&#161;','¡'),
                            array('&#162;','¢'),
                            array('&#163;','£'),
                            array('&#164;','¤'),
                            array('&#165;','¥'),
                            array('&#166;','¦'),
                            array('&#167;','§'),
                            array('&#168;','¨'),
                            array('&#169;','©'),
                            array('&#170;','ª'),
                            array('&#171;','«'),
                            array('&#172;','¬'),
                            array('&#174;','®'),
                            array('&#175;','¯'),
                            array('&#176;','°'),
                            array('&#177;','±'),
                            array('&#178;','²'),
                            array('&#179;','³'),
                            array('&#180;','´'),
                            array('&#181;','µ'),
                            array('&#182;','¶'),
                            array('&#183;','·'),
                            array('&#184;','¸'),
                            array('&#185;','¹'),
                            array('&#186;','º'),
                            array('&#187;','»'),
                            array('&#188;','¼'),
                            array('&#189;','½'),
                            array('&#190;','¾'),
                            array('&#191;','¿'),
                            array('&#192;','À'),
                            array('&#193;','Á'),
                            array('&#194;','Â'),
                            array('&#195;','Ã'),
                            array('&#196;','Ä'),
                            array('&#197;','Å'),
                            array('&#198;','Æ'),
                            array('&#199;','Ç'),
                            array('&#200;','È'),
                            array('&#201;','É'),
                            array('&#202;','Ê'),
                            array('&#203;','Ë'),
                            array('&#204;','Ì'),
                            array('&#205;','Í'),
                            array('&#206;','Î'),
                            array('&#207;','Ï'),
                            array('&#208;','Ð'),
                            array('&#209;','Ñ'),
                            array('&#210;','Ò'),
                            array('&#211;','Ó'),
                            array('&#212;','Ô'),
                            array('&#213;','Õ'),
                            array('&#214;','Ö'),
                            array('&#215;','×'),
                            array('&#216;','Ø'),
                            array('&#217;','Ù'),
                            array('&#218;','Ú'),
                            array('&#219;','Û'),
                            array('&#220;','Ü'),
                            array('&#221;','Ý'),
                            array('&#222;','Þ'),
                            array('&#223;','ß'),
                            array('&#224;','à'),
                            array('&#225;','á'),
                            array('&#226;','â'),
                            array('&#227;','ã'),
                            array('&#228;','ä'),
                            array('&#229;','å'),
                            array('&#230;','æ'),
                            array('&#231;','ç'),
                            array('&#232;','è'),
                            array('&#233;','é'),
                            array('&#234;','ê'),
                            array('&#235;','ë'),
                            array('&#236;','ì'),
                            array('&#237;','í'),
                            array('&#238;','î'),
                            array('&#239;','ï'),
                            array('&#240;','ð'),
                            array('&#241;','ñ'),
                            array('&#242;','ò'),
                            array('&#243;','ó'),
                            array('&#244;','ô'),
                            array('&#245;','õ'),
                            array('&#246;','ö'),
                            array('&#247;','÷'),
                            array('&#248;','ø'),
                            array('&#249;','ù'),
                            array('&#250;','ú'),
                            array('&#251;','û'),
                            array('&#252;','ü'),
                            array('&#253;','ý'),
                            array('&#254;','þ'),
                            array('&#255;','ÿ'));
        $str = $response->body();
        $search = array();
        $replace = array();
        foreach($translate as $t){
            $search[] = $t[1];
            $replace[] = $t[0];
        }
        $str = str_replace($search,$replace,$str);
        /*$response->body($str);*/
        
        /* Another way to do it is convert all characters with an ord over 160 */
        $str = $response->body();
        $a = str_split($str);
        $out = '';
        foreach($a as $c){
            $ord = ord($c);
            if($ord > 161){
                $out .= "&#$ord;";
            } else {
                $out .= $c;
            }
        }
        $response->body($out);
    }
}

?>