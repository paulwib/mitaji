<?php


class Cache {
    
    private static $config = false;
// ----------------------------------------------------------------------
    static function before($config, &$request){

		//JsConsole::logToFile('/tmp/mporium.log');
        self::$config = $config;
        self::$config->host = $request->host;
        
        // Do nothing if not switched on
        if(!$config->on || $request->method != 'GET'){
            return;
        }
        // Do nothing if not a logged in user (depends on config)
        if(self::$config->not_for_request_user && @$request->user){
            return;
        }
        
        $cached_file = self::cacheFile($config,$request);
        if(!file_exists($cached_file)){
			//JsConsole::log("No cached file $cached_file so just calling normal responder");
            return;
        }
        // If file older than when system updated then delete and return
        $system_updated = @filemtime($config->temp_dir.'system_updated');
        $cached_filemtime = filemtime($cached_file);
        if($system_updated && $cached_filemtime < $system_updated){
			/*JsConsole::log("Cached file $cached_file is older than when system".
							" updated so deleting and will call normal responder");*/
            unlink($cached_file);
			unlink($cached_file.'.meta');
            return;
        } 
		// Get file contents and meta
        $cached_file_contents = file_get_contents($cached_file);
		$meta = json_decode(file_get_contents($cached_file.'.meta'));
		$expires = $cached_filemtime+$meta->max_age;
		// If file older than max age then regenerate (auto invalidation)
		if($expires < time()){
			//JsConsole::log('Auto expired file');
			unlink($cached_file);
			unlink($cached_file.'.meta');
			return;
		}
		//JsConsole::log("Getting URL from cached file: $cached_file");
        // Work out etag and last modified
		$etag = md5($cached_file_contents);
        $last_modified = date('r',$cached_filemtime);
        $response = new miResponder($request, mitaji_get_config());
        $response->status = 304;
        $response->headers['Etag'] = '"'.$etag.'"';
        $response->headers['Last-Modified'] = $last_modified;
        $response->headers['Cache-Control'] = 'max-age='.$meta->max_age.', must-revalidate';
        $response->headers['Expires'] = date('r',$expires);
		$response->headers['Pragma'] = '';
        
        // Etag based caching first 
        if($if_none_match = @$request->headers['If-None-Match']){
            // Remove gzip part of Etag if present
            $if_none_match = trim(preg_replace('/\-gzip$/','',$if_none_match),'"');
            if($etag == $if_none_match){
                return $response;
            }
        }
        
        // Then resort to dates
        if(isset($request->headers['If-Modified-Since']) && 
                    $last_modified == $request->headers['If-Modified-Since']){
            return $response;
        }
        
        /*
		 * Otherwise change response code to 200, set Content-Type to whatever
		 * this was originally served as set the body to the files contents
		 * and return
		 */
        $response->status = 200;
		$response->headers['Content-Type'] = $meta->content_type;
        $response->body = $cached_file_contents;
        return $response;
    }
// ----------------------------------------------------------------------    
    static function after($config, &$response, &$request){
        
        // Only cache GET request 200 response with body and cache set to true
        if(!$config->on || !@$response->cache || 
                $response->status != 200 || !$response->body
				|| $request->method != 'GET'){
            return;
        }
        // Do nothing if not a logged in user (depends on config)
        if(self::$config->not_for_request_user && @$request->user){
            return;
        }
        // Get the file to cache as
        $cached_file = self::cacheFile($config, $request);
        // Write the contents to the file
        $path = pathinfo($cached_file,PATHINFO_DIRNAME);
        if(!is_dir($path)){
            mkdir($path,0777,true);
        }
        file_put_contents($cached_file, $response->body);
		
		// Store meta data - content type and max age
		$meta = new stdClass();
		$meta->content_type = $response->headers['Content-Type'];
		$meta->max_age = @$response->cache_max_age ? $response->cache_max_age : self::$config->max_age;
		file_put_contents($cached_file.'.meta', json_encode($meta));
		
		//JsConsole::log('Saving cached file: '.$cached_file);
        // Make sure this response is cached by the client
        $etag = md5($response->body);
        $last_modified = date('r',filemtime($cached_file));
        $response->headers['Etag'] = '"'.$etag.'"';
        $response->headers['Last-Modified'] = $last_modified;
        $response->headers['Cache-Control'] = 'max-age='.$meta->max_age.', must-revalidate';
        $response->headers['Expires'] = date('r',time()+$meta->max_age);
		$response->headers['Pragma'] = '';
    } 
// ----------------------------------------------------------------------    
    static function cacheFile($config, $request){
        // Find cache file
        $cached_file = $config->temp_dir.'cache'.$request->url_path;
		if($request->url_ext){
			$cached_file .= '.'.$request->url_ext;
		}
        // Add file name if directory
        if(!pathinfo($cached_file,PATHINFO_EXTENSION)){
            $cached_file .= 'index.html';
        }
        // Add md5 of query string if there is one
        if($request->url_query){
            $cached_file .= '.'.md5($request->url_query);
        }
        return $cached_file;
    }
// ----------------------------------------------------------------------
/**
 * Clear a page from the cache
 * @params str $path The absolute URL path to clear, all files below that 
 *                   will be deleted.
 * @params str $domain The domain to clear from, defaults to current
 * @returns int The number of files deleted
 */ 
    static function invalidate($path, $host=false){
		//JsConsole::log("Attempting to invalidate cached file: $path, $host");
        if(!self::$config){
            return false;
        }
        if(!$host){
            $cached = self::$config->temp_dir.'cache'.$path;
        } else {
            $cached = str_replace(self::$config->host,$host,self::$config->temp_dir).'cache'.$path;
        }
		//JsConsole::log("Looking for: $cached");
        // File or directory?
        if(is_dir($cached)){
            $pattern = $cached.'*.*'; 
        } else {
            $pattern = $cached;
        }
		//JsConsole::log('Globbing pattern:'.$pattern);
        // Unlink all files (doesn't matter about directories)
        $files = glob($pattern);
        if(!$files){
			//JsConsole::log('No files found for pattern:'.$pattern);
            return false;
        }
        foreach($files as $f){
			//JsConsole::log("Invalidating cached file: $f");
            unlink($f);
        }
        return count($files);
    }
}
