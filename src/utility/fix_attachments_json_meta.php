<?php

/*
 * There was an issue in pre-r1186 Mitaji that the Attachments plugin wasn't very
 * careful about writing the JSON meta data files. These files should contain an
 * array of objects with file ids, but sometimes it would end up with a nested
 * object structure. And if there was only one attachment it would just write an
 * object without an array which could lead to dangerous behaviour, including 
 * deleting files it shouldn't when calling Attachment::setMeta() with the
 * $delete_missing flag set to true.
 * 
 * This script will fix broken meta data files. It's idempotent so you can run it
 * as many times as you like and it won't break anything.
 * 
 * Call it like:
 * 
 * php fix_attachments_json_meta.php -p=/path/to/media
 */
$opts = getopt('p:');
$path = rtrim(trim($opts['p'],'='),'/').'/';
if(!$path){
    exit("Specify a path to the media directory with -p=/path/to/media\n");
}

$files = glob($path.'*/*/*.json');
if(!$files){
    exit("No files found\n");
}

foreach($files as $f){
    $data = json_decode(file_get_contents($f));
    $tmp = array();
    if(@$data->file_id){
        $tmp[] = $data;
    } else {
        foreach($data as $meta){
            $tmp[] = $meta;
        }
    }
    echo "Writing data to file $f:\n";
    echo(json_encode($tmp))."\n";
    file_put_contents($f,json_encode($tmp));
}
exit();
