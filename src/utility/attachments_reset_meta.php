<?php

/*
 * This will reset all meta data for all attachments. This is useful when you import
 * or copy and paste images directly into the media directoty. By default they are
 * not included in the meta data which has the odd consequence that you cannot
 * delete them using the setMeta method
 * 
 * Actually I think this will leave any existing meta data as it is, the point is it will
 * insert any files not mentioned in the data.
 */

include('mitaji-latest/plugins-available/Attachments.php');
include('mitaji-latest/helpers/AkInflector.php');
include('mitaji-latest/helpers/miArray.php');

// Path to config
$config_file = $_SERVER['argv'][1];
if(!$json = file_get_contents($config_file)){
    echo("Config $config_file could not be found\n");
    exit(1);
}
if(!is_object(json_decode($json))){
    echo("Config $config_file is not json\n");
    exit(1);
}

// Configure
$json = json_decode($json);
$config = $json->Attachments;
Attachments::before($config);

// Find resource media folders
$glob = $config->media_dir.'*/*';
$folders = glob($glob,GLOB_ONLYDIR);
if(!$folders){
    echo "No media directories found\n";
    exit();
}

// Process meta data
foreach($folders as $f){
    $finfo = pathinfo($f);
    $id = $finfo['filename'];
    $path_tokens = explode('/',$finfo['dirname']);
    $resource_name = array_pop($path_tokens);
    echo "$resource_name $id\n";
    // This actually creates a meta file and has one for each transformation so
    // need to work out what kind of transforms are present. Do this from config.
    foreach($config->transformations as $k=>$v){
        if(!glob($f.'/*'.$k.'*')){
            echo "Transformation '$k' DOES NOT apply to this directory\n";
            continue;
        }
        echo "Transformation '$k' DOES apply to this directory\n";
        // OK now we have a tranformation that matters, so get the meta file name from plugin
        $meta_file = Attachments::getMetaFile($resource_name,$id,$k);
        // Get file ids already in meta
        if(file_exists($meta_file)){
            $meta = json_decode(file_get_contents($meta_file));
            $x_file_ids = miArray::extractByKey($meta,'file_id');
        } else {
            $meta = array();
            $x_file_ids = array();
        }
        // Get all file ids in directory
        $files = glob($f.'/*'.$k.'*');
        $file_ids = array();
        foreach($files as $f){
            $file_tokens = explode('_',basename($f));
            $file_ids[] = $file_tokens[0];
        }
        $file_ids = array_unique($file_ids);
        foreach($file_ids as $file_id){
            // Skip file ids already mentioned in meta
            if(in_array($file_id,$x_file_ids)){
                continue;
            }
            // Add this file id to meta
            $meta_data = new stdClass();
            $meta_data->file_id = $file_id;
            $meta[] = $meta_data;
        }
        echo "Updating meta $meta_file\n";
        file_put_contents($meta_file,json_encode($meta));
    }
}
exit("\nDone\n");
