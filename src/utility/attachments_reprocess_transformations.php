<?php

/*
 * This script will reprocess all attachment transformations described in your
 * config file. Works in place so will overwrite any existing transfromed files. 
 * If your concerned about this back up your files first! You can always just
 * reprocess with your original settings.
 * 
 * Arguments:
 * 
 * -c Path to plugins.json config file to use containing Attachments
 *    config, required
 * -r Resource name (which will probably map to the name of the folder in your 
 *    media directory, but we''ll leave that to the plugin)
 * -s The file suffix - all files that are transformed must have a suffix
 * -i The id of a particular resource, optional
 */

// -------------------------------------------------------------------------
// Requires Attachments plug-in
require('../helpers/miMath.php');
require('../helpers/AkInflector.php');
require('../plugins-available/Attachments.php');

// -------------------------------------------------------------------------
// Get options
$opts = getopt('c:r:s:i:');
// -------------------------------------------------------------------------
// Get config
$config_file = trim($opts['c'],'=');
if(!$json = file_get_contents($config_file)){
	exit("Config $config_file could not be found\n");
}
$config = json_decode($json);
if(!is_object($config)){
	exit("Config $config_file could not be parsed as JSON\n");
}
$config = $config->Attachments;
if(!is_object($config)){
	exit("Config $config_file does not contain config for Attachments plugin\n");
}
if(!@$config->transformations){
	exit("Config $config_file has no Attachment transformations defined so not a lot of point continuing\n");
}
// -------------------------------------------------------------------------
// Get resource type
if(!@$opts['r']){
	exit("Please provide the resource type with -p {resource type}\n");
}
$resource_type = trim($opts['r'],'=');
// -------------------------------------------------------------------------
// Get suffix
if(!@$opts['s']){
	exit("Please provide the suffix with -s {suffix}\n");
}
$suffix = trim($opts['s'],'=');
// -------------------------------------------------------------------------
// Get id
if($id = @$opts['i']){
	$id = trim($opts['i'],'=');
}

// -------------------------------------------------------------------------
// Set the current working directory to wherever plugin config is to 
// ensure all paths resolved correctly
if(!chdir(pathinfo($config_file,PATHINFO_DIRNAME))){
	exit("Could not switch to plugins config directory");
}
// -------------------------------------------------------------------------
// Initialise Attachments config
Attachments::before($config);

// -------------------------------------------------------------------------
// Find all files of resource type with suffix
$glob = Attachments::$config->media_dir;
$glob .= $resource_type.'/';
if($id){
	$glob .= $id;
} else {
	$glob .= '*';
}
$glob .= '/*'.$suffix.'.*';
echo "Finding files in $glob\n";
$files = glob($glob);
$file_count = count($files);
if(!$file_count){
	echo "No files found\n";
	exit(1);
}
$i = 1;
// Re-put all the files
foreach($files as $f){
	$info = pathinfo($f);
	$p_tokens = explode('/',$info['dirname']);
	$id = array_pop($p_tokens);
	$f_tokens = explode('_',$info['filename']);
	$file_id = $f_tokens[0];
	print "Processing file $i of $file_count: $resource_type/$id/{$info['basename']}\n";
	Attachments::put($resource_type,$id,$f,$suffix,$file_id);
	$i++;
}

// -------------------------------------------------------------------------
exit("Processing complete!\n");
