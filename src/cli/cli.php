<?php

class cli {

	public $mitaji = false;
	private $opt_error = false;
// -------------------------------------------------------------------------
	function __construct($mitaji){
		if(php_sapi_name() != 'cli'){
			exit(1);
		}
		$this->mitaji = $mitaji;
	}
// -------------------------------------------------------------------------
	function getOptError(){
		return $this->opt_error;
	}
// -------------------------------------------------------------------------
/*
 * Gets just one option from the command line and will never throw an error.
 */
	static function getOpt($short_name, $long_name=null, $requires_value=false){ 
		$nn = array();
		if($short_name && $long_name){
			$nn[$short_name] = $long_name;
		}
		if($requires_value){			
			if($short_name){
				$use_short_name = trim($short_name,':').':';
			}
			if($long_name){
				$use_long_name = trim($long_name,'=').'=';
			}
		} else {
			$use_short_name = $short_name;
			$use_long_name = $long_name;
		}
		$options = cli::getOpts($use_short_name,array($use_long_name),$nn,false);
		if($nn){
			return @$options[$long_name];
		}
		return @$options[$short_name];
	}
// ------------------------------------------------------------------------
/**
 * Similar to the above but let's you get multiple options at once. 
 * @param string $so Short opts @see Console_Getopt
 * @param string $lo Long Opts @see Console_Getopt
 * @param array $nn Normalize names An array that maps short options to long 
 * option names. Will always return the array using long option names for keys.
 * The mapping should not include any dashes.
 * @param bool $trigger_error Whether to trigger an error if an option or it's
 * value is missing. If you suppress this to handle errors in your own way can
 * use getOpTError to see the error message.
 * printing an error and exiting.
 * @return mixed Array of options on success, false if called with $no_bad set
 * to false, otherwise will exit on a bad option.
 */
	static function getOpts($so, $lo=array(), $nn=array(), $trigger_error=true){
		$cg = new Console_Getopt(); 
		$argv = @$cg->readPHPArgv();
		// Make sure we got them (for non CLI binaries)
		if (!is_array($argv)) {
			trigger_error($argv->getMessage(),E_USER_ERROR);
		}
        $use_argv = array();
        foreach ($argv as $a) {
            if (substr($a, 0, 1) == '-') {
                $use_argv[] = $a;
            }
        }
		$options = @$cg->getOpt($use_argv, $so, $lo);
		// Check the options are valid
		if (!is_array($options)) {
            echo "Error parsing options:".$options->getMessage()."\n";
			// Display error?
			if($trigger_error){
				trigger_error($options->getMessage(), E_USER_ERROR);
			}
			// No? Then return empty array
			return array();
		}
        if(!$options){
            return array();
        }
		// Flatten the array
		$tmp = array();
		if($options[0]){
			foreach($options[0] as $opt){
				// If value is in array, but no value as doesn't have one, set value to true
				$tmp[trim($opt[0],'-')] = $opt[1] == null ? true : $opt[1];
			}
		}
		if($options[1]){
			foreach($options[1] as $opt){
				$tmp[$opt] = true;
			}
		}
		// Normalize names
		if($nn){
			foreach($nn as $short=>$long){
				if(array_key_exists($short,$tmp)){
					$val = $tmp[$short];
					unset($tmp[$short]);
					$tmp[$long] = $val;
				}
			}
		}
		return $tmp;
	}
// -------------------------------------------------------------------------
	function help($method='def'){
		
		$const = strtoupper('CLI_HELP_'.strtolower(
											str_replace('cli','',
												get_class($this))).
										'_'.$method);
		if(defined($const)){
			fputs(STDOUT, constant($const)."\n");
			return 0;
		}
		fputs(STDERR, "No help available\n");
		return 1;
	}
// -------------------------------------------------------------------------
}
