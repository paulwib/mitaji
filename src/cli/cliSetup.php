<?php

define('CLI_HELP_SETUP_DEF','Set up a Mitaji application');

class cliSetup extends cli {
	
// -------------------------------------------------------------------------
	function def($name, $db_driver=false){
		// Check doesn't already exist to avoid accidents
		if(is_dir($name)){
			trigger_error("$name already exists, delete if you want to set up again",E_USER_ERROR);
			return 1;
		}
		if(!is_writable(getcwd())){
			trigger_error(ERR_CLI_PATH_NOT_WRITABLE,E_USER_ERROR);
		}
		fputs(STDOUT,"Setting up $name in ".getcwd()."\n");
		// Get names
		$name_mod = AkInflector::modulize($name);		
		// Make root directory
		mkdir($name);		
		// Make other directories
		$dirs = array('cli',
		              'config',
		              'config/development',
					  'config/production',
					  'config/test',
					  'helpers',
					  'public',
					  'public/css',
					  'responders',
					  'messages',
					  'models',
					  'migrations',
					  'templates',
					  'templates/layout',
					  'test');
		foreach($dirs as $d){
			fputs(STDOUT,"Creating directory $d\n");
			mkdir($name.'/'.$d, 0755);
		}
		// General templates that take names for sprintf arguments
		$tp_in = 'templates/scaffold/';
		$templates = array(
		   'htaccess.txt'=>'/public/.htaccess',
		   'urls.json.txt'=>'/config/urls.json',
		   'app.json.txt'=>'/config/'.$name.'.json',
		   'mitaji.development.json.txt'=>'/config/development/mitaji.json',
		   'mitaji.test.json.txt'=>'/config/test/mitaji.json',
		   'mitaji.production.json.txt'=>'/config/production/mitaji.json',
		   'layout.php.txt'=>'/templates/layout/default.html.php',
		   'home.php.txt'=>'/templates/'.$name.'.html.php',
		   'home.responder.php.txt'=>'/responders/'.$name_mod.'Responder.php'
		   );
		 foreach($templates as $tpl=>$output){
			fputs(STDOUT,"Writing file $name$output\n");
		 	file_put_contents($name.$output,
				sprintf(file_get_contents($tp_in.$tpl, FILE_USE_INCLUDE_PATH),
						$name, $name_mod)
				);
		 }
		 // Index file which needs a full reference to this path so need to a few tricks for that
		 $cwd = getcwd();
		 $setup_path = dirname(__FILE__);
		 chdir($setup_path);
		 $mitaji_file = realpath('../').'/mitaji.php';
		 chdir($cwd);
		 fputs(STDOUT,"Writing file $name/public/index.php to include $mitaji_file\n");
		 file_put_contents($name.'/public/index.php',
				sprintf(file_get_contents($tp_in.'index.php.txt', FILE_USE_INCLUDE_PATH),
						$mitaji_file)
				);
		fputs(STDOUT,"Writing file $name/env.php\n");
		 file_put_contents($name.'/env.php',
		 					file_get_contents($tp_in.'env.php.txt', FILE_USE_INCLUDE_PATH));
		
		// Do database config
		if($db_driver){
			// Switch to app directory
			$cwd = getcwd();
			chdir($cwd.'/'.$name.'/');
			$this->dbconfig($db_driver);
			// Switch back to original directory
			chdir($cwd);
		}
		return 0;
	}
// -------------------------------------------------------------------------
/*
 * How do we do this out of context without the name? This is related to paths
 * as well, so I guess if you call this independently it needs to be from in the
 * correct path.
 * 
 * So I guess this assumes that the current working directory is always the top level
 * of the app, so in the case of the initial setup need to switch there.
 */ 
	function dbconfig($db_driver){
		if(!is_dir('config')){
			trigger_error('No config directory',E_USER_ERROR);
		}
		$path_tokens = preg_split('/\//',getcwd(),null, PREG_SPLIT_NO_EMPTY);
		$name = array_pop($path_tokens);
		$f = 'templates/scaffold/database.'.$db_driver.'.json.txt';
		if(!$tpl = file_get_contents($f,FILE_USE_INCLUDE_PATH)){
			trigger_error(sprintf(ERR_CLI_DATABASE_NOT_SUPPORTED,$db_driver),E_USER_ERROR);
			exit(1); 
		}
		foreach(array('development','production','test') as $env){
			fputs(STDOUT,"Writing file config/$env/database.json\n");
			file_put_contents('config/'.$env.'/database.json',
							sprintf($tpl,
								$name.'_'.$env,
								$env == 'production' ? 'true' : 'false')
								);
		}
		return 0;
	}	
// -------------------------------------------------------------------------
}
