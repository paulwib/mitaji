<?php

define('CLI_HELP_PATH_DEF','Print the current include path for this environment');

class cliPath extends cli {
// -------------------------------------------------------------------------
	function def(){
		fputs(STDOUT, get_include_path()."\n");
		return 0;
	}
	
// -------------------------------------------------------------------------
}
