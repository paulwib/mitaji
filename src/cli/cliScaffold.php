<?php

include_once('cli/cliDb.php');

class cliScaffold extends cli {

// ---------------------------------------------------------------------------	
	function def(){
		// Create an instance of the db cli helper
		$db = new cliDb($this->mitaji);
		
		$args = func_get_args();
		$model = AkInflector::modulize(AkInflector::singularize($args[0]));
		// Get names
		$model_pl = AkInflector::pluralize($model);
		$table = AkInflector::tableize($model);
		$model_title = AkInflector::titleize($model);
		$model_title_pl = AkInflector::pluralize($model_title);
		$model_const = strtoupper(AkInflector::underscore($model));
		
		// Fields
		$flds = array_slice($args,1);
		
		// Add a primary key id as first field if no id primary key defined
		if(!preg_grep('/^([a-z_]+)?id:primary_key/',$flds)){
			array_unshift($flds,'id:primary_key');
		}
		
		// Create table
		$create = $db->addTable($table, $flds);
		if($create > 0){
			return $create;
		}
		// Normalize fields 
		$n_flds = array();
		foreach($flds as $fld){
			@list($name,$type,$required,$default) = explode(':',$fld);
			if(!$type){
				$type = 'string';
			}
			$n_flds[$name] = $db->parseCol($fld);
		}
		// Where input templates are kept (can be a local path so can override defaults)
		$tp_in = 'templates/scaffold/';
		
		// Model class
		$tpl = file_get_contents($tp_in.'model.php.txt',FILE_USE_INCLUDE_PATH);
		$f = 'models/'.$model.'.php';
		file_put_contents($f,sprintf($tpl,$model));
		fputs(STDOUT,"Writing file $f\n");
		
		// Responder class
		$responder = $model.'Responder';
		$tpl = file_get_contents($tp_in.'responder.php.txt',FILE_USE_INCLUDE_PATH);
		$f = 'responders/'.$responder.'.php';
		file_put_contents($f,sprintf($tpl,$model,$model_pl,$table,$model_const));
		fputs(STDOUT,"Writing file $f\n");
		
		// Helper class
		$helper = $model.'Helper';
		$tpl = file_get_contents($tp_in.'helper.php.txt',FILE_USE_INCLUDE_PATH);
		$f = 'helpers/'.$helper.'.php';
		file_put_contents($f,sprintf($tpl,$model,$model_pl,$table,$model_const));
		fputs(STDOUT,"Writing file $f\n");
		
		// Message constants
		$tpl = file_get_contents($tp_in.'messages.php.txt',FILE_USE_INCLUDE_PATH);
		$f = 'messages/'.$model.'.php';
		file_put_contents($f,sprintf($tpl,$model_title,$model_title_pl,$model_const));
		fputs(STDOUT,"Writing file $f\n");
		
		// Add URLs
		$urls = $this->mitaji->config->urls;
		if(!@$urls->$model){
			$urls->$model = array('create'=>'/'.$table.'/-/',
								  'edit'=>'/'.$table.'/{id}/',
								  'list'=>'/'.$table.'/');
			// Encode as JSON
			$urls = json_encode($urls);
			// Get rid of back slash escaping (it's not needed, is that a bug in json_encode?)
			$urls = stripslashes($urls);
			// Beautify
			include('helpers/beautify.php');
			$urls = js_beautify($urls);	
			$f = 'config/urls.json';		
			file_put_contents($f,$urls);
			fputs(STDOUT,"Appending urls to $f\n");
		}
		// Output template path
		$tp_out = 'templates/'.$table.'/';
		if(!is_dir($tp_out)){
			mkdir($tp_out);
			fputs(STDOUT,"Creating directory $tp_out\n");
		}
		
		// Read in list template fragments
		$tpl_edit_link = file_get_contents($tp_in.'list_edit_link.php.txt',
											FILE_USE_INCLUDE_PATH); 
		$tpl_coll_val = file_get_contents($tp_in.'list_value.php.txt',
											FILE_USE_INCLUDE_PATH);
		$tpl_coll_vals_wrapper = file_get_contents($tp_in.'list_values_wrapper.php.txt',
											FILE_USE_INCLUDE_PATH);
		$tpl_coll_header = file_get_contents($tp_in.'list_header.php.txt',
											FILE_USE_INCLUDE_PATH);
		$tpl_coll = file_get_contents($tp_in.'list.php.txt',
											FILE_USE_INCLUDE_PATH);
		// Write
		$row_values = sprintf($tpl_edit_link,$model);
		$row_header = '';
		foreach($n_flds as $fld_name=>$fld){
			$row_values .= sprintf($tpl_coll_val,$model,$fld_name);
			$row_header .= sprintf($tpl_coll_header,AkInflector::titleize($fld_name),$fld_name);
		}
		$row_values = sprintf($tpl_coll_vals_wrapper,rtrim($row_values));
		$content = sprintf($tpl_coll,$model,$model_pl,$model_const,rtrim($row_header),$row_values);
		$f = $tp_out.'list.html.php';
		file_put_contents($f,$content);
		fputs(STDOUT,"Writing file $f\n");
		
		// Form template
		$html_flds = '';
		$tpl_cache = array();
		foreach($n_flds as $fld_name=>$fld){
			// Primary keys, ids and timestamps do not appear form
			if(in_array($fld['type'],array('id','primary_key')) || 
			in_array($fld_name,array('created_on','updated_on','creator_id','updater_id'))){
				continue;
			}
			$tpl = 'form_'.$fld['type'].'.php.txt';
			if(!@$tpl_cache[$tpl]){
				if(!$content = file_get_contents($tp_in.$tpl,FILE_USE_INCLUDE_PATH)){
					trigger_error('No template for '.$fld['type'].' ('.$tp_in.$tpl.')',E_USER_WARNING);
					continue;
				}
				$tpl_cache[$tpl] = $content;
			}
			$html_flds .= sprintf($tpl_cache[$tpl],
									$model,
									$fld_name,
									($fld['required'] ? ' required' : ''));
		}
		$tpl_form = file_get_contents($tp_in.'form.php.txt',FILE_USE_INCLUDE_PATH);
		if(!$tpl_form){
			trigger_error('form.php.txt not found',E_USER_ERROR);
		}
		$content = sprintf($tpl_form,$model,$model_const,$html_flds);
		$f = $tp_out.'form.html.php';
		file_put_contents($f,$content);
		fputs(STDOUT,"Writing file $f\n");
		return 0;
	}

// -------------------------------------------------------------------------
}
