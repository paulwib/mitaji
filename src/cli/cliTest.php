<?php

include('simpletest/unit_tester.php');
define('CLI_HELP_TEST_DEF','Run a test');

class cliTest extends cli {
// -------------------------------------------------------------------------
	function def($test=false){
		if(!$test){
			return $this->listTests();
		}
		if('production' == @$_ENV['MITAJI']){
			trigger_error('Cannot run tests on production system',E_USER_ERROR);
		}
		fputs(STDOUT,"Running Test...\n");
		$t = microtime(true);
		$ob = new $test($this->mitaji);
		$res = $ob->run(new TextReporter()) ? 0 : 1;
		$qc = miDb::queryCount();
		$qt = miDb::queryTime();
		if($qc){
			fputs(STDOUT, "$qc queries in $qt ms\n");
		}
		$d = number_format((microtime(true)-$t),4,'.','');
		fputs(STDOUT, $d." seconds total\n");
		return $res;
	}
// -------------------------------------------------------------------------
	function listTests(){
		$tests = $this->mitaji->globPath('test/*Test*.php');
		if(!$tests){
			fputs(STDOUT, "No tests available in ".getcwd()."\n");
			return 0;
		}
		fputs(STDOUT, "The followings tests are available:\n(mitaji test <testName> will run the test)\n");
		foreach($tests as $i=>$test){
			fputs(STDOUT,($i+1).") ".basename($test,'.php')."\n");
		}
		return 0;
	}	
// -------------------------------------------------------------------------
}
