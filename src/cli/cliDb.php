<?php

class cliDb extends cli {

	private $migration_log_file = false;

    // -------------------------------------------------------------------------
    function help($method=false) {
        fputs(STDOUT, "Database methods, see src/cli/cliDb.php ;)\n");
        return 0;
    }
    // -------------------------------------------------------------------------
    function create(){
        // Connect without selecting database
        miDb::autoConnect(true);
        if(miDb::getResults(sprintf(MIDB_CREATE,
            miDb::$config->database,
            miDb::$config->encoding))){
                fputs(STDOUT, "DB ".miDb::$config->database." has been created\n");
                return 0;
            }
        trigger_error('Db could not be created',E_USER_ERROR);
        return 1;
    }
    //----------------------------------------------------------------------------
    function parseCol($col){
        miDb::autoConnect();
        @list($name,$type,$required,$default) = explode(':',$col);
        if(!$type){
            $type = 'string';
        }
        $len = null;
        if(strstr($type,'[')){
            preg_match('/^([^\[]+)\[([0-9]+)\]/',$type,$matches);
            $type = $matches[1];
            $len = $matches[2];
        }

        if(!$sql_type = miDb::type2sql($type)){
            trigger_error("Col type $type not understood",E_USER_ERROR);
            return false;
        }
        if($len){
            $sql_type = preg_replace('/\([0-9]*\)/',"($len)",$sql_type);
        }
        $sql = "`$name` $sql_type";
        if($type != 'primary_key'){
            $sql .= (@$required ? ' NOT NULL' : ' NULL');
            // If required (and not text) you can specify a default value
            if($type != 'text' && @$required && !is_null($default)){
                $sql .= " Default '$default' ";
            }
            $primary_key = false;
        } else {
            $primary_key = true;
        }
        // If named id then presumed to be an auto increment
        if($name === 'id'){
            $sql .= ' auto_increment';
        }
        if(in_array($name,array('created_on','updated_on')) ||
            in_array($type,array('id'))){
                $index = true;
            } else {
                $index = false;
            }
        return array('name'=>$name,'type'=>$type,'required'=>$required,
            'sql'=>$sql,'index'=>$index,'primary_key'=>$primary_key);
    }
    //----------------------------------------------------------------------------
    function addTable($table, $flds){
        miDb::autoConnect();
        if ($this->mitaji->globPath('migrations/*_add_table_' . $table . '.php')) {
            trigger_error(sprintf(MIDB_ERR_MIGRATION_EXIST, 'add table', $table), E_USER_ERROR);
        }
        if (miDb::tableExists($table)) {
            trigger_error(sprintf(MIDB_ERR_ADD_TABLE_EXIST, $table), E_USER_ERROR);
        }
        $flds_sql = array();
        $primary_keys = array();
        $indexes = array();

        // Parse into columns and sort primary and index keys
        foreach ($flds as $fld) {
            $col = $this->parseCol($fld);
            $flds_sql[] = $col['sql'];
            if ($col['primary_key']) {
                $primary_keys[] = $col['name'];
            }
            if ($col['index']) {
                $indexes[] = $col['name'];
            }
        }
        $def = implode(",\n", $flds_sql);
        // Add primary keys
        if ($primary_keys) {
            $def .= ",\nPRIMARY KEY(`" . implode("`,\n`", $primary_keys) . '`)';
        }
        // Add indexes
        if ($indexes) {
            foreach ($indexes as $index) {
                $def .= ",\nINDEX(`" . $index . '`)';
            }
        }
        // Migrations SQL
        $up = sprintf(MIDB_ADD_TABLE, $table, $def);
        $down = sprintf(MIDB_DROP_TABLE, $table);

        // Create migration file
        $id = date('YmdHis') . '_add_table_' . $table;
        $f = 'migrations/' . $id . '.php';
        file_put_contents($f, sprintf(MIDB_MIGRATE_PHP, $id, $up, $down));
        return 0;
    }
    //----------------------------------------------------------------------------
    function addCol($table,$col,$after=false){
        miDb::autoConnect();
        $table = AkInflector::tableize($table);
        $col = $this->parseCol($col);
        if ($this->migrationExists('*_add_col_'.$table.'_'.$col['name'])) {
            trigger_error(sprintf(MIDB_ERR_MIGRATION_EXIST, 'add column', $table . '.' . $col['name']), E_USER_ERROR);
        }
        if (!miDb::tableExists($table)) {
            trigger_error(sprintf(MIDB_ERR_TABLE_MISSING, $table), E_USER_ERROR);
        }
        if (miDb::colInfo($table,$col['name'])) {
            trigger_error(sprintf(MIDB_ERR_COL_EXIST, 'add', $table . '.' . $col['name']), E_USER_ERROR);
        }
        if (@$after && !miDb::colInfo($table,$after)) {
            trigger_error(sprintf(MIDB_ERR_COL_NOT_EXIST, 'add after', $table . '.' . $after), E_USER_ERROR);
        }
        if ($after) {
            $up = sprintf(MIDB_ADD_COLUMN_AFTER, $table, $col['sql'], $after);
        } else {
            $up = sprintf(MIDB_ADD_COLUMN, $table, $col['sql']);
        }
        $down = sprintf(MIDB_DROP_COLUMN, $table, $col['name']);
        // If an id then add an index
        if ($col['index']) {
            $up .= ";\n" . sprintf(MIDB_ADD_INDEX, $table, $col['name']);
            $down .= ";\n" . sprintf(MIDB_DROP_INDEX, $table, $col['name']);
        }

        // Create migration file
        $id = date('YmdHis') . '_add_col_' . $table . '_' . $col['name'];
        $this->saveMigration($id, sprintf(MIDB_MIGRATE_PHP, $id, $up, $down));
        return 0;
    }
    //----------------------------------------------------------------------------
    function alterCol($table,$old_col,$new_col){
        miDb::autoConnect();
        $table = AkInflector::tableize($table);
        $new_col = $this->parseCol($new_col);

        if(!miDb::tableExists($table)){
            trigger_error(sprintf(MIDB_ERR_TABLE_MISSING,$table),E_USER_ERROR);
        }
        if(!$old_col_info = miDb::colInfo($table,$old_col)){
            trigger_error(sprintf(MIDB_ERR_COL_EXIST,'alter',$table.'.'.$old_col),E_USER_ERROR);
        }

        $up = sprintf(MIDB_ALTER_COLUMN,$table,$old_col,$new_col['sql']);
        $down = sprintf(MIDB_ALTER_COLUMN,$table,$new_col['name'],$old_col_info['sql_create']);

        // Create migration file
        $id = date('YmdHis') . '_alter_col_' . $table . '_' . $old_col;
        $this->saveMigration($id, sprintf(MIDB_MIGRATE_PHP, $id, $up, $down));
        return 0;
    }
    //----------------------------------------------------------------------------
    function dropTable($table){
        miDb::autoConnect();
        $table = AkInflector::tableize($table);
        if($this->migrationExists('*_drop_table_'.$table)){
            trigger_error(sprintf(MIDB_ERR_MIGRATION_EXIST,'drop table',$table),E_USER_ERROR);
        }
        if(!miDb::tableExists($table)){
            trigger_error(sprintf(MIDB_ERR_TABLE_MISSING, $table),E_USER_ERROR);
        }
        $up = sprintf(MIDB_DROP_TABLE, $table);
        // @todo This doesn't include table definition so pretty useless, just dumps data
        $down = miDb::dumpTableData($table);
        $id = date('YmdHis') . '_drop_table_' . $table;
        $this->saveMigration($id, sprintf(MIDB_MIGRATE_PHP, $id, $up, $down));
        return 0;
    }
    //----------------------------------------------------------------------------
    function dropCol($table,$col){
        miDb::autoConnect();
        $table = AkInflector::tableize($table);
        if($this->migrationExists('*_drop_col_'.$table.'_'.$col)){
            trigger_error(sprintf(MIDB_ERR_MIGRATION_EXIST,'drop column',$table.'.'.$col),E_USER_ERROR);
        }
        if(!miDb::tableExists($table)){
            trigger_error(sprintf(MIDB_ERR_TABLE_MISSING,$table),E_USER_ERROR);
        }
        if(!$col_info = miDb::colInfo($table,$col)){
            trigger_error(sprintf(MIDB_ERR_COL_NOT_EXIST,'drop',$table.'.'.$col),E_USER_ERROR);
        }
        $table_info = miDb::tableInfo($table);
        foreach($table_info as $k=>$info){
            if($k == $col && $last_col){
                $after = $last_col;
                break;
            }
            $last_col = $k;
        }
        // Add col query
        $up = sprintf(MIDB_DROP_COLUMN,$table,$col);
        $sql = $col.' '.$col_info['sql_create'];
        // Restore col query
        $down = sprintf(MIDB_ADD_COLUMN,$table,$sql,$after);
        // Try and save data to restore if an id column
        // @todo Work out primary key from table info rather than just using id
        if($table_info['id']){
            if($restore_data = miDb::getResults("SELECT id,$col FROM $table;")){
                $tmp = array();
                foreach($restore_data as $val){
                    $tmp[] = "UPDATE $table SET ".$col.'='.
                        miDb::quote($val[$col])." WHERE id=".$val['id'];
                }
            }
            $down .= "\n".implode(";\n",$tmp).';';
        } else {
            fputs(STDOUT,"WARNING: No id column in $table so cannot restore ".
                " data after migration run - back up your data before migrating!");
        }
        $id = date('YmdHis') . '_drop_col_' . $table . '_' . $col;
        $this->saveMigration($id, sprintf(MIDB_MIGRATE_PHP, $id, $up, $down));
        return 0;
    }
    //----------------------------------------------------------------------------
    function addIndex($table,$col){
        miDb::autoConnect();
        $table = AkInflector::tableize($table);
        if($this->migrationExists('*_add_index_'.$table.'_'.$col)){
            trigger_error(sprintf(MIDB_ERR_MIGRATION_EXIST,'add index',$table.'.'.$col),E_USER_ERROR);
        }
        if(!miDb::tableExists($table)){
            trigger_error(sprintf(MIDB_ERR_TABLE_MISSING,$table),E_USER_ERROR);
        }
        if(!$col_info = miDb::colInfo($table,$col)){
            trigger_error(sprintf(MIDB_ERR_COL_NOT_EXIST,'add index',$table.'.'.$col),E_USER_ERROR);
        }
        $up = sprintf(MIDB_ADD_INDEX,$table,$col);
        $down = sprintf(MIDB_DROP_INDEX,$table,$col);
        $id = date('YmdHis') . '_add_index_' . $table . '_' . $col;
        $this->saveMigration($id, sprintf(MIDB_MIGRATE_PHP, $id, $up, $down));
        return 0;
    }
    //----------------------------------------------------------------------------
    function dropIndex($table,$col){
        miDb::autoConnect();
        $table = AkInflector::tableize($table);
        if($this->migrationExists('*_drop_index_'.$table.'_'.$col)){
            trigger_error(sprintf(MIDB_ERR_MIGRATION_EXIST,'drop index',$table.'.'.$col),E_USER_ERROR);
        }
        if(!miDb::tableExists($table)){
            trigger_error(sprintf(MIDB_ERR_TABLE_MISSING,$table),E_USER_ERROR);
        }
        if(!$col_info = miDb::colInfo($table,$col)){
            trigger_error(sprintf(MIDB_ERR_COL_NOT_EXIST,'drop index',$table.'.'.$col),E_USER_ERROR);
        }

        $up = sprintf(MIDB_DROP_INDEX,$table,$col);
        $down = sprintf(MIDB_ADD_INDEX,$table,$col);
        $id = date('YmdHis') . '_drop_index_' . $table . '_' . $col;
        $this->saveMigration($id, sprintf(MIDB_MIGRATE_PHP, $id, $up, $down));
        return 0;
    }
    //----------------------------------------------------------------------------
    function migrationExists($pattern) {
        $matches = glob(getcwd() . '/migrations/' . $pattern . '.php');
        return count($matches) ? true : false;
    }
    //----------------------------------------------------------------------------
    function saveMigration($id, $content) {
        file_put_contents(getcwd() . '/migrations/' . $id . '.php', $content);
    }
    //----------------------------------------------------------------------------
    function migrate(){
        miDb::autoConnect();
        fputs(STDOUT, "Running migration on database: " . miDb::getDatabase() . "\n");
        // Find migrations, either PHP or SQL
        $php_files = glob(getcwd() . '/migrations/*.php');
        $sql_files = glob(getcwd() . '/migrations/*.sql');
        $files = array_merge($php_files, $sql_files);
        if (!$files) {
            trigger_error(MIDB_ERR_NO_MIGRATIONS,E_USER_ERROR);
        }
        // Store just base names for comparison with those already run
        $basename_files = array();
        foreach ($files as $f) {
            $basename_files[] = basename($f);
        }
        // See what migrations have already been run from the log
        $migrate_log = $this->migrateLog();
        // Skip those already run
        if ($migrate_log) {
            $migrations_to_run = array_diff($basename_files, $migrate_log);
            if(!$migrations_to_run){
                fputs(STDOUT, "Nothing to do, all migrations up to date\n");
                exit(0);
            }
        }
        else {
            $migrations_to_run = $basename_files;
        }
        sort($migrations_to_run);
        $run = array();
        // Clear cache
        miDb::deleteCachedTableInfo();
        foreach($files as $f){
            if (!in_array(basename($f), $migrations_to_run)) {
                continue;
            }
            $bname = basename($f);
            $id = basename($f, '.php');
            // Standard PHP migration
            if ('php' == pathinfo($f, PATHINFO_EXTENSION)) {
                $up = false;
                require $f;
                // Old style migrations run a query in $up
                if ($up) {
                    miDb::getResults($up);
                } else {
                    // new style has classes with apply() method
                    $c = 'Migration_' . $id;
                    if (!class_exists($c)) {
                        trigger_error("Migration class $c does not exist", E_USER_ERROR);
                        return false;
                    }
                    // Instantiate migration class and call up method
                    $m = new $c();
                    if (!$m->apply()) {
                        trigger_error(sprintf(MIDB_ERR_MIGRATE_FAILED,$f),E_USER_ERROR);
                        return false;
                    }
                }
                $run[] = date('YmdHis') . ':' . $bname;
                fputs(STDOUT, "PHP Migration run: $bname\n");

                // Load a SQL file directly
            } else if('sql' == pathinfo($f, PATHINFO_EXTENSION)){
                if (!miDb::load($f)) {
                    trigger_error(sprintf(MIDB_ERR_MIGRATE_FAILED, $f), E_USER_ERROR);
                    return false;
                }
                $run[] = date('YmdHis') . ':' . $f;
                fputs(STDOUT, "SQL Migration run: $f\n");
            }
        }
        // Add the migration to the log
        file_put_contents($this->migrateLogFile(),implode("\n",$run)."\n",FILE_APPEND);
        // Clear cache again
        miDb::deleteCachedTableInfo();
        return 0;
    }
    //----------------------------------------------------------------------------
    function unmigrate(){
        miDb::autoConnect();
        miDb::deleteCachedTableInfo();
        $files = glob(getcwd() . '/migrations/*.php');
        if(!$files){
            trigger_error(MIDB_ERR_NO_MIGRATIONS,E_USER_ERROR);
        }
        rsort($files);
        $f = $files[0];
        // If this migration has been run then do the down stuff
        $log = $this->migrateLog();
        if($log && in_array($f, $log)){
            $id = basename($f, '.php');
            require $f;
            $c = 'Migration_' . $id;
            if (!class_exists($c)) {
                trigger_error("Migration class $c does not exist", E_USER_ERROR);
                return false;
            }
            // Instantiate migration class and call revert method
            $m = new $c();
            if (!$m->revert()) {
                trigger_error(sprintf(MIDB_ERR_MIGRATE_FAILED,$f),E_USER_ERROR);
                return false;
            }
        }
        // Unlink the migration
        unlink($f);
        // Remove the last entry from the log file
        $log = self::migrateLog(false);
        array_pop($log);
        file_put_contents(self::migrateLogFile(), implode("\n", $log)."\n");
        // Output and return
        fputs(STDOUT, "Unmigrated: $f\n");
        miDb::deleteCachedTableInfo();
        return 0;
    }
    //----------------------------------------------------------------------------
    function migrateLogFile(){
		if ($this->migration_log_file) {
			return $this->migration_log_file;
		}
        miDb::autoConnect();
		if (isset($_ENV['MITAJI_MIGRATE_LOG_DIR'])) {
			$path = rtrim($_ENV['MITAJI_MIGRATE_LOG_DIR'], '/');
		} else {
			$path = getcwd() . '/migrations';
		}
		$file = $path . '/migrations_run_'.miDb::getDatabase().'.log';
		$this->migration_log_file = $file;

        fputs(STDOUT, "Looking for migration log: $file" . "\n");

		return $file;
    }
    //----------------------------------------------------------------------------
    function migrateLog($files_only=true){
        miDb::autoConnect();
        $log_file = $this->migrateLogFile();
        if(!is_file($log_file)){
            return false;
        }
        $lines = file($log_file,FILE_IGNORE_NEW_LINES);
        if(!$files_only){
            return $lines;
        }
        $tmp = array();
        foreach($lines as $line){
            // Skip blank lines
            if(!trim($line)){
                continue;
            }
            list($d,$f) = explode(':', $line);
            $tmp[] = basename(trim($f));
        }
        return $tmp;
    }
    //----------------------------------------------------------------------------
    function load($f){
        return miDb::load($f);
    }
    //----------------------------------------------------------------------------
    function dump(){
        return miDb::dump();
    }
    //----------------------------------------------------------------------------
    function dumpTableData($tbl){
        return miDb::dumpTableData($tbl);
    }
    //----------------------------------------------------------------------------
    function dumpTableDataWhere($tbl, $where){
        return miDb::dumpTableDataWhere($tbl, $where);
    }
    //----------------------------------------------------------------------------
    function exportFixtures($tbl, $key=false) {
        return miFixture::export($tbl, $key);
    }
    //----------------------------------------------------------------------------
    function loadFixtures($f, $suspend_formulas=true, $suspend_indexing=true) {
        if ($suspend_indexing && class_exists('miSphinxResource')) {
            miSphinxResource::suspendIndexing(true);
        }
        if ($suspend_formulas && class_exists('miResource')) {
            miResource::suspendFormulas(true);
        }
        miFixture::load($f);
    }
    //----------------------------------------------------------------------------
    function resetCachedFixtures($f) {
        miFixture::resetCached($f);
    }
    //----------------------------------------------------------------------------
    function reset() {
        fputs(STDOUT, "Resetting database: " . miDb::getDatabase() . "\n");
        $args = func_get_args();
        if ($args) {
            $tables = $args;
        } else {
            $tables = array_keys(miDb::allTables());
        }
        foreach ($tables as $table) {
            $c = miDb::getVar("SELECT COUNT(*) FROM $table");
            if ('0' == $c) {
                fputs(STDOUT, "$table is empty                            \r");
                continue;
            }
            fputs(STDOUT, "$table reset, deleting $c rows                 \r");
            miDb::getResults("TRUNCATE $table");
            miDb::getResults("ALTER TABLE $table AUTO_INCREMENT = 1");
        }
        fputs(STDOUT, "\rReset completed                                  \n");
    }
    //----------------------------------------------------------------------------
}
