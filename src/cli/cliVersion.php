<?php

define('CLI_HELP_VERSION_DEF','Print the current Mitaji version');

class cliVersion extends cli {
// -------------------------------------------------------------------------
	function def(){
		fputs(STDOUT, $this->mitaji->version()."\n");
		return 0;
	}
	
// -------------------------------------------------------------------------
}
