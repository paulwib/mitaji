<?php

// --------------------------------------------------------------------
/**
 * The main Mitaji class
 */
class Mitaji {

    public  $config = false,
            $doc_root = false,
            $rel_root = false,
            $cwd = false,
            $method = false,
            $uri = false,
            $host = false,
            $scheme = 'http',
            $headers = array(),
            $get_vars = array(),
            $post_vars = array(),
            $input = array(),
            $uploads = array(),
            $location = false,
            $client_ip = false,
            $user = false,
            $url_path = false,
            $url_path_tokens = false,
            $url_query = false,
            $url_ext = false,
            $url_fragment = false,
            $url_vars = array(),
            $url_tpl,
            $url_tpl_name,
            $ajax = false,
            $raw_request_body = false,
            $end_slash_added = false;

    private $__errors = array();
    private static $__instance = false;

// ------------------------------------------------------------------------
    function __construct($base=false){
        // Singleton so trigger error if created more than once
        if(Mitaji::$__instance){
            $this->internalerror();
        }
        Mitaji::$__instance = $this;

        // Add base to include path, used for templates, config and other non-PHP files
        if ($base) {
            set_include_path($base . PATH_SEPARATOR . get_include_path());
        }

        // Parse config
        if(!$this->config = $this->parseConfig()){
            $this->internalerror(ERR_EMPTY_CONFIG);
            return;
        }
        // Set timezone based on config as early as possible
        $timezone = isset($this->config->mitaji->timezone) ?
                        $this->config->mitaji->timezone : 'Europe/London';
        date_default_timezone_set($timezone);
    }
// --------------------------------------------------------------------
// General methods
// --------------------------------------------------------------------
    function version(){
        return $this->config->mitaji->app_version;
    }
// --------------------------------------------------------------------
    function getTempDir(){
        $tmp_dir = $_ENV['TMPDIR'] . '/' .
                    $this->config->mitaji->app_name . '/' .
                    $this->config->mitaji->app_version . '/' .
                    $_ENV['MITAJI'] . '/';
        if (!file_exists($tmp_dir) && !mkdir($tmp_dir, 0777, true)) {
            trigger_error('No temp directory available ' . $tmp_dir,
                E_USER_ERROR);
            return false;
        }
        @chmod($tmp_dir, 0777);
        return $tmp_dir;
    }
// ------------------------------------------------------------------------
/*
 * Will search all directories in path for files matching glob pattern and
 * return as a flat array. So for example to find all JSON config files:
 * $mitaji->globPath('config/*.json')
 */
    function globPath($pattern){
        // Initialize config path
        $path = explode(PATH_SEPARATOR, get_include_path());
        $files = array();
        foreach($path as $p) {
            // Break when get to default path that is not part of app
            if($p == '.') {
                break;
            }
            $p = rtrim($p, '/').'/'.$pattern;
            $files = array_merge(glob($p),$files);
        }
        return $files;
    }
// ------------------------------------------------------------------------
    function parseConfig(){

        // Get main config
        $files = $this->globPath('config/*.json');
        // Get environment config
        if (@$_ENV['MITAJI']) {
            $env_files = $this->globPath('config/'.$_ENV['MITAJI'].'/*.json');
            $files = array_merge($files,$env_files);
        }
        // Parse files
        $config = array();
        foreach($files as $f) {
            $namespace = basename($f,'.json');
            $raw_data = file_get_contents($f);
            $data = json_decode($raw_data);
            if (!is_object($data) && !is_array($data)) {
                $this->internalerror(sprintf(ERR_CONFIG_NOT_JSON,$f));
            }
            // Add the data
            if (is_array($data)) {
                // Arrays overwrite previous completely
                $config[$namespace] = $data;

            } else if (is_object($data)) {
                if(!array_key_exists($namespace,$config)){
                    $config[$namespace] = new stdClass();
                }
                // Object props overwite previous
                foreach($data as $k => $v) {
                    $config[$namespace]->$k = $v;
                }
            }
        }
        // If found something convert to an object for easy access
        if($config){
            return (object)$config;
        }
        return false;
    }
// --------------------------------------------------------------------
// HTTP methods
// ------------------------------------------------------------------------
/**
 * Recursively normalize an array of variables as you might find in
 * $_GET or $_POST. It does several things: strip slashes, trim whitespace,
 * normalize the kind of names you get with image inputs like 'image_x' to
 * be just keyed 'image' (whilst retainining originals for compatibility) and
 * finally coverts value 'true' and 'false' to boolean tru and false.
 *
 * @todo Not really sure why this is here. If you need some sanitizing or
 * nomrlaizing behaviour it should really go in a plugin.
 *
 * @param array $a                      The array of variables
 * @param bool $strip_tags_whitelist    A list of variables that do not need tags
 *                                      stripped. If this is false all will have
 *                                      tags stripped. If an array will look up
 *                                      the field. To whitelist nested POST vars
 *                                      like foo['bar']['baz'] delimit with double
 *                                      unbderscores like foo__bar__baz
 * @return array                        The cleaned, normlaized variables
 */
    function parseRequestVars($a, $strip_tags_whitelist=false, $nested=''){
        $tmp = array();
        if(!count($a)) return false;
        foreach($a as $k=>$v){
            if(is_array($v)) {
                $tmp[$k] = $this->parseRequestVars($v, $strip_tags_whitelist, $nested . $k . '__');
            } else {
                // Store image input values without _x and _y
                if(preg_match('/^.*(\.x|\.y|_x|_y)$/',$k)){
                    $tmp[$k] = $strip_tags ? strip_tags($v) : $v;
                    $k = preg_replace('/(\.x|\.y|_x|_y)$/','',$k);
                }
                // Trim whitespace
                $v = trim($v);

                // Convert string booleans to literals
                if($v == 'true'){
                    $tmp[$k] = true;
                } else if($v == 'false'){
                    $tmp[$k] = false;
                } else {
                    $v = stripslashes($v);
                    // Whitelist is false so always strip tags
                    if (false == $strip_tags_whitelist) {
                        $v = strip_tags($v);
                    // Only strip tags for the fields *not* in whitelist
                    } elseif (!in_array($nested . $k, $strip_tags_whitelist)) {
                        $v = strip_tags($v);
                    }
                    $tmp[$k] = $v;
                }
            }
        }
        return $tmp;
    }
// ------------------------------------------------------------------------
/**
 * Parses info in the $_FILES array and normalizes so one two dimensional
 * array and also checks that it is an uploaded file before continuing.
 *
 * @todo Like parseRequestvars this should probably go into a plugin as
 * people will have different preferences about this behaviour
 */
    function parseUploads($files){

        if(!$files) return false;
        $uploads = array();
        foreach($files as $k=>$f){
            if(is_array($f['tmp_name'])){
                $tmp = array();
                foreach($f['tmp_name'] as $i=>$v){
                    if($f['error'][$i] == UPLOAD_ERR_NO_FILE) { continue; }
                    if(!is_uploaded_file($f['tmp_name'][$i])){
                        continue;
                    }
                    $tmp[$i]['frm_name'] = preg_replace('/\[|\]/','',$k);
                    $tmp[$i]['tmp_name'] = $f['tmp_name'][$i];
                    $tmp[$i]['name'] = $f['name'][$i];
                    $tmp[$i]['type'] = $f['type'][$i];
                    $tmp[$i]['error'] = $f['error'][$i];
                    $tmp[$i]['size'] = $f['size'][$i];
                }
                $uploads[$k] = $tmp;
            } else {
                if($f['error'] == UPLOAD_ERR_NO_FILE) { continue; }
                if(!is_uploaded_file($f['tmp_name'])){
                    continue;
                }
                $f['frm_name'] = $k;
                $uploads[$k] = $f;
            }
        }
        return $uploads;
    }
// --------------------------------------------------------------------
    function runPlugins($hook,$response=false){

        if(!in_array($hook,array('before','after'))){
            return false;
        }

        if (!isset($this->config->plugins)) {
            return;
        }

        $plugins = (array)$this->config->plugins;

        // Reverse order of plugins for after
        if ($hook == 'after') {
            $plugins = array_reverse($plugins);
        }
        foreach($plugins as $plugin=>$plugin_config) {
            $fnc = array($plugin, $hook);
            if (!@is_callable($fnc)) {
                continue;
            }
            if ($hook == 'before') {
                $result = call_user_func_array($fnc, array($plugin_config, &$this));
                /*
                 * Before plug-ins can return a response before anything else
                 * happens, useful for maintenance, redirects, auth, cache etc.
                 * If this is any kind of object then it's assumed to be a
                 * response to output.
                 */
                if (is_object($result)) {
                    $this->output($result);
                }
            } else if ($hook == 'after') {
                call_user_func_array($fnc, array($plugin_config, &$response, &$this));
            }
        }
    }
// --------------------------------------------------------------------
    function httpRun() {

        // Set error handler
        set_error_handler(array($this,'errorHandler'));

        $this->uri = $_SERVER['REQUEST_URI'];
        // If URL is not parseable trigger a bad request error straight off
        if(!$url = parse_url($this->uri)){
            $this->badrequest();
        }
        // Request info
        $this->method = strtoupper($_SERVER['REQUEST_METHOD']);
        // POST method can be overriden by POSTing a REQUEST_METHOD_HACK
        if('POST' == $this->method
        && array_key_exists('REQUEST_METHOD_HACK',$_POST)
        && in_array($_POST['REQUEST_METHOD_HACK'],array('PUT','PATCH','DELETE'))){
            $this->method = $_POST['REQUEST_METHOD_HACK'];
        }
        $this->url_path = $url['path'];
        $this->url_path_tokens = preg_split('/\//',$url['path'],-1,
                                                        PREG_SPLIT_NO_EMPTY);
        $this->url_query = isset($url['query']) ? $url['query'] : false;
        $this->url_ext = pathinfo($this->url_path,PATHINFO_EXTENSION);
        // Remove extension from path if present
        if($this->url_ext){
            $this->url_path = preg_replace('/\.'.$this->url_ext.'$/','',$this->url_path);
        }
        /*
         * If no extension and no trailing slash on the URL path add one,
         * but keep  a note as mitaji may redirect to link with trailing
         * slash if matched and configured to do so @see mitaji_output
         */
        if($this->config->mitaji->add_trailing_slash_to_url_path &&
          !$this->url_ext && substr($this->url_path,-1) != '/'){
            $this->url_path .= '/';
            $this->end_slash_added = true;
        }
        $this->url_fragment = array_key_exists('fragment',$url) ?
                                $url['fragment'] : false;
        // Tidy up host name so lowercase, no port 80 and no trailing dot
        $host = strip_tags(array_key_exists('host',$url) ?
                                    $url['host'] : $_SERVER['HTTP_HOST']);
        $this->host = rtrim(strtolower(
                                preg_replace('/\:80$/', '', $host)
                            ),'.');
        // Get correct scheme
        $this->scheme = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ?
          'https' : 'http';
        // Store full location
        $this->location = $this->scheme.'://'.$this->host.$this->url_path;
        if($this->url_ext){
            $this->location .= '.'.$this->url_ext;
        }
        if($this->url_query){
            $this->location .= '?'.$this->url_query;
        }
        // Parse GET
        $this->get_vars = $this->parseRequestVars($_GET, false);
        if(is_array($this->get_vars)){
            $this->input = array_merge($this->input,$this->get_vars);
        }
        // Parse POST
        $this->post_vars = $this->parseRequestVars($_POST,@$this->config->mitaji->strip_tags_whitelist);
        if(is_array($this->post_vars)){
            $this->input = array_merge($this->input,$this->post_vars);
        }
        $this->uploads = $this->parseUploads($_FILES);
        $this->client_ip = isset($_SERVER['REMOTE_ADDR']) ?
                                strip_tags($_SERVER['REMOTE_ADDR']) : false;
        $this->raw_request_body = file_get_contents('php://input');

        // Copy headers to an array with their proper names as the keys
        $this->headers = array();
        foreach($_SERVER as $k=>$v){
            if(preg_match('/^HTTP_/',$k)){
                $h = str_replace(' ','-',ucwords(strtolower(str_replace('_',' ',substr($k,5)))));
                $this->headers[$h] = strip_tags($v);
            }
        }

        // Add Apache headers if available
        if(function_exists('apache_request_headers')){
            $this->headers = array_merge($this->headers,apache_request_headers());
        }

        // Have a guess if this is an Ajax request
        $ajax_header = 'X-Requested-With';
        $ajax_value = 'xmlhttprequest';
        if(strtolower(@$this->headers[$ajax_header]) == $ajax_value ||
           strtolower(@$this->headers[strtolower($ajax_header)]) == $ajax_value ||
           strtolower(@$this->input[$ajax_header]) == $ajax_value ||
           strtolower(@$this->input[strtolower($ajax_header)]) == $ajax_value){
               $this->ajax = true;
        }

        // Run before plugins
        $this->runPlugins('before');

        // No URLS? 404
        if(!$this->config->urls){
            $this->notfound();
            return false;
        }
        // Get match URI with and without extension
        $match_uri = $this->url_path;
        $match_uri_with_ext = $this->url_ext ? $match_uri.'.'.$this->url_ext : false;
        // If app not in document root then trim the local path off so URLs
        // relative to current working directory are understood.
        // Check the cwd has a common root to avoid breaking symlinked directories.
        $doc_root = rtrim($_SERVER['DOCUMENT_ROOT'],'/').'/';
        $cwd = rtrim(getcwd(),'/').'/';
        if($doc_root != $cwd && miStr::beginsWith($cwd, $doc_root)){
            $rel_root = str_replace($doc_root,'',$cwd);
            $match_uri = substr($match_uri,strlen($rel_root));
            if($match_uri_with_ext){
                $match_uri_with_ext = substr($match_uri_with_ext,strlen($rel_root));
            }
            $this->rel_root = $rel_root;
        }
        $this->doc_root = $doc_root;
        $this->cwd = $cwd;

        // Match a URL pattern to a responder
        $responder = false;
        foreach($this->config->urls as $model=>$url_tpls) {
            // Break if responder set on prior loop
            if($responder){
                break;
            }
            // Normalize to an array of URIs to test
            if(is_string($url_tpls)){
                $url_tpls = array($url_tpls);
            }

            foreach($url_tpls as $url_tpl_name=>$url_tpl) {
                // If template URI extension, compare to URL with extension
                if(pathinfo($url_tpl,PATHINFO_EXTENSION) && $match_uri_with_ext){
                    $match_to_uri = $match_uri_with_ext;
                // If not then just the path
                } else {
                    $match_to_uri = $match_uri;
                }
                // Check to see if it looks like a template URI
                if (strstr($url_tpl,'{')) {
                    $ob = new TemplateUri($url_tpl);
                    $matches = $ob->matchUri($match_to_uri);
                // Otherwise just a string equality comparison
                } else {
                    $matches = $url_tpl == $match_to_uri;
                }

                // Matched responder
                if ($matches) {
                    $this->url_tpl = $url_tpl;
                    $this->url_tpl_name = $url_tpl_name;
                    // URL vars added to the request
                    if(is_array($matches)){
                        $this->url_vars = $matches;
                    }
                    // Set responder
                    $responder = $model.'Responder';
                    break;
                }
            }
        }
        // No responder? 404
        if(!$responder){
            $this->notfound();
            return false;
        }

        // Create responder and pass $this
        $ob = new $responder($this);

        // HTTP method does not exist? Then method not allowed...
        if (!method_exists($ob, $this->method)) {
            $this->nomethod();
            return false;
        }

        // Invoke HTTP method passing reference to $this again
        // (so responder does not require constructor)
        if (!$response = $ob->{$this->method}($this)) {
            // No response? Then page not found...
            $this->notfound();
            return false;
        }

        // Output response
        return $this->output($response);
    }
// --------------------------------------------------------------------
    function output(&$response){

        // Default status to 200 if not set
        if(!isset($response->status)){
            $response->status = 200;
        }
        // Default content type if not set
        if(!isset($response->headers) || !is_array($response->headers) ||
        !array_key_exists('Content-Type', $response->headers)){
            $d = $this->config->mitaji;
            $response->headers['Content-Type'] = $d->default_mime_type.
                                    '; charset='.$d->default_charset;
        }
        /*
         * If an end slash was added for comparison, permanently redirect
         * to URL with trailing slash. This behaviour can be switched off in
         * config by setting redirect_no_trailing_slash to false.
         */
        if($response->status >= 200 && $response->status < 300 && $this->end_slash_added){
            $this->redirect($this->location,301);
        }

        // Run after plugins
        $this->runPlugins('after',$response);

        // Clear any existing output buffers
        while (@ob_end_clean());

        // Add overall time as a meta tag as late as possible
        if ($this->config->mitaji->add_time_to_head_tag &&
                stristr($response->body,'</head>')) {
            $secs = number_format(
                        (microtime(true)-MITAJI_REQUEST_START_MICROTIME),
                    4, '.', '');
            $ms = $secs*1000;
            $response->body = str_replace('<head>',
                "<head>\n<meta name=\"generated_in\" content=\"$secs seconds ($ms ms)\">",
                $response->body);
        }

        // HTTP header
        header('HTTP/1.x '.$response->status.' '.
            $this->httpReasonPhrase($response->status),
            true, $response->status);

        // Output headers
        foreach($response->headers as $k => $v) {
            header("$k: $v", true);
        }

        // Gzipped output (ob_gzhandler sets Content-Length reliably?)
        if(@$this->config->mitaji->gzip_output){
            ob_start('ob_gzhandler');
            echo $response->body;
            ob_end_flush();
            exit(0);
        }
        // Normal output, make sure Content-Length set explicitly
        ob_start();
        echo $response->body;
        header('Content-Length: '.ob_get_length(),true);
        ob_end_flush();
        exit(0);
    }
// ------------------------------------------------------------------------
    function httpReasonPhrase($status){
        if(!defined('HTTP_REASON_PHRASE_'.$status)){
            return false;
        }
        return constant('HTTP_REASON_PHRASE_'.$status);
    }
// ------------------------------------------------------------------------
    function badrequest(){
        trigger_error(400, E_USER_ERROR);
    }
// ------------------------------------------------------------------------
    function nomethod(){
        trigger_error(405, E_USER_ERROR);
    }
// ------------------------------------------------------------------------
    function notfound(){
        trigger_error(404, E_USER_ERROR);
    }
// ------------------------------------------------------------------------
    function internalerror($msg=500){
        trigger_error($msg, E_USER_ERROR);
    }
// ------------------------------------------------------------------------
    function redirect($url, $status=302){
        // Make sure scheme and host name added to URL
        $url_arr = parse_url($url);
        if(!isset($url_arr['host'])){
            $url = $this->host.$url;
        }
        if(!isset($url_arr['scheme'])){
            $url = $this->scheme.'://'.$url;
        }
        $response = new stdClass();
        /*
         * As per RFC2616 include a short body with a link to the new URL for
         * clients that don't support redirection
         */
        $response->body = "<html><title>Redirecting</title>
                         <p>Redirecting to $url,
                         <a href=\"$url\">click here to continue</a>
                         </p>";
        $response->status = $status;
        $response->headers['Location'] = $url;
        $this->output($response);
    }

// --------------------------------------------------------------------
// CLI interface
// --------------------------------------------------------------------
    function cliRun($opts, $args){
        // Set error handler
        set_error_handler(array($this,'cliErrorHandler'));
        if (isset($args[0])) {
            $command = $args[0];
            // Work out method to call
            if(stristr($command, '::')){
                list($class, $method) = explode('::', $command);
            // If not a method then call def (default) method
            } else {
                $class = $command;
                $method = 'def';
            }
            $command_args = array_slice($args, 1);
        }
        else if(isset($opts['help'])){
            $this->cliPrintCommands();
            exit(0);
        }
        else {
            fputs(STDOUT, "Type --help for commands\n");
            exit(0);
        }
        // Try and include the class
        $real_class = 'cli' . ucfirst($class);
        if(!@include('cli/' . $real_class . '.php')){
            trigger_error(ERR_CLI_NO_COMMAND, E_USER_NOTICE);
            exit(E_USER_NOTICE);
        }
        // Instigate the object
        if(!$ob = new $real_class($this)){
            trigger_error(ERR_CLI_NO_COMMAND, E_USER_NOTICE);
            exit(E_USER_NOTICE);
        }
        // If help opt set $method becomes the argument and "help" the method
        if (isset($opts['help']) && $opts['help']) {
            if (!method_exists($ob, 'help')) {
                trigger_error(sprintf(ERR_CLI_NO_HELP, $class), E_USER_NOTICE);
                exit(E_USER_NOTICE);
            }
            $command_args = array($method);
            $method = 'help';
        }
        // Check method exists
        if (!method_exists($ob, $method)) {
            trigger_error(ERR_CLI_NO_COMMAND, E_USER_NOTICE);
            exit(E_USER_NOTICE);
        }
        // Now call the func which should return an exit code
        $exit_code = call_user_func_array(array($ob, $method), $command_args);
        exit($exit_code);
    }
// --------------------------------------------------------------------
    function cliPrintCommands(){
        $cli_files = $this->globPath('./cli*.php');
        if (!$cli_files) {
            fputs(STDOUT,"No commands available\n");
            exit(0);
        }
        fputs(STDOUT, CLI_HELP_PREAMBLE."\n");
        foreach ($cli_files as $f) {
            $c = strtolower(str_replace('cli','',basename($f,'.php')));
            if ($c) {
                fputs(STDOUT,"  ".$c."\n");
            }
        }
        exit(0);
    }
// --------------------------------------------------------------------
// Error handling
// --------------------------------------------------------------------
    function getErrors($n=false) {
        if($n){
            return array_slice(array_reverse($this->__errors), 0, $n);
        }
        return $this->__errors;
    }

// ------------------------------------------------------------------------
    function getLastError() {
        return $this->getErrors(1);
    }

// ------------------------------------------------------------------------
    function getErrorLevelString($no) {
        switch ($no) {
            case 1:
                return 'E_ERROR';
            case 2:
                return 'E_WARNING';
            case 4:
                return 'E_PARSE';
            case 8:
                return 'E_NOTICE';
            case 16:
                return 'E_CORE_ERROR';
            case 32:
                return 'E_CORE_WARNING';
            case 64:
                return 'E_COMPILE_ERROR';
            case 128:
                return 'E_COMPILE_WARNING';
            case 256:
                return 'E_USER_ERROR';
            case 512:
                return 'E_USER_WARNING';
            case 1024:
                return 'E_USER_NOTICE';
            case 6143:
                return 'E_ALL';
            case 2048:
                return 'E_STRICT';
            case 4096:
                return 'E_RECOVERABLE_ERROR';
            default:
                return $no;
        }
    }
// ------------------------------------------------------------------------
    function cliErrorHandler($no,$str,$file=false,$line=false,$context=false) {
        if (error_reporting() == 0) { return; }
        // error_reporting() seems to be broken on cli so skip E_STRICT here
        if($no == E_STRICT){
            return;
        }
        fwrite(STDERR,"ERROR: $str (on line $line of $file)\n");
        if ($no != E_USER_ERROR) {
            return;
        }
        exit(E_USER_ERROR);
    }
// ------------------------------------------------------------------------
    function errorHandler($no,$str,$file=false,$line=false,$context=false) {
        if (error_reporting() == 0) { return; }

        $err = compact('no', 'str', 'file', 'line', 'context');
        $this->__errors[] = $err;

        // If not an E_USER_ERROR continue script execution
        if ($no != E_USER_ERROR) {
            return;
        }
        // Otherwise create an error responder
        $responder = new ErrorResponder($this, $this->config);

        /*
         * HTTP status code read from first 3 characters of the error message,
         * if not a number status is set to 500 (Internal Server Error)
         */
        if(!$status = (int)substr($str, 0, 3)){
            $status = 500;
        }
        $this->output($responder->get($status, $str, $context));
    }

// --------------------------------------------------------------------
}
