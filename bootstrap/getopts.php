<?php


function getargv() {
    $cg = new Console_Getopt();
    $argv = $cg->readPHPArgv();
    // Make sure we got them (for non CLI binaries)
    if (gettype($argv) !== 'array') {
        trigger_error($argv->getMessage(), E_USER_ERROR);
        exit(1);
    }
    return $argv;
}

/**
 * Get a normalised set of options from command line
 * i.e. all parts that being with a hyphen
 */
function getopts($so, $lo=array(), $nn=array()){
    $cg = new Console_Getopt();
    $argv = stripargs(getargv());
    $options = $cg->getOpt($argv, $so, $lo);

    // Check the options are valid
    if (gettype($options) !== 'array') {
        fputs(STDERR, $options->getMessage() . "\n");
        exit(1);
    }
    if(!$options){
        return array();
    }
    // Flatten the array
    $tmp = array();
    if($options[0]){
        foreach($options[0] as $opt){
            // If value is in array, but no value as doesn't have one, set value to true
            $tmp[trim($opt[0],'-')] = $opt[1] == null ? true : $opt[1];
        }
    }
    if($options[1]){
        foreach($options[1] as $opt){
            $tmp[$opt] = true;
        }
    }
    // Normalize names
    if($nn){
        foreach($nn as $short=>$long){
            if(array_key_exists($short,$tmp)){
                $val = $tmp[$short];
                unset($tmp[$short]);
                $tmp[$long] = $val;
            }
        }
    }
    return $tmp;
}

/**
 * Get all CLI arguments i.e. parts not starting with a hyphen
 */
function getargs() {
  return stripopts(array_slice(getargv(), 1));
}

/**
 * Remove any elements of argv that begin with a hyphen
 */
function stripopts($argv) {
  $tmp = array();
  foreach($argv as $a){
      if('-' === $a{0}){
          continue;
      }
      $tmp[] = $a;
  }
  return $tmp;
}

/**
 * Remove any elements of argv that do NOT begin with a hyphen
 */
function stripargs ($argv) {
  $tmp = array();
  foreach($argv as $a){
      if('-' !== $a{0}){
          continue;
      }
      $tmp[] = $a;
  }
  return $tmp;
}
