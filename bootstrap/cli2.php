<?php

/**
 * A simple bootstrap for running commands using Pear Console_GetOpt.
 * That's rather an old school library so isn't really recommended,
 * but will do the job.
 *
 * Your script should set these variables in the global scope, then require
 * this script.
 *
 * $short_opts
 * $long_opts
 * $normalize_opts - A mapping of short options to long options
 *
 * It will parse all the options into an array and put it in $_ENV['OPTS'].
 *
 * For free it gives you an -e, --env option for setting the mitaji environment
 * in $_ENV['MITAJI'], which is quite handy.
 */

// Include getopts function
require __DIR__ . '/getopts.php';

// Fix missing constant
if (!defined('FILE_USE_INCLUDE_PATH')) {
    define('FILE_USE_INCLUDE_PATH', true);
}

// These need to be defined at top of script if used
$__short_opts = 'e:';
if (isset($short_opts)) {
    $__short_opts .= $short_opts;
}
$__long_opts = array('env=');
if (isset($long_opts)) {
    $__long_opts = array_merge($__long_opts, $long_opts);
}
$__normalize_opts = array('e'=>'env');
if (isset($normalize_opts)) {
    $__normalize_opts = array_merge($__normalize_opts, $normalize_opts);
}

// Get options and store in ENV
$_ENV['OPTS'] = getopts($__short_opts, $__long_opts, $__normalize_opts);

// Get args and store in ENV
$_ENV['ARGS'] = getargs();

// Set environment based on ENV
if($mitaji_env = @$_ENV['OPTS']['env']){
    $_ENV['MITAJI'] = $mitaji_env;
}
// Or default to development
else {
    $_ENV['MITAJI'] = 'development';
}

// Set temp dir in ENV
$_ENV['TMPDIR'] = sys_get_temp_dir();

