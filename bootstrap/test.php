<?php

require_once __DIR__ . '/../vendor/autoload.php';

// Couple of global
if (!defined('FILE_USE_INCLUDE_PATH')) {
    define('FILE_USE_INCLUDE_PATH', true);
}

// Set temp dir
$_ENV['TMPDIR'] = sys_get_temp_dir();
if (!isset($_ENV['MITAJI'])) {
    $_ENV['MITAJI'] = 'development';
}
